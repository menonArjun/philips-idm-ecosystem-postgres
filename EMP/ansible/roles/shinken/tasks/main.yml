---

- name: ensure shinken cli is installed
  yum: name=shinken state=latest

- name: initialize shinken cli
  command: /usr/sbin/shinken --init
  args:
    creates: /root/.shinken.ini

# these are not created by rpm. Maybe a bug
- name: ensure shinken folders for doc and share are created
  file: path=/var/lib/shinken/{{item}} state=directory
  with_items:
    - share
    - doc

- name: ensure shinken is installed
  yum: name={{item}} state=latest
  with_items:
    - shinken-arbiter
    - shinken-poller
    - shinken-scheduler
    - shinken-reactionner
    - shinken-broker
    - shinken-receiver

- name: ensure shinken modules are installed
  yum: name={{item}} state=latest
  with_items:
    - shinken-module-livestatus
    - shinken-module-npcdmod
    - shinken-module-pickle-retention-file-scheduler
    - shinken-ws_passive_service_map-receiver
    - shinken-celery_task_perfdata-broker
    - shinken-module-cryptresource-poller
  notify:
    - reboot server for shinken
    - wait for server to shut down for shinken
    - wait for server to come back up for shinken

- name: ensure cron job for xml web output is removed
  file: path=/etc/cron.d/nagiosxml state=absent

- name: ensure shinken related services are started on boot
  service: name={{item}} enabled=yes
  with_items:
    - npcd
    - shinken-arbiter
    - shinken-poller
    - shinken-reactionner
    - shinken-scheduler
    - shinken-broker
    - shinken-receiver
  notify:
    - reboot server for shinken
    - wait for server to shut down for shinken
    - wait for server to come back up for shinken

- name: ensure phim-shinkencfg is installed
  yum: name=phim-shinkencfg state=latest
  notify:
    - stop shinken state services
    - remove shinken retention files
    - flush redis state
    - reboot server for shinken
    - wait for server to shut down for shinken
    - wait for server to come back up for shinken

- name: ensure monitor private key is installed
  copy: src=nagios-monitor dest=/etc/philips/shinken/resource.d/nagios-monitor owner=nagios group=philips mode=0700

- name: check if hosts dir is present
  command: "[ -d {{wc_location}}/hosts ]"
  register: shinken_hosts_present
  changed_when: False
  ignore_errors: yes

- name: update shinken hosts
  synchronize:
    src={{wc_location}}/hosts/ dest=/etc/philips/shinken/hosts
    archive=no delete=yes checksum=yes recursive=yes rsync_opts=--exclude=.*
  delegate_to: "{{inventory_hostname}}"
  when: shinken_hosts_present|success
  notify:
    - restart shinken-arbiter
    - flush redis state

- name: ensure shinken secret is set up
  template: src=secret.j2 dest=/etc/philips/secret

- name: ensure shinken resorces are set up
  template: src=resource.cfg.j2 dest=/etc/philips/shinken/resource.d/resource.cfg
  notify:
    - restart shinken-arbiter

- name: ensure shinken threshold are set up
  template: src=threshold.cfg.j2 dest=/etc/philips/shinken/resource.d/threshold.cfg
  notify:
    - restart shinken-arbiter

- name: ensure shinken backoffice resorces are set up
  template: src=backoffice.cfg.j2 dest=/etc/philips/shinken/resource.d/backoffice.cfg
  notify:
    - restart shinken-arbiter
