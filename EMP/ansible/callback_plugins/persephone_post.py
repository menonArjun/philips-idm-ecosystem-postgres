import linecache
import requests
import json
from datetime import datetime
# This plugin may be used before most things are installed in a running system that is why things here may be duplicated
# from other parts of the codebase


class PersephoneNotifier(object):
    ISO_FORMAT = '%Y-%m-%dT%H:%M:%S.%fZ'
    LAST_RUN_FILE = '/tmp/ansible_pull_last_complete_run_output'
    NOTIFICATION_URL = 'http://persephone/notification'
    TASK_URL = 'http://persephone/partial'
    HOSTNAME = 'localhost'
    HOSTADDRESS = '127.0.0.1'
    SERVICE = 'Administrative__Philips__Ansible__Last_Run__Status'

    def __init__(self, siteid_file='/etc/siteid', partial_file='/etc/philips/partial'):
        self.siteid = self.get_first_line_in_file(siteid_file)
        self.partial_enabled = self.get_first_line_in_file(partial_file)
        self.ok = False

    @staticmethod
    def get_first_line_in_file(filename):
        return linecache.getline(filename, 1).strip()

    def get_timestamp(self):
        return datetime.utcnow().strftime(self.ISO_FORMAT)

    def post_ansible_service(self, output=''):
        if not self.siteid:
            print('Did not post: Missing SiteID')
            return

        if self.ok:
            state = 'OK'
            notification_type = 'RECOVERY'
        else:
            state = 'CRITICAL'
            notification_type = 'PROBLEM'

        msg = dict(
            siteid=self.siteid,
            timestamp=self.get_timestamp(),
            hostname=self.HOSTNAME,
            hostaddress=self.HOSTADDRESS,
            service=self.SERVICE,
            type=notification_type,
            state=state,
            payload=dict(output=output)
        )

        self.post_json(self.NOTIFICATION_URL, msg)

    def post_json(self, url, data):
        try:
            # using old requests format as new 'json' parameter is not available on the initial requests version
            # installed in base image (ova)
            r = requests.post(url, data=json.dumps(data), headers={'content-type': 'application/json'})
            r.raise_for_status()
            print('Posted')
        except requests.exceptions.RequestException:
            print('Could not Post')

    def write_last_run(self):
        output = '1' if self.ok else '0'
        with open(self.LAST_RUN_FILE, 'w') as lrf:
            lrf.write(output)

    def get_last_run(self):
        last_run_raw = self.get_first_line_in_file(self.LAST_RUN_FILE)
        return int(last_run_raw) if last_run_raw else 2

    def summary_notify(self, total, failed, failed_tasks):
        if failed:
            summary = '{failed} Tasks Failed Out of {total}'.format(failed=failed, total=total)
            message = '\n'.join([summary, json.dumps(failed_tasks)])
            self.post_ansible_service(message)
        else:
            self.ok = True
            if self.get_last_run() != 1:
                message = 'Ansible Completed Run {total} Tasks'.format(total=total)
                self.post_ansible_service(message)
        self.write_last_run()

    def task_start_notify(self, name, task_type):
        if self.partial_enabled:
            self.post_json(self.TASK_URL, {'name': name, 'task_type': task_type})


class CallbackModule(object):
    """
    Ansible callback module to post stats to persephone
    """
    def __init__(self):
        self.failed_tasks = []
        self.notifier = PersephoneNotifier()

    def runner_on_failed(self, host, res, ignore_errors=False):
        if not ignore_errors:
            self.failed_tasks.append((host, res))

    def playbook_on_task_start(self, name, is_conditional):
        if not is_conditional:
            task_type = 'Task'
        else:
            task_type = 'Notify'
        self.notifier.task_start_notify(name=name, task_type=task_type)

    def playbook_on_stats(self, stats):
        self.notifier.summary_notify(
            stats.processed.popitem()[1] if stats.processed else 0,
            stats.failures.popitem()[1] if stats.failures else 0,
            self.failed_tasks
        )
