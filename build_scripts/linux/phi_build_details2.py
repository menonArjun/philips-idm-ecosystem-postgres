#!/usr/bin/env python

import argparse
import yaml
import platform
import time

## Needs yaml file like the following:
# ---
# next:
#   major: 1
#   minor: 2
#   drop: 4
#   patch: 0
#   build: 3272
#   buildpatch: 0
# log:
#   - major: 1
#     minor: 2
#     drop: 4
#     patch: 0
#     build: 3270
#     buildpatch: 0
#     revision: 6096
#   - major: 1
#     minor: 2
#     drop: 4
#     patch: 0
#     build: 3271
#     buildpatch: 0
#     revision: 6100



class phi_versioning(object):
    def __init__(self, version_file):
        self.version_file = version_file
        self._version_root = None

    def log(self, revision):
        new_build = self.version_root['next'].copy()
        new_build['revision'] = revision
        self.version_root['next']['build'] += 1
        self.version_root['log'].append(new_build)
        self.persist()

    def get_next_build(self):
        return self.version_root['next']['build']

    def get_next_long_version(self):
        return '{major}.{minor}.{patch}.{build}'.format(**self.version_root['next'])

    def get_next_short_version(self):
        return '{major}.{minor}'.format(**self.version_root['next'])

    def get_next_product_version(self):
        return self.get_next_short_version()

    def get_next_build_version(self):
        return '{major}.{minor}.{build}'.format(**self.version_root['next'])

    def get_next_major_minor(self):
        return '{major}.{minor}'.format(**self.version_root['next'])

    def load(self):
        with open(self.version_file) as version_file:
            self._version_root = yaml.load(version_file)

    @property
    def version_root(self):
        if self._version_root is None:
            self.load()
        return self._version_root

    def persist(self):
        with open(self.version_file, 'w') as version_file:
            yaml.dump(self._version_root, version_file, default_flow_style=False, explicit_start=True)


def check_arg(args=None):
    """Check the arguments passed"""
    parser = argparse.ArgumentParser(description='manage version data in yaml file')
    parser.add_argument('-f', '--file', help='versioning file', required=True)
    subparsers = parser.add_subparsers()

    parser_next = subparsers.add_parser('next')
    parser_next.set_defaults(func=next_build)

    parser_log = subparsers.add_parser('log')
    parser_log.set_defaults(func=log_build)
    parser_log.add_argument('revision', help='revision to log')

    parser_log = subparsers.add_parser('manifest')
    parser_log.set_defaults(func=manifest_build)
    parser_log.add_argument('-m', '--manifest_file', help='the manifest file to output', required=True)
    parser_log.add_argument('-n', '--product_name', help='the name of the product', required=True)
    parser_log.add_argument('-u', '--source_url', help='the url of the source', required=True)
    parser_log.add_argument('-r', '--source_revision', help='the revision of the source', required=True)
    parser_log.add_argument('-w', '--workspace', help='the workspace used for the build', required=True)
    parser_log.add_argument('-p', '--prefix', help='the prefix used for the output', default='')

    return parser.parse_args(args)


def next_build(args, ver):
    print(ver.get_next_build())


def log_build(args, ver):
    ver.log(args.revision)


def manifest_build(args, ver):
    template = """\
{prefix}BUILD_PRODUCT={product_name}
{prefix}BUILD_SOURCE_URL={source_url}
{prefix}BUILD_SOURCE_REVISION={source_revision}
{prefix}BUILD_TIME={build_time}
{prefix}BUILD_MACHINE={build_machine}
{prefix}BUILD_ROOT={workspace}
{prefix}BUILD_FULL_VERSION={full_version}
{prefix}BUILD_PRODUCT_VERSION={product_version}
{prefix}BUILD_VERSION={build_version}
{prefix}BUILD_BUILD_NUMBER={build_number}
"""

    prefix = '{0}_'.format(args.prefix.upper()) if args.prefix else ''

    result = template.format(
        product_name=args.product_name,
        source_url=args.source_url,
        source_revision=args.source_revision,
        build_time=time.strftime('%c'),
        build_machine=platform.node(),
        workspace=args.workspace,
        full_version=ver.get_next_long_version(),
        product_version=ver.get_next_product_version(),
        build_version=ver.get_next_build_version(),
        build_number=ver.get_next_build(),
        prefix=prefix
    )

    with open(args.manifest_file, 'w') as f:
        f.write(result)


def versioning(args):
    ver = phi_versioning(args.file)
    args.func(args, ver)


def main():
    args = check_arg(args=None)
    versioning(args)


if __name__ == '__main__':
    main()
