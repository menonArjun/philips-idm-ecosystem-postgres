import os
import urlparse

from build_help import (arg_value, wrap_text, space_join, rm_rf, wget, mkdir_p, ensure_clean_dir, arg_equals_value, cp)
from fabric.api import *


RPMBUILD_DIRS = ('SPECS', 'SOURCES', 'BUILD', 'RPMS', 'SRPMS')
BUILD_DIR = 'build'
DIST_DIR = 'dist'


class RPMBuilder(object):
    def __init__(self, version, package=None, directory='.'):
        self.package = package
        self.version = version
        self.directory = directory

    def build(self):
        if not self.package:
            raise ValueError('No package name given, cannot build')
        with cd(self.directory):
            self.execute_build()
            output = self.get_output()
        return output

    def get_output(self):
        ls_cmd = 'ls -d -1 {directory}/{pattern}.rpm'.format(
            directory=self.directory, pattern=self.get_output_pattern()
        )
        return run(ls_cmd)


class RPMSpecBuilder(RPMBuilder):
    source_extension = '.tar.gz'
    rpm_sources = os.path.join(BUILD_DIR, 'SOURCES', '')

    def __init__(self, version, specfile, package=None, directory='.', files=None, generate_source=True):
        super(RPMSpecBuilder, self).__init__(version, package, directory)
        self.specfile = specfile
        self.files = files or []
        self.generate_source = generate_source
        self._destination = None

    @property
    def destination(self):
        if self._destination is None:
            if self.generate_source:
                self._destination = os.path.join(self.rpm_sources, self.name_and_version)
                ensure_clean_dir(self._destination)
            else:
                self._destination = self.rpm_sources
        return self._destination

    @property
    def name_and_version(self):
        return '{name}-{version}'.format(name=self.package, version=self.version)

    def create_topdir(self):
        ensure_clean_dir(BUILD_DIR)
        for subdir in RPMBUILD_DIRS:
            mkdir_p(os.path.join(BUILD_DIR, subdir))

    def rollup_source(self):
        archive_basename = '{package_name}{ext}'.format(package_name=self.name_and_version, ext=self.source_extension)
        archive_filename = os.path.join(self.rpm_sources, archive_basename)
        archive_cmd = space_join('tar -C', self.rpm_sources, '-zcvf', archive_filename, self.name_and_version)
        run(archive_cmd)
        rm_rf(self.destination)

    def copy_source(self, entry):
        if urlparse.urlsplit(entry).scheme:
            wget(entry, self.destination)
        else:
            cp(entry, self.destination, opts='-rf')

    def setup_sources(self):
        for entry in self.files:
            self.copy_source(entry)

        if self.generate_source:
            self.rollup_source()

    def build_rpm(self):
        phiversion_value = wrap_text(arg_value('_phiversion', self.version), '"')
        phiversion_definition = arg_value('--define', phiversion_value)
        topdir_value = wrap_text(arg_value('_topdir', os.path.join(self.directory, BUILD_DIR)), '"')
        topdir_definition = arg_value('--define', topdir_value)
        rpmbuild_cmd = space_join('rpmbuild -bb', phiversion_definition, topdir_definition, self.specfile)
        run(rpmbuild_cmd)

    def execute_build(self):
        self.create_topdir()

        if self.files:
            self.setup_sources()

        self.build_rpm()

    def get_output_pattern(self):
        return '{build}/RPMS/*/*'.format(build=BUILD_DIR)


class RPMDistutilsBuilder(RPMBuilder):
    def __init__(self, version, package=None, directory='.', requires='', obsoletes='', arch='noarch'):
        super(RPMDistutilsBuilder, self).__init__(version, package, directory)
        self.requires = requires
        self.obsoletes = obsoletes
        self.arch = arch

    def execute_build(self):
        requires_string = arg_equals_value('--requires', wrap_text(self.requires)) if self.requires else ''
        obsoletes_string = arg_equals_value('--obsoletes', wrap_text(self.obsoletes)) if self.obsoletes else ''
        phiversion_env = arg_equals_value('phiversion', self.version) if self.version else ''
        setup_cmd = space_join(phiversion_env, 'python setup.py bdist_rpm', requires_string, obsoletes_string)
        run(setup_cmd)

    def get_output_pattern(self):
        return 'dist/{package}-{version}*.{arch}'.format(package=self.package, version=self.version, arch=self.arch)


BUILDER_MAP = {
    'spec': RPMSpecBuilder,
    'setup': RPMDistutilsBuilder
}


def get_builder(directory, task):
    for build_type, builder_type in BUILDER_MAP.iteritems():
        if not build_type in task:
            continue
        args = task.get(build_type)
        return builder_type(directory=directory, **args)
