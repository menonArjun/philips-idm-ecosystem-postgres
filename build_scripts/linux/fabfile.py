import json
import os
from StringIO import StringIO

import yaml
from fabric.api import *

from build_help import (rm_rf, wrap_text, arg_equals_value, arg_value, ensure_clean_dir, cp_r, cp, space_join, mkdir_p,
                        sign_rpms)
from rpm_builder import get_builder

# The following environment variables can be used to change locations
# WORKSPACE
WORKSPACE = os.environ.get('WORKSPACE', '/home/jenkins/workspace/IDMEcosystem_RPM')


TEST_OUTPUT_DIR = 'test_output'
ecosystem_version_file = 'ansible/roles/version-config/files/version.json'


@task(default=True)
def build_all(build_instruction, output_root, rpm_dir, manifest_dir, properties_file, build_number=0):
    """
    :param build_instruction: the build instruction yml file
    :param output_root: the output root directory
    :param rpm_dir: the rpm output directory, relative to output_root
    :param manifest_dir: the manifest output directory, relative to output_root
    :param build_number: the build number to use (optional, default=0)
    :return: None
    """
    with cd(WORKSPACE):
        ensure_clean_dir(TEST_OUTPUT_DIR)

        build_tasks = yaml.load(run(space_join('cat', build_instruction)))
        for sub_task in build_tasks:
            task_output = build_one(sub_task, build_number)
            if task_output is not None:
                name, task_output = task_output
                out_files = copy_output(task_output, rpm_dir, output_root)
                create_output_manifest(name, os.path.join(output_root, manifest_dir), out_files)

        abs_rpm_dir = os.path.join(output_root, rpm_dir)
        sign_rpms(abs_rpm_dir)
        cp(properties_file, os.path.join(output_root, 'latest', ''), opts='-f')
        cp_r(TEST_OUTPUT_DIR, abs_rpm_dir)


def get_build_version(version, build_number):
    return '{version}.{build}'.format(version=version, build=build_number)


def print_head(name):
    format_spec = '#^60'
    for out in ['', wrap_text(name, ' '), '']:
         print(format(out, format_spec))


def build_one(task, build_number):
    if task.get('skip', False):
        return 
    
    name = task.get('name')
    print_head(space_join('Building', name))
    directory = os.path.join(WORKSPACE, task.get('dir'))

    if 'tests' in task:
        for test in task.get('tests'):
            run_nosetests(directory=directory, **test)

    builder = get_builder(directory, task)

    if not builder:
        return

    if not builder.package:
        builder.package = name
    if not task.get('thirdparty'):
        builder.version = get_build_version(builder.version, build_number)

    output = builder.build()

    print_head(space_join('Done Building', name))
    return name, output


def run_nosetests(directory, package_name, src_dir=None, packages_to_test='', test_dir='tests', test_pattern='*.py'):
    source = os.path.join(directory, src_dir) if src_dir else directory
    with shell_env(PYTHONPATH=source):
        xunit_file = '{package}-xunit.xml'.format(package=package_name)
        cover_file = '{package}-coverage.xml'.format(package=package_name)
        nosetests_cmd = space_join(
            'nosetests --with-xunit',
            arg_equals_value('--xunit-file', os.path.join(TEST_OUTPUT_DIR, xunit_file)),
            '--with-coverage',
            arg_equals_value('--cover-package', packages_to_test or package_name),
            '--cover-xml',
            arg_equals_value('--cover-xml-file', os.path.join(TEST_OUTPUT_DIR, cover_file)),
            os.path.join(source, test_dir, test_pattern)
        )
        run(nosetests_cmd)


def copy_output(output, location, root_location):
    mkdir_p(os.path.join(root_location, location))
    copied_files = []
    for filename in output.splitlines():
        copied_output = os.path.join(location, os.path.basename(filename))
        cp(filename, os.path.join(root_location, copied_output), opts='-f')
        copied_files.append(copied_output)

    return copied_files


def create_output_manifest(name, location, files):
    mkdir_p(location)
    put(StringIO('\n'.join(files)), os.path.join(location, '{name}.txt'.format(name=name)))


@task
def assemble_pdsc(version, output_dir):
    ensure_clean_dir(output_dir)

    with cd(WORKSPACE):
        cp_r('Nebuchadnezzar/Merovingian', output_dir)
        cp_r('EMP/ansible', output_dir)

    create_ecosystem_version(version, os.path.join(output_dir, ecosystem_version_file))

    rm_rf(os.path.join(output_dir, 'Merovingian/test'))


def create_ecosystem_version(version, filename):
    version_output = {
        'Applications': [
            {
                'Version': version,
                'ApplicationName': 'IDM Ecosystem'
            }
        ]
    }
    put(StringIO(json.dumps(version_output, indent=4)), filename)


@task
def create_spinx(version, release):
    create_sphinx_cmd = space_join(
        'sphinx-build -b html',
        arg_value('-D', arg_equals_value('version', version)),
        arg_value('-D', arg_equals_value('release', release)),
        'docs _build/html'
    )
    with cd(WORKSPACE):
        run(create_sphinx_cmd)
