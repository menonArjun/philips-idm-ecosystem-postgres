#!/usr/bin/env python
import socket
import sys
import argparse
import yaml
import json
import time
import logging
from bottle import Bottle, request, BaseRequest
from StringIO import StringIO
BaseRequest.MEMFILE_MAX = 1024 * 1024 * 2


LOG_DATE_FORMAT = '%Y-%m-%dT%H:%M:%S'
LOG_FORMAT = '%(asctime)s.%(msecs).06dZ - %(process)d - %(name)s - %(levelname)s - %(message)s'


log = logging.getLogger('phim.apoc')
log.setLevel(logging.INFO)
logging.Formatter.converter = time.gmtime
formatter = logging.Formatter(fmt=LOG_FORMAT, datefmt=LOG_DATE_FORMAT)
handler_console = logging.StreamHandler()
handler_console.setFormatter(formatter)
log.addHandler(handler_console)


class MessagesExpecter(object):

    class MessageList(object):
        PADDING = '#^60'

        def __init__(self):
            self.data = []

        def append(self, item):
            self.data.append(item)

        @staticmethod
        def pretty_dumps(item):
            return json.dumps(item, indent=4, sort_keys=True)

        def pad_str(self, msg=''):
            return '{0:{1}}'.format(msg, self.PADDING)

        def message_body(self, item):
            return self.pretty_dumps(item)

        def __str__(self):
            result = []
            for num, item in enumerate(self.data, 1):
                result.append(self.pad_str(' Message {0} '.format(num)))
                result.append(self.message_body(item))
            result.append(self.pad_str())
            return '\n'.join(result)

        def __nonzero__(self):
            return True if self.data else False


    class MessageCompareList(MessageList):
        def message_body(self, item):
            result = [
                'Expecting:',
                self.pretty_dumps(item[0]),
                'Received:',
                self.pretty_dumps(item[1])
                ]
            return '\n'.join(result)


    def __init__(self, data):
        self.data = data
        self._items = None
        self.unexpected = self.MessageList()
        self.conflicting = self.MessageCompareList()
        self.successful = self.MessageList()

    @property
    def items(self):
        # check for None, otherwise it will reload on empty
        if self._items is None:
            with open(self.data, 'rb') as f:
                self._items = yaml.load(f)
        return self._items

    def pop_item(self, hostname, service):
        try:
            item_expectations = self.items[hostname][service]['messages']
        except KeyError:
            return
        item_expected = item_expectations.pop(0)
        # collapse if empty after pop
        if not item_expectations:
            del self.items[hostname][service]
        if not self.items[hostname]:
            del self.items[hostname]
        item_expected['hostname'] = hostname
        item_expected['service'] = service
        return item_expected

    def skipper(self, hostname, service):
        try:
            item_skip = self.items[hostname][service]['skip']
        except KeyError:
            item_skip = 0
        if item_skip < 1:
            return False
        self.items[hostname][service]['skip'] -= 1
        return True

    def process_message(self, message):
        if self.skipper(message['hostname'], message['service']):
            return True
        data_expected = self.pop_item(message['hostname'], message['service'])
        if not data_expected:
            self.unexpected.append(message)
        elif message != data_expected:
            self.conflicting.append((data_expected, message))
            return False
        else:
            self.successful.append(message)
        return True

    def empty(self):
        return False if self.items else True


def check_arg(args=None):
    """Check the arguments passed"""
    parser = argparse.ArgumentParser(description='Script to setup a service to expect messages in order')
    parser.add_argument('-p', '--port', help='Port number to use', required=True, type=int)
    parser.add_argument('-i', '--interface', help='IP address of the interface to bind to', default='0.0.0.0')
    parser.add_argument('-f', '--config', help='config file', default='data.yml')
    parser.add_argument('-s', '--siteid', help='siteid')
    results = parser.parse_args(args)
    return results.config, results.interface, results.port, results.siteid


app = Bottle(catchall=False)


@app.post('/notification')
def submit_result():
    yield "Success!"
    global data
    data_in = request.json
    data_in.pop('timestamp', None)
    data_in.pop('siteid', None)

    no_conflict = data.process_message(data_in)

    if not no_conflict or data.empty():
        # seems like double exception makes app exit, which is what is wanted
        sys.stderr.close()
        sys.exit()


def expective_server(config_data, interface, port, siteid):
    old_stderr = sys.stderr
    old_stdout = sys.stdout
    sys.stderr = StringIO()
    sys.stdout = StringIO()

    global data
    data = MessagesExpecter(config_data)

    try:
        app.run(host=interface, port=port, quiet=True)
    except socket.error:
        print('Could not bind to port')
        raise
    except Exception:
        pass
    finally:
        app.close()
    sys.stderr = old_stderr
    sys.stdout = old_stdout
    return data


def apoc(config_data, interface, port, siteid):
    results = expective_server(config_data, interface, port, siteid)

    if results.successful:
        print('Expected messages received:\n')
        print(results.successful)

    if results.unexpected:
        print('Some unexpected messages received:\n')
        print(results.unexpected)

    if results.conflicting:
        print('Message conflicted\n')
        print(results.conflicting)
        sys.exit(1)

    else:
        print('All messages expected were received!')


@app.post('/perfdata')
@app.post('/state')
@app.post('/configuration')
@app.post('/discovery')
@app.post('/heartbeat')
@app.get('/health')
@app.post('/queuetransport')
def discard():
    pass


if __name__ == '__main__':
    apoc(*check_arg())
