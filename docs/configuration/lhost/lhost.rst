=====================
IDM lhost.yml configuration
=====================

All sites in IDM has a **lhost.yml** configuration file, site specific configurations goes in to this file.

There are mainly 4 configurable section in the lhost.yml file.
***************************************************************

#. **DNS Name Servers**

	DNS server  details.

#. **Endpoints**

     Under this section the details about a site can be provided.

	**address** : IP address

	**scanner** : Product Scanner name 

		eg : IBE, ISP etc

	**username** : username

	**password** : password

	**monitor** : This flag is used to enable(true) or disable(false) monitoring for a particular site
	
			::

			   true : to Enable  Monitoring(default)
			   false : to Disable Monitoring

			If we set this flag to false, then during discovery *<hostname>.cfg* file would not be get created in the **SVN**, without this file monitoring may not be possible for any site.

	**discovery_url** : Discovery URL only when  generic Discovery is used ( See Generic discovery for  more details)


	**product_name**:  Name of the product, which will be part of discovery payload and will be used to display in the IDM Dashboard

	**product_id**: ID of the product, which will be part of discovery payload and will be used to display in the IDM Dashboard

	**product_version**: Product Version


#. **Shinken Resources**


	These are site-specific resources generally in an encoded format used to supply set of variables to Shinken.

	Thease values will be written to Shinken's `resource.cfg` file


#. **Threshold values** 

	Under this section its possible to configure WARN and CRIT conditions for various service checks (see Threshold configuration section for more details)

#. **An Example lhost.yml file**


.. image::  images/lhost_yml.png
    :align: center
    :alt: lhost.yml
