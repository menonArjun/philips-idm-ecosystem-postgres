..  _dwp:

==============================
I4Viewer Integration
==============================

**HostScanner** - DWPHostScanner

**Hostgroups** - dwp-servers

.. csv-table:: *Standard Services*
   :header: "Max. Check Attempts", "Normal Check Interval", "Retry Check Interal"
   :widths: 20, 20, 20

    3, "0h 5m 0s", "0h 2m 0s"

Service Checks
##############
 - Basic Checks
 - Rest Checks

.. csv-table:: *Application Specific Checks*
   :header: "Service", "Namespace", "Check Command"
   :widths: 20, 40, 40

   "W3SVC", "Product__IntelliSpace__DWP__W3SVC__Status", check_win_service!W3SVC
   "Schedule", "Product__IntelliSpace__DWP__Schedule__Status", check_win_service!^Schedule$!_NumGood=1:
   "Port", Product__IntelliSpace__DWP__Port__Status, check_port!443!1.5!1.0
   "URL", Product__IntelliSpace__DWP__URL__Status, check_url
