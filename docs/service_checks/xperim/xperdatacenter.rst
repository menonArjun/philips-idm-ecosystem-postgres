..  _xperim:

==============================
XPERDatacenter Integration
==============================

**HostScanner** - XPERDataCenterHostScanner

**Hostgroups** - xperdatacenter-servers


.. csv-table:: *Standard Services*
   :header: "Max. Check Attempts", "Normal Check Interval", "Retry Check Interal"
   :widths: 20, 20, 20

    3, "0h 5m 0s", "0h 2m 0s"

Service Checks
##############
 - Basic Checks
 - XPERConnect


.. csv-table:: **Application Specific Checks**
   :header: "Service", "Namespace", "Check Command"
   :widths: 20, 40, 40

   "XPERDataCenter", "Product__IntelliSpace__XPER__Datacenter__Status", check_win_process!Datacenter.exe
   
