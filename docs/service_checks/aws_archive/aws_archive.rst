..  _aws_archive:

==============================
AWS Archive Integration
==============================

**HostScanner** - AdvancedWorkflowArchiveServicesHostScanner

**Hostgroups** - aws-servers,aws-archive-servers

.. csv-table:: *Standard Services*
   :header: "Max. Check Attempts", "Normal Check Interval", "Retry Check Interal"
   :widths: 20, 20, 20

    3, "0h 5m 0s", "0h 2m 0s"


Service Checks
##############
 - Basic Checks
 - Rest Checks

.. csv-table:: *Application Specific Checks*
   :header: "Service", "Namespace", "Check Command"
   :widths: 20, 40, 40

   "Information__Version", "Product__IntelliSpace__AWS__Information__Version", check_aws!version
   "Disk", "Product__IntelliSpace__AWS__Disk__Status", check_aws!disks!20!5
   "Openfire_Service", "Product__IntelliSpace__AWS__Openfire_Service__Status", check_aws!process!openfire-service.exe
   "AWSMonitor_Service", "Product__IntelliSpace__AWS__AWSMonitor_Service__Status", check_aws!process!Philips.Piranha.AWS.AWSMonitor.exe
   "PubSub_Service", "Product__IntelliSpace__AWS__PubSub_Service__Status", check_aws!process!Philips.Hi.PubSub.Service.exe
   "Listener_Service", "Product__IntelliSpace__AWS__Listener_Service__Status", check_aws!process!Philips.Hi.PlugIn.iSite.Listener.exe
   "UserSyncService_Service", "Product__IntelliSpace__AWS__UserSyncService_Service__Status", check_aws!process!Philips.Hi.Piranha.UserSyncService.exe
   "RuleEngine_Service", "Product__IntelliSpace__AWS__RuleEngine_Service__Status", check_aws!process!AutoAssignment.jar
   "RuleEngine_Health", "Product__IntelliSpace__AWS__RuleEngine_Health__Status", check_json_status!AdvancedWorkflowServices/ServerMonitorService.svc/RuleEngine/Health!Applications[*]!Status!Message!ApplicationName
   "Authentication_Service", "Product__IntelliSpace__AWS__Authentication_Service__Status", check_http_service!/AdvancedWorkFlowServices/AuthenticationService.svc
