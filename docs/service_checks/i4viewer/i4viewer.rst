..  _i4ev:

==============================
I4Viewer Integration
==============================

**HostScanner** - I4ViewerHostScanner

**Hostgroups** - i4viewer-servers

.. csv-table:: *Standard Services*
   :header: "Max. Check Attempts", "Normal Check Interval", "Retry Check Interal"
   :widths: 20, 20, 20

    3, "0h 5m 0s", "0h 2m 0s"

Service Checks
##############
 - Basic Checks
 - Rest Checks

.. csv-table:: *Application Specific Checks*
   :header: "Service", "Namespace", "Check Command"
   :widths: 20, 40, 40

   "NetTcpPortingSharing", "Product__IntelliSpace__I4Viewer__NetTcpPortingSharing__Status", check_win_process!SMSvcHost.exe
   "IDSSystemService", "Product__IntelliSpace__I4Viewer__IDSSystemService__Status", check_win_process!IDSSystemService.exe
   "E2EService", "Product__IntelliSpace__I4Viewer__E2EService__Status", check_win_process!E2E.Service.exe
   "ExtendedRemotingService", "Product__IntelliSpace__I4Viewer__ExtendRemotingService__Status", check_win_process!ExtendedRemotingService.exe
   "PmsCTLogService", "Product__IntelliSpace__I4Viewer__PmsCTLogService__Status", check_win_process!LogService.exe
   "PmsPortalAdmin", "Product__IntelliSpace__I4Viewer__PmsPortalAdminService__Status", check_win_process!PmsPortalAdmin.exe
   "PortalService", "Product__IntelliSpace__I4Viewer__PortalService__Status", check_win_process!PortalService.exe
   "DataProvider", "Product__IntelliSpace__I4Viewer__DataProvider__Status", check_json_status!DataProvider/DataProvider/Monitor!Applications[*]!Status!Message!ApplicationName
   "MissionBriefing", "Product__IntelliSpace__I4Viewer__MissionBriefing__Status", check_json_status!IdmServices/MissionBriefing/Monitor!Applications[*]!Status!Message!ApplicationName
   "SnapITWebService", "Product__IntelliSpace__I4Viewer__SnapITWebService__Status", check_json_status!IdmServices/SnapITWebService/Monitor!Applications[*]!Status!Message!ApplicationName
   "ClinicalLabel", "Product__IntelliSpace__I4Viewer__ClinicalLabel__Status", check_json_status!ClinicalLabelService/ClinicalLabel/Monitor!Applications[*]!Status!Message!ApplicationName
   "GfnServlets", "Product__IntelliSpace__I4Viewer__GfnServlets__Status", check_json_status!IdmServices/Dashboard/Monitor!Applications[*]!Status!Message!ApplicationName
   "GfnApplicationService", "Product__IntelliSpace__I4Viewer__GfnApplicationService__Status", check_win_process!GfnApplicationService.exe
   "I4LoggingService", "Product__IntelliSpace__I4Viewer__I4LoggingService__Status", check_win_process!I4LoggingService.exe
   "W3SVC", "Product__IntelliSpace__I4Viewer__W3SVC__Status", check_win_service!W3SVC
