..  _concerto:

==============================
ISPortal Concerto Integration
==============================

**HostScanner** - ConcertoHostScanner

**Hostgroups** - concerto-servers

.. csv-table:: *Standard Services*
   :header: "Max. Check Attempts", "Normal Check Interval", "Retry Check Interal"
   :widths: 20, 20, 20

    3, "0h 5m 0s", "0h 2m 0s"

Service Checks
##############
 - Basic Checks
 - IIS Admin
 - SQL Server
 - NetTCPPortSharing

.. csv-table:: *Application Specific Checks*
   :header: "Service", "Namespace", "Check Command"
   :widths: 20, 40, 40

   "IIS Admin", "Product__IntelliSpace__Concerto__IISADMIN__Status", check_win_process!inetinfo.exe
   "SQL Server", "Product__IntelliSpace__Concerto__SQLServer__Status", check_win_process!sqlservr.exe
   "NetTCPPortSharing", "Product__IntelliSpace__Concerto__NetTcpPortingSharing__Status", check_win_process!SMSvcHost.exe
   "PhilipsConcertoScalabilityDaemon", "Product__IntelliSpace__Concerto__ConcertoScalabilityDaemon__Status", check_win_process!Concerto.Scalability.Daemon.exe
   "PhilipsServiceAgent", "Product__IntelliSpace__Concerto__PhilipsServiceAgent__Status", check_win_process!qsaMain.exe
   "PhilipsServicePlatformServicesHost", "Product__IntelliSpace__Concerto__PhilipsServicePlatformServicesHost__Status", check_win_process!Philips.ServicePlatform.ServiceManager.ServiceHostController.exe
   "PmsCTAuditLogService", "Product__IntelliSpace__Concerto__PmsCTAuditLogService__Status", check_win_process!Orchestrator.AuditLogService.exe
   "PmsCTLogService", "Product__IntelliSpace__Concerto__PmsCTLogService__Status", check_win_process!LogService.exe
