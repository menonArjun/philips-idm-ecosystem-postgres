..  _generic_discovery:

---------------------
Generic Discovery API
---------------------

Endpoints are located in the lhost.yml file for a particular site. The ``address`` and ``scanner`` keys are required for
all endpoints. Other keys may be required depending on the scanner and type of system. Most systems will require
the ``username`` and ``password`` for access.
This API will be used by scanners to get all nodes exposed by ``discovery_url`` present in lhost.yml.

For Example :::

  endpoints:
  - address: 162.65.66.78
    scanner: ISP
    username: administrator
    password: st3nt0r
    discovery_url: http://<ip_address>:port/discovery

* Address - This is the IP or Hostname that stores configuration of the product to scan.
* Scanner - What type of scanner to use on this. Current valid values are ``ISP``, ``I4``, ``LN``, ``vCenter``,
  ``AnalyticsPublisher``, ``AdvancedWorkflowServices``, ``Windows``, and ``Linux``
* Username - Username of the product scanner to retrieve configuration
* Password - Password of the username on the product scanner system.
* Discovery_url - The URL implemented by the endpoint which has all the host information

JSON expected from the discovery_url::

  {
    "productid": "ISPACS",
    "productname": "IntelliSpace PACS",
    "productversion": "4.4.550.0",
      "hostinfo": [
          {
              "address": "1.1.1.1",
              "applications": [
                  {
                      "name": "DynamicService.Service",
                      "version": "1.0.0.0"
                  },
                  {
                      "name": "DynamicService.FileService",
                      "version": "1.0.0.0"
                  }
              ],
              "name": "Host1.isyntax.net",
              "role": "infra"
          }
      ]
  }

JSON can have multiple hosts information in the hostinfo array.


Product Scanners
----------------

To get all nodes information ``Generic Product Scanner`` should get inherited with the scanner specific ``product scanner class``.
As while collecting site facts generic discovery will fetch all nodes present in location difined in ``discovery_url``.

i.e. ::

        uri = self.kwargs.get('discovery_url', '')
        if not uri:
            logger.info('url is missing in endpoint configuration')
        response = self.HTTPRequester.suppressed_get(uri)
        if response:
            response_result = response.json()
