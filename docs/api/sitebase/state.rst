==============
State REST API
==============
Provides means to query IDM for State related data.


Query
-----
The way to query is by issuing a GET request to:

state/siteid/<siteid>/hostname/<hostname>/key/<key>

Example querying to get distribution data `state/siteid/SS123/hostname/localhost/key/aa__bb__cc__dd__ff` may return::

    {"result": "OK"}


All Site IDs
------------
To get all siteids present in state data (this may include sites that are considered to be exceptions) is by issuing a
GET request to:

/state/siteids

An example for the result may be::

    {"siteids": ["ABC01", "XYX00"]}
