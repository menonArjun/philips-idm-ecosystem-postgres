=============
Fact REST API
=============
How to query the IDM for Site and Host related facts

Summary
-------
* /fact/<siteid>/hosts
* /fact/<siteid>/hosts/<hostname>
* /fact/<siteid>/modules
* /fact/<siteid>/modules/<module>
* /fact/<siteid>/modules/<module>/keys/<key>
* /fact/<siteid>/components
* /fact/<siteid>/components/<component>
* /fact/<siteid>/nodes

* /field/ual/<component>/version/<version>
* /field/components
* /field/product_names


All Hosts
---------
/fact/<siteid>/hosts

Host list of all discovered nodes in a site. Used for a basic site map to query extended details. 

Returns::

    {"hosts": ["host2.site2.isyntax.net", "host3.site2.isyntax.net", "host4.site2.isyntax.net"]}


All Product Names
-----------------
/field/product_names

Gives list of all unique product names,

Returns::
    {"result": ["PNAME1", "PNAME2"}


Host Details
------------
/fact/<siteid>/hosts/<hostname>

Gives primary key attributes of a specific host, usually gathered from `/fact/<siteid>/hosts`

Returns::

    {
        "hostname": "host2.site2.isyntax.net",
        "address": "10.0.0.2",
        "domain": "site2.isyntax.net",
        "modules": ["ISP, Windows", "LN"]
    }

Modules List
------------
/fact/<siteid>/modules

List all modules available at a given site across all hosts. Determines which modules are install on a site and
query-able.

Returns::

    {"modules": ["LN", "Windows", "ISP"]}

Details of Modules
------------------
/fact/<siteid>/modules/<module>

Delivers the details of a module across a site.

Returns::

    For module `LN`
    {
        "result": [
            {
                "hostname": "host2.site2.isyntax.net",
                "LN" : {"alias": "magic server", "use": "equallogic_san"}
            }
        ]
    }    

    For module `ISP`
    {
        "result": [
            {
                "ISP": {
                    "module_type": "8",
                    "anywhere_version": "1.2",
                    "version": "4,4,2,1",
                    "flags": ["anywhere", "domain_needed"],
                    "identifier": "4x"
                },
                "hostname": "host2.site9.isyntax.net"
            }
        ]
    }

Host Module Key Specifics
-------------------------
/fact/sites/<siteid>/modules/<module>/keys/<key>

Pulls the specific key from the site across all hosts that have it. For example the flags array.

Usually only developers will need this one

Returns::

    For module `ISP` and key module_type
    {
        "result": [
            {
                "ISP": {"module_type": "8"},
                "hostname": "host2.site9.isyntax.net"
            },
            {
                "ISP": {"module_type": "8"},
                "hostname": "host3.site9.isyntax.net"
            },
            {
                "ISP": {"module_type": "16"},
                "hostname": "host2.site9.isyntax.net"
            }
        ]
    }


Details of Components
---------------------
/fact/<siteid>/components

Delivers the details of a components across a site.

Returns::

    {
        "result": [
            {
                "hostname": "host2.site2.isyntax.net",
                "Components": [
                    {"name": "ISP", "version": "4,4,1,2"},
                    {"name": "Anywhere", "version": "1,2"},
                    {"name": "VL", "version": "1,3"},
                    {"name": "7Zip", "version": "5,4", "tag": ["production"]}
                ]
            },
            {
                "hostname": "host3.site2.isyntax.net",
                "Components": [
                    {"name": "XIRIS", "version": "4,2,3"},
                    {"name": "XMPE", "version": "4,2,2"}
                ]
            }
        ]
    }


Host Component Specifics
------------------------
/fact/<siteid>/components/<component>

Pulls the specific component from the site across all hosts that have it.

Returns::

    for `ISP` component

    {
        "result": [
            {
                "hostname": "host2.site2.isyntax.net",
                "Components": [{"name": "ISP", "version": "4,4,1,2"}]
            },
            {
                "hostname": "host3.site2.isyntax.net",
                "Components": [{"name": "ISP", "version": "4,4,1,2"}]
            }
        ]
    }

Details of Nodes for a Site
---------------------------
/fact/<siteid>/nodes

Pulls all the hosts of a site including the vCenter VMs that have not been discovered.

Returns::

    {"result": [
        {'status': 'Up', 
        'hostname': 'host1', 
        'module_type': 'N/A', 
        'address': 'N/A', 
        'discovery_type':'Discovery',
        'modules':['module1'], 
        'Windows':{'name':'Windows Server 1'},
        'Components':[{'timestamp':'2017/03/03  18:00:00', 'version':'Windows Server', 'name':'Windows'}]
        },
        {'status': 'Down', 
        'hostname': 'host2', 
        'module_type': 'N/A',
        'address': 'N/A', 
        'discovery_type':'Discovery',
        'servicetag':'tag 1',
        'modules':'N/A'
        },
        {'status': 'Up', 
        'hostname': 'host3', 
        'module_type': 'N/A',
        'address': 'N/A', 
        'discovery_type':'vCenter',
        'HPSwitch':{'switch_model':1}, 
        'modules':['HPSwitch']
        }]
    }


Field UAL for Components of a given Version
-------------------------------------------
/field/ual/<component>/version/<version>

Returns a list of sites and hosts that match the given version string. Version is a greedy match meaning 4,4 will match
both 4,4,233,2 and 4,4,516,0

Returns::

    For /field/ual/ISP/version/4,4
    
    {
        "result": [
            {
                "siteid": "site4",
                "hosts": [
                    {
                        "hostname": "host2.site4.isyntax.net",
                        "version": "4,4,2,1"
                    },
                    {
                        "hostname": "host3.site4.isyntax.net",
                        "version": "4,4,2,1"
                    },
                ]
            },
            {
                "siteid": "site9",
                "hosts": [
                    {
                        "hostname": "host2.site9.isyntax.net",
                        "version": "4,4,2,1"
                    },
                    {
                        "hostname": "host3.site9.isyntax.net",
                        "version": "4,4,2,1"
                    },
                    {
                        "hostname": "host4.site9.isyntax.net",
                        "version": "4,4,3,1"
                    }
                ]
            },
        ]
    }


Field Sitewide Components Query
-------------------------------
/field/components


To get site breakdown of installed components filtered by specific siteid,country/region and product
`field/components/search?siteid=YYY&product=PRO1&region=RGN`


This returns a site breakdown of installed components.

Returns::

    For /field/components
    
    {
        "result": [
            {
                "siteid": "test1",
                "Components": [
                    {
                        "name": "IntelliSpace Visible Light",
                        "version": "2.1.12.0"
                    },
                    {
                        "name": "Windows",
                        "version": "Microsoft Windows Server 2008 R2 Standard , Service Pack 1"
                    },
                    {
                        "name": "IntelliSpace Anywhere",
                        "version": "1.3.12.0"
                    }
                ]
            },
            {
                "siteid": "test0",
                "Components": [
                    {
                        "name": "IntelliSpace Visible Light",
                        "version": "2.1.12.0"
                    },
                    {
                        "name": "Windows",
                        "version": "Microsoft Windows Server 2008 R2 Standard , Service Pack 1"
                    },
                    {
                        "name": "IntelliSpace Anywhere",
                        "version": "1.3.12.0"
                    }
                ]
            }
        ]
    }
