=============
Site REST API
=============
Provides means to query IDM for Sites Data.


Query
-----
The way to query is by issuing a GET request to:

`/site/details`


To get all the sites filtered by siteids

`/site/details/siteids/<siteids>`

siteids is a comma separated list of siteid. If not passed all sites will be returned.


To get all the sites filtered by country

`/site/details/countries/<countries>`

countries is a comma separated list of countries. If not passed all sites will be returned.


To get sites that are candidate for a subscription. (This is based on subscription and site country)

`/site/details/candidates/subscription/<subscription_name>`


For example querying `/site/details/siteids/BBBB,EEEE` may return::

    {"sites": [
        {
            "name": "AAAA",
            "pacs_version": "0",
            "siteid": "BBBB",
            "country": {
                "country": "United States",
                "sap_code": "",
                "market": ""
            },
            "production": true,
            "hbdbsiteid" : "AA site",
            "neb_address" : "10.4.5.243"
        },
        {
            "name": "DDDD",
            "pacs_version": "2",
            "siteid": "EEEE",
            "country": {
                "country": "",
                "sap_code": "",
                "market": ""
            }.
            "production": true,
            "hbdbsiteid" : "DD site",
            "neb_address" : "10.84.5.243"
        }
    ]}


To get the site details of all the sites along with their host count and status

`/site/details/facts`

For example, querying `/site/details/facts` may return::

    {"result": [
        {
            "host_count": 20,
            "name": "AAAA",
            "pacs_version": "0",
            "country": {
                "country": "country1",
                "market": "market1"},
            "production": true,
            "siteid": "A",
            "status":"Up"
        },
        {
            "host_count": 46,
            "name": "BBBB",
            "pacs_version": "3",
            "country": {
                "country": "country2",
                "market": "market2"
                },
            "production": true,
            "siteid": "B",
            "status":"Down"
        }
    ]}

All Site IDs
------------
The way to query for all siteids present in sites data (this does not include sites that have are considered exceptions)
is by issuing a GET request to:

`/site/siteids`

An example for the result may be::

    {"siteids": ["ABC01", "XYX00"]}


Update Site Info
----------------
To update site data a POST request with JSON information to be used for the update must be sent.

`/site/<siteid>`

There are 2 fields, all must be present: name, country

For example to update the name and country object for a site entry for siteid "ABC01", name "XXTT hospital"
and country object "{"country:"CC" ,"market":"MM"}", issuing a POST request with JSON to  the
endpoint `/site/ABC01` will result in updated site: ::

    {
        "name": "XXTT hospital",
        "country": {"country":"CC","market":"MM"}
    }


Delete Site Info
----------------
To delete site data a DELETE request with the siteid to be deleted to the endpoint:

`/site/<siteid>`

For example to delete a site entry for siteid "ABC01  is issuing a DELETE request::

    /site/ABC01

will result in deleted site
