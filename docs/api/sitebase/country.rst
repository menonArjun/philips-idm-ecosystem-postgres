================
Country REST API
================
Provides means to query IDM for Countries data.


Query
-----
The way to query is by issuing a GET request to:

/country/

For example querying `/country/` may return::

    {"result": [
        {
            "country": "United States",
            "sap_code": "US",
            "market": "LATAM"
        },
        {
            "country": "Afghanistan",
            "sap_code": "AF",
            "market": "RUBS"
        }
        ...
    ]}

