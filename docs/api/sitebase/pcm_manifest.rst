..  _pcm_manifest:

================
PCM Manifest API
================
How to submit and retrieve PCM Manifest material. This generates the SiteInfo.xml used by PCM for software installation
and deployment

Details of PCM Site Information files can be found at :ref:`_api_pcm_site_information`

There are a few differences as we are converting over to a json format from the xml currently specified.

* DataBags are split into encrypted and non-encrypted sections, Vault and Data respectively. 
* Role Type seems to be duplicated in some examples between a databag and host variable.

URI Summary
-----------
* /in/softwaredeployment/manifest/
* /facts/<siteid>/modules/PCM_Manifest


Submit a new Manifest
---------------------
HTTP POST to <pma>/in/softwaredeployment/manifest/

Submits a new site PCM manifest. Even incremental updates should store a complete document.

Example::

    {
        "manifest": {
            "siteid": "DEV03",
            "site_vault": {
                "ISiteServiceAccountPassword": "fQRAfhjsfa1fOiVR0JugjZqJOF3uuHeqLOzTeUj1Qqhip34uxpMkqTmGKclokOyJqlgJsBptRgX+LuZ8FjlPECoU+OoNwvutQAiAxSFM5ZrtrmUjn7H1oElB7SY3zEzuXJu53GXAhAGkdguR7ryWRiFfLubswE9sq7CE+WDdBD6nLxxPYDMKyaW0SVNjtEv4Qt7w+DFQrAbHwoTxYuSQc7+bb8NzjJ2543Pg0D+t86+p0hloW/6ziks5VBn5QzSlfQMqGyXoVditwuE8N866I6sy4xlIq2trOuzhn7V8Tp7fy3bW02eKxYB0d2LOQXz1AnFQ1fIvoh8ZV03dyViIJg=="
            },
            "site_data": {
                "Federated": "False",
                "LocalizationCode": "ENG",
                "ISiteServiceAccountUser": "DEVO3\\iSiteService",
                "SolutionRootFolder": "S:\\Philips\\Apps\\iSite",
                "DeploymentScripts": "C:\\provision\\isite\\deploymentscripts"
            },
            "hosts": {
                "DEV03IF1.DEV03.iSyntax.net": {
                    "RoleType": "Infrastructure",
                    "Sequence": "1",
                    "ip": "167.81.184.117",
                    "host_vault": {},
                    "host_data": {
                        "RoleType": "Infrastructure"
                    },
                    "packages": [
                        "AWV-1.3",
                        "PCM-2.3.1.1.234.0"
                    ]
                },
                "DEV03AM1.DEV03.iSyntax.net": {
                    "RoleType": "ActiveMirror",
                    "Sequence": "5",
                    "ip": "167.81.184.117",
                    "host_vault": {},
                    "host_data": {
                        "RoleType": "ActiveMirror"
                    },
                    "packages": [
                        "AWV-1.3",
                        "PCM-2.3.1.1.234.0"
                    ]
                }
            }
        }
    }



Retrieving PCM Manifest Values
------------------------------
HTTP GET to <pma>/facts/<siteid>/modules/PCM_Manifest

This is not a separate URI per say, but a usage of the existing Site Management Facts API

One thing to note is that the site_vault and site_data are stored in every hostnode. This is due to their global nature
and that each stored fact much map to a hostname by design. There is no corresponding host for global site material, so
it is identically stored on each one. This allows partial site host queries from other Site Management URIs to contain
all the needed site information.

Returns::

    {
        "results": [
                {
                    "PCM_Info": {
                        "RoleType": "ActiveMirror",
                        "Sequence": "5",
                        "ip": "167.81.184.117",
                        "site_vault": {
                            "ISiteServiceAccountPassword": "fQRAfhjsfa1fOiVR0JugjZqJOF3uuHeqLOzTeUj1Qqhip34uxpMkqTmGKclokOyJqlgJsBptRgX+LuZ8FjlPECoU+OoNwvutQAiAxSFM5ZrtrmUjn7H1oElB7SY3zEzuXJu53GXAhAGkdguR7ryWRiFfLubswE9sq7CE+WDdBD6nLxxPYDMKyaW0SVNjtEv4Qt7w+DFQrAbHwoTxYuSQc7+bb8NzjJ2543Pg0D+t86+p0hloW/6ziks5VBn5QzSlfQMqGyXoVditwuE8N866I6sy4xlIq2trOuzhn7V8Tp7fy3bW02eKxYB0d2LOQXz1AnFQ1fIvoh8ZV03dyViIJg=="
                        },
                        "site_data": {
                            "LocalizationCode": "ENG",
                            "DeploymentScripts": "C:\\provision\\isite\deploymentscripts",
                            "ISiteServiceAccountUser": "DEVO3\\iSiteService",
                            "Federated": "False",
                            "SolutionRootFolder": "S:\\Philips\\Apps\\iSite"
                        },
                        "packages": [
                            "AWV-1.3",
                            "PCM-2.3.1.1.234.0"
                        ],
                        "host_data": {
                            "RoleType": "Infrastructure"
                        }
                    },
                    "siteid": "DEV02",
                    "hostname": "DEV03AM1.DEV03.iSyntax.net"
                    "domain": "dev01.isyntax.net",
                },
                {
                    "_id": "DEV02-DEV03IF1.DEV03.iSyntax.net",
                    "domain": "dev01.isyntax.net",
                    "PCM_Info": {
                        "RoleType": "Infrastructure",
                        "Sequence": "1",
                        "ip": "167.81.184.117",
                        "site_vault": {
                            "ISiteServiceAccountPassword": "fQRAfhjsfa1fOiVR0JugjZqJOF3uuHeqLOzTeUj1Qqhip34uxpMkqTmGKclokOyJqlgJsBptRgX+LuZ8FjlPECoU+OoNwvutQAiAxSFM5ZrtrmUjn7H1oElB7SY3zEzuXJu53GXAhAGkdguR7ryWRiFfLubswE9sq7CE+WDdBD6nLxxPYDMKyaW0SVNjtEv4Qt7w+DFQrAbHwoTxYuSQc7+bb8NzjJ2543Pg0D+t86+p0hloW/6ziks5VBn5QzSlfQMqGyXoVditwuE8N866I6sy4xlIq2trOuzhn7V8Tp7fy3bW02eKxYB0d2LOQXz1AnFQ1fIvoh8ZV03dyViIJg=="
                        },
                        "site_data": {
                            "LocalizationCode": "ENG",
                            "DeploymentScripts": "C:\\provision\\isite\deploymentscripts",
                            "ISiteServiceAccountUser": "DEVO3\\iSiteService",
                            "Federated": "False",
                            "SolutionRootFolder": "S:\\Philips\\Apps\\iSite"
                        },
                        "packages": [
                            "AWV-1.3",
                            "PCM-2.3.1.1.234.0"
                        ],
                        "host_data": {
                            "RoleType": "Infrastructure"
                        }
                    },
                    "siteid": "DEV02",
                    "hostname": "DEV03IF1.DEV03.iSyntax.net"
                }
        ]
    }
