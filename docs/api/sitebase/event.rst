==============
Event REST API
==============
Provides means to query IDM for event data.

Query
-----
The way to query is by issuing a GET request to:

/event/history

Filters may be passed via query string.

For example querying `/event/history?siteid=SITEITD&start_date=YYYY-mm-dd&end_date=YYYY-mm-dd&status=STATUS&service=Network__Cisco__Switch__Memory__Usage`
may return::

    {"events": [
        {
            "state": "CRITICAL",
            "service": "Network__Cisco__Switch__Memory__Usage",
            "timestamp": "2015/01/02  05:23:05",
            "hostname": "bhea1sr1.stbhea1.isyntax.net",
            "siteid": "BHEA1",
            "hostaddress": "10.1.252.1",
            "output": "Illegal division by zero at /usr/lib64/nagios/plugins/check-cisco.pl line 241.",
            "type": "PROBLEM"
        },
        ...
    ]}


Handler Services
----------------
Provides a way to query for the list of services that may have handler events.

The way to query is by issuing a GET request to:

/event/handlerservices

An example result would be::

    {"services": ["Product__IntelliSpace__PACS__iSyntaxServer__Aggregate"]}


Handler Report
--------------
The way to query is by issuing a GET request to:

/event/handlerreport/services/<service>

Filters may be passed via query string.

For example querying `/event/handlerreport/services/Product__IntelliSpace__PACS__iSyntaxServer__Aggregate?siteid=XYZ00&start_date=YYYY-mm-dd&end_date=YYYY-mm-dd`
may return::

    {"result": [
        {
           "hosts": [{
                "total": 3,
                "hostname": "AAA",
                "success": 1,
                "failure": 2
            }, {
                "total": 1,
                "hostname": "BBB",
                "success": 1,
                "failure": 0
            }],
            "siteid": "MMMM",
            "total": 0,
            "failure": 0,
            "success": 0
        },
        {
           "hosts": [{
                "total": 2,
                "hostname": "AABB",
                "success": 1,
                "failure": 1
            }],
            "siteid": "MXB54",
            "total": 0,
            "failure": 0,
            "success": 0
        }
        ...
    ]}
