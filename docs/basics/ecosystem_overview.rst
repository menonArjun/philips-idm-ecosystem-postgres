==================
Ecosystem Overview
==================

Key areas of review. 

Vigilant (aka the Back Office)
------------------------------

This is the off premise back end that stores all persistent configurations of the IDM Ecosystem. 

.. image:: ../images/basics/IDM_Vigilant_Overview.jpg


Nebuchadnezzar (aka the Neb Node)
---------------------------------

This is the on premise node that performs monitoring and ships information back to Vigilant. It is designed to be as
ephemeral as possible. No persistent data is stored on it and all state can be recovered via PDSC.

.. image:: ../images/basics/IDM_Neb_Overview.jpg


Portal
------

TBD
