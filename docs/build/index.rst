Build
=====

Contents:

.. toctree::
   :maxdepth: 2

   build_manifest
   phirepo
   ci_setup
   docs
   jenkins_client_setup
   build_requirements
