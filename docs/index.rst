.. IDM Ecosystem documentation master file, created by
   sphinx-quickstart on Sat Jun 14 21:56:26 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

IDM Ecosystem
=============

Contents:

.. toctree::
   :maxdepth: 1

   basics/index
   internal/index
   api/index
   deployment/index
   configuration/index
   developer_setup/index
   build/index
   pcm/index
   operations/index
   analytics/index
   service_checks/index



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

