..  _vwmare_workstation_dev_wks:

============================================
Developer Workstation for VMware Workstation
============================================

Virtual Machine Creation
------------------------

#. On the Home tab, Click **Create a New Virtual Machine**
#. In the **Create New Virtual Machine Wizard** screen, highlight **Customer (advanced)**
#. Click **Next**
#. On the **Choose the Virtual Machine Hardware Compatibility** screen, Select Workstation 10.0 from the Hardware
   Compatibility dropdown
#. On the **Guest Operating System Installation** screen, select teh Install from option for **I will install the
   operating system later.**
#. Click **Next**
#. On the **Select a Guest Operating System** screen , select Linux as the Guest Operating System
#. From the Version dropdown, highlight **CentOS 64-bit**
#. Click **Next**
#. On the **Name the Virtual Machine** screen, in the virtual machine name text box enter **idm-dev-wks**
#. Leave the default Location
#. Click **Next**
#. On the **Process Configuration** screen, leave the defaults
#. Click **Next**
#. On the **Memory for the Virtual Machine** screen, leave the defaults
#. Click **Next**
#. On the **Network Type** screen, select the **Use network address translation (NAT)** option
#. Click **Next**
#. On the **Select I/O Controller Types** screen, leave the defaults
#. Click **Next**
#. On the **Select a Disk Type** screen, leave the defaults
#. Click **Next**
#. On the **Select a Disk** screen, highlight **Create a new virtual disk**
#. Click **Next**
#. On the **Specify Disk Capacity** screen, leave the defaults **(20 GB)**
#. Click **Next**
#. On the **Specify Disk File** screen, leave the defaults
#. Click **Next**
#. On the **Ready to Create Virtual Machine** screen, click **Finish**

Modification of Virtual Machine Settings
----------------------------------------
Once the Virtual Machine is created, be sure to highlight the tab.

Before powering on the machine, additional configurations are necessary

- Click the **Edit virtual machine settings**
- On the **Virtual Machine Settings** screen, within the **Hardware** tab
- Highlight **USB Controller**
- Click **Remove**
- Repeat the remove of the hardware for the **Sound Card** and **Printer**
- Highlight **CD/DVD (IDE)**, on the right side of the screen, under the **Connection** heading,
  select **Use ISO image file**
- Click **Browse** and navigate to the location of the ISO
- Click **Ok** on the **Virtual Machine Settings** screen

Modification of Virtual Machine Inbound Network
-----------------------------------------------

The last step is to edit the network configurations to allow inbound access to the virtual machine for ssh and http
traffic

- Click **Edit** at the top of the VMware Workstation toolbar
- Select the **Virtual Network Editor**
- On the **Virtual Network Editor** screen, highlight the VMnet that has an external connection type **NAT**
- Under the **VMnet Information** section, Click **NAT Settings**
- Under the **NAT Settings** screen, Under the **Port Forwarding** section, click **Add**
- On the **Map Incoming Port** screen, enter the following information for the fields

::

    Host port: 62212
    Type: TCP
    Virtual Machine IP address: 192.168.27.101
    Virtual Machine port: 22
    Description: tcp-22

- Click **Ok**
- Click **Add** once more
- On the **Map Incoming Port** screen, enter the following information for the fields

::

    Host port: 62802
    Type: TCP
    Virtual Machine IP address: 192.168.27.101
    Virtual Machine port: 8181
    Description: tcp-8181

- Click **Ok**
- Click **Ok**
- Click **Ok**
- At this point, **Power On** the virtual machine
