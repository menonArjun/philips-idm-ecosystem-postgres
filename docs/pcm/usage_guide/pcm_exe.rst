===
PCM
===

This is the main executable and generally the only one people refer to when thinking of PCM. This may be invoked via the
PCMListener as part of a HTTP call in IDM or manually called from a system by a Software Delivery Engineer

Workflow
--------

.. image:: ../../images/pcm/pcm_workflow.png


Calling Convention
------------------

- /command:<string>       Switch to install/upgrade/remove/list/bootstrap/manage commands  (short form /cmd)
- /package:<string>      	package name to process (short form /pkg). This also accepts package-version numbering. 
- /version:<string> 	    package version to process (short form /ver)
- /repository:<string>   	package repository in url format (short form /repo)
- /siteinfo:<string>     	Site information URI to be used for bootstrap (short form /site)  
- /callback:<string>      Callback URI to post success jsob blob to
- /flush:<flag>                  Flush out persistent variables. All manual users should use this.

- /rel:<flag>        	        Used to set relative instead of absolute storage.
- /help[+|-]              Simple help option to show this screen (short form /?)

Example: ::

    pcm.exe /cmd:install /pkg:pcmListener-1.0.4.0.186.0 /site:https://167.81.176.67/info/SiteInfo.xml /repo:https://167.81.176.67/repo/Tools/PCM/packages/ /flush

Installation
------------

PCM Should generally be installed by bootstrapping via the PCM Listener, or via Ansible 1.8 when available. If a manual
install is required, unzip the pcm-x.x.x.x package and run the contained installer PowerShell script. This will copy the
executable to the correct location and set the path variable
 
Troubleshooting
---------------

The PCM output log is captured in the log directory as well as a color coded console log. 

Verify that pcm.exe is available on the path by opening a command console and typing ``pcm.exe --version``
