..  _f5_event_handler:

F5 Network Event Handling Design
================================

Problem Definition
------------------

The F5 Network Local Traffic Manager (LTM) is used as an abstraction to services offered in the Intellispace Pacs
solution. The F5 device is meant for use with Enterprise applications and not specifically for the healthcare industry
or medical protocols.


Use Case Definition
-------------------

1. An instance within the application is hosting a consolidation service on tcp port 104. If the service stops accepting
   a Dicom Echo (see definition), then node shall be removed from the pool that is servicing the inbound Dicom data.
   When the Dicom Echo is successful, then the node shall be added back into the appropriate pool that is was previously
   in to allow for continued service operation.

2. An instance within the application is hosting a web service on tcp port 80 and 443. If the service stops rendering
   pages, then the node shall be removed from the pool that is servicing the inbound http/s data. When the page
   rendering is successful, then the node shall be added back into the appropriate pool that is was previously in to
   allow for continued service operation.

Definitions
-----------

+-------------------------+------------------------------------------------------------------------------------+
| Definition Name         |                                                                                    |
+-------------------------+------------------------------------------------------------------------------------+
| Dicom Echo / C-Echo     |  This is a basic "is everything working OK" service - sometimes referred to as     |
|                         |  "DICOM Ping". It should not be confused with a basic ICMP ping however as it is a |
|                         |  full-blown DICOM service, using full negotiation, and therefore tests more        |
|                         |  than simple IP connectivity. Support for C-ECHO is mandatory for all              |
|                         |  Application_Entities which accept associations.                                   |
+-------------------------+------------------------------------------------------------------------------------+

Design
------

- The IDM service checks will need to interact with the F5 Network devices that operate on version 10.2, 11.1 and 11.3
  as the currently released systems.
- The IDM service plugins will communicate to the F5 Network device via an F5 api
- The framework should be able to easily support newer versions of the F5 Networks api
- The service check for any of the defined services shall be applied to the host (Proc node or Storage node , etc.)
- The event handler will be assigned to the host (Proc node or Storage node , etc.)
- The load balancer shall have a configurable variable on the host (Proc node or Storage node , etc.) as to define the
  endpoint
- The service check shall assume the latest released version of the F5 network device is to be used and those api calls,
  if the device version does not match, the retrieved version api commands will be used by a session disconnect and
  reconnect.
- The event handler should send a notification upon a trigger of the event handler
- The event handler should only try to enable or disable the host in question a defined number of times before notifying
  that the event handler has failed.
- The communication to the F5 api should be connected via a celery task
