==================
Service Namespaces
==================

Service Checks for Linux Generic
--------------------------------

| OS__CentOS__Linux__CPU__Usage
| OS__CentOS__Linux __RAM__Usage
| OS__CentOS__Linux __Disk__Usage
| OS__CentOS__Linux __Information__Version
| OS__CentOS__Linux __Network__Status
|    http://www.vanstechelman.eu/linux/nagios_check_bandwidth_usage_plugin

Service Checks for Vigilant
---------------------------

| Datastore__Mongo10Gen__MongoDB__Connection__Status
| Datastore__Mongo10Gen__MongoDB__Database__Status
| Datastore__Mongo10Gen__MongoDB__Collection__Count
| Datastore__Mongo10Gen__MongoDB__Database__Size
| Datastore__Mongo10Gen__MongoDB__Insert_Query__Status
| Datastore__Mongo10Gen__MongoDB__Query_Query__Status
| Datastore__Mongo10Gen__MongoDB__IDM_Events_Collection__Size
| Datastore__Mongo10Gen__MongoDB__IDM_State_Collection__Size
| Datastore__Mongo10Gen__MongoDB__IDM_Alives_Collection__Size
| Datastore__Mongo10Gen__MongoDB__IDM_Events_Collection__Status
| Datastore__Mongo10Gen__MongoDB__IDM_Alives_Collection__Status
| Datastore__Mongo10Gen__MongoDB__IDM_States_Collection__Status
|    https://github.com/mzupan/nagios-plugin-mongodb

| Datastore__Elasticsearch__ElasticSearch__Cluster__Status
|    https://github.com/orthecreedence/check_elasticsearch/blob/master/check_elasticsearch

| Subversion__Collabnet__Edge__csvn_Process__Status
|    http://exchange.nagios.org/directory/Plugins/Others/Check_Svn/details

| OS__CentOS__Linux__ntpd_Process__Status
| OS__CentOS__Linux__named_Process__Status

Service Checks for Neb
----------------------

| Datastore__Pivotal__Redis__redis_Process__Status (Needs implemented still)
| Datastore__Pivotal__Redis__Response__Time
| Datastore__Pivotal__Redis__Response__Hitrate
| Datastore__Pivotal__Redis__Memory__Utilization
| Datastore__Pivotal__Redis__Connections__Received
| Datastore__Pivotal__Redis__CPU__Utilized
| Datastore__Pivotal__Redis__Client__Connections
|    http://exchange.nagios.org/directory/Plugins/Databases/check_redis-2Epl/details

| Messaging__Pivotal__RabbitMQ_Server__celery_Queue__Stats
| Messaging__Pivotal__RabbitMQ_Server__perfdata_Queue__Stats
|    http://www.thegeekstuff.com/2013/12/nagios-plugins-rabbitmq/

Service Checks for IDM
----------------------

| Task_Queue__Celery__Celery__celeryd Process__Status
| Task_Queue__Celery__Celery__celerybeat Process__Status

| Messaging__Pivotal__RabbitMQ_Server__Overview__Status
| Messaging__Pivotal__RabbitMQ_Server__Object__Counts
| Messaging__Pivotal__RabbitMQ_Server__Connection__Counts
| Messaging__Pivotal__RabbitMQ_Server__Aliveness__Status
| Messaging__Pivotal__RabbitMQ_Server__discovery_all_Queue__Stats
| Messaging__Pivotal__RabbitMQ_Server__discovery_isp_Queue__Stats
| Messaging__Pivotal__RabbitMQ_Server__discovery_ln_Queue__Stats
| Messaging__Pivotal__RabbitMQ_Server__lh5_events_Queue__Stats
| Messaging__Pivotal__RabbitMQ_Server__lh5_alives_Queue__Stats
| Messaging__Pivotal__RabbitMQ_Server__lh5_states_Queue__Stats
| Messaging__Pivotal__RabbitMQ_Server__lh5_perfdata_Queue__Stats
| Messaging__Pivotal__RabbitMQ_Server__notification_queue_Queue__Stats
| Messaging__Pivotal__RabbitMQ_Server__mongo_insert_Queue__Stats
| Messaging__Pivotal__RabbitMQ_Server__elastic_insert_Queue__Stats
| Messaging__Pivotal__RabbitMQ_Server__celery_stale_Queue__Stats
| Messaging__Pivotal__RabbitMQ_Server__jh1_configurations_Queue__Stats
| Messaging__Pivotal__RabbitMQ_Server__hbmail_raw_queue_Queue__Stats
| Messaging__Pivotal__RabbitMQ_Server__email_site_Queue__Stats
| Messaging__Pivotal__RabbitMQ_Server__queue_transport_Queue__Stats
| Messaging__Pivotal__RabbitMQ_Server__phim_analytics_clinicaldata_inbound_Queue__Stats
| Messaging__Pivotal__RabbitMQ_Server__phim_analytics_dicomdata_inbound_Queue__Stats
|    http://www.thegeekstuff.com/2013/12/nagios-plugins-rabbitmq/

| Gateway__Nginx__Nginx__nginx_Process__Status
| Gateway__Apache__Apache_HTTP_Server__httpd_Process__Status
| Gateway__Unbit__uWSGI__uwsgi_Process__Status

| Proxy__Squid__Squid_Proxy__squid_Process__Status

| Datastore__Oracle__MySQL__mysqld_Process__Status
| Mail__Postfix__Postfix__postfix_Process__Status

| Monitoring__Thruk__Thruk__thruk_Process__Status
