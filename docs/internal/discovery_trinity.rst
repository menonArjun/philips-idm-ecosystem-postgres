Discovery Overview
==================

Discovery is a process through which information from the field is used to generate configuration to monitor and mange
nodes.

High Level Workflow
-------------------

.. image:: ../images/discovery_trinity_overview.png

The discovery process starts with 1-N user defined endpoints in the lhost.yml file. Each endpoint is matched with a
Product Scanner which will retrieve the product configuration, including the list of host systems in the product
offering. For ISP this would be all the PACS nodes. In the case of single server products such as vCenter it will
product a host list of one host, itself.

Once the host list is generated, each host is given to a Host Scanner of the appropriate type to retrieve the actual
facts from the machine, including any add-on components. These facts are then bulk uploaded per product to the Facts
collection.

From the Facts Collection, the host entries are converted to Shinken config files and placed into the SVN repo
associated with the site for sync down to the Neb node.

Discovery Endpoints
-------------------

Endpoints are located in the lhost.yml file for a particular site. The ``address`` and ``scanner`` keys are required for
all endpoints. Other keys may be required depending on the scanner and type of system. Most systems will require
the ``username`` and ``password`` for access.

For Example : ::

    ---
    fqdn : idm00idm01.stidm00.isyntax.net
    siteid : IDM00
    philips_subnets : []
    dns_nameservers:
      - 10.6.88.52
      - 10.6.88.57

    endpoints:
      - address: 167.81.183.99
        scanner: ISP
        tags: [Prod, Primary]
        username: tester
        password: CryptThis

      - address: 167.81.183.8
        scanner: vCenter
        username: Administrator
        password: {$resource: $user43$}

* Address - This is the IP or Hostname that stores configuration of the product to scan.
* Scanner - What type of scanner to use on this. Current valid values are ``ISP``, ``I4``, ``LN``, ``vCenter``,
  ``AnalyticsPublisher``, ``AdvancedWorkflowServices``, ``Windows``, and ``Linux``
* Tags - This is a list of properties we wish to propagate up to the configuration generator. In the case of ISP
  scanners it used to differentiate Production from Test and BCS systems.
* Username - Username of the product scanner to retrieve configuration
* Password - Password of the username on the product scanner system.

Values may be passed as a dictionary with a "$resource" key, this will use the value of the specified resource from
the shinken configuration. Decryption is used on values, as when the cryptresource module is used.

The individual endpoint dictionary is passed to the Product scanner, the product scanner decides which keys to pass to
the host scanners used.

Product Scanners
----------------

The purpose of these items is to take an endpoint and scan the given product configuration for all associated hosts.
Only methods involved with configuration retrieval and parsing should be here.

This set of classes are passed on the Template pattern with the ``ProductScanner`` as the base class. In general, the
methods that need to be over-ridden are ``get_hosts`` and ``get_hostname``. The ``get_hosts`` method should return an
iterable of host objects to pass to the ``HostScanner``, host objects are not strictly defined they may simply be IPs or
Hostnames. The ``get_hostname`` method should take a host object as parameter and return a hostname for it. In the case
of single machine products, there is no need to override ``get_hosts`` as the base method returns the configured
endpoint IP as the only host.

Host Scanners
-------------

The purpose of host scanners is to retrieve host specific items and place them in the facts collection format.

In the fact format, information is split up into modules, a module holds information specific to a particular part of
the host being scanned. For example, information about ISP would be found under the ISP module. Windows information for
the host would be found under the Windows module.

Facts found on the host must be available as properties on HostScanner objects. A HostScanner must only return
information for one module.

There are two types of information that a HostScanner can find, *general_properties* and *module_properties*.
*general_properties* are pieces of information that would be the same if discovered by other HostScanners such as the
host address, this type of properties are rare.

This set of classes are passed on the Template pattern with the ``HostScanner`` as the base class. A derived
``HostScanner`` must have class variables defined for a ``module_name`` string, and iterables for ``general_properties``
and ``module_properties``. These will be used by ``to_dict`` to output the resulting set of facts.

Tags are passed through from the original endpoint dictionary to the host at this point.

Components
----------

If the product has any additional add-on components the method get_components should be overridden in the derived
``HostScanner`` to return a dictionary of the components to scan for such the following:

ISP Example: ::

    def get_components(self):
        return {
            "anywhere": AnywhereComponent(),
            "vlcapture": VLCaptureComponent(),
            "cca": CCAComponent()
        }

Component classes must implement a ``scan`` method to determine if the component was found on the host.

These components will set `flags` for the Shinken Config if these components return a version value.

Components are meant to be individually callable, separate from HostScanners and the ``scan`` method should return
something that when evaluated as boolean indicates that the component was found or not.
 
