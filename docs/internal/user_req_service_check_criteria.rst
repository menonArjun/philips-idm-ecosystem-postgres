..  _user_req_service_check_criteria:

====================================
IDM Ecosystem Service Check Criteria
====================================

-------------------
Feature Description
-------------------

Service Checks are the components of the IDM Ecosystem that provide information that can be used for proactive
monitoring of installed systems.  Each individual service check will result in either a success or failure status based
on the criteria defined for that check.   A notification message can then be generated to communicate the results of
this service check to the back office where it can be accessed for monitoring purposes.

------------
Intended Use
------------

The intended use of the service checks themselves is to make information about the state of services and infrastructure
available for use in proactively monitoring systems deployed at our customer sites globally.

The intended purpose of this document is to outline the service checks that are available within the tool and provide
details on the current criteria in place for each specific check.

This document is version controlled, allowing a historical view of service check criteria.  The most recent version of
the document will always reflect the criteria currently in use.

--------
Benefits
--------

<insert something from Q plan or project plan>

------------
Requirements
------------

In order to be effective in proactive monitoring of installed systems, the service checks need to be configured for
appropriate event notification.   This requirements section documents the current configuration of the IDM Ecosystem
service checks and event notifications.

Some service checks are binary in that they only have ability to determine success or failure.  Other service checks
actually capture some unit of measure at a point in time.  For these service checks additional criteria must be defined
in order to trigger a notification event.  Criteria can be defined for both warning and critical level notification in
unit of measure service checks.

Note that the generation of a notification message for any given service check is a configurable item that can be
enabled and disabled as part of the service check configuration.

Infrastructure Service Checks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Cisco Switch
~~~~~~~~~~~~

The following table defines the configuration of service checks for Cisco Switch devices.

+---------------------+----------------------+-------------------+--------------------+
| Service Check       | Notification Enabled | Warning Threshold | Critical Threshold |
+=====================+======================+===================+====================+
| CPU usage           | Yes                  | 50%               | 60%                |
+---------------------+----------------------+-------------------+--------------------+
| Memory usage        | Yes                  | N/A               | Not OK             |
+---------------------+----------------------+-------------------+--------------------+
| Temperature status  | Yes                  | 40 C              | 60%                |
+---------------------+----------------------+-------------------+--------------------+
| Fan Status          | No                   | N/A               | N/A                |
+---------------------+----------------------+-------------------+--------------------+
| Power Supply Status | No                   | N/A               | N/A                |
+---------------------+----------------------+-------------------+--------------------+


HP Switch
~~~~~~~~~~~~

The following table defines the configuration of service checks for HP Switch devices.

+---------------------+----------------------+-------------------+--------------------+
| Service Check       | Notification Enabled | Warning Threshold | Critical Threshold |
+=====================+======================+===================+====================+
| CPU usage           | Yes                  | 80%               | 90%                |
+---------------------+----------------------+-------------------+--------------------+
| Memory usage        | Yes                  | 80%               | 80%                |
+---------------------+----------------------+-------------------+--------------------+
| Temperature status  | Yes                  | 50 C              | 60 C               |
+---------------------+----------------------+-------------------+--------------------+
| Serial Number       | No                   | N/A               | N/A                |
+---------------------+----------------------+-------------------+--------------------+
| Switch Model        | No                   | N/A               | N/A                |
+---------------------+----------------------+-------------------+--------------------+
| Switch Description  | No                   | N/A               | N/A                |
+---------------------+----------------------+-------------------+--------------------+
| Switch Uptime       | No                   | N/A               | N/A                |
+---------------------+----------------------+-------------------+--------------------+


F5 BigIP
~~~~~~~~

While it is possible to capture performance data for the F5 Big IP LTM or VE components in the following table, this
feature currently has no event criteria defined and is not enabled for notification.  It is captured here as reference
for future criteria definition.

+--------------------------+----------------------+-------------------+--------------------+
| Service Check            | Notification Enabled | Warning Threshold | Critical Threshold |
+==========================+======================+===================+====================+
| Interface status overall | No                   | N/A               | N/A                |
+--------------------------+----------------------+-------------------+--------------------+
| Network interface        | No                   | N/A               | N/A                |
+--------------------------+----------------------+-------------------+--------------------+
| Management interface     | No                   | N/A               | N/A                |
+--------------------------+----------------------+-------------------+--------------------+
| Pool member state        | No                   | N/A               | N/A                |
+--------------------------+----------------------+-------------------+--------------------+

VMware vCenter
~~~~~~~~~~~~~~

The following table defines the configuration of service checks for VMware vCenter

+---------------------+----------------------+-------------------+--------------------+
| Service Check       | Notification Enabled | Warning Threshold | Critical Threshold |
+=====================+======================+===================+====================+
| Cluster HA Status   | Yes                  | N/A               | Not OK             |
+---------------------+----------------------+-------------------+--------------------+

VMWare ESX Hypervisor
~~~~~~~~~~~~~~~~~~~~~

+--------------------------+----------------------+-------------------+--------------------+
| Service Check            | Notification Enabled | Warning Threshold | Critical Threshold |
+==========================+======================+===================+====================+
| ESX CPU usage            | Yes                  | 80%               | 90%                |
+--------------------------+----------------------+-------------------+--------------------+
| ESX memory usage         | Yes                  | 80%               | 90%                |
+--------------------------+----------------------+-------------------+--------------------+
| ESX VM status            | Yes                  | N/A               | Not OK             |
+--------------------------+----------------------+-------------------+--------------------+
| ESX hardware status      | Yes                  | N/A               | Not OK             |
+--------------------------+----------------------+-------------------+--------------------+
