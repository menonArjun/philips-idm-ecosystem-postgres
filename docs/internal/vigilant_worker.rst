Vigilant Worker
===============

Overview
--------
The vigilant worker is implemented using celery, there are several configuration options that may need to be changed
depending on the desired setup of the backoffice.

MongoDB Parameters
------------------

The vigilant worker uses PyMongo to perform Mongo related tasks, the parameters are passed to a PyMongo MongoClient
instance.

The relevant parameters:

- MONGODB_URLS The first parameter passed to MongoClient ::

    MONGODB_URLS = 'mongodb://mongo.phim.isyntax.net:27017/'

Parameter can be a list, or string as decribed in: http://api.mongodb.org/python/current/examples/high_availability.html

EG:

  - "morton.local"
  - 'morton.local:27019'
  - ["morton.local:27018", "morton.local:27019"]
  - "mongodb://morton.local:27017,morton.local:27018,morton.local:27019"

- MONGODB_REPLICA_SET, the name of the replicaSet to use, if this is something that evaluates to None, then this
  parameter will not be passed.

- MONGODB_REPLICA_SET_CLIENT, if this is set to true then MongoReplicaSetClient will be used instead of MongoClient::

    MONGODB_REPLICA_SET_CLIENT = True

Elasticsearch Parameters
------------------------

The parameters used:

- ES_URLS a list of RFC-1738 urls to use as elasticsearch nodes to connect to::

    ES_URLS = ['el.phim.isyntax.net:9200']
