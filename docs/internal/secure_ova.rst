==============
OVA Base Setup
==============

This is an overview of what was done to created the RedHat STIG Compliant OVA for IDM

Space Allocations (in GB)
=========================

Two disks are part of the OVA, one for host os and the other, larger, for application data.

::

    /dev/sda    30gb
    /dev/sda1 - /boot (500mb)
    /dev/sda2 - /swap   8GB
    /dev/sda3 - Encrypted Physical LVM
    10GB /
    1.5GB /home
    2GB /tmp
    3GB /var
    3GB /var/log
    3GB /var/log/audit

    /dev/sdb1 - Encrypted Standard Partition (80GB)
    /var/philips


Encrypted Drives
================

Since human entry is not always possible or desirable for system restarts, the follow two stage decryption workflow is
used.

A keyfile is stored within the initramfs disk image on the boot partition. This will unlock /dev/sda3 which allows only
the OS partitions to mount. initramfs is used by grub to bootstrap as part of the normal loading sequence.

Inside the encrypted OS is a second keyfile that decrypts the data partition on /dev/sdb1

It is best to set up the embedded keypairs after all the base installs and updates are done, as various actions may
change dracut or update initdramfs

This is not currently implemented in the 1.7 OVA for perceived performance reasons.

Create OS Key and Embed
-----------------------

::

    mkdir -p /boot/tmp
    dd if=/dev/urandom of=/boot/tmp/luks.key bs=4096 count=1
    chmod 400 /boot/tmp/luks.key

    cryptsetup luksAddKey /dev/disk/by-uuid/74a6f916-c4d2-4091-b94f-01b5f2cab332 /boot/tmp/luks.key
    enter default passphrase created at disk partitoning time. This authorizes the keyfile.

    cryptsetup luksRemoveKey /dev/disk/by-uuid/74a6f916-c4d2-4091-b94f-01b5f2cab332
    Type in the default password used during disk creation. This command removes it, leaving only keyfile access

    vi /etc/crypttab
    luks-74a6f916-c4d2-4091-b94f-01b5f2cab332 UUID=74a6f916-c4d2-4091-b94f-01b5f2cab332 /boot/tmp/luks.key

    vi /boot/grub/grub.conf
    add in rd.luks.key=/boot/tmp/luks.key

    vi /usr/share/dracut/modules.d/90crypt/install
    inst /boot/tmp/luks.key

    dracut -f  (if adding an AppData Key, just wait till finishing that section for dracut and reboot)
    reboot

Create App Data Key
===================

::

    mkdir -p /root/.keys
    dd if=/dev/urandom of=/root/.keys/idm_data.key bs=4096 count=1
    chmod -R 400 /root/.keys

    cryptsetup luksAddKey /dev/disk/by-uuid/4394839c-3a6a-43e7-a84e-e0cf7b210672 /root/.keys/idm_data.key
    enter default passphrase created at disk partitoning time. This authorizes the keyfile.

    cryptsetup luksRemoveKey /dev/disk/by-uuid/4394839c-3a6a-43e7-a84e-e0cf7b210672
    Type in the default password used during disk creation. This command removes it, leaving only keyfile access

    vi /etc/crypttab
    luks-4394839c-3a6a-43e7-a84e-e0cf7b210672 UUID=4394839c-3a6a-43e7-a84e-e0cf7b210672 /root/.keys/idm_data.key

    dracut -f
    reboot


Hardening Files
===============

Here is a listing of all the files touched during the hardening operations. The values inserted are inside the STIGS,
however this list is provided as a quick way to reference those files or extract them for an Ansible DSC playbook

::

    /etc/grub.conf
    /etc/audit/auditd.conf

    /etc/init/control-alt-delete.override

    /etc/pam.d/system-auth
    /etc/pam.d/password-auth
    /etc/ssh/sshd_config
    /etc/audit/auditd.conf
    /etc/audit/audit.rules
    /etc/audisp/plugins.d/syslog.conf
    /etc/modprobe.d/philips.conf
    /etc/modprobe.d/blacklist.conf
    /etc/sysctl.conf
    /etc/sysconfig/init
    /etc/login.defs

    /etc/security/limits.d/90-nproc.conf
    /etc/security/limits.conf
    /etc/bashrc
    /etc/csh.cshrc
    /etc/profile
    /etc/sysconfig/ip6tables

    /etc/philips/pip.conf
    /etc/exports
    /etc/sysconfig/network-scripts/ifcfg-eth0
    /etc/yum.conf
    /etc/yum.repos.d/
    /etc/fstab
    /etc/default/useradd
    /etc/sysconfig/iptables
    /etc/alias
    /etc/sudoers


Base Packages
=============

Certain packages are installed by default, prior to any IDM software being installed.

::

    python get-pip.py

    pip install -U distribute
    pip uninstall setuptools
    pip install -U setuptools

    yum localinstall epel-release-6-8.noarch.rpm
    !!!Disable BaseCentos-Updates REPO!!!
    yum clean all

    yum -y groupinstall "Development Tools"
    yum -y install ansible
    yum -y install python-devel
    yum -y install aide

    place VMTools on /root/VMwareTools-8.6.5-621624.tar.gz
    tar -xvf VMwareTools-8.6.5-621624.tar.gz
    cd vmware-tools-distrib
    ./vmware-install.pl -d


Create Admin Account
====================

Since root is not able to log in via SSH, a separate admin account is required. The last step moves the root SSH keys to
the phiadmin account for password-less authentication. Certification login in should be used in place of password login,
however the phiadmin password is not yet disabled.

::

    useradd phiadmin
    passwd phiadmin
    usermod -G wheel phiadmin
    visudo
      # Add the following last lines
      # Administrator can use all root privilege
      phiadmin    ALL=(ALL)       ALL

    vi /etc/pam.d/su
      # Uncomment the following line to require a user to be in the "wheel" group.
      auth            required        pam_wheel.so use_uid

    vi /etc/aliases

        # Person who should get root's mail
        root:           phiadmin
    newaliases


    scp -r .ssh /home/phiadmin/
    chown -R phiadmin:phiadmin /home/phiadmin/.ssh


Rebuilding initdramfs for VMWareTools or Kernel Upgrade
=======================================================

If an embedded decryption key is present in the initramfs disk as is when following the above two-step encryption
method, care must be used when upgrading the kernel or vmWare tools as both will incorrectly rebuild the initramfs
without the decryption key.

The following is how to safely extract and rebuild the initramfs

::

    cp /boot/initramfs-2.6.32-504.el6.x86_64.img .
    mv initramfs-2.6.32-504.el6.x86_64.img initramfs-2.6.32-504.el6.x86_64.gz
    gunzip initramfs-2.6.32-504.el6.x86_64.gz
    mkdir tmp
    cd tmp
    cpio -id < ../initramfs-2.6.32-504.el6.x86_64

    Extract root drive key
    mkdir /boot/tmp
    cp boot/tmp/luks.key /boot/tmp/

    Do actions that required the rebuild such as VMWare Tools install or Kernel upgrade

    dracut --force --add-drivers "vmxnet3 vmw_pvscsi vmxnet" /boot/initramfs-2.6.32-504.el6.x86_64.img 2.6.32-504.el6.x86_64 >/dev/null 2>&1


McAffee AV 1.9
==============

Before installing make sure dependencies are present. Since we are using 64bit Minimal, there are some pre-reqs
according to https://kc.mcafee.com/corporate/index?page=content&id=KB78847

::

    yum -y install unzip
    yum -y install ed
    yum -y install pam.i686    (You must accept install of "all" depenedencies)
    yum -y install libgcc.i686  (You must accept install of "all" depenedencies)

    Follow ReadMe.txt install instructions

    Installation to /opt/NAI/LinuxShield complete.
    To connect to the McAfeeVSEForLinux web monitor, browse to https://192.168.135.143:55443
    logon as the Linux user 'nails' and supply the password entered during installation.

    Check status
    /etc/init.d/nails status

Pip Changes
===========
Since execution is denied on the tmp partition, certain applications such as pip will not function as the compile on tmp.

The below sets up the pip configuration to use a tmp folder under the root home folder and fixes the umask for correct
permission of those created files.


::

    vi /etc/fstab
    Add defaults,noexec to /tmp partition

    Create the following in /etc/philips/pip.conf
        [global]
        index-url = http://pypi.phim.isyntax.net/simple/
        use-mirrors = false
        trusted-host = pypi.phim.isyntax.net
        build=/root/tmp

    In /root/.pip
    ln -s /etc/philips/pip.conf pip.conf

    add umask 0022 to /root/.bashrc


Neb Node Creation
=================

Below are the steps followed when creating the base IDM Neb node software. Certain packages are baked into the OVA for
simplicities sake.

::

    disable CentOS-Base
    Add in authorized_keys
    /etc/hosts
    192.168.55.14 repo.phim.isyntax.net
    192.168.55.14 pypi.phim.isyntax.net


    mkdir -p /usr/lib/philips/initialization/
    copied in init script.

    yum -y install redis
    yum -y install zsync
    yum -y install nginx

    pip install celery
    pip install uwsgi
    pip install ipaddr
    pip install requests
    pip install pycontrol
    pip install pexpect
    pip install pywbem


Misc Hardening
==============

These are most of the miscellaneous STIG hardening items done to a base CentOS 6.7 install.

::

    vi /etc/securetty  # Remove all vc entries

    chmod go-w /bin/fusermount
    chmod go-w /bin/mount
    chmod go-w /bin/ping
    chmod go-w /bin/ping6
    chmod go-w /bin/su
    chmod go-w /bin/umount

    chmod go-w /sbin/netreport
    chmod go-w /sbin/pam_timestamp_check
    chmod go-w /sbin/unix_chkpwd

    chmod go-w /usr/bin/chage
    chmod go-w /usr/bin/chfn
    chmod go-w /usr/bin/chsh
    chmod go-w /usr/bin/crontab
    chmod go-w /usr/bin/fusermount
    chmod go-w /usr/bin/gpasswd
    chmod go-w /usr/bin/newgrp
    chmod go-w /usr/bin/passwd
    #chmod go-w /usr/bin/pkexec
    #chmod go-w /usr/bin/screen
    chmod go-w /usr/bin/sg
    chmod go-w /usr/bin/ssh-agent
    chmod go-w /usr/bin/staprun
    chmod go-w /usr/bin/sudo
    chmod go-w /usr/bin/sudoedit
    chmod go-w /usr/bin/vmware-user
    chmod go-w /usr/bin/wall
    chmod go-w /usr/bin/write

    chmod go-w /usr/sbin/fping
    chmod go-w /usr/sbin/fping6
    chmod go-w /usr/sbin/ping6
    chmod go-w /usr/sbin/postdrop
    chmod go-w /usr/sbin/postqueue
    chmod go-w /usr/sbin/suexec
    chmod go-w /usr/sbin/usernetctl


    vi /boot/grub/grub.conf
        password --encrypted $6$8YPn6UbH617g8U37$455axbr6LUASvZVxEDjxswXU0Fh038tuCX863oIa.2NcEvmCrB3dQqrPhSdc4o20yHyDfLfnNuzxBZhEomSA9.
    vi /etc/sysctl.conf
        net.ipv4.conf.default.accept_redirects = 0
    vi /etc/pam.d/password-auth
    vi /etc/pam.d/system-auth


    chmod 0600 /var/log/boot.log
    chmod 0600 /var/log/dmesg
    chmod 0600 /var/log/dmesg.old
    chmod 0600 /var/log/dracut.log
    chmod 0600 /var/log/lastlog
    chmod 0600 /var/log/uwsgi.log    ##NEEDS TO BE DONE POST NEB DEPLOY
    chmod 0600 /var/log/vmware-install.log
    chmod 0600 /var/log/vmware-tools-upgrader.log
    chmod 0600 /var/log/vmware-vmsvc.log
    chmod 0600 /var/log/wtmp
    chmod 0600 /var/log/btmp

    chgrp root /var/log/btmp
    chgrp root /var/log/wtmp


    find -L /bin -perm /022 -type f
    find -L /usr/bin -perm /022 -type f
    find -L /usr/local/bin -perm /022 -type f
    find -L /sbin -perm /022 -type f
    find -L /usr/sbin -perm /022 -type f
    find -L /usr/local/sbin -perm /022 -type f

Baking for Export
=================

When preparing for export, some actions are required to make the OVA easily deployable to sites, for example getting rid
of the persistent net rules that make cloning problematic, to cleaning out file allocation space so the export operation
auto-trims empty space from the virtual disks.

::

    mkdir -p /usr/lib/philips/initialization/
    copied in init script.

    rm -rf /etc/udev/rules.d/70-persistent-net.rules
    remove HWID from ifconfig and network settings
    clean /etc/hosts
    make sure yum repos are disabled.
    remove yum proxy
    remove server from resolve.conf
    yum clean all
    nuke pip caches
    /usr/sbin/aide --init
    cp /var/lib/aide/aide.db.new.gz /var/lib/aide/aide.db.gz
    remove encryption keys from /boot/tmp/luks.key
    clean root directory of installs

    Clean out any allocated space with zeros so VM can be shrunk
    cd / ; cat /dev/zero > zero.fill ; sync ; sleep 1 ; sync ; rm -f zero.fill


SE Linux
========

Re-label the filesystem on boot to convert the polices over.

::

    touch /.autorelabel
    reboot


policycoreutils-python-2.0.83-24.el6.x86_64 : SELinux policy core python utilities
or
setroubleshoot.x86_64
yum install policycoreutils policycoreutils-python
yum install -y setroubleshoot


SSL Everywhere
==============

All traffic should be migrated from HTTP to HTTPS for security concerns.

Since SVN traffic now contains credentials, it must be secured by HTTPS for transmission.

To enable SSL on a CollabNet Edge Server, log into the Web Console->Server Settings and make sure the boxes for
`Subversion Server should serve via https` and `Subversion Edge Management Console should require https.` are enabled

The load balance for the Vigilant node should be updated to correctly proxy the HTTPS traffic.


SCAP Test
=========

To install the tool, copy the zip file
`\\\\usdsfosfc1msna1\\ComputeEnvironment\\Security\\Tools\\SCC_4.0.1_rhel_x86_64.zip`, and uncompress it. An rpm file is
inside. Install it using yum::

    yum install /root/SCC_4.0.1_rhel_x86_64/scc-4.0.1.x86_64.rpm

Delete the xmls present in `/opt/scc/Resources/Content/SCAP_Content/`.

Copy the updated content zip from
`\\\\usdsfosfc1msna1\\ComputeEnvironment\\Security\\Tools\\U_RedHat_6_V1R12_STIG_SCAP_1-1_Benchmark.zip` and place the
xml files within in `/opt/scc/Resources/Content/SCAP_Content/`.

Since this is CentOS instead of RHEL some changes must be made to the installed SCAP profiles for the CPE to be correct.

::

    sed -i -e "s#<platform>Red Hat Enterprise Linux 6</platform>#<platform>CentOS 6</platform>##g" /opt/scc/Resources/Content/SCAP_Content/U_RedHat_6_V1R12_STIG_SCAP_1-1_Benchmark-cpe-oval.xml
    sed -i -e "s#cpe:/o:redhat:enterprise_linux:6#cpe:/o:centos:centos:6##g" /opt/scc/Resources/Content/SCAP_Content/U_RedHat_6_V1R12_STIG_SCAP_1-1_Benchmark-cpe-oval.xml
    sed -i -e 's#6Server\$#6##g' /opt/scc/Resources/Content/SCAP_Content/U_RedHat_6_V1R12_STIG_SCAP_1-1_Benchmark-cpe-oval.xml
    sed -i -e "s#redhat-release-server#centos-release##g" /opt/scc/Resources/Content/SCAP_Content/U_RedHat_6_V1R12_STIG_SCAP_1-1_Benchmark-cpe-oval.xml

    sed -i -e "s#cpe:/o:redhat:enterprise_linux#cpe:/o:centos:centos##g" /opt/scc/Resources/Content/SCAP_Content/U_RedHat_6_V1R12_STIG_SCAP_1-1_Benchmark-xccdf.xml

    sed -i -e "s#cpe:/o:redhat:enterprise_linux#cpe:/o:centos:centos##g" /opt/scc/Resources/Content/SCAP_Content/U_RedHat_6_V1R12_STIG_SCAP_1-1_Benchmark-cpe-dictionary.xml
    sed -i -e "s#Red Hat Enterprise Linux 6#CentOS 6##g" /opt/scc/Resources/Content/SCAP_Content/U_RedHat_6_V1R12_STIG_SCAP_1-1_Benchmark-cpe-dictionary.xml

    sed -i -e "s#<platform>Red Hat Enterprise Linux 6</platform>#<platform>CentOS 6</platform>##g" /opt/scc/Resources/Content/SCAP_Content/U_RedHat_6_V1R12_STIG_SCAP_1-1_Benchmark-oval.xml


To run::


    cd /opt/scc
    ./cscc

Results will be found in a directory. <please document this directory here when you run the tool>
