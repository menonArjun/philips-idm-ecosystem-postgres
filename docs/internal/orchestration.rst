=============
Orchestration
=============

Orchestration refers to interacting with PCM boostrap service to trigger PCM actions and wait for the results,
and then proceed based on the outcome.

This is to be done using an ansible playbook. The vital part of this procedure is handled by a script "trainman.py".
This script produces an HTTP post which is expected by the bootstrap service, then it sets up a lightweight web server
to wait for bootstrap to signal tasks have been performed.
In the HTTP post to bootstrap the callback address is passed, including a port which is selected at random from a
configurable range.
The pcm boostrap service will post a message back to the lightweight server once it is done performing the required
tasks specified in the call. Once this message is received, the script exits and returns control to the playbook.
If the message in the callback contains "State": "0", then the return of the script will be OK (Exit 0) otherwise
it will be FAILED (Exit 1). Ansible knows how to deal with these codes.

The following is a curl example of a callback that would yield a successful result::

    curl -XPOST -d '{"State": "0"}' -H "Content-Type: application/json" http://167.81.182.62:9673/submit_result

The following is a curl example of a callback that would yield a failure result::

    curl -XPOST -d '{}' -H "Content-Type: application/json" http://167.81.182.62:9831/submit_result



Dynamic Inventory from SiteInfo.xml
-----------------------------------

An ansible inventory may be created dynamically from a SiteInformation.xml. Using the dynamic inventory feature with
the 'dinventory.py' script.
The dinventory.py script will attempt to create an inventory by reading a SiteInformation.xml located by default at
'/var/philips/repo/Tools/PCM/SiteInfo.xml', this location may be changed by setting the environment variable
SITE_INFO_FILE. eg ::

    export SITE_INFO_FILE=/etc/philips/orchestration/SiteInfoDev03.xml

The script will create a pcm_boot group with the following groups as children:

    - infrastructure
    - hl7
    - cache
    - integration
    - proc
    - archive

The script will create a 'local' group containing localhost, this is used to get the address of the machine running
ansible.

If there is a loadbalancer node in SiteInformation.xml then a variable will be set that may be used to determine when
loadbalancer tasks may need to be executed.



