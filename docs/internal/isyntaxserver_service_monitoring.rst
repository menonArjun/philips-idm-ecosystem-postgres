iSyntaxServer Service Monitoring
================================

Monitoring
----------

iSyntaxServer Service check (**Product__IntelliSpace__PACS__iSyntaxServer__Aggregate**) is performed by aggregating the
output of two base service checks:

1. TCP port (**Product__IntelliSpace__PACS__iSyntaxServer__Port**)
2. Process presence via WMI (**Product__IntelliSpace__PACS__iSyntaxServer__Presence**)

The aggregate check checks both base checks and will become critical if any of the base checks is critical at the time
checked. A service restart is issued via event handler on checks number 2, 3 and 4 when a critical state is encountered.
The hard CRITICAL (problem) state is when the five checks have been performed and the service remains in CRITICAL state.

A total of five aggregate checks are performed to determine if the service is to be considered in hard CRITICAL state.
During the aggregate check period multiple base checks may be performed.

The following depicts the workflow for a Successful restart on the first attempt.

.. image:: ../images/service_restart_first_attempt.png

The following depicts the workflow for an unsuccessful series of restart attempts, reulting in hard CRITICAL state.

.. image:: ../images/service_restart_unsuccessful.png

A notification is sent with type=PROBLEM and state=CRITICAL if the service reaches Hard CRITICAL state.
The output field will contain a comma separated list of the base services determined to be in CRITICAL state, eg:
**"CLUSTER CRITICAL: iSyntaxServer: 0 ok, 0 warning, 0 unknown, 2 critical [
Product__IntelliSpace__PACS__iSyntaxServer__Presence, Product__IntelliSpace__PACS__iSyntaxServer__Port ]"**

A notification is sent with type=RECOVERY and state=OK when the service leaves Hard CRITICAL state, this is when all
base services are in OK state.


Restart Action
--------------

iSyntaxServer Service restart are performed the event handler plugin restart_win_service_and_notify. The plugin provides
 the capability to sent a notification before performing a Windows service restart via RPC.

The event handler for iSyntaxServer is configured to execute restart on soft CRITICAL state attempt 2, 3 and 4.

Notifications are sent before the actual restart is executed. The notification issued will have type=HANDLER and
state=CRITICAL whenever a restart is about to be attempted. The notification will contain in the output field the
output from the the aggregate check additionally it will contain the restart count in the last set of square brackets.
Eg. for the second attempt count where the two base services failed: **"iSiteMonitor service restart attempted - CLUSTER
CRITICAL: iSyntaxServer: 0 ok, 0 warning, 0 unknown, 2 critical [ Product__IntelliSpace__PACS__iSyntaxServer__Presence,
Product__IntelliSpace__PACS__iSyntaxServer__Port ] [2/3]“**

A notification will be sent when the soft CRITICAL state is exited after service restart should have been attempted
(attempt greater than 2). This notification would have type=HANDLER and state=OK. In the output field the restart count
for the last attempt will be present in the last set of square brackets. Eg. for the service recovering after the second
restart attempt: **"iSiteMonitor service restart attempted - CLUSTER OK: iSyntaxServer: 0 ok, 0 warning, 0 unknown, 0
critical [2/3]"**. (This is a good hint that the last restart attempt was successful)
