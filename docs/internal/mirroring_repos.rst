Mirroring Repos
===============
Overview
--------
Repos containing RPMs and python packages are used by the IDM Ecosystem solution. These are mirrored from internet repos
to reduce the dependency on internet connection and increase availability.

Application wget is used to mirror the RPM repositories via http, rsync port/ssh was closed at the time this was
decided. Cutting directories is needed to avoid unnecessary directory depth.

PYPI Repos are not mirrored completely, just needed packages are downloaded, then simplepypi is used to serve the
packages.

Repos are collected in a /repo directory, they usually contain an x86_64 directory and an i386 one when needed (not
often).

Extra Repos
-----------
Two "Extra" Package repos are used: repoforge and epel

Rpmforge
~~~~~~~~
For rpmforge list of mirrors look at:
http://mirror-status.repoforge.org/#us

Mirroring command: ::

    wget -N -r -np -nH --cut-dirs=6 -R 'index.html' -P /repo/rpmforge/x86_64/ http://mirror.teklinks.com/repoforge/redhat/el6/en/x86_64/rpmforge/

    wget -N -r -np -nH --cut-dirs=6 -R 'index.html' -P /repo/rpmforge_extras/x86_64/ http://mirror.teklinks.com/repoforge/redhat/el6/en/x86_64/extras/

EPEL
~~~~
For epel list of mirrors look at:
https://mirrors.fedoraproject.org/publiclist/EPEL/6/x86_64/#US

Mirroring command: ::

    wget -N -r -np -nH --cut-dirs=2 -R 'index.htm*' -P /repo/epel/ http://mirror.sfo12.us.leaseweb.net/epel/6/x86_64/

CentOS Repos
------------

To get list of mirrors:

http://mirrorlist.centos.org/?release=6.8&arch=x86_64&repo=os

Latest 6.x mirrors:

http://mirrorlist.centos.org/?release=6&arch=x86_64&repo=os

The arch (probably only x86_64 needed) and repo parameters can be changed to get the appropriate lists.
The repo names to duplicate are: os, updates, extras
os is sometimes referred to also as "base"

OS
~~

::

    wget -N -r -np -nH --cut-dirs=5 -R 'index.htm*' -P /repo/base/ http://mirror.san.fastserv.com/pub/linux/centos/6.8/os/x86_64/

Updates
~~~~~~~

::

    wget -N -r -np -nH --cut-dirs=3 -R 'index.htm*' -P /repo/updates/ http://mirror.rackspace.com/CentOS/6/updates/x86_64/

Extras
~~~~~~

::

    wget -N -r -np -nH --cut-dirs=3 -R 'index.htm*' -P /repo/extras/ http://mirror.rackspace.com/CentOS/6/extras/x86_64/

Others
------

MongoDB
~~~~~~~

::

    wget -N -r -np -nH --cut-dirs=5 -R 'index.htm*' -P /repo/mongo https://repo.mongodb.org/yum/redhat/6/mongodb-org/3.2/x86_64/

Nginx
~~~~~

::

    wget -N -r -np -nH --cut-dirs=4 -R 'index.htm*' -P /repo/nginx http://nginx.org/packages/mainline/centos/6/x86_64/

Pypi Packages
-------------

To get newer pip to work requests[security] was needed which needed cffi: ::

    yum install python-cffi
    yum install libffi-devel
    pip install cryptography --upgrade
    pip install requests[security]
    pip install --upgrade pip

Pip packages are downloaded on a case by case basis. Run this from a system with the same architecture as the target
system, otherwise incorrect packages may be downloaded ::

    pip download --dest /repo/pypi/ 'amqp<2.0,>=1.4.9'  # celery requirement that seems to not be handled on its own
    pip download --dest /repo/pypi/ fabric celery suds librabbitmq bottle bottle-extras bottle-redis bottle-mongo elasticsearch pymongo redis hiredis uwsgi pycontrol pexpect pip setuptools pynagios ipaddr supervisor pywinrm pypiserver coverage nose envoy grequests npyscreen urwid ansible pysphere virtualenv wheel pex sphinx jenkinsapi jsonpath_rw pyinotify boto kerberos mimerender wmi-client-wrapper kazoo temporary py cached-property pylint pep8 requests nose-timer paramiko cryptography pywbem

Supervisor is used to keep the server running, and nginx to route the traffic.
