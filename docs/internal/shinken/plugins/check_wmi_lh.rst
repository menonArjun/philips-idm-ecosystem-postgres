check_wmi_lh
============

This package contains Nagios plugins to perform various WMI related actions.

check_wmi_lh
------------

check_wmi_lh.pl is a plugin to monitor Windows subfolder count via WMI
created from tearing check_wmi_plus to pieces just to get one query to run,
because I could not get it to work via ini file.

check_cluster_lh
----------------

Plugin to evaluate the results from other plugins to provide aggregation.

Based on check_cluster, the main difference is the ability to display the labels of the offending services to easily
determine which services are the ones in problem state.

filter_perfdata
---------------

a plugin to filter performance data, it makes it possible for a check to report filtered
information based on the output of another test.

short_c
-------

a plugin to wrap a command call, deciding whether to run depending on a condition parameter.

check_wmi_access
----------------

A lighter plugin to check if WMI queries can be made.

info_param
----------

A plugin to parse a string for information in phim format and pass it to a command as parameters.
