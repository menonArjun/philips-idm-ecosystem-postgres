===========
Credentials
===========

Encrypting
==========

Default key: -BEhr4qfb-GMRMZU1YK-Q6ifPIrQkKstA5cHbxHaF9M=

Steps to encrypt credentials::

    from cryptography.fernet import Fernet
    secret = bytes('-BEhr4qfb-GMRMZU1YK-Q6ifPIrQkKstA5cHbxHaF9M=')
    cipher_suite = Fernet(secret)
    cipher_suite.encrypt(bytes('st3nt0r#'))

output will need to be wrapped in '{++}' in the YAML file

Known Credentials
=================


These are some known credentials::

    # Store some usernames and passwords (hidden from the CGIs)
    #$USER3$=someuser
    #$USER4$=somepassword

    # CE definition for user access
    # ESXi
    $USER3$=monitor
    $USER4$=st3nt0r#

    # ISP Node
    $USER5$=administrator
    $USER6$=st3nt0r

    # Cisco
    $USER7$=kyuss

    # Dell Equallogic
    $USER8$=kyuss

    # Load Balancers F5
    $USER9$=kyuss

    # Xcelera system
    $USER10$=phiadmin
    $USER11$=1nf0M@t1cs

    # CEV system
    $USER12$=phiadmin
    $USER13$=1nf0M@t1cs

    # DCX system
    $USER14$=administrator
    $USER15$='Service\;'

    # IBM IMM
    $USER16$=kyuss

    # MongoDB
    $USER17$=''
    $USER18$=''

    # RabbitMQ Server
    $USER19$=guest
    $USER20$=guest

    # Internal Philips application platform passwords
    $USER21$=''
    $USER22$=''

    # ICCC System
    $USER23$=administrator
    $USER24$=\#welcome123

    # Vidyo Server
    $USER25$=root
    $USER26$=Welcome123*

    # HP ILO
    $USER27$=administrator
    $USER28$=1nf0M@t1cs

    # Linux Servers
    $USER31$=nagios-monitor
    $USER32$=/etc/philips/nagios/private/nagios-monitor

    # I4 node
    $USER33$=fse_admin
    $USER34$=Philips!

    # IBM Storwize
    $USER35$=monitor

    # vCenter
    $USER36$=administrator
    $USER37$=st3nt0r

    # HP MSA Username and Password
    $USER38$=phimonitor
    $USER39$='Nsah2damsj@5nM'

    # HP Switch Username, Protocal(SHA & DES), _HOSTSNMP_V3_AUTH/_HOSTSNMP_V3_PRIV
    $USER40$=phimonitor
    $USER41$='sha,des'
    $USER42$='Nsah2damsj@5nM'
    $USER140$=des
    $USER142$=sha
    $USER141$=authPriv
