On Site Data Query
==================

Overview
--------

Data collected on onsite nodes is placed in a database to transfer periodically to the backoffice, the data is
gathered by the various shinken plugins.

Various plugins collecting data for informational purposes exist currently to capture specific pieces of information,
examples of these are: IP Address, OS Version, etc.

The ability to query this data to generate simple tables with the data stored in this database may be useful in certain
scenarios.


Querying
--------

Querying for data involves accessing a web resource with the data being queried for and possibly a filter.


Case: VM Object Name
--------------------

Creating an informational plugin to get the VM Object name for a node based on its Hostname could be an achievable task,
since the vSphere information is available to shinken, the VM Object name could be perhaps be found based on the host
name.

Once the data is in the on site collected data, having the ability to query for hostname and the key containing the VM
Object name.

Supposing a hostname key was present on the database 'Administrative__Philips__Host__Information__hostname' and the
key for the VM object name could be 'Administrative__Philips__Host__Information__VM_Object', the result of querying the
data could result in the following table:

====================================================  =====================================================
Administrative__Philips__Host__Information__hostname  Administrative__Philips__Host__Information__VM_Object
----------------------------------------------------  -----------------------------------------------------
host1.isyntax.net                                     VM-01
host2.isyntax.net                                     VM-02
host4.isyntax.net                                     VM-04
====================================================  =====================================================

Filtering this data would be trivial.

Web Service for VM Object Name
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

For the Stack Move project a web service has been created to allow the user to provide a list of host names.  The web
service can be accessed at http://<Neb_Node_IP_Address>/phim/state/query/vmobjcl.

  .. NOTE::  To access the web service the user will need to authenticate with the same credentials utilized to
             access Thruk.

.. image:: ../images/nebwebservice.PNG

This will obtain the VM object name, Cluster Name, and a list of the vmdk files (virtual hard disk) in the VM
`(see Shinken Plugin for VMware Guest Information) <shinken/plugins/vmware_guest_information.html>`_

Once the information has been obtained from Shinken, a csv file is produced with the list of host names, VM object
names, disks, and cluster names for the user to consume.