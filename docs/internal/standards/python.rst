======
Python
======

Python 2.6 is targeted.

Read PEP8: https://www.python.org/dev/peps/pep-0008/

Code should be as PEP8 compliant as possible using judgement.

Read PEP20: https://www.python.org/dev/peps/pep-0020/

Read this: http://docs.python-guide.org/en/latest/writing/style/
Rest of the guide is good too, if you have time to read it.

The rest is based on this
http://google-styleguide.googlecode.com/svn/trunk/pyguide.html

Watch these videos by Raymond Hettinger:

Transforming Code into Beautiful, Idiomatic Python https://www.youtube.com/watch?v=OSGv2VnC0go

Raymond Hettinger - Beyond PEP 8 -- Best practices for beautiful intelligible code - PyCon 2015
https://www.youtube.com/watch?v=wf-BqAjZb8M

--------------
Language Rules
--------------

Imports
-------
Use import x for importing packages and modules.

Use from x import y where x is the package prefix and y is the module name with no prefix.

Use from x import y as z if two modules named y are to be imported or if y is an inconveniently long name.

Do not use relative names in imports. Even if the module is in the same package, use the full package name. This helps
prevent unintentionally importing a package twice.

Global Variables
----------------
Avoid global variables.

Deprecated Language Features
----------------------------
Avoid.

Iterators
---------
Prefer to use iterators if available, as in iteritems vs items.

Generators
----------
Use generators as needed.

Default Argument Values
-----------------------
Default arguments are evaluated at module load. ::

    def foo(a, b=[]):

The list assigned to b is only created once, at load time. This may be what is meant: ::

    def foo(a, b=None):
        if b is None:
            b = []

Properties
----------
Use properties for accessing or setting data where you would normally have used simple, lightweight accessor or setter
methods.

True/False Evaluation
---------------------
Never do silly things like comparing a boolean to False.

If there is need to distinguish None then use "is None".

-----
Style
-----

Semicolons
----------
Do not terminate your lines with semi-colons and do not use semi-colons to put two commands on the same line.

Line length
-----------
Maximum line length is 80 characters.

Parentheses
-----------
Use parentheses sparingly.

Indentation
-----------
Indent your code blocks with 4 spaces.

Do NOT use tabs.

Blank Lines
-----------
Two blank lines between top-level definitions, one blank line between method definitions.

Whitespace
----------
Follow standard typographic rules for the use of spaces around punctuation.

Shebang Line
------------
Most .py files do not need to start with a #! line.

If it's a script then its needed, use env: ::

    #!/usr/bin/env python

Comments
--------
Be sure to use the right style for module, function, method and in-line comments.

Only add comment if really needed.

Classes
-------
If a class inherits from no other base classes, explicitly inherit from object. This also applies to nested classes.

Strings
-------
Use the format method or the % operator for formatting strings, even when the parameters are all strings. Use your best
judgement to decide between + and % (or format) though.

When using 'logging' use % as this is implemented by default and will be better performant when logging does not take
place.

Files and Sockets
-----------------
Explicitly close files and sockets when done with them.

TODO Comments
-------------
Use TODO comments for code that is temporary, a short-term solution, or good-enough but not perfect.

Imports formatting
------------------
Imports should be on separate lines. ::

    import os
    import sys

Statements
----------
Generally only one statement per line.

Access Control
--------------
If an accessor function would be trivial you should use public variables instead of accessor functions to avoid the
extra cost of function calls in Python. When more functionality is added you can use property to keep the syntax
consistent.

Name Mangling
-------------
(Properties and methods using double underscore leading names.)

Avoid.

Names to Avoid
--------------
Single character names except for counters or iterators

Dashes (-) in any package/module name

__double_leading_and_trailing_underscore__ names (reserved by Python)

Naming Convention
-----------------
Classes should be PascalCase (CamelCase with first letter always capitalized)

Constants UPPERCASE_SNAKE_CASE.

Everything else lowercase_snake_case.

Main
----
Even a file meant to be used as a script should be importable and a mere import should not have the side effect of
executing the script's main functionality. The main functionality should be in a main() function.

Large elif
----------
Many if/elif may be better written using a dictionary.

Unit Test Dependencies
----------------------
Mock dependencies when writing unit tests, most will not be installed in the build nodes.
