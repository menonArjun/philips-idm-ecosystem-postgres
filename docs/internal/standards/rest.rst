================
reStructuredText
================

Lines
-----
Limit length to 80 characters.

For notes this can be done like: ::

    .. note:: This is a very long note that is just here for an example. The quick brown fox jumps over the lazy fox,
              that fox must be really lazy not to move when being jumped over.

Line Blocks
-----------
Line blocks, the ones starting with | should only be used if needed, in most cases keeping the arrangement of the lines
is not important.

Numbering
---------
Avoid hard numbering, use # to fill numbers automatically.

Images
------
Images are to be stored in "images" directory at root of the structure, separate images into directories there where
appropriate.

Linking
-------
Use standard reST labels for cross-referencing.

These look like: ::

    .. _my-reference-label:

    Section to cross-reference
    --------------------------

Be mindful that these labels need to be unique between all documents.

If adding label to top of document, using the filename makes sense.
