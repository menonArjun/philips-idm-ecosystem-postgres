===============
Hydra Standards
===============

Read clean code!

Unit tests are code. It should abide like rest of code does.

This is intended to be used as a guideline, there are places where following the guideline does not make sense. Use your
best judgement. Be prepared to explain why the guideline was not followed. There should be a good reason for it.

.. toctree::
   :maxdepth: 1

   python
   ansible
   rest
