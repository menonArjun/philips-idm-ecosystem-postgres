=================
Phimutils message
=================
Message Class
-------------
The Message class is to be used to contain information to be transmitted, somewhat like serializing/unserializing.

It provides capabilities to generate JSON out of the expected information, or a python dictionary.
It has the ability to compress or base 64 encode the payload or parts of the payload.

Payload may be compressed if it is a compressible type of data, such as a string. If payload is a dictionary, parts
of it may be compressed by specifying them as target keys.

Perfdata passed to the constructor will get parsed using the nagios perfdata specification and stored as a list of
dictionaries. to_json/to_dict will return this structure. from_json/from_dict expect it in list of dictionaries form.

A timestamp may be passed otherwise it will be created from the current time.

Creating a Message
~~~~~~~~~~~~~~~~~~

To create a message, using no compression or encoding. Then generating JSON for it.::

    msg = Message(siteid='sit01', hostname='if01.isyntax.net', type='error',  payload='a string payload')
    json_string = msg.to_json()
    msg_dict = msg.to_dict()

Adding compression: ::

    Message(siteid='sit01', hostname='if01.isyntax.net', type='error',  payload='a string payload', compress_payload=True)

Compressed messages will be also base 64 encoded.
To do base 64 encoding: ::

    Message(siteid='sit01', hostname='if01.isyntax.net', type='error',  payload='a string payload', encode_payload=True)

The payload may be a dictionary of items that have to be themselves JSON serializable::

    Message(siteid='sit01', hostname='if01.isyntax.net', type='error',  payload={"key": "one", "foo": "bar"})

Keys in the payload may be compressed, they must meet the same requirements as when the payload is to be compressed.
The "target keys" to be compressed must be passed in a list: ::

    Message(siteid='sit01', hostname='if01.isyntax.net', type='error',  payload={"key": "one", "foo": "bar", "baz": "string here"}, compress_payload=True, target_keys=['foo', 'baz'])


Getting Message
~~~~~~~~~~~~~~~

To get a message from a dictionary or JSON created with the to_dict or to_json methods: ::

    msg1 = Message.from_dict(msg_dict)
    msg2 = Message.from_json(json_string)

A dictionary may be unpacked with the static method unpack_dict, this will change the dictionary to unpack the payload
as it was when the to_dict method was called.::

    msg = Message(siteid='sit01', hostname='if01.isyntax.net', type='error',  payload={"key": "one", "foo": "bar", "baz": "string here"}, compress_payload=True, target_keys=['foo', 'baz'])
    msg_dict = msg.to_dict()
    Message.unpack_dict(msg_dict)
    # msg_dict['payload'] will have the uncompressed payload payload={"key": "one", "foo": "bar", "baz": "string here"}

Perfdata
~~~~~~~~

Creating a message using perfdata like the following: ::

    msg1 = Message(siteid='sit01', hostname='if01.isyntax.net', perfdata="'metric one'=35m;4;5;3;7")

Will result in the following when to_dict is called on it: ::

    msg1.to_dict()
    {
        'timestamp': '2015-09-16T04:12:06.335042Z',
        'hostname': 'if01.isyntax.net',
        'siteid': 'sit01',
        'perfdata': [
                        {
                            'min': '3',
                            'max': '7',
                            'value': '35',
                            'label': 'metric one',
                            'warn': '4',
                            'crit': '5',
                            'uom': 'm'
                        }
                    ]
    }

Timestamp
~~~~~~~~~
A timestamp will be created from one of timestamp, epoch_time, datetime parameters passed when instantiated, if none of
these is passed then the current timestamp will be used. The timestamp will be a string using ISO 8601 extended format
in UTC with no offset. Eg: "2013-07-23T13:38:45.404123Z".

Precedence of parameters for timestamp in order is: timestamp, epoch_time, datetime.

Timezone information from any parameter will be ignored and UTC will be assumed.

An offset-naive datetime will be available as well as the 'datetime' attribute.
