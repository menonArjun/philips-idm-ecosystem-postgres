================
Node setup order
================

| Repository
| Nameserver
| Source Control
| Linux Vigilant Hop Server
| Windows 2012 Hop Server
| Windows 2008 Remote Desktop Gateway Service
| F5 VE Load Balancers
| IPAM Application
| Backoffice IDM
| Ansible Control System


Repository - See setup_repository notes + the following
-------------------------------------------------------

1. Deploy a vigilant template
#. Execute initialization ::

    /usr/lib/philips/initialization.py -i 10.77.45.11 -n 255.255.255.0 -g 10.77.45.1 -d 10.77.45.5 -s phim0 -e 1.1.1.1 -t

#. Fix the following ::

    /etc/sysconfig/network
      HOSTNAME=phim0rp1.phim0.isyntax.net

    vi /etc/hosts
    Modify all the hostnames to match the hostname and fqdn
    Remove all the 1.1.1.1 entries from the file.

    Stop all vigilant services as they are not operating on this server
        chkconfig celerybeat off
        chkconfig celeryd off
        chkconfig elasticsearch off
        chkconfig httpd off
        chkconfig mongod off
        chkconfig mysqld off
        chkconfig nagios off
        chkconfig nginx off
        chkconfig postfix off
        chkconfig rabbitmq-server off
        chkconfig redis off
        chkconfig shinken-arbiter off
        chkconfig shinken-broker off
        chkconfig shinken-poller off
        chkconfig shinken-reactionner off
        chkconfig shinken-receiver off
        chkconfig shinken-scheduler off
        chkconfig squid off
        chkconfig supervisord off
        chkconfig thruk off
        chkconfig uwsgi off
        chkconfig webmin off

        reboot

Follow the notes for the repository setup of the disks (createrepo will not be installed on the appliance at this point
and there is no yum repo available yet)

Once the data has been synchronized, power on the VM and perform the following post configurations ::

    chkconfig nginx on

Edit the /etc/nginx/nginx.conf to show the following ::

    http {
        server {
            listen 80;
            server_name repo.phim.isyntax.net;

            access_log /var/log/nginx/access.log;
            error_log /var/log/nginx/error.log;
            location / {
                autoindex on;
                root /repo;
            }
        }
    }

Start the nginx service ::

    service nginx start

Reboot the server

Add the host file entry for the repo (itself) ::

    echo "10.77.45.11  repo.phim.isyntax.net" >> /etc/hosts

Install the createrepo application ::

    yum -y install createrepo

Remove any phirepo files ::

    rm -rf /repo/phirepo/x86_64/ /repo/phirepo/i386/ /repo/phirepo/noarch/ /repo/pypi/ /repo/repodata/ /repo/phirepo/comps.xml


Setup Pypi Services ::

    mkdir -pv /repo/pypi

    pip install --download /repo/pypi/ pypiserver
    pip install /repo/pypi/pypiserver-1.1.6.zip
    rm -rf /tmp/pip-build-*

Copy the Pypi files from a internal file repo and place in /repo/pypi

Create the repo user ::

    useradd repo


Remove the shortbus configs from supervisord.conf ::

    vi /etc/supervisord.conf

    [program:shortbus]
    command=/usr/bin/python -u /usr/lib/philips/workers/shortbus.py
    user=shortbus
    autorestart=true
    log_stderr=true
    logfile=/var/log/supervisor/shortbus.log
    logfile_maxbytes=10MB
    logfile_backups=10

Add the following for Pypi server ::

    [program:pypiserver]
    command=pypi-server -p 8084 /repo/pypi/
    user=repo
    autorestart=true
    log_stderr=true
    redirect_stderr=true
    logfile=/var/log/supervisor/pypiserver.log
    logfile_maxbytes=10MB
    logfile_backups=10

Set Supervisor to start and run the supervisor service ::

    chkconfig supervisord on
    service supervisord start

Adding Philips Repo in place

Copy comps.xml from the release location as it is needed to generate the Philips Monitoring packages to /repo/phirepo

Copy el6-i686-rpmbuild and el6-x86_64-rpmbuild from the release location

Create the following folders under /repo/phirepo ::

    mkdir -pv /repo/phirepo/{noarch,i386,x86_64}

Copy the appropriate packages from i686 to the i386 folder
Copy the noarch packages from the x86_64 to noarch folder
Copy the x86_64 packages from the x86_64 to noarch folder

Execute the following commands ::

    cd ..
    createrepo -g comps.xml phirepo

Execute updates for this release and drop notes for 1.0.1.3.1013.88 ::

    yum remove -y phim-shinkencfg
    yum -y update
    reboot

------------

Nameserver + NTP
----------------

1. Deploy a vigilant template
#. Execute initialization ::

    /usr/lib/philips/initialization.py -i 10.77.45.5 -n 255.255.255.0 -g 10.77.45.1 -d 1.1.1.1 -s phim0 -e 1.1.1.1 -t

#. Fix the following ::

    /etc/sysconfig/network
      HOSTNAME=phim0ns1.phim0.isyntax.net

    cat /dev/null > /etc/resolv.conf

    vi /etc/hosts
    Modify all the hostnames to match the hostname and fqdn
    Remove all the 1.1.1.1 entries from the file.
    Add an entry for the Repository
    10.77.45.11 repo.phim.isyntax.net

    Stop all vigilant services as they are not operating on this server
        chkconfig celerybeat off
        chkconfig celeryd off
        chkconfig elasticsearch off
        chkconfig httpd off
        chkconfig mongod off
        chkconfig mysqld off
        chkconfig nagios off
        chkconfig nginx off
        chkconfig postfix off
        chkconfig rabbitmq-server off
        chkconfig redis off
        chkconfig shinken-arbiter off
        chkconfig shinken-broker off
        chkconfig shinken-poller off
        chkconfig shinken-reactionner off
        chkconfig shinken-receiver off
        chkconfig shinken-scheduler off
        chkconfig squid off
        chkconfig supervisord off
        chkconfig thruk off
        chkconfig uwsgi off
        chkconfig webmin off

        reboot

Change /etc/ntp.conf
restrict entry to match the IDM network ::

    restrict 10.77.45.0 mask 255.255.255.0 nomodify notrap

Add a public server to the list to synchronise against, the default will have all entries commented out

Comment out the lines for ::

    includefile /etc/ntp/crypto/pw
    keys /etc/ntp/keys

Remove the line that points to ::

    sever ntp.phim.isyntax.net

Change to ::

    server 10.2.1.20
    server 10.2.1.87

Restart the service to bring up ::

    service ntpd restart

Setup Bind configurations for the domain (phim0.isyntax.net) ::

    vi /etc/named.conf

    options {
        listen-on port 53 { 10.77.45.5; };
        allow-query       { 10.77.45.0/24; localhost; };
        dnssec-enable no;
        dnssec-validation no;
        forwarders { 10.2.1.20; 10.2.1.87; };
    };

    Remove --
    zone "." IN {
        type hint;
        file "named.ca";
    }

    Add in its place --
    zone "phim0.isyntax.net" IN {
        type master;
        file "/var/named/phim0.isyntax.net.zone";
        allow-update { none; };
        allow-query { any; };
    };

    zone "phim.isyntax.net" IN {
        type master;
        file "/var/named/phim.isyntax.net.zone";
        allow-update { none; };
        allow-query { any; };
    };

    zone "45.77.10.in-addr.arpa" IN {
        type master;
        file "/var/named/45.77.10.rev";
        notify no;
        allow-update { none; };
    };

    cat > /var/named/phim0.isyntax.net.zone << "EOF"
    ;
    ;  This is the forward zone for the phim0.isyntax.net internal domain
    ;
    $TTL    86400
    @       IN     SOA     phim0ns1.phim0.isyntax.net. root.phim0.isyntax.net. (
    100     ; serial
    1H      ; refresh
    1M      ; retry
    1W      ; expiry
    1D )    ; minimum
    @       IN     NS      phim0ns1.phim0.isyntax.net.
    @       IN     A       127.0.0.1
    ;
    ;  Below are the A records for the domain
    ;
    phim0ns1     IN     A     10.77.45.5
    phim0rp01    IN     A     10.77.45.11
    phim0sc01    IN     A     10.77.45.61

    ; Below are the CNAME Record Address (Aliases) - Point to an A Record Address

    ns1          IN     CNAME    phim0ns1.phim0.isyntax.net.
    repo         IN     CNAME    phim0rp01.phim0.isyntax.net.
    sc           IN     CNAME    phim0sc01.phim0.isyntax.net.
    EOF

    cat > /var/named/45.77.10.rev << "EOF"
    ;
    ;  This is the reverse zone for the phim0.isyntax.net internal domain
    ;
    $TTL    86400
    @       IN     SOA     phim0ns1.phim0.isyntax.net. root.phim0.isyntax.net. (
    100     ; serial
    1H      ; refresh
    1M      ; retry
    1W      ; expiry
    1D )    ; minimum
    @       IN     NS      phim0ns1.phim0.isyntax.net.

    ; Below are the pointer records for the zone

    11         IN     PTR     phim0rp01.phim0.isyntax.net.
    61         IN     PTR     phim0sc01.phim0.isyntax.net.

    EOF

    cat > /var/named/phim.isyntax.net.zone << "EOF"
    ;
    ;  This is the forward zone for the phim0.isyntax.net internal domain
    ;
    $TTL    86400
    @       IN     SOA     phim0ns1.phim0.isyntax.net. root.phim0.isyntax.net. (
    100     ; serial
    1H      ; refresh
    1M      ; retry
    1W      ; expiry
    1D )    ; minimum
    @       IN     NS      phim0ns1.phim0.isyntax.net.
    @       IN     A       127.0.0.1
    ;
    ;  Below are the A records for the domain
    ;
    phim0ns1     IN     A     10.77.45.5
    phim0rp01    IN     A     10.77.45.11
    phim0gw01    IN     A     10.77.45.21
    phim0sb01    IN     A     10.77.45.31
    phim0wk01    IN     A     10.77.45.41
    phim0ps01    IN     A     10.77.45.51   ; Mongo
    phim0ps02    IN     A     10.77.45.52   ; ElasticSearch
    phim0sc01    IN     A     10.77.45.61
    phim0sg1     IN     A     10.77.45.249  ; Raritan Secure Gateway
    phim0vs01    IN     A     10.77.45.250  ; Load balancer Virtual Server endpoint

    ; Below are the CNAME Record Address (Aliases) - Point to an A Record Address

    ns1          IN     CNAME    phim0ns1.phim0.isyntax.net.
    ntp          IN     CNAME    phim0ns1.phim0.isyntax.net.
    repo         IN     CNAME    phim0vs01.phim0.isyntax.net.
    sc           IN     CNAME    phim0vs01.phim0.isyntax.net.
    sb           IN     CNAME    phim0sb01.phim0.isyntax.net.
    el           IN     CNAME    phim0ps02.phim0.isyntax.net.
    mongo        IN     CNAME    phim0ps01.phim0.isyntax.net.
    services     IN     CNAME    phim0vs01.phim0.isyntax.net.  ; IDM Portal web service
    ipam         IN     CNAME    phim0vs01.phim0.isyntax.net.
    EOF

Setup Bind service to be started ::

    chkconfig named on
    rndc-confgen -a -r /dev/urandom
    service named start

Edit host file to add entries ::

    cat >> /etc/hosts << "EOF"
    10.77.45.250            repo.phim.isyntax.net
    10.77.45.250            phirepo.phim.isyntax.net
    10.77.45.250            pypi.phim.isyntax.net
    EOF

Execute updates for this release and drop notes for 1.0.1.3.1013.88 ::

    yum remove -y phim-shinkencfg
    yum -y update
    reboot

------------

Source Control
--------------

The source control system operates using subversion style management. The SCM manager utilizes the CollabNet Subversion Edge server for Linux. The source file can be obtained from the third party file share. The current available being used is 4.0.2. This software requires a java runtime environment and is already installed with the Vigilant appliance.

1. Deploy a vigilant template
#. Execute initialization ::

    /usr/lib/philips/initialization.py -i 10.77.45.5 -n 255.255.255.0 -g 10.77.45.1 -d 10.77.45.5 -s phim0 -e 1.1.1.1 -t

#. Fix the following ::

    /etc/sysconfig/network
      HOSTNAME=phim0sc01.phim0.isyntax.net

    vi /etc/hosts
    Modify all the hostnames to match the hostname and fqdn
    Remove all the 1.1.1.1 entries from the file.

    Stop all vigilant services as they are not operating on this server
        chkconfig celerybeat off
        chkconfig celeryd off
        chkconfig elasticsearch off
        chkconfig httpd off
        chkconfig mongod off
        chkconfig mysqld off
        chkconfig nagios off
        chkconfig nginx off
        chkconfig postfix off
        chkconfig rabbitmq-server off
        chkconfig redis off
        chkconfig shinken-arbiter off
        chkconfig shinken-broker off
        chkconfig shinken-poller off
        chkconfig shinken-reactionner off
        chkconfig shinken-receiver off
        chkconfig shinken-scheduler off
        chkconfig squid off
        chkconfig supervisord off
        chkconfig thruk off
        chkconfig uwsgi off
        chkconfig webmin off

        reboot

Once the system has rebooted, follow these commands to install the CollabNet Subversion Edge server and services. ::

    useradd -d /home/sc -m --shell /bin/bash sc -g users && passwd sc
        1nf0M@t1cs
    cat >> /etc/sudoers.d/sc << "EOF"
    sc ALL=(ALL) ALL
    EOF

    su - sc
    sudo mkdir -v /scdata
    sudo chown -v sc:users /scdata

    cd /scdata
    tar -zxf /tmp/CollabNetSubversionEdge-4.0.2_linux-x86_64.tar.gz
    ln -s /scdata/csvn svn
    sed -i '$ a\export JAVA_HOME=/usr/java/latest' ~/.bash_profile
    source ~/.bash_profile
    cd svn
    sudo -E bin/csvn install
    bin/csvn start
    sudo -E bin/csvn-httpd install  (should be accomplished after the gui config for the port)
    sudo reboot

The system must now be configured from the web gui ::

    http://phim0sc01.phim0.isyntax.net:3343/csvn/

Change the admin password - 1nf...

| On the Server settings page:
| Hostname: phim0sc01.phim0.isyntax.net
| Apache Encryption: Disabled
| Port: 8080
| Repository Directory: /scdata/csvn/data/repositories
| Exit the wizard

| Under Maintenance -> Status
|    Click Start

| Make sure the Subversion status is showing as UP

| At the top of the screen, click Repositories
| Create
| Name: ibc
| Initialize: Template - Empty template

| On the left, click Repositories list
| Click Create
| Name: network_devices
| Initialize: Template - Empty template

| On the left, click Access Rule
| Click Edit

Enter the following information ::

    [groups]
    admins = jasonmalobicky, leonardoruiz
    field = phisvnuser
    autocommit = phiautocommit
    dl_PHI_SCS-NA_EII_NetOps = vinaygupta, timothychristian, noahshelton, syedraza, marcokemmeren
    dl_PHI_SDE = abdulaiyaz

    [/]
    @admins = rw

    [ibc:/]
    @admins = rw
    @field = r

    [network_devices:/]
    @admins = rw
    @autocommit = rw
    @dl_PHI_SCS-NA_EII_NetOps = r
    @dl_PHI_SDE = r

At the top of the screen, click Users
Click Create
Add the following list of users ::

    phisvnuser (st..or)
    phiautocommit (st..or)
    jasonmalobicky

Execute updates for this release and drop notes for 1.0.1.3.1013.88 ::

    yum remove -y phim-shinkencfg
    yum -y update
    reboot


------------

Windows 2012 R2 Hop Server
--------------------------

* Prerequisites:
  - Install .Net Framework 3.5

  Mount the Server installation media to the VM guest
  Launch Powershell ::

      Install-WindowsFeature NET-Framework-Core -Source D:\Sources\sxs

* Add VMDK with capacity 100GB
* TourtiseSVN 1.8.4.24972
* Java 1.6.0_22
* 7-Zip 4.65
* Notepad++ 6.3.2
* WinMerge 2.14.0
* WinSCP 5.54
* Google Chrome
* MongoView 1.6.9
* ElasticSearch Standalone (royrusso-elasticsearch-HQ-14ac197.zip)
  Needs to go on a web server (phim0app006.phim0.isyntax.net)

Add the VMDK as S:\

Create the following folders ::

    S:\Philips
    S:\Views

Right click S:\Views

TourtiseSVN -> Repo-browser

Enter the following URL ::

    http://sc.phim.isyntax.net/svn/ibc

Username:

Password:

Once connected, create the following structure ::

    common
    sites

Once these are completed, the working copy must be created

Right click S:\Views

SVN Checkout

Complete the form with the following information ::

    URL of Repository: http://sc.phim.isyntax.net/svn/ibc
    Checkout Directory: S:\Views\ibc

    Click OK

Setup for each site needs to follow REF4.09-189

------------

Linux Hop Server
----------------
Edit host file to add entries ::

    cat >> /etc/hosts << "EOF"
    10.77.45.250            repo.phim.isyntax.net
    10.77.45.250            phirepo.phim.isyntax.net
    10.77.45.250            pypi.phim.isyntax.net
    EOF

Execute updates for this release and drop notes for 1.0.1.3.1013.88 ::

    yum remove -y phim-shinkencfg
    yum -y update
    reboot

------------

Windows 2008 Remote Desktop Gateway Service
-------------------------------------------

| A 2008 system is used as 2012 requires the instance to be joined to a domain, where 2008 does not have this requirement. If the backoffice systems require a domain, then this might be reevaluated at a later date.
| Deploy a Win2k8r2-base OVA template
| name: phim0app007.phim0.isyntax.net

| Power On
| After IP Address has been assigned and the hostname changed
| Turn off the firewall for all the policies
| Setup the windows password to meet the other windows instances... 1n...cs

| Follow the Microsoft TechNet for installation of the service and roles needed
| Use the Local Administrators group
| Keep the policy names all the same.

| Continue down the page to export the certificate that is needed on the client.

| On the client system
| Double Click the phim0app007.cer
| Click Open
| Click Install Certificate
| Click Next
| Select 'Place all certificates in the following store'
| Click Browse
| Enable the 'Show Physical stores'
| Navigate to Trusted Root Certification Authorities
| Expand that location
| Highlight 'Local Computer'
| Click Next
| Click Finish

| On the Certificate Import Wizard screen, Click Ok
| Click Ok to close the Certificate information screen

| Modify the client host file to match the following entry ::

    63.147.62.9          phim0app007

| Configuration of the Remote Desktop Connection:
| Launch Remote desktop connection
| Click Show Options
| Click Advanced Tab
| Under the 'Connect from anywhere' heading, Click Settings
| In the Connection settings area, Select Use these RD Gateway server settings
| Server Name: phim0app007
| Logon Method: Ask for password (NTLM)

| Enable - Bypass RD Gateway server for local addresses
| Click Ok

| In the General tab
| The computer name must be a FQDN of the actual destination system ::

  Example: phim0app001.phim0.isyntax.net

| Click Connect.

| You will be prompted for credentials to access the RD Gateway system. These are NOT stentor or code1 credentials and must be in the form of DOMAIN\USER
| You will then be prompted again for the credentials of the destination system

| * TODO
| Configure the policies for the BUILTIN\Remote Desktop User group
| Add users to the local system and make them a member of the Remote Desktop group


------------

F5 VE Load Balancer
-------------------

* Configure SNMP
* Configure the proper DNS and NTP servers (10.77.45.5)
* Configure HA per REF4.09-131
* Be sure that the hostnames for the systems are in DNS prior to configuring them in the load balancer

Configure the following iRule Data Groups ::

    ltm data-group internal gateway_uri {
    records {
        /configuration { }
        /discovery { }
        /health { }
        /heartbeat { }
        /notification { }
        /perfdata { }
        /state { }
    }
    type string
    }
    ltm data-group internal ipam_uri {
        records {
            /ipam { }
        }
        type string
    }
    ltm data-group internal monitoring_uri {
        records {
            /pnp4nagios { }
            /thruk { }
        }
        type string
    }
    ltm data-group internal portal_uri {
        records {
            /idmapp { }
            /idmportalservices/api { }
            /monitoringtool { }
        }
        type string
    }
    ltm data-group internal pypi_uri {
        records {
            /packages { }
            /simple { }
        }
        type string
    }
    ltm data-group internal sourcecontrol_uri {
        records {
            /svn { }
            /viewvc { }
            /viewvc-static { }
        }
        type string
    }

Configure the following monitor ::

    ltm monitor http phim_gateway_health {
        defaults-from http
        destination *:*
        interval 120
        recv OK
        send "GET /health\\r\\n"
        time-until-up 0
        timeout 190
    }

Configure the following nodes ::

    ltm node phim0app001.phim0.isyntax.net {
        address 10.77.45.245
        description IDM Portal
    }
    ltm node phim0app005.phim0.isyntax.net {
        address 10.77.45.244
        description IPAM
    }
    ltm node phim0gw01.phim0.isyntax.net {
        address 10.77.45.21
    }
    ltm node phim0idm01.phim0.isyntax.net {
        address 10.77.45.71
    }
    ltm node phim0rp01.phim0.isyntax.net {
        address 10.77.45.11
    }
    ltm node phim0sc01.phim0.isyntax.net {
        address 10.77.45.61
    }

Configure the following Pools ::

    ltm pool Pool-Gateway-80 {
        members {
            phim0gw01.phim0.isyntax.net:http {
                address 10.77.45.21
                session monitor-enabled
            }
        }
        monitor phim_gateway_health
    }
    ltm pool Pool-Ipam-80 {
        members {
            phim0app005.phim0.isyntax.net:http {
                address 10.77.45.244
                session monitor-enabled
            }
        }
        monitor http
    }
    ltm pool Pool-Monitoring-443 {
        members {
            phim0idm01.phim0.isyntax.net:https {
                address 10.77.45.71
                session monitor-enabled
            }
        }
        monitor https
    }
    ltm pool Pool-Portal-80 {
        members {
            phim0app001.phim0.isyntax.net:http {
                address 10.77.45.245
                session monitor-enabled
            }
        }
        monitor http
    }
    ltm pool Pool-Pypi-8084 {
        members {
            phim0rp01.phim0.isyntax.net:8084 {
                address 10.77.45.11
                session monitor-enabled
            }
        }
        monitor http
    }
    ltm pool Pool-Repository-80 {
        members {
            phim0rp01.phim0.isyntax.net:http {
                address 10.77.45.11
                session monitor-enabled
            }
        }
        monitor http
    }
    ltm pool Pool-SCM-8080 {
        members {
            phim0sc01.phim0.isyntax.net:webcache {
                address 10.77.45.61
                session monitor-enabled
            }
        }
        monitor http
    }

Configure the following iRules ::

    ltm rule Philips_IDM {
        when HTTP_REQUEST {
        if {[matchclass [string tolower [HTTP::uri]] starts_with sourcecontrol_uri]}{
          log local0. "Using Pool-SourceControl-8080 - [HTTP::uri]"
              pool Pool-SCM-8080
        } elseif {[matchclass [string tolower [HTTP::uri]] starts_with gateway_uri]}{
          log local0. "Using Pool-Gateway-80 - [HTTP::uri]"
          pool Pool-Gateway-80
        } elseif {[matchclass [string tolower [HTTP::uri]] starts_with pypi_uri]}{
          log local0. "Using Pool-Pypi-8084 - [HTTP::uri]"
          pool Pool-Pypi-8084
        } elseif {[matchclass [string tolower [HTTP::uri]] starts_with portal_uri]}{
          log local0. "Using Pool-Portal-80 - [HTTP::uri]"
          pool Pool-Portal-80
        } elseif {[matchclass [string tolower [HTTP::uri]] starts_with ipam_uri]}{
          log local0. "Using Pool-Ipam-80 - [HTTP::uri]"
          pool Pool-Ipam-80
        } elseif {[matchclass [string tolower [HTTP::uri]] starts_with monitoring_uri]}{
          log local0. "Using Pool-Monitoring-443 - [HTTP::uri]"
          pool Pool-Monitoring-443
        } else {
          log local0. "Using Pool-Repository-80 - [HTTP::uri]"
          pool Pool-Repository-80
        }
      }
    }

Configure the following Virtual Servers ::

    ltm virtual VS-HTTP-80 {
        destination 10.77.45.250:http
        ip-protocol tcp
        mask 255.255.255.255
        profiles {
            http { }
            tcp-lan-optimized { }
        }
        rules {
            Philips_IDM
        }
        source 0.0.0.0/0
        source-address-translation {
            type automap
        }
        vlans {
            vlan231
        }
        vlans-enabled
    }
    ltm virtual VS-HTTPS-443 {
        destination 10.77.45.250:https
        ip-protocol tcp
        mask 255.255.255.255
        profiles {
            clientssl {
                context clientside
            }
            http { }
            serverssl-insecure-compatible {
                context serverside
            }
            tcp-lan-optimized { }
        }
        rules {
            Philips_IDM
        }
        source 0.0.0.0/0
        source-address-translation {
            type automap
        }
        vlans {
            vlan231
        }
        vlans-enabled
    }

----------------
IPAM Application
----------------



--------------
Backoffice IDM
--------------

---------------
Backoffice Repo
---------------
Follow deployment of standard repo.

Modify load balancer to only allow certain source addresses access to the repository

-----
Mongo
-----

1. Deploy a vigilant template
#. Execute initialization ::

    /usr/lib/philips/initialization.py -i 10.77.45.51 -n 255.255.255.0 -g 10.77.45.1 -d 10.77.45.5 -s phim0 -e 1.1.1.1 -t

#. Fix the following ::

    /etc/sysconfig/network
      HOSTNAME=phim0ps01.phim0.isyntax.net

    vi /etc/hosts
    Modify all the hostnames to match the hostname and fqdn
    Remove all the 1.1.1.1 entries from the file.

The following are the services that needs configured to operate a Mongo system ::

    chkconfig celerybeat off
    chkconfig celeryd off
    chkconfig elasticsearch off
    chkconfig httpd off
    chkconfig mongod on
    chkconfig mysqld off
    chkconfig nagios off
    chkconfig nginx off
    chkconfig postfix off
    chkconfig rabbitmq-server off
    chkconfig redis off
    chkconfig shinken-arbiter off
    chkconfig shinken-broker off
    chkconfig shinken-poller off
    chkconfig shinken-reactionner off
    chkconfig shinken-receiver off
    chkconfig shinken-scheduler off
    chkconfig squid off
    chkconfig supervisord off
    chkconfig thruk off
    chkconfig uwsgi off
    chkconfig webmin off

Once complete, issue a reboot of the node to cleanly bring the services to the correct state ::

    reboot

Execute updates for this release and drop notes for 1.0.1.3.1013.88 ::

    yum remove -y phim-shinkencfg
    yum -y update
    reboot

-------
Gateway
-------

1. Deploy a vigilant template
#. Execute initialization ::

    /usr/lib/philips/initialization.py -i 10.77.45.21 -n 255.255.255.0 -g 10.77.45.1 -d 10.77.45.5 -s phim0 -e 1.1.1.1 -t

#. Fix the following ::

    /etc/sysconfig/network
      HOSTNAME=phim0gw01.phim0.isyntax.net

    vi /etc/hosts
    Modify all the hostnames to match the hostname and fqdn
    Remove all the 1.1.1.1 entries from the file.

The following are the services that needs configured to operate a Gateway role ::

    chkconfig celerybeat off
    chkconfig celeryd off
    chkconfig elasticsearch off
    chkconfig httpd off
    chkconfig mongod off
    chkconfig mysqld off
    chkconfig nagios off
    chkconfig postfix off
    chkconfig rabbitmq-server off
    chkconfig redis off
    chkconfig shinken-arbiter off
    chkconfig shinken-broker off
    chkconfig shinken-poller off
    chkconfig shinken-reactionner off
    chkconfig shinken-receiver off
    chkconfig shinken-scheduler off
    chkconfig squid off
    chkconfig supervisord off
    chkconfig thruk off
    chkconfig uwsgi on
    chkconfig webmin off
    chkconfig nginx on

    reboot

Execute updates for this release and drop notes for 1.0.1.3.1013.88 ::

    yum remove -y phim-shinkencfg
    yum -y update
    reboot

-------------
ElasticSearch
-------------

1. Deploy a vigilant template
#. Execute initialization ::

    /usr/lib/philips/initialization.py -i 10.77.45.52 -n 255.255.255.0 -g 10.77.45.1 -d 10.77.45.5 -s phim0 -e 1.1.1.1 -t

#. Fix the following ::

    /etc/sysconfig/network
      HOSTNAME=phim0ps02.phim0.isyntax.net

    vi /etc/hosts
    Modify all the hostnames to match the hostname and fqdn
    Remove all the 1.1.1.1 entries from the file.

The following are the services that needs configured to operate a Gateway role ::

    chkconfig celerybeat off
    chkconfig celeryd off
    chkconfig elasticsearch on
    chkconfig httpd off
    chkconfig mongod off
    chkconfig mysqld off
    chkconfig nagios off
    chkconfig nginx off
    chkconfig postfix off
    chkconfig rabbitmq-server off
    chkconfig redis off
    chkconfig shinken-arbiter off
    chkconfig shinken-broker off
    chkconfig shinken-poller off
    chkconfig shinken-reactionner off
    chkconfig shinken-receiver off
    chkconfig shinken-scheduler off
    chkconfig squid off
    chkconfig supervisord off
    chkconfig thruk off
    chkconfig uwsgi off
    chkconfig webmin off

    reboot

Execute updates for this release and drop notes for 1.0.1.3.1013.88 ::

    yum remove -y phim-shinkencfg
    yum -y update
    reboot

A second node is needed as part of the cluster, follow the above sets for the second node
phim0ps03.phim0.isyntax.net / 10.77.45.53

-----------
Service Bus
-----------

1. Deploy a vigilant template
#. Execute initialization ::

    /usr/lib/philips/initialization.py -i 10.77.45.31 -n 255.255.255.0 -g 10.77.45.1 -d 10.77.45.5 -s phim0 -e 1.1.1.1 -t

#. Fix the following ::

    /etc/sysconfig/network
      HOSTNAME=phim0sb01.phim0.isyntax.net

    vi /etc/hosts
    Modify all the hostnames to match the hostname and fqdn
    Remove all the 1.1.1.1 entries from the file.

The following are the services that needs configured to operate a Gateway role ::

    chkconfig celerybeat off
    chkconfig celeryd off
    chkconfig elasticsearch off
    chkconfig httpd off
    chkconfig mongod off
    chkconfig mysqld off
    chkconfig nagios off
    chkconfig nginx off
    chkconfig postfix off
    chkconfig rabbitmq-server on
    chkconfig redis off
    chkconfig shinken-arbiter off
    chkconfig shinken-broker off
    chkconfig shinken-poller off
    chkconfig shinken-reactionner off
    chkconfig shinken-receiver off
    chkconfig shinken-scheduler off
    chkconfig squid off
    chkconfig supervisord off
    chkconfig thruk off
    chkconfig uwsgi off
    chkconfig webmin off

    reboot

Execute updates for this release and drop notes for 1.0.1.3.1013.88 ::

    yum remove -y phim-shinkencfg
    yum -y update
    reboot

------
Worker
------

1. Deploy a vigilant template
#. Execute initialization ::

    /usr/lib/philips/initialization.py -i 10.77.45.41 -n 255.255.255.0 -g 10.77.45.1 -d 10.77.45.5 -s phim0 -e 1.1.1.1 -t

#. Fix the following ::

    /etc/sysconfig/network
      HOSTNAME=phim0sb01.phim0.isyntax.net

    vi /etc/hosts
    Modify all the hostnames to match the hostname and fqdn
    Remove all the 1.1.1.1 entries from the file.

The following are the services that needs configured to operate a Gateway role ::

    chkconfig celerybeat off
    chkconfig celeryd on
    chkconfig elasticsearch off
    chkconfig httpd off
    chkconfig mongod off
    chkconfig mysqld off
    chkconfig nagios off
    chkconfig nginx off
    chkconfig postfix off
    chkconfig rabbitmq-server off
    chkconfig redis off
    chkconfig shinken-arbiter off
    chkconfig shinken-broker off
    chkconfig shinken-poller off
    chkconfig shinken-reactionner off
    chkconfig shinken-receiver off
    chkconfig shinken-scheduler off
    chkconfig squid off
    chkconfig supervisord on
    chkconfig thruk off
    chkconfig uwsgi off
    chkconfig webmin off

    reboot

Execute updates for this release and drop notes for 1.0.1.3.1013.88 ::

    yum remove -y phim-shinkencfg
    yum -y update
    reboot

------------------
Testing the system
------------------

Execute the following on the gateway role ::

    curl -XPOST 'http://localhost/notification' -d '{"siteid" : "fake01","timestamp" : "19701212","hostname" : "somehost","hostaddress" : "127.0.0.1","service" : "OS__some__event","type" : "PROBLEM","state" : "CRITICAL","output" : "nope","perfdata" : ""}' -H "Content-Type: application/json"



----------------------
Ansible Control System
----------------------

1. Deploy a vigilant template
#. Execute initialization ::

    /usr/lib/philips/initialization.py -i 10.77.45.241 -n 255.255.255.0 -g 10.77.45.1 -d 10.77.45.5 -s phim0 -e 1.1.1.1 -t

#. Fix the following ::

    /etc/sysconfig/network
      HOSTNAME=phim0app09.phim0.isyntax.net

    vi /etc/hosts
    Modify all the hostnames to match the hostname and fqdn
    Remove all the 1.1.1.1 entries from the file.

    Stop all vigilant services as they are not operating on this server
        chkconfig celerybeat off
        chkconfig celeryd off
        chkconfig elasticsearch off
        chkconfig httpd off
        chkconfig mongod off
        chkconfig mysqld off
        chkconfig nagios off
        chkconfig nginx off
        chkconfig postfix off
        chkconfig rabbitmq-server off
        chkconfig redis off
        chkconfig shinken-arbiter off
        chkconfig shinken-broker off
        chkconfig shinken-poller off
        chkconfig shinken-reactionner off
        chkconfig shinken-receiver off
        chkconfig shinken-scheduler off
        chkconfig squid off
        chkconfig supervisord off
        chkconfig thruk off
        chkconfig uwsgi off
        chkconfig webmin off

        reboot

Execute updates for this release and drop notes for 1.0.1.3.1013.88 ::

    yum remove -y phim-shinkencfg
    yum -y update
    reboot

Add the updated Repo to the host file ::

    vi /etc/hosts
    10.77.45.12             repo.phim.isyntax.net

Get the lastest Ansible pkg from the Repo ::

    yum clean all
    yum update ansible

Create the directories necessary to store all the ansible configurations, including inventory, playbooks and plays. ::

    mkdir -pv /etc/philips/ansible

Copy the Ansible Bootstrapping playbook and keys to a folder inside the /etc/philips/ansible location. This should also
contain the necessary ansible user private / public keypair. These files should be located in
the /etc/philips/ansible/bootstrap/

Modify the inventory file for all the known hosts that you need to target with the boostrapping service. ::

    [all]
    phim1cmg.phim1.isyntax.net ansible_ssh_host=phim1cmg.phim1.isyntax.net
    phim1nn01.phim1.isyntax.net ansible_ssh_host=phim1nn01.phim1.isyntax.net
    phim1dn01.phim1.isyntax.net ansible_ssh_host=phim1dn01.phim1.isyntax.net
    phim1dn02.phim1.isyntax.net ansible_ssh_host=phim1dn02.phim1.isyntax.net
    phim1dn03.phim1.isyntax.net ansible_ssh_host=phim1dn03.phim1.isyntax.net
    phim1dn04.phim1.isyntax.net ansible_ssh_host=phim1dn04.phim1.isyntax.net

------------
Hadoop Nodes
------------

Must have the associated DNS suffix in place, below will go through the additional zone file creations

Name Nodes will only have a single network interface on the VM - VLAN232

Cloud Manager will only have a single network interface on the VM - VLAN232

Data Nodes will have two network interfaces, both on VLAN232

Add another zone configuration to the main config for bind services located in /etc/named.conf ::

    options {
            ...
            allow-query     { 10.77.45.0/24; 10.77.46.0/24; localhost; };
            }

    zone "phim1.isyntax.net" IN {
            type master;
            file "/var/named/phim1.isyntax.net.zone";
            allow-update { none; };
            allow-query { any; };
    };

    zone "46.77.10.in-addr.arpa" IN {
            type master;
            file "/var/named/46.77.10.rev";
            notify no;
            allow-update { none; };
    };

    zone "phimb.isyntax.net" IN {
            type master;
            file "/var/named/phimb.isyntax.net.zone";
            allow-update { none; };
            allow-query { any; };
    };

    zone "20.20.20.in-addr.arpa" IN {
            type master;
            file "/var/named/20.20.20.rev";
            notify no;
            allow-update { none; };
    };

Create a forward and reverse zone files ::

    touch /var/named/phim1.isyntax.net.zone
    touch /var/named/46.77.10.rev
    touch /var/named/phimb.isyntax.net.zone
    touch /var/named/20.20.20.rev

 Add the following information for the forward zone file (phim1.isyntax.net.zone) ::

    ;
    ;  This is the forward zone for the phim1.isyntax.net internal domain
    ;
    $TTL    86400
    @       IN     SOA     phim0ns1.phim0.isyntax.net. root.phim0.isyntax.net. (
    100     ; serial
    1H      ; refresh
    1M      ; retry
    1W      ; expiry
    1D )    ; minimum
    @       IN     NS      phim0ns1.phim0.isyntax.net.
    @       IN     A       127.0.0.1
    ;
    ;  Below are the A records for the domain
    ;
    phim1mg1     IN     A     10.77.46.8
    phim1db1     IN     A     10.77.46.11
    phim1cmg     IN     A     10.77.46.41
    phim1nn01    IN     A     10.77.46.51
    phim1dn01    IN     A     10.77.46.101
    phim1dn02    IN     A     10.77.46.106
    phim1dn03    IN     A     10.77.46.111
    phim1dn04    IN     A     10.77.46.116

    ; Below are the CNAME Record Address (Aliases) - Point to an A Record Address

Add the following information for the reverse zone file (46.77.10.rev) ::

    ;
    ;  This is the reverse zone for the phim1.isyntax.net internal domain
    ;
    $TTL    86400
    @       IN     SOA     phim0ns1.phim0.isyntax.net. root.phim0.isyntax.net. (
    100     ; serial
    1H      ; refresh
    1M      ; retry
    1W      ; expiry
    1D )    ; minimum
    @       IN     NS      phim0ns1.phim0.isyntax.net.

    ; Below are the pointer records for the zone
    8          IN     PTR     phim1mg1.phim1.isyntax.net.
    11         IN     PTR     phim1db1.phim1.isyntax.net.
    41         IN     PTR     phim1cmg.phim1.isyntax.net.
    51         IN     PTR     phim1nn01.phim1.isyntax.net.
    101        IN     PTR     phim1dn01.phim1.isyntax.net.
    106        IN     PTR     phim1dn02.phim1.isyntax.net.
    111        IN     PTR     phim1dn03.phim1.isyntax.net.
    116        IN     PTR     phim1dn04.phim1.isyntax.net.

 Add the following information for the forward zone file (phimb.isyntax.net.zone) ::

    ;
    ;  This is the forward zone for the phimb.isyntax.net internal domain
    ;
    $TTL    86400
    @       IN     SOA     phim0ns1.phim0.isyntax.net. root.phim0.isyntax.net. (
    100     ; serial
    1H      ; refresh
    1M      ; retry
    1W      ; expiry
    1D )    ; minimum
    @       IN     NS      phim0ns1.phim0.isyntax.net.
    @       IN     A       127.0.0.1
    ;
    ;  Below are the A records for the domain
    ;
    phim1dn01    IN     A     20.20.20.101
    phim1dn02    IN     A     20.20.20.106
    phim1dn03    IN     A     20.20.20.111
    phim1dn04    IN     A     20.20.20.116

    ; Below are the CNAME Record Address (Aliases) - Point to an A Record Address

Add the following information for the reverse zone file (20.20.20.rev) ::

    ;
    ;  This is the reverse zone for the phimb.isyntax.net internal domain
    ;
    $TTL    86400
    @       IN     SOA     phim0ns1.phim0.isyntax.net. root.phim0.isyntax.net. (
    100     ; serial
    1H      ; refresh
    1M      ; retry
    1W      ; expiry
    1D )    ; minimum
    @       IN     NS      phim0ns1.phim0.isyntax.net.

    ; Below are the pointer records for the zone
    101        IN     PTR     phim1dn01.phimb.isyntax.net.
    106        IN     PTR     phim1dn02.phimb.isyntax.net.
    111        IN     PTR     phim1dn03.phimb.isyntax.net.
    116        IN     PTR     phim1dn04.phimb.isyntax.net.

Load the hadoop base image - hadoop-base-rev1451.ova

Configure the network and hostname information based on the below data structure as an example ::

    /etc/sysconfig/network-scripts/ifcfg-eth0
    DEVICE=eth0
    TYPE=Ethernet
    ONBOOT=yes
    NM_CONTROLLED=no
    BOOTPROTO=none
    IPADDR=10.77.46.101
    NETMASK=255.255.255.0
    GATEWAY=10.77.45.1

    /etc/sysconfig/network
    NETWORKING=yes
    HOSTNAME=phim1dn01.phim1.isyntax.net

    /etc/resolv.conf
    nameserver 10.77.45.5

    /etc/hosts
    127.0.0.1                localhost localhost.localdomain localhost4 localhost4.localdomain4
    ::1                      localhost localhost.localdomain localhost6 localhost6.localdomain6
    10.77.46.101             phim1dn01.phim1.isyntax.net phim1dn01

Additional network interfaces should be used for DataNode to DataNode communication within the phimb.isyntax.net
network space.

These interfaces be assigned to the sane network as the frontend 10.77.46.0/24 network. In the current setup this is
VLAN232. The additional interfaces should not have a GATEWAY tag inside the interface configurations. ::

    cp -v /etc/sysconfig/network-scripts/ifcfg-eth{0-1}
    sed -i '/GATEWAY=/d' /etc/sysconfig/network-scripts/ifcfg-eth1
    sed -i 's/10.77.46./20.20.20./' /etc/sysconfig/network-scripts/ifcfg-eth1
    sed -i 's/eth0/eth1/' /etc/sysconfig/network-scripts/ifcfg-eth1


Repeat the above changes for the phim1nn01 and phim1cmg. These systems will only have a single network interface with
the IP Address specified in the zone files.


Additional Notes

- The Cloudera Manager repository was not put into place when the base template was built. This needs to come from
  minimum version off the bulid server of 1.2.1.0.1777.0 - pcm5-1.0.1777-1.el6.x86_64.rpm
- This file needs to be placed in the repository that the systems are using - 10.77.45.12:/repo/phirepo/x86_64/

Once in place, the following command should be executed to build the repo ::

    cd /repo
    createrepo phirepo

From each node the repository file should be installed using the following command if not within the ansible playbook as
a root user ::

    yum clean all
    yum -y install pcm5

To install the cloud manager on phim1cmg.phim1.isyntax.net, this is usually via ssh from the ansible control system
(10.77.45.241 - phim0app009.phim0.isyntax.net) ::

    ssh ansible@phim1cmg.phim1.isyntax.net -i /etc/philips/ansible/bootstrap/ansibler_private

This should then log you in directly as the ansible user. All commands will need to be executed with 'sudo'

On the locahost line in /etc/hosts - remove the FQDN that relates to the local node - if this is there then the
installation will fail

http://www.cloudera.com/content/cloudera-content/cloudera-docs/CM5/latest/Cloudera-Manager-Installation-Guide/cm5ig_install_path_B.html

Follow this logic ::

    cat > /proc/sys/vm/swappiness << "EOF"
    0
    EOF

    sudo yum -y install oracle-j2sdk1.7
    sudo ln -s /usr/java/jdk1.7.0_55-cloudera /usr/java/latest
    sudo yum -y install cloudera-manager-server-db-2
    sudo service cloudera-scm-server-db start
    sudo service cloudera-scm-server start

Launch the admin console ::

    http://phim1cmg.phim1.isyntax.net:7180
    username: admin
    password: admin

 Choose the Free Edition
 Click Continue to the default application installs
 Add the hosts, we will start will only adding the local node to the system ::

    phim1cmg.phim1.isyntax.net

Click Search
| The hosts should all be discovered with all hosts being selected
| Click Continue

On the Cluster Installation page,
| Choose Method: Use Packages
| Select the version of CDH: Custom Repository ::

    http://repo.phim.isyntax.net/cdh5/x86_64/

| Select a specific release of the CDH you want to install on your hosts: Latest Release of CDH5
| Select the specific release of the Cloudera Manager Agent you want to install on your hosts: Custom Repository ::

    http://repo.phim.isyntax.net/cm5/x86_64/

Click Continued

Configure Java Encryption - Click Continue

Provide SSH login details
| Login To All Hosts As: Another User - ansible
| Authentication Method: All hosts use the same private key
| Upload the private key that is used by the ansible service user
| Leave all other settings to default

| Wait for the installation to successfully complete
| Review the output to see if there are any errors or warnings
| Click Finish

At this point, close the wizard as there are no further nodes to add to the system

Adding new nodes to the cluster is the same process for the single node above.

Add the Cluster Management Service
| Use the PostgreSQL embedded, this will create a new cluster for the management services


Add the new mount point for the larger partitions as ext4 - this will be sdc and only on the datanodes ::

    sudo mkdir -pv /dfs
    sudo blkid /dev/sdc1
    su - root
    echo "UUID={from above output} /dfs ext4 defaults 1 2" >> /etc/fstab
    exit
    sudo mount -av

| Once the cluster is created with the nodes, you then must add the services to the nodes
| Only necessary service is HDFS
| Primary Name Node: phim1nn01
| Secondary Name Node: phim1nn01
| Balancer Node: phim1nn01
| HttpFS Node: phim1nn01
| NFS Gateway: N/A
| Datanodes: phim1dn0[1-4]
| Click Continue

DataNode Data Directory: /dfs/dn
| Rest of values should be default
| Click Continue

| If all Services are successful, click Continue
| Click Finish when completed

Install Scala (phirepo/thirdparty) on the NameNode (phim1nn01) ::

    sudo yum -y install scala

Install Spark
- This should be in Phirepo 3rd party
- Download tgz (http://spark.apache.org/downloads.html)
- 1.1.0
- Pre-built for Hadoop 2.3

| Copy to all nodes (name and datanodes)
| Extract the file and configure the 2 files ::

    sudo tar -zxf spark-1.1.0-bin-hadoop2.3.tgz -C /opt/
    sudo cp -v /opt/spark-1.1.0-bin-hadoop2.3/conf/spark-env.sh{.template,}

    sudo bash -c 'cat >> /opt/spark-1.1.0-bin-hadoop2.3/conf/spark-env.sh << "EOF"
    export SPARK_MASTER_IP=phim1nn01.phim1.isyntax.net
    export SPARK_MASTER_WEBUI_PORT=18080
    export SPARK_WORKER_WEBUI_PORT=18081
    export SPARK_WORKER_MEMORY=42g
    export SPARK_EXECUTOR_MEMORY=40g
    EOF'

This should be only on the master node in the cluster (name node) ::

    sudo bash -c 'cat > /opt/spark-1.1.0-bin-hadoop2.3/conf/slaves << "EOF"
    phim1dn01.phim1.isyntax.net
    phim1dn02.phim1.isyntax.net
    phim1dn03.phim1.isyntax.net
    phim1dn04.phim1.isyntax.net
    EOF'

This requires the password-less ssh keys to start the slaves. ::

    /opt/spark../sbin/start-all.sh

Create init scripts are left.

Create a user on the Namenode and Datanode for spark as CDH install spark
- psys-spark


NameNode ::

    sudo yum install mysql mysql-server

App Server ::

    sudo yum install nginx

App server needs nginx configuration data ::

    mkdir -pv /var/www/philips (This was not where the data is today)
    chkconfig nginx on

    sudo bash -c 'cat > /etc/nginx/nginx.conf << "EOF"
    # For more information on configuration, see:
    #   * Official English Documentation: http://nginx.org/en/docs/
    #   * Official Russian Documentation: http://nginx.org/ru/docs/

    user              nginx;
    worker_processes  1;

    error_log  /var/log/nginx/error.log;
    #error_log  /var/log/nginx/error.log  notice;
    #error_log  /var/log/nginx/error.log  info;

    pid        /var/run/nginx.pid;


    events {
        worker_connections  1024;
    }

    http {
        include       /etc/nginx/mime.types;
        default_type  application/octet-stream;

        log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                          '$status $body_bytes_sent "$http_referer" '
                          '"$http_user_agent" "$http_x_forwarded_for"';

        access_log  /var/log/nginx/access.log  main;
        sendfile        on;

        keepalive_timeout  10;
        client_max_body_size  20m;
        gzip  on;

        server {
            listen 443;
            server_name analytics.isyntax.net;

            # serve static files
            location ~ ^/(images|javascript|js|css|flash|media|static)/  {
            root    /var/www/philips;
            expires 30d;
            }
            location ~ ^/api/ {
                proxy_set_header Host $host;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_pass https://63.147.62.11;
            }
        }
    }
    EOF'

    service nginx start
    
Setup of the certificate information ::

    mkdir -pv /etc/ssl/private
    chmod 0700 /etc/ssl/private
    mv -v analytics.isyntax.net.key /etc/ssl/private
    chmod -v 0700 /etc/ssl/private/analytics.isyntax.net.key
    mv -v c9a89aea3e15/{c9a89aea3e15,analytics.isyntax.net}.crt
    cat c9a89aea3e15/analytics.isyntax.net.crt c9a89aea3e15/gd_bundle-g2-g1.crt > c9a89aea3e15/analytics.isyntax.net.chained.crt
    mv -v c9a89aea3e15/analytics.isyntax.net.crt /etc/ssl/certs/
    mv -v c9a89aea3e15/gd_bundle-g2-g1.crt /etc/ssl/certs/
    mv -v c9a89aea3e15/analytics.isyntax.net.chained.crt /etc/ssl/certs/
    
Nginx configs ::

    cat > cat /etc/nginx/conf.d/default.conf << "EOF"
    #
    # The default server
    #
    server {
         listen 80;
         server_name analytics.isyntax.net;
         ## redirect http to https ##
         rewrite  ^ https://$server_name$request_uri? permanent;
    }

    server {
        listen 443;
        server_name analytics.isyntax.net;

        ssl on;
        ssl_certificate /etc/ssl/certs/analytics.isyntax.net.chained.crt;
        ssl_certificate_key /etc/ssl/private/analytics.isyntax.net.key;
        ssl_prefer_server_ciphers on;

        #charset koi8-r;

        #access_log  logs/host.access.log  main;

        location / {
            return 301 http://$server_name/demo;
        }

        location /holston {
            # root   /usr/share/nginx/html;
            # index  index.html index.htm;
            root /usr/share/AnalyticsApp;
            index index.html index.htm;
        }

        location /demo {
            root /usr/share/AnalyticsApp;
            index index.html index.htm;
        }

        location /api/query/ {
            proxy_set_header HOST $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_buffers 8 512k;
            proxy_buffer_size 2024k;
            proxy_busy_buffers_size 2024k;
            proxy_read_timeout 3000;
            #rewrite ^/api/query/(.*)$ /$1;
            proxy_pass http://63.147.62.11:8899/AnalyticsService/;
        }

        location /api/auth/ {
            proxy_buffers 8 512k;
            proxy_buffer_size 2024k;
            proxy_busy_buffers_size 2024k;
            proxy_read_timeout 3000;
            proxy_set_header HOST $host;
            proxy_set_header X-Real-IP $remote_addr;
            #rewrite ^/api/auth/(.*)$ /$1;
            proxy_pass http://63.147.62.11:8896/AuthenticationService/;
        }

        location /api/report/ {
            proxy_set_header HOST $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_buffers 8 512k;
            proxy_buffer_size 2024k;
            proxy_busy_buffers_size 2024k;
            proxy_read_timeout 3000;
            #rewrite ^/api/report/(.*)$ /$1;
            proxy_pass http://63.147.62.11:8899/ReportService/;
        }

        location /api/preference/ {
            proxy_set_header HOST $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_buffers 8 512k;
            proxy_buffer_size 2024k;
            proxy_busy_buffers_size 2024k;
            proxy_read_timeout 3000;
            #rewrite ^/api/preference/(.*)$ /$1;
            proxy_pass http://63.147.62.11:8898/Preferences/;
        }

        location /api/admin/ {
            proxy_set_header HOST $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_buffers 8 512k;
            proxy_buffer_size 2024k;
            proxy_busy_buffers_size 2024k;
            proxy_read_timeout 3000;
            #rewrite ^/api/admin/(.*)$ /$1;
            proxy_pass http://63.147.62.11:8899/AdminService/;
        }

        error_page  404              /404.html;
        location = /404.html {
            root   /usr/share/nginx/html;
        }

        # redirect server error pages to the static page /50x.html
        #
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   /usr/share/nginx/html;
        }

        # proxy the PHP scripts to Apache listening on 127.0.0.1:80
        #
        #location ~ \.php$ {
        #    proxy_pass   http://127.0.0.1;
        #}

        # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
        #
        #location ~ \.php$ {
        #    root           html;
        #    fastcgi_pass   127.0.0.1:9000;
        #    fastcgi_index  index.php;
        #    fastcgi_param  SCRIPT_FILENAME  /scripts$fastcgi_script_name;
        #    include        fastcgi_params;
        #}

        # deny access to .htaccess files, if Apache's document root
        # concurs with nginx's one
        #
        #location ~ /\.ht {
        #    deny  all;
        #}
    }
    EOF

Load Balancer configs ::

    ltm node phim1nn01.phim1.isyntax.net {
        address 10.77.46.51
    }
    ltm pool Pool-Analytics-8896 {
        members {
            phim1nn01.phim1.isyntax.net:8896 {
                address 10.77.46.51
                session monitor-enabled
                state up
            }
        }
        monitor tcp
    }
    ltm pool Pool-Analytics-8898 {
        members {
            phim1nn01.phim1.isyntax.net:8898 {
                address 10.77.46.51
                session monitor-enabled
                state up
            }
        }
        monitor tcp
    }
    ltm pool Pool-Analytics-8899 {
        members {
            phim1nn01.phim1.isyntax.net:8899 {
                address 10.77.46.51
                session monitor-enabled
                state up
            }
        }
        monitor tcp
    }
    ltm virtual VS-HTTP-8896 {
        destination 10.77.45.250:8896
        ip-protocol tcp
        mask 255.255.255.255
        pool Pool-Analytics-8896
        profiles {
            http { }
            tcp-lan-optimized { }
        }
        source 0.0.0.0/0
        source-address-translation {
            type automap
        }
        vlans {
            vlan231
        }
        vlans-enabled
    }
    ltm virtual VS-HTTP-8898 {
        destination 10.77.45.250:8898
        ip-protocol tcp
        mask 255.255.255.255
        pool Pool-Analytics-8898
        profiles {
            http { }
            tcp-lan-optimized { }
        }
        source 0.0.0.0/0
        source-address-translation {
            type automap
        }
        vlans {
            vlan231
        }
        vlans-enabled
    }
    ltm virtual VS-HTTP-8899 {
        destination 10.77.45.250:8899
        ip-protocol tcp
        mask 255.255.255.255
        pool Pool-Analytics-8899
        profiles {
            http { }
            tcp-lan-optimized { }
        }
        source 0.0.0.0/0
        source-address-translation {
            type automap
        }
        vlans {
            vlan231
        }
        vlans-enabled
    }

