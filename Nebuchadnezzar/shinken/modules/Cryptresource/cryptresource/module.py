# -*- coding: utf-8 -*-

# Shinken module based on shinken worker and dummy poller
#   - https://github.com/naparuba/shinken/blob/master/shinken/worker.py
#   - https://github.com/naparuba/shinken/blob/v2.0.3/modules/dummy_poller/module.py

#    Leonardo Ruiz, leonardo.ruiz@philips.com

# This Class is an example of an Poller module
# Here for the configuration phase AND running one

import sys
import signal
import time
import linecache
from Queue import Empty

from phimutils.resource import ResourceDecrypter, InvalidToken
from shinken.basemodule import BaseModule
from shinken.log import logger

properties = {
    'daemons': ['poller'],
    'type': 'cryptresource',
    'external': False,
    # To be a real worker module, you must set this
    'worker_capable': True,
}


# called by the plugin manager to get a broker
def get_instance(mod_conf):
    logger.info('[Crypt Poller] Get a Crypt poller module for plugin %s' % mod_conf.get_name())
    instance = Cryptresource_poller(mod_conf)
    return instance


class Cryptresource_poller(BaseModule):
    def __init__(self, mod_conf):
        BaseModule.__init__(self, mod_conf)
        self.secret_file = getattr(mod_conf, 'secret_file', '')
        self.secret = linecache.getline(self.secret_file, 1).rstrip()
        self.max_plugins_output_length = getattr(mod_conf, 'max_plugins_output_length', 65536)
        self.resource_decrypter = ResourceDecrypter(self.secret)

    # Called by poller to say 'let's prepare yourself guy'
    def init(self):
        logger.info('[Crypt Poller] Initialization of the Cryptresource poller module')
        self.i_am_dying = False

    def decrypt_resources(self, command):
        result = ''
        try:
            result = self.resource_decrypter.decrypt(command)
        except (AttributeError, ValueError):
            logger.error('[Crypt Poller] Cipher not available, check secret')
        except InvalidToken:
            logger.error('[Crypt Poller] Could not decrypt "%s"' % command)
        except TypeError:
            logger.error('[Crypt Poller] Could not decrypt Token must be bytes')
        return result

    # Get new checks if less than nb_checks_max
    # If no new checks got and no check in queue,
    # sleep for 1 sec
    # REF: doc/shinken-action-queues.png (3)
    def get_new_checks(self):
        try:
            while True:
                msg = self.s.get(block=False)
                if msg is not None:
                    self.checks.append(msg.get_data())
        except Empty, exp:
            if len(self.checks) == 0:
                time.sleep(1)

    # Launch checks that are in status
    # REF: doc/shinken-action-queues.png (4)
    def launch_new_checks(self):
        # queue
        for chk in self.checks:
            if chk.status == 'queue':
                chk.command = self.decrypt_resources(chk.command)
                r = chk.execute()
                # Maybe we got a true big problem in the
                # action launching
                if r == 'toomanyopenfiles':
                    # We should die as soon as we return all checks
                    logger.error('[Crypt Poller] I am dying Too many open files %s ... ' % chk)
                    self.i_am_dying = True

    # Check the status of checks
    # if done, return message finished :)
    # REF: doc/shinken-action-queues.png (5)
    def manage_finished_checks(self):
        to_del = []
        wait_time = 1
        now = time.time()
        for action in self.checks:
            if action.status == 'launched' and action.last_poll < now - action.wait_time:
                action.check_finished(self.max_plugins_output_length)
                wait_time = min(wait_time, action.wait_time)
                # If action done, we can launch a new one
            if action.status in ('done', 'timeout'):
                to_del.append(action)
                # We answer to the master
                # msg = Message(id=self.id, type='Result', data=action)
                try:
                    self.returns_queue.put(action)
                except IOError, exp:
                    logger.error('[Crypt Poller] exiting: %s' % exp)
                    sys.exit(2)

        # Little sleep
        self.wait_time = wait_time

        for chk in to_del:
            self.checks.remove(chk)

        # Little sleep
        time.sleep(wait_time)

    # Wrapper function for work in order to catch the exception
    # to see the real work, look at do_work
    def work(self, s, returns_queue, c):
        try:
            self.do_work(s, returns_queue, c)
        except Exception as err:
            logger.exception('[Crypt poller] Got an unhandled exception: %s' % err)
            # Ok I die now
            raise

    # id = id of the worker
    # s = Global Queue Master->Slave
    # m = Queue Slave->Master
    # return_queue = queue managed by manager
    # c = Control Queue for the worker
    def do_work(self, s, returns_queue, c):
        logger.info('[Crypt Poller] Module Crypt started!')
        ## restore default signal handler for the workers:
        signal.signal(signal.SIGTERM, signal.SIG_DFL)
        timeout = 1.0
        self.checks = []
        self.returns_queue = returns_queue
        self.s = s
        self.t_each_loop = time.time()
        while True:
            begin = time.time()
            msg = None
            cmsg = None

            # If we are dying (big problem!) we do not
            # take new jobs, we just finished the current one
            if not self.i_am_dying:
                # REF: doc/shinken-action-queues.png (3)
                self.get_new_checks()
                # REF: doc/shinken-action-queues.png (4)
                self.launch_new_checks()
            # REF: doc/shinken-action-queues.png (5)
            self.manage_finished_checks()

            # Now get order from master
            try:
                cmsg = c.get(block=False)
                if cmsg.get_type() == 'Die':
                    logger.info('[Crypt Poller] : Dad say we are dying...')
                    break
            except Exception:
                pass

            timeout -= time.time() - begin
            if timeout < 0:
                timeout = 1.0
