# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Basic Information
Name:      shinken-ws_passive_service_map-receiver
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   Shinken web service passive service check with mapping module

Group:     none
License:   GPL
URL:       http://www.philips.com/healthcare
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

# Source Information
Source0:   %{name}-%{version}.tar.gz
#Patch0:

Requires:  shinken-receiver
Provides:  shinken-ws_passive_service_map-receiver

%description
Shinken web service passive service check with mapping module
Allows submitting service check results via web services
Can map ip address to hostname
Can map service names using a yaml configuration file


%prep
%setup -q -n %{name}-%{version}


%build
echo OK


%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{_sharedstatedir}/shinken/modules
%{__cp} -r %{_builddir}/%{name}-%{version}/ws-psm %{buildroot}%{_sharedstatedir}/shinken/modules/
%{__install} -d -m 0755 %{buildroot}%{_sysconfdir}/philips/shinken
%{__install} -Dp -m 0644 %{_builddir}/%{name}-%{version}/servicemap.yml %{buildroot}%{_sysconfdir}/philips/shinken/


%pre
%{__mkdir} -p %{_sysconfdir}/philips/shinken/


%clean
%{__rm} -rf %{buildroot}


%files
%defattr(-,root,root,-)
%config %{_sysconfdir}/philips/shinken/servicemap.yml
%{_sharedstatedir}/shinken/modules/ws-psm


%changelog
* Fri Dec 04 2015 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File
