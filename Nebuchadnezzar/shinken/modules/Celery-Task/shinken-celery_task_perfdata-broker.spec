# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Basic Information
Name:      shinken-celery_task_perfdata-broker
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   Shinken celery tasks broker module

Group:     none
License:   GPL
URL:       http://www.philips.com/healthcare
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

# Source Information
Source0:   %{name}-%{version}.tar.gz
#Patch0:

Requires:  shinken-broker
Requires:  phim-celery-tasks-client
Provides:  shinken-celery_task_perfdata-broker

%description
Shinken celery tasks broker module
Allows sending a celery task by name for processing service perfdata


%prep
%setup -q

%build
echo OK


%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{_sharedstatedir}/shinken/modules
%{__cp} -r %{_builddir}/%{name}-%{version}/celery-task %{buildroot}%{_sharedstatedir}/shinken/modules/


%clean
%{__rm} -rf %{buildroot}


%files
%defattr(-,root,root,-)
%{_sharedstatedir}/shinken/modules/celery-task


%changelog
* Fri Dec 04 2015 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File
