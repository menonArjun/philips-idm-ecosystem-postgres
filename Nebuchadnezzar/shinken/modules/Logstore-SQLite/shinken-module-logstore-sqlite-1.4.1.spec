##TEMPORARILY UNTIL SHINKEN > 2.4 CAN BE USED WITH CONFIG ## After that remove the -1.4.1 and replace other file
# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Name of the module
%global module_name logstore-sqlite

# Basic Information
Name:      shinken-module-%{module_name}
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   Shinken Logstore-SQLite module

Group:     none
License:   GPL
URL:       http://www.philips.com/healthcare
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

# Source Information
Source0:   %{module_name}.tar.gz
#Patch0:

Provides:  shinken-module-%{module_name}

%description
Shinken Logstore-SQLite module


%prep
%setup -q -c

%build
echo OK


%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{_sharedstatedir}/shinken/modules/%{module_name}
%{__cp} -r %{_builddir}/%{name}-%{version}/module/* %{buildroot}%{_sharedstatedir}/shinken/modules/%{module_name}


%clean
%{__rm} -rf %{buildroot}


%files
%defattr(-,root,root,-)
%{_sharedstatedir}/shinken/modules/%{module_name}


%changelog
* Fri Dec 11 2015 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File
