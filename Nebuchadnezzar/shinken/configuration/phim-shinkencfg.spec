# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

%global phimconfig %{_sysconfdir}/philips


# Basic Information
Name:      phim-shinkencfg
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   Phim configuration for shinken

Group:     none
License:   GPL
URL:       http://www.philips.com/healthcare
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: i386 x86_64

# Source Information
Source0:   %{name}-%{version}.tar.gz
#Patch0:

Provides:  phim-shinkencfg

%description
Phim configuration for shinken

%prep
%setup -q -n %{name}-%{version}

%build
echo OK

%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{phimconfig}
%{__cp} -R %{_builddir}/%{name}-%{version}/shinken  %{buildroot}%{phimconfig}/shinken
%{__install} -d -m 0755 %{buildroot}%{_sysconfdir}/sysconfig/
%{__cp} %{_builddir}/%{name}-%{version}/etc/shinken-* %{buildroot}%{_sysconfdir}/sysconfig/


%clean
%{__rm} -rf %{buildroot}


%files
%defattr(-,root,root,-)
%config %{_sysconfdir}/sysconfig/shinken-*
%{phimconfig}/shinken


%changelog
* Wed Dec 09 2015 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File
