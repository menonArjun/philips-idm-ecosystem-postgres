from tchelp import load_config


## RabbitMQ EXCHANGES
ANALYTICS_CLINICAL_EXCHANGE = 'phim.analytics.clinicaldata.outbound'
ANALYTICS_DICOM_EXCHANGE = 'phim.analytics.dicomdata.outbound'

## RabbitMQ QUEUES
ANALYTICS_CLINICAL_QUEUE = 'phim.analytics.clinicaldata.outbound'
ANALYTICS_DICOM_QUEUE = 'phim.analytics.dicomdata.outbound'

EXCHANGE_QUEUE_MAP = {
    ANALYTICS_CLINICAL_EXCHANGE: [ANALYTICS_CLINICAL_QUEUE],
    ANALYTICS_DICOM_EXCHANGE: [ANALYTICS_DICOM_QUEUE],
}
QUEUE_TASK_MAP = {
    ANALYTICS_CLINICAL_QUEUE: 'phim_onsite.transport.transport_clinical_queue',
    ANALYTICS_DICOM_QUEUE: 'phim_onsite.transport.transport_dicom_queue',
}

GATEWAY_TASK_MAP = {'disk': {'bulk_move': 'phim_onsite.tasks.bulk_disk_move'},
                    'deployment': {'status': 'phim_onsite.operate.update_deployment_status'}
                    }

REPO_PATH = '/var/philips/repo'

DEFAULT = {
    'ENDPOINT': {
        'VERIFY': False,
    },
    'RABBITMQ': {'URL': 'amqp://'},
    'REDIS': {
        'URL': 'redis://localhost'
        # REDIS_URL: 'redis://username:psswrd@localhost:6379/0'
    },
    'SITEID_FILE': '/etc/siteid',
    'PERFDATA': {
        'QUEUE': 'perfdata_queue',
        'KILLSWITCH': '/tmp/killperfdata',
        'ENDPOINT': 'http://vigilant/perfdata'
    },
    'METRICSDATA': {
        'QUEUE': 'metricsdata_queue',
        'KILLSWITCH': '/tmp/killmetricsdata',
        'ENDPOINT': 'http://vigilant/metricsdata'
    },
    'STATE': {
        'KILLSWITCH': '/tmp/killstate',
        'ENDPOINT': 'http://vigilant/state',
        'REDIS_PREFIX': 'phistate#',
        'REDIS_SET': 'phistate_keys',
    },
    'NOTIFICATION_ENDPOINT': 'http://vigilant/notification',
    'DISCOVERY': {
        'ENDPOINT': 'http://vigilant/discovery',
        'KILLSWITCH': '/tmp/killispdiscovery',
        'CONFIGURATION': '/etc/philips/discovery.yml',
        'NAGIOS_RESOURCE_FILE': '/etc/philips/shinken/resource.d/resource.cfg',
        'SECRET_FILE': '/etc/philips/secret',
        'DEPLOYMENT': 'http://vigilant/deployment'
    },
    'VMWARE': {
        'DISCOVERY_CONF': '/etc/philips/vmware_discovery.conf',
        'NAGIOS_RESOURCE_FILE': '/etc/philips/shinken/resource.d/resource.cfg',
        'USER_RESOURCE': '$USER5$',
        'PASSWORD_RESOURCE': '$USER6$',
        'SECRET_FILE': '/etc/philips/secret'
    },
    'HEARTBEAT': {
        'ENDPOINT': 'http://vigilant/heartbeat',
        'KILLSWITCH': '/tmp/killhbmsg'
    },
    'PSMITH': {
        'XML_DIRS': ['/etc/philips/psmith/site', '/etc/philips/psmith/subscriptions'],
        'REPO_PATH': REPO_PATH,
        'URL': 'http://repo.phim.isyntax.net/philips',
        'XMLS': ['/etc/philips/psmith/common-repo.xml'],
        'TIMEOUT': 60 * 60 * 3
    },
    'SHINKEN_PASSIVE_RESULT_URL': 'https://localhost/check_result',
    'QUEUE_TRANSPORT': {
        'ENDPOINT': 'http://vigilant/queuetransport',
        'KILLSWITCH': '/tmp/killqueuetransport',
        'RETRIES': 3,
        'RETRY_DELAY': 10 * 60,
    },
    'MEROVINGIAN': {
        'PLAYBOOK': '/usr/lib/philips/Merovingian/ansible/pcm_boot.yml',
        #'INVENTORY': '/etc/philips/orchestrator/inventory',
        'INVENTORY': '/usr/lib/philips/Merovingian/dinventory.py',
        'TIMEOUT': 60 * 180,
    },
    'DISCOVERY_VERSION_KEYS': {
        'OS__CentOS__Linux__Information__Version': 'CentOS',
        'OS__Microsoft__Windows__Information__Version': 'Windows',
        'Product__IntelliSpace__WFL__Information__Version': 'IntelliSpace Workflow Layer',
        'Product__IntelliSpace__PACS__Information__Version': 'IntelliSpace PACS',
        'Product__IntelliSpace__AWS__Information__Version': 'IntelliSpace Advanced Workflow Solution',
        'Product__IntelliSpace__Anywhere__Information__Version': 'IntelliSpace Anywhere',
        'Product__IntelliSpace__VisibleLight__Information__Version': 'IntelliSpace Visible Light',
        'Product__IntelliSpace__ImageExchange__Information__Version': 'IntelliSpace Image Exchange_',
        'Product__IntelliSpace__Analytics__Information__Version': 'IntelliSpace Analytics',
        'Product__IntelliSpace__CCA__Information__Version': 'IntelliSpace Clinical Care Applications',
        'Product__IDM__Ecosystem__Information__Version': 'IDM Ecosystem'
    },
    'SHINKEN_STATE': {'UNKNOWN': 3, 'CRITICAL': 2, 'WARNING': 1, 'OK': 0},
    'PCM': {
        'STATE_KEY': 'Administrative__Philips__Host__Inforamtion__PCM__State',
        'REPO_PATH': REPO_PATH + '/Tools/PCM/packages',
        'HASH': 'DEPLOYMENT',
        'KEY': 'PACKAGE',
        'VERSION': 'VERSION',
        'SITE_INFO_XML': 'SITE_INFO_XML'
    },
    'DEPLOYMENT_STATUS':{
        'IN_PROGRESS':'In Progress',
        'SUCCESS':'Success'
    }
}

TBCONFIG_FILE = '/etc/philips/task/tbconfig.yml'


globals().update(load_config(DEFAULT, TBCONFIG_FILE))
