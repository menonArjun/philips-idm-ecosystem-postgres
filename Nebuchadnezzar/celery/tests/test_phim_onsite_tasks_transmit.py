import unittest
from mock import MagicMock, patch, call


# Parameterized decorator to mock celery.task
# It returns the function undecorated
def fakeorator_arg(*args, **kwargs):
    def fakeorator(func):
        return func
    return fakeorator


class POTasksTest(object):
    class TestCase(unittest.TestCase):
        def setUp(self):
            unittest.TestCase.setUp(self)
            self.mock_po = MagicMock(name='phim_onsite')
            self.mock_po.celery.app.task = fakeorator_arg
            self.mock_taskbase = MagicMock(name='taskbase')
            self.mock_tbconfig = MagicMock(name='tbconfig')
            self.mock_celery = MagicMock(name='celery')
            self.mock_requests = MagicMock(name='requests')
            self.mock_phimutils = MagicMock(name='phimutils')
            self.mock_scanline = MagicMock(name='scanline')
            modules = {
                'tbconfig': self.mock_tbconfig,
                'taskbase': self.mock_taskbase,
                'taskbase.redistask': self.mock_taskbase.redistask,
                'phim_onsite.celery': self.mock_po.celery,
                'celery': self.mock_celery,
                'celery.canvas': self.mock_celery.canvas,
                'celery.utils': self.mock_celery.utils,
                'celery.utils.log': self.mock_celery.utils.log,
                'requests': self.mock_requests,
                'phimutils': self.mock_phimutils,
                'phimutils.stateredis': self.mock_phimutils.stateredis,
                'phimutils.message': self.mock_phimutils.message,
                'phimutils.timestamp': self.mock_phimutils.timestamp,
                'phimutils.resource': self.mock_phimutils.resource,
                'scanline': self.mock_scanline,
                'scanline.trinity': self.mock_scanline.trinity,
            }
            self.module_patcher = patch.dict('sys.modules', modules)
            self.module_patcher.start()
            import phim_onsite.transmit
            self.transmit = phim_onsite.transmit

            self.transmit.PERFDATA = {'QUEUE': 'perfdata_queue'}

        def tearDown(self):
            unittest.TestCase.tearDown(self)
            self.module_patcher.stop()


class GetPerfblobTestCase(POTasksTest.TestCase):
    def setUp(self):
        POTasksTest.TestCase.setUp(self)

        self.msgs = []
        self.msgs.append(
            {
                'timestamp': '2013-10-16T09:15:54.353045Z',
                'hostname': 'host1',
                'hostaddress': '127.0.0.2',
                'service': 'Cat#Ven#Obj#Atr#Dim1',
                'perfdata': 'x=1m;1;2'
            }
        )
        self.msgs.append(
            {
                'timestamp': '2013-10-16T12:23:14.233540Z',
                'hostname': 'host2',
                'hostaddress': '127.0.0.3',
                'service': 'Cat#Ven#Obj#Atr#Dim2',
                'perfdata': 'y=31k;3;4'
            }
        )
        self.msgs.append(
            {
                'timestamp': '2013-11-16T02:44:61.468134Z',
                'hostname': 'host1',
                'hostaddress': '127.0.0.2',
                'service': 'Cat#Ven#Obj#Atr#Dim1',
                'perfdata': 'x=2m;1;2'
            }
        )

        self.conn = MagicMock(name='conn')
        self.queue = self.conn.SimpleQueue.return_value
        self.queue.__len__.return_value = 3

        self.transmit.msg2perfdict = MagicMock(name='msg2perfdict', side_effect=self.msgs)

    def test_get_perfblob_normal(self):
        self.assertEqual(
            self.transmit.get_perfblob(self.conn),
            [
                {
                    'timestamp': '2013-10-16T09:15:54.353045Z',
                    'hostname': 'host1',
                    'hostaddress': '127.0.0.2',
                    'service': 'Cat#Ven#Obj#Atr#Dim1',
                    'perfdata': 'x=1m;1;2'
                },
                {
                    'timestamp': '2013-10-16T12:23:14.233540Z',
                    'hostname': 'host2',
                    'hostaddress': '127.0.0.3',
                    'service': 'Cat#Ven#Obj#Atr#Dim2',
                    'perfdata': 'y=31k;3;4'
                },
                {
                    'timestamp': '2013-11-16T02:44:61.468134Z',
                    'hostname': 'host1',
                    'hostaddress': '127.0.0.2',
                    'service': 'Cat#Ven#Obj#Atr#Dim1',
                    'perfdata': 'x=2m;1;2'
                }
            ]
        )
        self.conn.SimpleQueue.assert_called_once_with('perfdata_queue')
        self.assertEqual(self.queue.get_nowait.call_count, 3)

    def test_get_perfblob_index_error(self):
        self.transmit.msg2perfdict.side_effect = IndexError
        self.assertRaises(IndexError, self.transmit.get_perfblob, self.conn)
        self.conn.SimpleQueue.assert_called_once_with('perfdata_queue')

    def test_get_perfblob_key_error(self):
        self.transmit.msg2perfdict.side_effect = KeyError
        self.assertEqual(self.transmit.get_perfblob(self.conn), [])
        self.conn.SimpleQueue.assert_called_once_with('perfdata_queue')
        self.assertEqual(self.queue.get_nowait.call_count, 3)

    def test_get_perfblob_queue_empty_before_len_0(self):
        self.queue.get_nowait.side_effect = self.transmit.Empty
        self.assertEqual(self.transmit.get_perfblob(self.conn), [])
        self.conn.SimpleQueue.assert_called_once_with('perfdata_queue')
        self.assertEqual(self.queue.get_nowait.call_count, 1)


class TxTrinityDiscoveryDataTestCase(POTasksTest.TestCase):

    def test_tx_trinity_discovery_data_monitor_flg_true(self):
        _resolvd_endpnt_cfg = {'scanner': 'TestScanner',
                               'address': '1.0.0.1', 'monitor': True}
        self.transmit.process_endpoint_config = MagicMock(
            name='process_endpoint_config', return_value=_resolvd_endpnt_cfg
        )
        _mk_scanner = MagicMock(name='scanline_scanner',
                                **{'site_facts.return_value': {'f1': "fact1"}})
        self.mock_scanline.trinity.scanline_scanner.return_value = _mk_scanner
        self.transmit.tx_discovery = MagicMock(name="tx_discovery")
        response = self.transmit.tx_trinity_discovery_data(_resolvd_endpnt_cfg)
        self.assertEqual(
            response, (0, 'Using TestScanner on 1.0.0.1 Succeeded'))
        _mk_scanner.site_facts.assert_called_once_with()
        _expected_call_tx_discovery = [call(endpoint={
            'monitor': True, 'scanner': 'TestScanner', 'address': '1.0.0.1'}, facts={'f1': 'fact1'})]
        self.assertEqual(
            self.transmit.tx_discovery.call_args_list, _expected_call_tx_discovery)
        _expected_call_process_end_config = [call(
            {'monitor': True, 'scanner': 'TestScanner', 'address': '1.0.0.1'})]
        self.assertEqual(self.transmit.process_endpoint_config.call_args_list,
                         _expected_call_process_end_config)

    def test_tx_trinity_discovery_data_monitor_flg_false(self):
        _resolvd_endpnt_cfg = {'scanner': 'TestScanner',
                               'address': '1.0.0.1', 'monitor': False}
        self.transmit.process_endpoint_config = MagicMock(
            name='process_endpoint_config', return_value=_resolvd_endpnt_cfg
        )
        _mk_scanner = MagicMock(name='scanline_scanner',
                                **{'site_facts.return_value': {'f1': "fact1"}})
        self.mock_scanline.trinity.scanline_scanner.return_value = _mk_scanner
        self.transmit.tx_discovery = MagicMock(name="tx_discovery")
        response = self.transmit.tx_trinity_discovery_data(_resolvd_endpnt_cfg)
        self.assertEqual(
            response, (0, 'Using TestScanner on 1.0.0.1 Succeeded'))
        _mk_scanner.site_facts.assert_called_once_with()
        _expected_call_process_end_config = [call(
            {'monitor': False, 'scanner': 'TestScanner', 'address': '1.0.0.1'})]
        self.assertEqual(self.transmit.process_endpoint_config.call_args_list,
                         _expected_call_process_end_config)
        _expected_call_tx_discovery = [call(endpoint={'monitor': False,
            'scanner': 'TestScanner', 'address': '1.0.0.1'}, facts={'f1': 'fact1'})]
        self.assertEqual(
            self.transmit.tx_discovery.call_args_list, _expected_call_tx_discovery)

    def test_tx_trinity_discovery_data_without_monitor_flg(self):
        _resolvd_endpnt_cfg = {'scanner': 'TestScanner',
                               'address': '1.0.0.1'}
        self.transmit.process_endpoint_config = MagicMock(
            name='process_endpoint_config', return_value=_resolvd_endpnt_cfg
        )
        _mk_scanner = MagicMock(name='scanline_scanner',
                                **{'site_facts.return_value': {'f1': "fact1"}})
        self.mock_scanline.trinity.scanline_scanner.return_value = _mk_scanner
        self.transmit.tx_discovery = MagicMock(name="tx_discovery")
        response = self.transmit.tx_trinity_discovery_data(_resolvd_endpnt_cfg)
        self.assertEqual(
            response, (0, 'Using TestScanner on 1.0.0.1 Succeeded'))
        _mk_scanner.site_facts.assert_called_once_with()
        _expected_call_process_end_config = [call(
            {'scanner': 'TestScanner', 'address': '1.0.0.1'})]
        self.assertEqual(self.transmit.process_endpoint_config.call_args_list,
                         _expected_call_process_end_config)
        _expected_call_tx_discovery = [call(endpoint={
            'scanner': 'TestScanner', 'address': '1.0.0.1'}, facts={'f1': 'fact1'})]
        self.assertEqual(
            self.transmit.tx_discovery.call_args_list, _expected_call_tx_discovery)


if __name__ == '__main__':
    unittest.main()
