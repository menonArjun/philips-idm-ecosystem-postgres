# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Basic Information
Name:      phim-celery-tasks-client
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   Phim celery tasks for client

Group:     none
License:   GPL
URL:       http://www.philips.com/healthcare
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

# Source Information
Source0:   %{name}-%{version}.tar.gz
#Patch0:

Requires:  phim-celery-tasks-taskbase
Requires:  phimutils
Requires:  psmith
Requires:  scanline
Requires:  phim-isp-stack-ops
Provides:  phim-celery-tasks-client
Conflicts: phim-celery-tasks-server

%description 
Phim celery tasks for client


%prep
%setup -q

%build
echo OK

# Using Multilib Exempt Location %{_prefix}/lib instead of %{_libdir}

%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{_prefix}/lib/celery
%{__install} -Dp -m 0755 %{_builddir}/%{name}-%{version}/celeryconfig.py %{buildroot}%{_prefix}/lib/celery/
%{__install} -Dp -m 0755 %{_builddir}/%{name}-%{version}/tbconfig.py %{buildroot}%{_prefix}/lib/celery/
%{__install} -d -m 0755 %{buildroot}%{_prefix}/lib/celery/phim_onsite
%{__cp} %{_builddir}/%{name}-%{version}/phim_onsite/*.py %{buildroot}%{_prefix}/lib/celery/phim_onsite/
%{__install} -d -m 0755 %{buildroot}%{_sysconfdir}/default
%{__cp} %{_builddir}/%{name}-%{version}/celeryd-default %{buildroot}%{_sysconfdir}/default/celeryd


%clean
%{__rm} -rf %{buildroot}


%files
%defattr(-,root,root,-)
%config %{_sysconfdir}/default/celeryd
%attr(0755,root,root) %{_prefix}/lib/celery/celeryconfig.py
%attr(0755,root,root) %{_prefix}/lib/celery/tbconfig.py
%attr(0755,root,root) %{_prefix}/lib/celery/phim_onsite

%changelog
* Tue Jul 01 2014 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Changed package file structure

* Mon Jun 30 2014 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Added celeryd default config

* Tue Apr 08 2014 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Added reposync as a periodic task

* Fri Oct 25 2013 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File
