from __future__ import absolute_import

# Consider adding -Ofair to the worker process as long running tasks like psmith and merovingian may cause problems
import linecache
import socket
import os
import sys
import ansible.playbook
from ansible import callbacks, utils, errors

from phim_onsite.shinken import passive_service
from phim_onsite.transmit import tx_data, tx_pcm_discovery, tx_deployment_status
from phimutils.stateredis import StateRedis

from phimutils.resource import NagiosResourcer
from taskbase.redistask import create_redis_task
from psmith import PSmith
from phim_isp_stack_ops.stack_ninja import StackNinja
from itertools import groupby
from operator import itemgetter

from celery.utils.log import get_logger
from celery.exceptions import SoftTimeLimitExceeded

from phim_onsite.celery import app

from tbconfig import (
    NOTIFICATION_ENDPOINT,
    GATEWAY_TASK_MAP,
    SHINKEN_STATE,
    REDIS,
    STATE,
    PSMITH,
    MEROVINGIAN,
    VMWARE,
    PCM,
    DEPLOYMENT_STATUS
)


logger = get_logger(__name__)


def get_sync_details(sync_results):
    values = ['listed_files', 'listed_dirs', 'removed_files', 'removed_dirs', 'errors']
    status_details = []
    perfdata_details = []
    for value in values:
        label = value.replace('_', ' ').title()
        status_details.append('{value} {label}'.format(label=label, value=sync_results[value]))
        perfdata_details.append("'{label}'={value}".format(label=label, value=sync_results[value]))
    return ', '.join(status_details), ' '.join(perfdata_details)


@app.task(ignore_result=True, time_limit=PSMITH['TIMEOUT'] + 1, soft_time_limit=PSMITH['TIMEOUT'])
def psmith(path=PSMITH['REPO_PATH'], url=PSMITH['URL'], xml_files=PSMITH['XMLS'], xml_dirs=PSMITH['XML_DIRS'],
           hostname=None, service=None):
    rs = PSmith(path, url, xml_files, xml_dirs)
    sync_results = rs.sync()
    if not sync_results:
        state = 1
        msg = 'WARNING - Psmith did not run, lock is probably in place or Package download is in progress for earlier SWD request, skipping this one'
    else:
        state = 0
        status_output, perfdata = get_sync_details(sync_results)
        msg = 'OK - Psmith results: {output} | {perfdata}'.format(output=status_output, perfdata=perfdata)
        # send information notification to backoffice
        tx_data.delay(NOTIFICATION_ENDPOINT, status_output, '',
                      hostname=hostname, service=service, type='INFORMATION',
                      state='OK', perfdata=perfdata)
        tx_pcm_discovery.delay()

    # submit the result passively
    passive_service.delay(hostname, service, state, msg)


def get_lock(process_name):
    lock_socket = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
    try:
        lock_socket.bind('\0' + process_name)
    except socket.error:
        lock_socket = None
    return lock_socket


def run_playbook(dinventory, playbook, rc):
    failed_hosts = []
    unreachable_hosts = []
    file_iter = (
        os.path.join(root, single_file) for root, dirs, files in os.walk('/var/philips/info') for single_file in files
    )
    xml_file_iter = (file_name for file_name in file_iter if os.path.splitext(file_name)[1] == '.xml')
    for pcm_xml in xml_file_iter:
        id = pcm_xml.split('_')[1].split('.')[0]
        rc.hset(PCM['HASH'],PCM['SITE_INFO_XML'],pcm_xml)
        logger.warning(rc.hget(PCM['HASH'],PCM['SITE_INFO_XML']))
        stats = callbacks.AggregateStats()
        playbook_cb = callbacks.PlaybookCallbacks(verbose=utils.VERBOSITY)
        runner_cb = callbacks.PlaybookRunnerCallbacks(stats, verbose=utils.VERBOSITY)
        inventory = ansible.inventory.Inventory(dinventory)
        extra_vars = {'pcm_packages': rc.hget(PCM['HASH'], PCM['KEY']),'version': rc.hget(PCM['HASH'], PCM['VERSION']), 'deployment_id': id,
                      'pcm_site_info_file': '/info/'+os.path.basename(pcm_xml)}
        pb = ansible.playbook.PlayBook(playbook=playbook, inventory=inventory, forks=1,
                                       callbacks=playbook_cb, runner_callbacks=runner_cb, stats=stats,
                                       extra_vars=extra_vars)
        lock = get_lock(playbook)
        if not lock:
            msg = 'Playbook could not run, lock in place.'
            logger.warning(msg)
            return SHINKEN_STATE['UNKNOWN'], 'UNKNOWN - %s' % msg
        try:
            tx_deployment_status(id, DEPLOYMENT_STATUS['IN_PROGRESS'])
            pb.run()
            hosts = sorted(pb.stats.processed.keys())
            logger.warning(" HOSTS IDENTIFIED %s ",hosts)
            for h in hosts:
                t = pb.stats.summarize(h)
                if t['failures'] > 0:
                    failed_hosts.append(h)
                if t['unreachable'] > 0:
                    unreachable_hosts.append(h)
            if len(failed_hosts) > 0:
                return SHINKEN_STATE['CRITICAL'], 'CRITICAL - Some hosts failed'
            if len(unreachable_hosts) > 0:
                return SHINKEN_STATE['UNKNOWN'], 'UNKNOWN - Some hosts were unreachable'
        except errors.AnsibleError, e:
            msg = 'Ansible Error: %s' % e
            logger.error(msg)
            return SHINKEN_STATE['UNKNOWN'], 'UNKNOWN - %s' % msg
        finally:
            if lock:
                lock.close()

    return SHINKEN_STATE['OK'], 'OK - Playbook completed'


@app.task(base=create_redis_task(REDIS['URL']), ignore_result=True,
          time_limit=MEROVINGIAN['TIMEOUT'] + 5, soft_time_limit=MEROVINGIAN['TIMEOUT'])
def merovingian(inventory=MEROVINGIAN['INVENTORY'], playbook=MEROVINGIAN['PLAYBOOK'], hostname=None, service=None):
    try:
        state, msg = run_playbook(inventory, playbook, merovingian.rc)
        state_redis = StateRedis(merovingian.rc, STATE['REDIS_PREFIX'], STATE['REDIS_SET'])
        pcm_state_data = state_redis.query(keys=[PCM['STATE_KEY']])

        state, msg = merovingian_perfdata(pcm_state_data, state, msg)

    except SoftTimeLimitExceeded:
        state = SHINKEN_STATE['CRITICAL']
        msg = 'Playbook run has timed out'

    passive_service.delay(hostname, service, state, msg)


def merovingian_perfdata(pcm_state_data, state, msg):
    # Reformat returned REDIS keys and filter out None items for localhost and other hosts that will never be called
    filtered_data = {}
    for host, host_data in pcm_state_data.iteritems():
        filtered_host_data = dict((k, v) for k, v in host_data.items() if v)
        if filtered_host_data:
            filtered_data[host] = filtered_host_data.get(PCM['STATE_KEY'])

    # Checked from highest to lowest so only the most critical overall finding will be shown from all hosts
    if filtered_data:
        values = filtered_data.values()
        if 'CRITICAL' in values:
            state = SHINKEN_STATE['CRITICAL']
        elif 'UNKNOWN' in values:
            state = SHINKEN_STATE['UNKNOWN']
        elif 'WARNING' in values:
            state = SHINKEN_STATE['WARNING']
        else:
            state = SHINKEN_STATE['OK']

        info = list(
            '{state_text} - {host}'.format(host=host, state_text=state) for host, state in filtered_data.iteritems()
        )
        perf = list('{host} = {state_code}'.format(host=host, state_code=SHINKEN_STATE[state]) for host, state in
                    filtered_data.iteritems())
        msg = '{info} | {perf}'.format(info=','.join(info), perf=';'.join(perf))
    return state, msg


@app.task(ignore_result=True)
def gateway(resource, action, payload):
    app.send_task(GATEWAY_TASK_MAP[resource][action], [], kwargs=payload)


def get_keys(in_dict, *keys):
    return dict((k, in_dict.get(k)) for k in keys)


@app.task(ignore_result=True)
def bulk_disk_move(rows):
    moves = [get_keys(row, 'source_vm', 'source_disk', 'destination_vm', 'destination_disk') for row in rows]
    destination_vm_getter = itemgetter('destination_vm')
    moves_sorted_by_destination_vm = sorted(moves, key=destination_vm_getter)
    for key, moves in groupby(moves_sorted_by_destination_vm, key=destination_vm_getter):
        disk_moves.delay(list(moves))


def stack_ninja_disk_moves(sn, moves):
    for move in moves:
        try:
            sn.move_disk(**move)
        except ValueError:
            logger.error('could not perform move with arguments %s', move)


@app.task(ignore_result=True)
def disk_moves(moves):
    secret = linecache.getline(VMWARE['SECRET_FILE'], 1).strip()
    nr = NagiosResourcer(VMWARE['NAGIOS_RESOURCE_FILE'], secret)
    vcenter_user, vcenter_pass = nr.get_resources(VMWARE['USER_RESOURCE'], VMWARE['PASSWORD_RESOURCE'])
    vcenter_address = linecache.getline(VMWARE['DISCOVERY_CONF'], 1).strip()
    sn = StackNinja(vcenter_address, vcenter_user, vcenter_pass)
    sn.connect()
    try:
        stack_ninja_disk_moves(sn, moves)
    finally:
        sn.disconnect()


@app.task(ignore_result=True)
def update_deployment_status(**results):
    path ='/var/philips/info'
    fls = ['{path}/pcm_manifest_{deploymentid}.json'.format(path=path,deploymentid=results.get('deployment_id')),
           '{path}/SiteInfo_{deploymentid}.xml'.format(path=path,deploymentid=results.get('deployment_id'))]
    if results.get('status') == DEPLOYMENT_STATUS['SUCCESS']:
        for fl in fls:
            try:
                logger.warning("Removing {fl} from path {path}  deletion failed,".format(fl=fl,path=path))
                os.remove(fl)
            except OSError:
                logger.warning("{fl} deletion failed,".format(fl=fl))
    tx_deployment_status(results.get('deployment_id'), results.get('status'),results.get('output',''))
