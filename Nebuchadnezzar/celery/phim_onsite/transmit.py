from __future__ import absolute_import

import linecache
import os
from Queue import Empty

import requests

from celery.canvas import chord
from celery.utils.log import get_logger
from phim_onsite.celery import app
from phim_onsite.shinken import passive_service
from phimutils import pcm
from phimutils.message import Message
from phimutils.stateredis import StateRedis
from phimutils.timestamp import iso_timestamp
from phimutils.resource import NagiosResourcer
from scanline.trinity import scanline_endpoints, scanline_scanner
from taskbase.redistask import create_redis_task
from tbconfig import SITEID_FILE, DISCOVERY_VERSION_KEYS, REDIS, PERFDATA, METRICSDATA, STATE, DISCOVERY, PCM, ENDPOINT


logger = get_logger(__name__)
requests.packages.urllib3.disable_warnings()


def get_perfblob(conn):
    """Get items form a queue and build a list of performance metrics dictionaries"""
    perfblob = []
    queue = conn.SimpleQueue(PERFDATA['QUEUE'])
    for i in range(len(queue)):
        try:
            m = queue.get_nowait()
        except Empty:
            break

        try:
            perfblob.append(msg2perfdict(m))
        except KeyError:
            pass
        finally:
            m.ack()
    return perfblob


def msg2perfdict(msg):
    """Map message payload to a performance metrics dictionary"""
    return {
        'timestamp': iso_timestamp(msg.payload['timestamp']),
        'hostname': msg.payload['hostname'],
        'service': msg.payload['service'],
        'perfdata': msg.payload['perfdata']
    }


def get_siteid(siteid='', siteid_file=SITEID_FILE):
    if not siteid:
        siteid = linecache.getline(siteid_file, 1).strip()
    return siteid


def get_metrics_json(data):
    data['timestamp'] = iso_timestamp(data.get('timestamp'))
    return data

@app.task(ignore_result=True)
def tx_data(url, data, killswitch=None, verify=ENDPOINT['VERIFY'], is_metricsdata=False, **kwargs):
    """Transmit data via http post"""
    if killswitch and os.path.exists(killswitch):
        logger.warning('killswitch %s found, aborting', killswitch)
        return
    siteid = get_siteid()
    if not siteid:
        raise Exception('Could not post due to missing siteid')
    if not data:
        logger.warning('no data to post')
        return
    if is_metricsdata:
        req = requests.post(url, json=get_metrics_json(data), verify=verify)
    else:
        message = Message(siteid=siteid, payload=data, **kwargs)
        req = requests.post(url, json=message.to_dict(), verify=verify)
    req.raise_for_status()

## Tasks that are to be triggered from the trigger nagios plugin need to support taking hostname and service kwargs,
## even if not used


@app.task(ignore_result=True)
def tx_perfdata(hostname=None, service=None):
    """Transmit the gathered performance data via http post"""
    with app.connection_or_acquire(connection=None) as conn:
        tx_data(PERFDATA['ENDPOINT'], get_perfblob(conn), PERFDATA['KILLSWITCH'])


@app.task(ignore_result=True)
def tx_metricsdata(payload=None):
    """Transmit the gathered performance data via http post"""
    with app.connection_or_acquire(connection=None) as conn:
        tx_data(METRICSDATA['ENDPOINT'], payload, METRICSDATA['KILLSWITCH'], is_metricsdata=True)


@app.task(base=create_redis_task(REDIS['URL']), ignore_result=True)
def tx_state(hostname=None, service=None):
    """Transmit gathered state information via http post"""
    state_redis = StateRedis(tx_state.rc, STATE['REDIS_PREFIX'], STATE['REDIS_SET'])
    tx_data(STATE['ENDPOINT'], state_redis.query(), killswitch=STATE['KILLSWITCH'])


@app.task(ignore_result=True)
def tx_discovery(hostname=None, service=None, facts=None, endpoint=None):
    """Transmit the gathered discovery data via http post"""
    output = {'facts': facts}
    if endpoint:
        output['endpoint'] = endpoint
    tx_data(DISCOVERY['ENDPOINT'], output, DISCOVERY['KILLSWITCH'])


@app.task(ignore_result=True)
def tx_trinity_discovery(hostname=None, service=None):
    header = [
        tx_trinity_discovery_data.s(endpoint_config)
        for endpoint_config
        in scanline_endpoints(DISCOVERY['CONFIGURATION'])
    ]
    callback = tx_trinity_discovery_status.subtask(kwargs={'hostname': hostname, 'service': service})
    chord(header)(callback)


def resolve_resource(resourcer, item):
    if isinstance(item, dict) and '$resource' in item:
        resource = item.get('$resource')
        result = resourcer.get_resource(resource)
        if result is None:
            logger.warning('resource "%s" could not be retrieved', resource)
        return result
    return item


def process_endpoint_config(endpoint_config):
    secret = linecache.getline(DISCOVERY['SECRET_FILE'], 1).strip()
    nr = NagiosResourcer(DISCOVERY['NAGIOS_RESOURCE_FILE'], secret)
    return dict((key, resolve_resource(nr, value)) for key, value in endpoint_config.iteritems())


@app.task(time_limit=3600)
def tx_trinity_discovery_data(endpoint_config):
    resolved_endpoint_config = process_endpoint_config(endpoint_config)
    scanner = scanline_scanner(resolved_endpoint_config)
    endpoint = dict(scanner=resolved_endpoint_config['scanner'], address=resolved_endpoint_config['address'])
    if 'monitor' in resolved_endpoint_config:
        endpoint['monitor'] = resolved_endpoint_config['monitor']
    rc, state = 2, 'Failed'
    if scanner is not None:
        try:
            facts = scanner.site_facts()
            if facts:
                tx_discovery(facts=facts, endpoint=endpoint)
                rc, state = 0, 'Succeeded'
        except Exception, ex:
            logger.error(ex)
    return rc, 'Using {scanner} on {address} {state}'.format(state=state, **endpoint)


@app.task(ignore_result=True)
def tx_trinity_discovery_status(task_result_list, hostname, service):
    logger.info(task_result_list)
    # [(2, 'Using ISP on 10.6.88.60'), (0, 'Using AdvancedWorkflowServices on 167.81.184.155')]

    states, messages = zip(*task_result_list)
    state = 0 if sum(states) == 0 else 2
    msg = ', '.join(messages)
    passive_service.delay(hostname, service, state, msg)


@app.task(base=create_redis_task(REDIS['URL']), ignore_result=True)
def tx_version_discovery(hostname=None, service=None):
    """Transmit gathered version information via http post"""

    state_redis = StateRedis(tx_version_discovery.rc, STATE['REDIS_PREFIX'], STATE['REDIS_SET'])
    version_data = state_redis.query(keys=DISCOVERY_VERSION_KEYS.keys(), key_map=DISCOVERY_VERSION_KEYS)

    # Filter assumes a format of {"localhost" : { "KeyOne" : "ValueOne", "KeyTwo" : None, "KeyThree" : None, "KeyFour" : None}},
    #                       {"host1" : { "KeyOne" : None, "KeyTwo" : "ValueTwo", "KeyThree" : None, "KeyFour" : None}}

    filtered_data = {}
    for host, host_data in version_data.iteritems():
        filtered_host_data = [dict(name=k, version=v) for k, v in host_data.items() if v]
        filtered_data[host] = {'Components': filtered_host_data}

    tx_discovery(facts=filtered_data)


@app.task(ignore_results=True)
def tx_pcm_discovery(hostname=None, service=None):
    """Transmit available pcm files and required databags to facts via http post"""

    package_list = []

    file_iter = (
        os.path.join(root, single_file) for root, dirs, files in os.walk(PCM['REPO_PATH']) for single_file in files
    )
    xml_file_iter = (file_name for file_name in file_iter if os.path.splitext(file_name)[1] == '.xml')
    for pcm_xml in xml_file_iter:
        package = pcm.PCMPackage(pcm_xml)
        package_list.append({
            'name' : package.Name + ''.join(package.Version.split('.')[:4]),
            'version' : package.Version.replace('.', ','),
            'dependencies': package.get_dependencies(),
            'databags': package.get_required_databags()
        })

    data = {'localhost': {'PCM': package_list}}
    tx_discovery(facts=data)


@app.task(ignore_results=True)
def tx_deployment_status(id, status, output=''):
    """Transmit available siteifno files and required databags to audit via http post"""
    data = {"deployment_id": id, 'status': status, 'output': output}
    tx_data(DISCOVERY['DEPLOYMENT'], data)
