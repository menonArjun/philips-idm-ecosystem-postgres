# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

%global phimuwsgi %{_prefix}/lib/philips/uwsgi/

# Basic Information
Name:      phim-tokenizer
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   phim-tokenizer setup

Group:     none
License:   GPL
URL:       http://www.philips.com/healthcare
# Packager Information
Packager:  Manoj Kumar MC<manoj.chandrankutty@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

# Source Information
Source0:   %{name}-%{version}.tar.gz
#Patch0:


Requires:  phimutils
Requires:  phim-celery-tasks-client
Requires:  phim-uwsgi
Provides:  phim-tokenizer
Conflicts: phim-gateway

%description
phim-tokenizer setup.


%prep
%setup -q

%build
echo OK

# Using Multilib Exempt Location %{_prefix}/lib instead of %{_libdir}

%install
%{__rm} -rf %{buildroot}
%{__install} -d -m 0755 %{buildroot}%{_sysconfdir}/uwsgi
%{__cp} %{_builddir}/%{name}-%{version}/uwsgi.tokenizer.ini %{buildroot}%{_sysconfdir}/uwsgi/tokenizer.ini
%{__install} -d -m 0755 %{buildroot}%{phimuwsgi}/tokenizer/
%{__install} -Dp -m 0755 %{_builddir}/%{name}-%{version}/tokenizer.py %{buildroot}%{phimuwsgi}/tokenizer/
%{__install} -Dp -m 0755 %{_builddir}/%{name}-%{version}/tkconfig.py %{buildroot}%{phimuwsgi}/tokenizer/

%clean
%{__rm} -rf %{buildroot}


%files
%defattr(-,root,root,-)
%{_sysconfdir}/uwsgi/tokenizer.ini
%{phimuwsgi}/tokenizer/


%changelog
* Mon Feb 06 2017 Manoj Kumar MC<manoj.chandrankutty@philips.com>
- Initial Spec File
