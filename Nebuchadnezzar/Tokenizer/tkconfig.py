CONFIGURATION_PATH = '/usr/lib/celery'
CONFIGURATION_OBJECT = 'celeryconfig'
METRICS_TASK_NAME = 'phim_onsite.transmit.tx_metricsdata'
SHINKEN_CFG_PATH = '/etc/philips/shinken/hosts/'
USER_KEY_URL = 'http://vigilant/pma/userkey/'
#USER_KEY_URL = 'http://161.85.111.160:8080/userkey/'
TOKEN_TIMEOUT = 300
KEY_SECRET = 'secret'
TOKEN_SECRET = 'secret1'

REDIS = {
    'HOST': 'localhost',
    'PORT': '6379',
    'STATE_PREFIX': 'phistate#',
    'STATE_SET': 'phistate_keys'
}

