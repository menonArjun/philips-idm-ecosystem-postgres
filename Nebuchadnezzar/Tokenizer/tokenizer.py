#!/usr/bin/env python
from bottle import Bottle, request, BaseRequest, HTTPResponse
import bottle_redis
import traceback

BaseRequest.MEMFILE_MAX = 1024 * 1024 * 2

import sys
import logging
import time
import json
import jwt
import requests
from celery import Celery
from os import listdir
from os.path import isfile, join
from functools import wraps
from phimutils.plogging import LOG_DATE_FORMAT, LOG_FORMAT

from tkconfig import (
    CONFIGURATION_OBJECT,
    CONFIGURATION_PATH,
    METRICS_TASK_NAME,
    REDIS,
    SHINKEN_CFG_PATH,
    USER_KEY_URL,
    TOKEN_TIMEOUT,
    KEY_SECRET,
    TOKEN_SECRET
)
sys.path.append(CONFIGURATION_PATH)
from phim_onsite.transmit import get_siteid

app = Bottle()
redis_plugin = bottle_redis.RedisPlugin(host=REDIS['HOST'], port=REDIS['PORT'])
app.install(redis_plugin)

celery = Celery()
celery.config_from_object(CONFIGURATION_OBJECT)

log = logging.getLogger('phim.tokenizer')
log.setLevel(logging.INFO)
logging.Formatter.converter = time.gmtime
formatter = logging.Formatter(fmt=LOG_FORMAT, datefmt=LOG_DATE_FORMAT)
handler_console = logging.StreamHandler()
handler_console.setFormatter(formatter)
log.addHandler(handler_console)


class TokenResponse(object):
    _msgs = {}
    _msgs['a_h_d_n_e'] = 'Authorization header does not exist.'
    _msgs['a_h_m_s_b'] = 'Authorization header must start with Bearer.'
    _msgs['a_h_m_b_t'] = 'Authorization header must be Bearer token.'
    _msgs['i_e_t'] = 'Invalid or expired token.'
    _msgs['t_n_f'] = 'Token not found.'
    _msgs['i_k'] = 'Invalid key.'
    _msgs['i_h_o_s'] = 'Invalid hostname or siteid.'

    def __init__(self, status_code=401, content_type='application/json', charset='UTF-8', desc_code='', ret_code='invalid_header'):
        self._status_code = status_code
        self._content_type = content_type
        self._charset = charset
        self._desc_code = desc_code
        self._json_str = {}
        self._ret_code = ret_code

    def ro(self):
        return HTTPResponse(status=self._status_code,
                            content_type=self._content_type,
                            charset=self._charset,
                            body=self.get_json())

    def get_json(self):
        self._json_str['ret_code'] = self._ret_code
        self._json_str['description'] = self.get_desc(self._desc_code)
        return json.dumps(self._json_str)

    @staticmethod
    def get_desc(desc_code):
        return TokenResponse._msgs[desc_code]


class TokenMgr(object):

    def __init__(self, rdb=None):
        self.rdb = rdb

    def decrypt_token(self, token):
        try:
            tk = jwt.decode(token, TOKEN_SECRET)
        except Exception as ex:
            raise ex
        jo = json.loads(json.dumps(tk))
        return jo.get('key')

    def decrypt_key(self, key):
        ky = jwt.decode(key, KEY_SECRET)
        jo = json.loads(json.dumps(ky))
        return (jo.get('hostname'),
                jo.get('siteid'))

    def get_key(self, hn, si):
        kv = get_cached_value(self.rdb, '{0}|{1}|key'.format(hn, si))
        if kv == None:
            json_str = '{"hostname": "%s", "siteid": "%s"}' % (hn, si)
            log.info(json_str)
            key = jwt.encode(json.loads(json_str), KEY_SECRET).decode('utf-8')
            self.rdb.set('{0}|{1}|key'.format(hn, si), key)
            return '{"key": "%s"}' % (key)
        else:
            return '{"key": "%s"}' % (str(kv))

    def get_token(self):
        key = request.json.get('key')
        json_str = {
            'key': key,
            'timestamp': time.time(),
            'exp': time.time() + TOKEN_TIMEOUT
        }
        log.info(json_str)
        token = jwt.encode(json_str, TOKEN_SECRET,
                           algorithm='HS256').decode('utf-8')
        ret_token = '{"token": "%s"}' % token
        log.info(token)
        return ret_token


class InvalidKeyException(Exception):
    pass


class Validator(object):

    host_map = dict((f.replace('.cfg',''),f) for f in listdir(SHINKEN_CFG_PATH)
                   if isfile(join(SHINKEN_CFG_PATH, f)))

    def __init__(self, rdb=None, T_Mgr=None):
        self.rdb = rdb
        self.T_Mgr = T_Mgr
        self.key = request.json.get('key')
        if self.key:
            try:
                self.hn, self.si = self.T_Mgr.decrypt_key(self.key)
            except:
                raise InvalidKeyException('i_k')
        else:
            self.hn, self.si = input_params()

    def validate_token(self):
        auth = request.headers.get('Authorization', None)
        if not auth:
            return (False, TokenResponse(desc_code='a_h_d_n_e').ro())
        parts = auth.split()
        if parts[0].lower() != 'bearer':
            return (False, TokenResponse(desc_code='a_h_m_s_b').ro())
        elif len(parts) == 1:
            return (False, TokenResponse(desc_code='t_n_f').ro())
        elif len(parts) > 2:
            return (False, TokenResponse(desc_code='a_h_m_b_t').ro())
        try:
            self.token = parts[1]
            self.key = self.T_Mgr.decrypt_token(self.token)
        except:
            return (False, TokenResponse(desc_code='i_e_t').ro())
        try:
            hostname, siteid = self.T_Mgr.decrypt_key(self.key)
        except:
            return (False, TokenResponse(desc_code='i_e_t').ro())
        return (True, (hostname, siteid))

    def validate_local_siteid(self, siteid):
        ret_val = True
        if siteid != get_siteid():
            log.error('Invalid siteid.')
            ret_val = False
        return ret_val

    def validate_hs(self):
        if not self.si:
            return (False, TokenResponse(desc_code='i_h_o_s', ret_code='invalid_creds').ro())
        if not self.validate_local_siteid(self.si):
            return (False, TokenResponse(desc_code='i_h_o_s', ret_code='invalid_creds').ro())
        if not self.hn:
            return (False, TokenResponse(desc_code='i_h_o_s', ret_code='invalid_creds').ro())
        if not Validator.host_map.get(str(self.hn)):
            return (False, TokenResponse(desc_code='i_h_o_s', ret_code='invalid_creds').ro())
        return (True, (self.hn, self.si))

    def key_in_cache(self):
        cache_query = '{0}|{1}|key'.format(self.hn, self.si)
        kv = get_cached_value(self.rdb, cache_query)
        log.info('%s not in cache' % cache_query)
        return (kv != None, kv)

    def api_in_backoffice(self):
        ret_val = False
        ret_json = json.loads(json.dumps(requests.get(USER_KEY_URL + self.si).json()))['result']
        for k,v in enumerate(ret_json):
            if str(v['siteid']) == self.si and str(v['user_cred']).starts_with(self.hn):
                self.rdb.set(v['user_cred'], v['key'])
                ret_val = True
        return ret_val

    def register_api(self):
        try:
            ks, kv = self.key_in_cache()
            if ks:
                return (True, kv)
            key = str(json.loads(self.T_Mgr.get_key(self.hn, self.si)).get('key'))
            payload = {
                    'hostname': self.hn,
                    'siteid': self.si,
                    'key': key
                    }
            headers = {
                    'content-type': 'application/json'
                    }
            ret_obj = requests.post(USER_KEY_URL, data=json.dumps(payload), headers=headers)
            if ret_obj.status_code != 200:
                return (False, '{"description": "Unable to register api in backoffice"}')
            cache_query = '{0}|{1}|key'.format(self.hn, self.si)
            self.rdb.set(cache_query, key)
        except:
            return (False, '{"description" : "Unexpected error while registering the api"}')
        return (True, key)


def input_params():
    return (request.json.get('hostname'),
            request.json.get('siteid'))


def get_cached_value(rdb, key):
    return rdb.get(key)


@app.get('/health')
def health():
    log.info('Health! %s', request.path)
    return '<p>OK</p>'


@app.post('/api/v1/metricsdata')
def metricsdata_action(rdb):
    if not request.json:
        log.debug(request.body.read())
        return '{"description": "Invalid json"}'
    try:
        s, o = Validator(rdb, T_Mgr=TokenMgr()).validate_token()
    except InvalidKeyException as ex:
        return TokenResponse(desc_code=str(ex), ret_code='invalid_key').ro()
    if not s:
        return o
    hostname, siteid = o
    attributes = {'path': request.path, 'payload': request.json}
    attributes['payload']['hostname'] = str(hostname)
    attributes['payload']['siteid'] = str(siteid)
    log.info('attributes : %s' % attributes)
    del attributes['path']
    log.debug(request.json)
    celery.send_task(METRICS_TASK_NAME, [], kwargs=attributes)
    log.debug('Sent to task: %s', METRICS_TASK_NAME)


@app.post('/api/v1/register')
def register(rdb):
    if not request.json:
        log.debug(request.body.read())
        return '{"description": "Invalid json"}'
    try:
        status, s_obj = Validator(rdb).validate_hs()
    except InvalidKeyException as ex:
        return TokenResponse(desc_code=str(ex), ret_code='invalid_key').ro()
    if not status:
        return s_obj
    status, s_obj = Validator(rdb, T_Mgr=TokenMgr(rdb)).register_api()
    return '{"key": "%s"}' % s_obj


@app.post('/api/v1/authenticate')
def get_token(rdb):
    if not request.json:
        log.debug(request.body.read())
        return '{"description": "Invalid json"}'
    try:
        status, s_obj = Validator(rdb, T_Mgr=TokenMgr()).validate_hs()
    except InvalidKeyException as ex:
        return TokenResponse(desc_code=str(ex), ret_code='invalid_key').ro()
    if not status:
        return s_obj
    return TokenMgr(rdb).get_token()


if __name__ == '__main__':
    log.info('MemFileMax set to %s', BaseRequest.MEMFILE_MAX)
    app.run(host='0.0.0.0', port=8080, debug=True)
