#! /bin/bash

export http_proxy=http://165.225.96.34:9480/
export https_proxy=http://165.225.96.34:9480/

printf "130.147.168.99 repo.phim.isyntax.net\n130.147.168.99 pypi.phim.isyntax.net\n">>/etc/hosts

yum -y install thruk

mkdir -p /var/{cache,lib,log}/thruk
chown uwsgi:uwsgi /var/{cache,lib,log}/thruk

chown -Rv uwsgi:uwsgi /var/lib/thruk

cd /etc/thruk/

mv thruk_local.conf thruk_local_old.conf

