#! /bin/bash

#export http_proxy=http://165.225.96.34:9480/
#export https_proxy=http://165.225.96.34:9480/

printf "130.147.168.99 repo.phim.isyntax.net\n130.147.168.99 pypi.phim.isyntax.net">>/etc/hosts

yum install -y shinken-poller

yum install -y shinken-module-cryptresource-poller
yum install -y fping nmap flex bison nagios-plugins nagios-plugins-all
/usr/bin/yum -y groupinstall nagiosphi --exclude=perl-XML-SAX-Base --exclude=perl-Scalar-List-Utils
