#! /bin/bash

export http_proxy=http://165.225.96.34:9480/
export https_proxy=http://165.225.96.34:9480/

printf "130.147.168.99 repo.phim.isyntax.net\n130.147.168.99 pypi.phim.isyntax.net\n">>/etc/hosts

yum remove -y php-common
yum install -y --disablerepo=* --enablerepo=phirepo php-common
yum install -y --disablerepo=* --enablerepo=phirepo php
yum install -y --disablerepo=* --enablerepo=phirepo php-fpm

mkdir -p /var/lib/php/session
chown apache:apache /var/lib/php/session
mv /etc/php-fpm.d/www.conf /etc/php-fpm.d/www_old.conf

