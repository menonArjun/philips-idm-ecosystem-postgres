#! /bin/bash

export http_proxy=http://165.225.96.34:9480/
export https_proxy=http://165.225.96.34:9480/

printf "130.147.168.99 repo.phim.isyntax.net\n130.147.168.99 pypi.phim.isyntax.net">>/etc/hosts

yum install -y celery-daemon
yum install -y phim-celery-tasks-taskbase phim-celery-tasks-client phimutils psmith scanline phim-isp-stack-ops

unset http_proxy https_proxy

pip install --upgrade pip
pip install celery
pip install pyyaml
pip install redis
pip install argparse
pip install billiard
pip install anyjson
pip install pytz
pip install cryptography --upgrade
pip install cached_property
pip install requests --upgrade
pip install pynagios --upgrade
pip install jsonpath_rw --upgrade
pip install uwsgi

