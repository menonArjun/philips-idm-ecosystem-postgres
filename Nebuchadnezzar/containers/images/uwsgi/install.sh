#! /bin/bash

printf "130.147.168.99 repo.phim.isyntax.net\n130.147.168.99 pypi.phim.isyntax.net\n">>/etc/hosts

export http_proxy=http://165.225.96.34:9480/
export https_proxy=http://165.225.96.34:9480/

cd /etc/yum.repos.d 
mv CentOS-Base.repo.old CentOS-Base.repo
mv CentOS-Debuginfo.repo.old CentOS-Debuginfo.repo
mv CentOS-Media.repo.old CentOS-Media.repo
mv CentOS-Vault.repo.old CentOS-Vault.repo
mv CentOS-fasttrack.repo.old CentOS-fasttrack.repo
yum install -y perl-ExtUtils-Embed
unset http_proxy https_proxy

mv CentOS-Base.repo CentOS-Base.repo.old
mv CentOS-Debuginfo.repo CentOS-Debuginfo.repo.old
mv CentOS-Media.repo CentOS-Media.repo.old
mv CentOS-Vault.repo CentOS-Vault.repo.old
mv CentOS-fasttrack.repo CentOS-fasttrack.repo.old

yum install -y perl-ExtUtils-MakeMaker perl-ExtUtils-ParseXS

pip install uwsgi

cd /tmp/uwsgi-2.0.14 
python uwsgiconfig.py --plugin plugins/psgi core

mkdir -p /usr/lib/uwsgi/plugins
mv psgi_plugin.so /usr/lib/uwsgi/plugins/
useradd uwsgi

