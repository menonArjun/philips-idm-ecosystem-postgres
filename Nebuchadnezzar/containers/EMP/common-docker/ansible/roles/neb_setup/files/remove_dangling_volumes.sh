if [[ $(docker volume ls -q -f dangling=true) ]]; then
    docker volume rm `docker volume ls -q -f dangling=true`
fi
