# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Basic Information
Name:      uwsgi-plugin-psgi
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   uWSGI - Plugin for Perl PSGI support

Group:     none
License:   GPL
URL:       http://projects.unbit.it/uwsgi
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: i386 x86_64

# Source Information
Source:    uwsgi-%{version}.tar.gz
#Patch0:

BuildRequires: perl-ExtUtils-Embed

%description
uWSGI - Plugin for Perl PSGI support


%prep
%setup -q -n uwsgi-%{version}


%build
/usr/bin/uwsgi --build-plugin plugins/psgi


# Using Multilib Exempt Location %{_prefix}/lib instead of %{_libdir}

%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{_prefix}/lib/uwsgi/plugins
%{__install} -Dp -m 0755 %{_builddir}/uwsgi-%{version}/psgi_plugin.so %{buildroot}%{_prefix}/lib/uwsgi/plugins/


%clean
rm -rf %{buildroot}


%files
%{_prefix}/lib/uwsgi/plugins/psgi_plugin.so


%changelog
* Tue Dec 29 2015 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File
