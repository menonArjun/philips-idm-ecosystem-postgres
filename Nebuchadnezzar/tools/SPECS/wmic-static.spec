# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

%define localbin %{_prefix}/local/bin

# Basic Information
Name:     wmic-static
Version:  %{_phiversion}
Release:  1%{?dist}
Summary:  Windows Managament Instrumentation Client for Linux by Zenoss.

Group:    none
License:  GPL
URL:      http://www.philips.com/healthcare
# Packager Information
Packager: Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: i386 x86_64

# Source Information
%ifarch x86_64
Source0:   wmic_%{version}_static_64bit.tar.gz
%else
Source0:   wmic_%{version}_static_i386.tar.gz
%endif
#Patch0:

Provides:  wmic

%description
Windows Managament Instrumentation Client for Linux by Zenoss.
Precompiled binaries.

%prep
%setup -c -q

%build
echo OK

%install
%{__rm} -rf %{buildroot}
%{__install} -d -m 0755 %{buildroot}%{localbin}
%{__install} -Dp -m 0755 %{_builddir}/%{name}-%{version}/winexe %{buildroot}%{localbin}/
%{__install} -Dp -m 0755 %{_builddir}/%{name}-%{version}/wmic %{buildroot}%{localbin}/

%clean
%{__rm} -rf %{buildroot}

%post
if [ $1 -eq 1 ] ; then
    # Initial installation
    #setup a link to command
    #no macro for ln
    ln -s %{localbin}/wmic /bin/wmic
fi

%preun
if [ $1 -eq 0 ] ; then
    # Package removal, not upgrade
    #no macro for unlink
    unlink /bin/wmic
fi

%files
%defattr(-,root,root,-)
%attr(0755,root,root) %{localbin}/winexe
%attr(0755,root,root) %{localbin}/wmic

%changelog
* Mon Jul 23 2012 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File
