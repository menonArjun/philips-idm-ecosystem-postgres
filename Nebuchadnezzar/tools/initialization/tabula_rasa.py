#!/usr/bin/env python
__author__ = 'Cornell Lawrence'
"""
DESCRIPTION
This script is for reseting the configuration files of an onsite IDM node.  This is useful for cloning a template.
The following steps need to be performed:
- Remove DNS configuration (/etc/resolv.conf)
- Remove entry for network card (/etc/udev/rules.d/70-persistent-net.rules)
- Reset /etc/sysconfig/network-scripts/ifcfg-eth0 to defaults
- Shutdown the system (shutdown -h now)
"""
# Import standard Python libraries
import os
import sys
# Import initialization and phimutils
import initialization
from phimutils import pcommand
# Globals
config_files = dict()
config_files['DNS'] = '/etc/resolv.conf'
config_files['Interface'] = '/etc/sysconfig/network-scripts/ifcfg-eth0'
config_files['Rules'] = '/etc/udev/rules.d/70-persistent-net.rules'
ROOT_HISTORY = '/root/.bash_history'
states = ('OK', 'WARNING', 'CRITICAL', 'UNKNOWN')


# Functions
def remove_config_files():
    for config_file in config_files:
        if os.path.exists(config_files[config_file]):
            os.remove(config_files[config_file])


def reset_network_interface():
    """
    Reset /etc/sysconfig/network-scripts/ifcfg-eth0 file to the following:
    DEVICE=eth0
    TYPE=Ethernet
    ONBOOT=no
    NM_CONTROLLED=no
    BOOTPROTO=dhcp
    """
    network_file = ['DEVICE=eth0',
                    'TYPE=Ethernet',
                    'ONBOOT=no',
                    'NM_CONTROLLED=no',
                    'BOOTPROTO=dhcp\n'
                    ]
    initialization.replace_file_content('\n'.join(network_file), config_files['Interface'])


def main():
    state = 0
    try:
        remove_config_files()
        reset_network_interface()
        #clear command history for root user
        if os.path.exists(ROOT_HISTORY):
            os.remove(ROOT_HISTORY)
            pcommand.execute_command('bash -c "history -c && history -w"')
        message = 'Tabula rasa complete.'
        #shutdown the system
        pcommand.execute_command('shutdown -h now')
    except OSError:
        message = 'Error accessing file.'
        state = 2
    except IOError:
        message = 'Error reading/writing file.'
        state = 2
    except Exception as e:
        message = 'Unexpected error %s' % e
        state = 3
    print '%s STATUS - %s ' % (message, states[state])
    sys.exit(state)


if __name__ == '__main__':
    main()