#!/usr/bin/env python
from bottle import Bottle, request, BaseRequest, response, abort, jinja2_view
import bottle_redis

BaseRequest.MEMFILE_MAX = 1024 * 1024 * 2

import sys
import logging
import time
import csv
import json
from celery import Celery
from phimutils.plogging import LOG_DATE_FORMAT, LOG_FORMAT
from phimutils.stateredis import StateRedis
from StringIO import StringIO
# couldn't get mimerender to work with redis plugin
# import mimerender

from spconfig import (
    CONFIGURATION_OBJECT,
    CONFIGURATION_PATH,
    TASK_NAME,
    REDIS,
    PCM,
    CANNED1,
    DISK_MOVE_CSV_FIELDS
)
sys.path.append(CONFIGURATION_PATH)

MIME_CSV='text/csv'

app = Bottle()
redis_plugin = bottle_redis.RedisPlugin(host=REDIS['HOST'], port=REDIS['PORT'])
app.install(redis_plugin)

celery = Celery()
celery.config_from_object(CONFIGURATION_OBJECT)

log = logging.getLogger('phim.saltpillar')
log.setLevel(logging.INFO)
logging.Formatter.converter = time.gmtime
formatter = logging.Formatter(fmt=LOG_FORMAT, datefmt=LOG_DATE_FORMAT)
handler_console = logging.StreamHandler()
handler_console.setFormatter(formatter)
log.addHandler(handler_console)


@app.post('/wk/<resource>/<action>')
def resource_action(resource, action):
    attributes = {'resource': resource, 'action': action, 'path': request.path}
    log.info('%(path)s: %(resource)s - %(action)s! ', attributes)
    if not request.json:
        log.error('no json')
        log.debug(request.body.read())
        return
    attributes['payload'] = request.json
    del attributes['path']
    log.debug(request.json)
    sp_task(**attributes)


def sp_task(resource, action, payload):
    attributes = {'resource': resource, 'action': action, 'payload': payload}
    celery.send_task(TASK_NAME, [], kwargs=attributes)
    log.debug('Sent to task: %s', TASK_NAME)


def csv_repr(data, fieldnames):
    output = StringIO()
    csvwriter = csv.DictWriter(output, delimiter=',', fieldnames=fieldnames)
    # Write headers writeheader() not available in python 2.6
    csvwriter.writerow(dict((fn,fn) for fn in fieldnames))
    for row in data.values():
        csvwriter.writerow(row)
    output.seek(0)
    return output.read()


def demultiplex_rows(data, multiplex_key):
    result = {}
    for host, row in data.iteritems():
        multiplex_field = row.get(multiplex_key)
        demuxed = json.loads(multiplex_field) if multiplex_field else None
        if not demuxed:
            new_row = row.copy()
            new_row[multiplex_key] = ''
            result[host] = new_row
            continue
        for item in demuxed:
            new_row = row.copy()
            new_row[multiplex_key] = item
            new_key = '{0}_{1}'.format(host, item)
            result[new_key] = new_row
    return result


def state_query(rdb, hosts, keys, key_map):
    state_redis = StateRedis(rdb, REDIS['STATE_PREFIX'], REDIS['STATE_SET'])
    result = {}
    result['data'] = state_redis.query(keys=keys, hosts=hosts, key_map=key_map)
    result['keys'] = state_redis.get_mapped_keys(keys, key_map) if (keys and key_map) else keys
    return result


def render_query_result(data, keys, accept):
    if MIME_CSV in accept:
        if not keys:
            abort(text='Keys must be passed when requesting CSV')
        if not data:
            abort(text='No data found.')
        response.content_type = MIME_CSV
        return csv_repr(data, keys)
    return data


@app.get('/state/query/vmobjcl')
@jinja2_view('simple_csv_textarea_submit')
def state_query_form_can1():
    return dict(title='Query hosts', name='hosts')


@app.post('/state/query/vmobjcl')
def state_query_vmobjcl(rdb):
    hosts = filter(None, map(str.strip, request.forms.get('hosts').splitlines()))
    result = state_query(rdb, hosts, CANNED1['KEYS'], CANNED1['MAP'])
    data = demultiplex_rows(result['data'], CANNED1['MULTIPLEX_KEY'])
    return render_query_result(accept=MIME_CSV, data=data, keys=result['keys'])


@app.post('/state/query')
def incoming_state_query(rdb):
    keys = request.json.get('keys')
    hosts = request.json.get('hosts')
    key_map = request.json.get('key_map')
    accept = request.headers.get('Accept')
    result = state_query(rdb, hosts, keys, key_map)
    return render_query_result(accept=accept, **result)


@app.get('/disk/move')
@jinja2_view('simple_csv_textarea_submit')
def disk_move_form():
    return dict(title='Move Disks', name='data', maxlength='10000', cols='250', rows='25')


@app.post('/disk/move')
def disk_move():
    csvreader = csv.DictReader(request.forms.get('data').splitlines(), DISK_MOVE_CSV_FIELDS)
    sp_task('disk', 'bulk_move', {'rows': list(csvreader)})
    return 'Disk move submitted'


@app.get('/health')
def health():
    log.info('Health! %s', request.path)
    return '<p>OK</p>'


@app.get('/pcm')
def merovingian_read(rdb):
    PCM_KEY = PCM['KEY']
    result = state_query(rdb, hosts=None, keys=[PCM_KEY], key_map=None)
    return dict((host, state[PCM_KEY]) for host, state in result['data'].iteritems() if state[PCM_KEY])


@app.get('/pcm/<hostname>/<deplyoment_id>')
def merovingian_read_hostname(rdb, hostname,deplyoment_id):
    PCM_KEY = PCM['KEY']
    result = state_query(rdb, hosts=[hostname], keys=[PCM_KEY], key_map=None)
    deplyoment_result = state_query(rdb, hosts=[hostname], keys=[PCM['DEPLOYMENT_KEY']], key_map=None)
    if hostname in result['data'] and hostname in deplyoment_result['data'] :
        if deplyoment_result['data'][hostname][PCM['DEPLOYMENT_KEY']] == deplyoment_id:
            return result['data'][hostname][PCM_KEY]
    return 'NOTFOUND'


@app.post('/pcm/<hostname>')
def merovingian_insert(rdb, hostname):
    if not request.json:
        return

    state_redis = StateRedis(rdb, REDIS['STATE_PREFIX'], REDIS['STATE_SET'])
    state_redis.insert(
        hostname=hostname,
        service=PCM['INSERT_KEY'],
        state=request.json.get('Status', 'UNKNOWN'),
        infos=request.json.get('Infos', {}),
        perfdata=request.json.get('perfdata', {})
    )


if __name__ == '__main__':
    #run(host='0.0.0.0', port=8080, debug=True, server='tornado')
    log.info('MemFileMax set to %s', BaseRequest.MEMFILE_MAX)
    #app.run()
    app.run(host='0.0.0.0', port=8080, debug=True)
