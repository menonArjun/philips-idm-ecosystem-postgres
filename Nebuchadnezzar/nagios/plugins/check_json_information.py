#!/usr/bin/env python

import argparse
import requests
import sys
from jsonpath_rw import parse
from phimutils.perfdata import INFO_MARKER


QUERY_HELP = 'JSONPath query to get the elements to check for information'
STATES = ('OK', 'WARNING', 'CRITICAL', 'UNKNOWN')


def check_arg(args=None):
    """Check the arguments passed"""
    parser = argparse.ArgumentParser(description='Nagios Plugin to grab information from JSON object given an URL')
    parser.add_argument('-u', '--url', help='url to check', required=True)
    parser.add_argument('-q', '--query', help=QUERY_HELP, required=True)
    parser.add_argument('-K', '--info_map', help='the keys to use for label and value separated by "="', required=True)
    parser.add_argument('-k', '--insecure', help='do not verify ssl', action='store_true')
    results = parser.parse_args(args)
    return results.url, results.query, results.info_map, results.insecure


def get_json(url, verify):
    req = requests.get(url, verify=verify)
    req.raise_for_status()
    return req.json()


def get_query(query):
    try:
        parsed_query = parse(query)
    except Exception:
        raise AttributeError('Could not parse query: "%s" ' % query)
    return parsed_query


def check_json_information(url, query, info_map, insecure):
    data = get_json(url, not insecure)
    parsed_query = get_query(query)
    jp_results = parsed_query.find(data)
    info_key, _, info_value = info_map.rpartition('=')

    def get_kv(entry):
        try:
            key_label = entry[info_key] if info_key else info_value
            result = '{label}={value}'.format(label=key_label, value=entry[info_value])
        except KeyError:
            result = ''
        return result

    kv_results = filter(None, [get_kv(x.value) for x in jp_results])
    return '{marker} {results}'.format(marker=INFO_MARKER, results='; '.join(kv_results))


def main():
    state = 0
    try:
        msg = check_json_information(*check_arg())
    except Exception, e:
        state = 3
        msg = e
    print('{state} - {msg}'.format(state=STATES[state], msg=msg))
    sys.exit(state)


if __name__ == '__main__':
    main()
