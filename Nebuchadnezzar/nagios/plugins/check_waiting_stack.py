#!/usr/bin/env python
# This script should always exit 0
# it may display problem information as output
# problems with this script are meant to be determined by the system expecting the post

import requests
import linecache
import zlib
from base64 import *
from lxml import objectify, etree
from scanline.utilities.db import _CursorMgr, QueryMgr
from phimutils.resource import NagiosResourcer

DISCOVERY = {
    'NAGIOS_RESOURCE_FILE': '/etc/philips/shinken/resource.d/resource.cfg',
    'SECRET_FILE': '/etc/philips/secret'
}

secret = linecache.getline(DISCOVERY['SECRET_FILE'], 1).strip()
nr = NagiosResourcer(DISCOVERY['NAGIOS_RESOURCE_FILE'], secret)

user = nr.get_resource("$USER135$")
password = nr.get_resource("$USER136$")
_cm = _CursorMgr(user=user, password=password, db='StentorConfiguration')

def CursorMgr():
    return _cm


class DBQueryMgr(QueryMgr):
    columns = ['itemValue']
    table_name = 'items'
    where_cdn = "itemname like '%StrategyConfiguration' and host like '%if1%'"

    def __init__(self):
        self.cursor = CursorMgr().connect()


def waiting_stack():
    waiting_stack_obj = DBQueryMgr()
    data = waiting_stack_obj.execute()
    if data:
        data = str(data[0])
    packet = b64decode(data)
    xml_stream = zlib.decompress(packet[4:])
    xml_stream = ''.join(xml_stream.split('\r\n'))
    xml_obj = objectify.fromstring(xml_stream)
    waiting_stack = sum(xml_obj.xpath('//IsStackUnused'))
    return waiting_stack

def get_waiting_stack():
    try:
        res = waiting_stack()
        if res > 1:
            status = 0
            outmsg = 'OK - {res} Waiting stack(s) are configured.'.format(res=res)
        elif res == 1:
            status = 1
            outmsg = 'WARNING - Single waiting stack available (warning threshold = 1)'
        else:
            status = 2
            outmsg = 'CRITICAL - No waiting stacks found configured/available.'
    except Exception:
        outmsg = 'Could not connect to UDM Database.'
        status = 3
    return status, outmsg

def main():
    state, msg = get_waiting_stack()
    print(msg)
    exit(state)


if __name__ == '__main__':
    main()