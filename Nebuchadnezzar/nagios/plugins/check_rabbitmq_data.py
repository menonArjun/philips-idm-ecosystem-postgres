#!/usr/bin/env python


###  check_rabbitmq_data.py

# Plugin gives the status of:
# 01. Total objects on channels.
  # It returns 0, if objects are less than warning param.
  # It returns 1, if objects are more than warning but less than
  # critical param.
  # It returns 2, if objects are more than critical param.
# 02. Memory watermark status.
  # It returns 0, if watermark is less than 0.4 or 40%.
  # It returns 2, if watermark is more than 0.4 or 40%.
# 03. Network partition status.
  # It returns 0, if no partition found.
  # It returns 2, if partition found.
# Use the management API to check if mem_alarm or disk_free_alarm
# has been triggered.
# Use the management API to check if partitions error conditions
# exist.
# Use the management API to check channel objects.

# Originally written by Chandan Kumar.
# Nov 14 2017


import argparse
import sys
import json
import requests


def check_args(args=None):
    parser = argparse.ArgumentParser(
        description='Script get status of IIS Application Pool')
    parser.add_argument('-H', '--hostname', required=True,
                        help='The host address of the rabbitmq server')
    parser.add_argument('-P', '--port', required=True,
                        help='The port of the rabbitmq server')
    parser.add_argument('-u', '--username', required=True,
                        help='The username of the rabbitmq server')
    parser.add_argument('-p', '--password', required=True,
                        help='The password of the rabbitmq server')
    parser.add_argument('-r', '--protocol', default='https',
                        help='The password of the iis application server')
    parser.add_argument('-s', '--subpath', required=True,
                        help='The subpath for the rabbitmq server')
    parser.add_argument('-S', '--service',
                        help='Service name of the rabbitmq server')
    parser.add_argument('-w', '--channel_obj_warn', default=500,
                        help='The rabbitmq server timeout.')
    parser.add_argument('-c', '--channel_obj_critical', default=1000,
                        help='The rabbitmq server timeout.')
    parser.add_argument('-t', '--timeout', default=15,
                        help='The rabbitmq server timeout.')
    results = parser.parse_args(args)

    return (
        results.hostname,
        results.port,
        results.username,
        results.password,
        results.protocol,
        results.subpath,
        results.service,
        results.channel_obj_warn,
        results.channel_obj_critical,
        results.timeout)


def get_service_status(hostname, port, username, password, protocol, subpath,
                       service, channel_obj_warn, channel_obj_critical,
                       timeout):
    url = '{0}://{1}:{2}/api/{3}'.format(protocol, hostname, port, subpath)
    headers = {'content-type': 'application/json', }
    auth = (username, password)
    try:
        response = requests.get(url, headers=headers, auth=auth,
                                timeout=timeout, verify=False)
        config_json = json.loads(response.content)
        if subpath == 'channels':
            return get_channel_obj_status(config_json,
                                          channel_obj_warn,
                                          channel_obj_critical)
        elif subpath == 'nodes' and service == 'watermark':
            return get_watermark_status(config_json)
        elif subpath == 'nodes' and service == 'partitions':
            return get_partitions_status(config_json)
    except Exception:
        return 2, 'Could not connect to RabbitMQ server.'


def get_watermark_status(config_json):
    mem_alarm = config_json[0]['mem_alarm']
    disk_alarm = config_json[0]['disk_free_alarm']
    if mem_alarm and not disk_alarm:
        return 2, 'Watermark is critical, memory alarm is {0}.'.format(mem_alarm)
    elif not mem_alarm and disk_alarm:
        return 2, 'Watermark is critical, disk alarm is {0}.'.format(disk_alarm)
    elif mem_alarm and disk_alarm:
        return 2, 'Watermark is critical, disk alarm and memory alarm are True.'
    else:
        return 0, 'Watermark is OK, disk alarm and memory alarm are False.'


def get_channel_obj_status(config_json, channel_obj_warn, channel_obj_critical):
    warn_condition1 = len(config_json) >= int(channel_obj_warn)
    warn_condition2 = len(config_json) < int(channel_obj_critical)
    if len(config_json) < int(channel_obj_warn):
        return 0, 'Gathered Object Counts: channel={0}'.format(len(config_json))
    elif warn_condition1 and warn_condition2:
        return 1, 'Gathered Object Counts: channel={0}'.format(len(config_json))
    elif len(config_json) >= int(channel_obj_critical):
        return 2, 'Gathered Object Counts: channel={0}'.format(len(config_json))


def get_partitions_status(config_json):
    partitions = config_json[0]['partitions']
    if not partitions:
        return 0, 'No network partitions.'
    else:
        return 2, 'Network partitions detected: {0}'.format(partitions[0])


def main():
    state, msg = get_service_status(*check_args())
    print(msg)
    sys.exit(state)


if __name__ == '__main__':
    main()
