#!/usr/bin/env python
import sys
import re
import argparse
import libxml2
import linecache

from phimutils.heartbeat import send_message, create_msg
from phimutils.timestamp import custom_timestamp



# Nagios macros as defined from documentation ->
# NOTIFICATIONTYPE: A string identifying the type of notification that is being sent ("PROBLEM", "RECOVERY", "ACKNOWLEDGEMENT", "FLAPPINGSTART", "FLAPPINGSTOP", "FLAPPINGDISABLED", "DOWNTIMESTART", "DOWNTIMEEND", or "DOWNTIMECANCELLED").
# LONGDATETIME:	Current date/time stamp (i.e. Fri Oct 13 00:30:28 CDT 2000). Format of date is determined by date_format directive.
# TIMET: Current time stamp in time_t format (seconds since the UNIX epoch).
# SERVICESTATE:	A string indicating the current state of the service ("OK", "WARNING", "UNKNOWN", or "CRITICAL").

def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Script to send a HB message depending of service description')
    parser.add_argument('-H', '--hostaddress',  
                        required=True, help='The address of the server originating the message')
    parser.add_argument('-a', '--hostname', 
                        required=True, help='The hostname of the server originating the message')
    parser.add_argument('-d', '--description', 
                        required=True, help='The description of the object for the message')
    parser.add_argument('-t', '--timestamp', 
                        help='The time for the message, expected in UNIX epoch time')
    parser.add_argument('-s', '--output', 
                        required=True, help='The object output for the message')
    parser.add_argument('--notificationtype', 
                        help='The notification type for the message')
    parser.add_argument('--state', 
                        help='The state for the message')
    parser.add_argument('--smtpto', 
                        help='The "to" address to use for the message',
                        default='hbeat@stentor.com')
    parser.add_argument('--smtpfrom', 
                        help='The "from" address to use for the message',
                        default='isyntaxserver@stentor.com')
    parser.add_argument('--smtpserver', 
                        help='The SMTP server',
                        default='localhost')
    parser.add_argument('--services_xml', 
                        help='The xml file containing the service mappings',
                        default='/usr/lib/nagios/plugins/nagios-hb-services.xml')
    group = parser.add_mutually_exclusive_group()
    group.add_argument('--siteid_file', help='A file containing the site id', default='/etc/siteid')
    group.add_argument('--siteid', help='The site id')
    results = parser.parse_args(args)
    
    # if no serviceoutput (empty string), then use description
    if not results.output: 
        results.output = results.description
    
    return (results.hostname, 
            results.hostaddress, 
            results.description, 
            results.notificationtype, 
            results.state, 
            results.timestamp, 
            results.output,
            results.smtpto,
            results.smtpfrom,
            results.smtpserver,
            results.services_xml,
            results.siteid,
            results.siteid_file)


def find_errcode(description, doc):
    # cleanup cluster number, xml will just have "Cluster status" entry
    if re.search(r'Cluster\d* status', description): description = "Cluster status"
    
    found_errcode = doc.xpathEval("//service[description='%s']/errorcode" % description)
    found_errtype = doc.xpathEval("//service[description='%s']/errortype" % description)
   
    return found_errcode[0].content, found_errtype[0].content

    
def hb_nagios(
        hostname, 
        hostaddress, 
        description, 
        notificationtype, 
        state, 
        timestamp, 
        output, 
        smtpto, 
        smtpfrom,
        smtpserver,
        services_xml,
        siteid,
        siteid_file):
    
    if not siteid:
        siteid = linecache.getline(siteid_file, 1)
        if not siteid:
            print 'Siteid not found in file %s' % siteid_file
            sys.exit(1)
            
    try:
        doc = libxml2.parseFile(services_xml)
    except libxml2.parserError:
        print 'Could not parse %s' % services_xml
        sys.exit(1) 

    try:
        (errtype, errcode) = find_errcode(description, doc)
    except IndexError:
        print '%s does not match known services, exiting.' % description
        sys.exit(1) 
        
    message = create_msg(siteid, hostname, hostaddress, custom_timestamp(timestamp), errtype, errcode, output)
    
    send_message(message, smtpto = smtpto, smtpfrom = smtpfrom, smtpserver = smtpserver)
    

def main():
    hb_nagios(*check_arg())

    
if __name__ == "__main__":
    main()


# Changelog
#
# Version: 0.4
# Date: 2013-07-29
# Owner: Leonardo Ruiz
# Filename: hb_nagios.py
# Description: Used to send heartbeat formatted message
#
# End of file - hb_nagios.py
