#!/usr/bin/env python
# Version:0.1
# This Plugin is designed keeping, statistics data for 24 hours won't
# be more than how much its possible to send over a single HTTP Request
# Time consideration through out the plugin is UTC
# Regardless of the time when plugin runs data would be generated for the past day (24 hours)
# That is if the Plugin is running at June 26,11:00 AM UTC, data would be fetched respectively,
# from June 25, 12:00:00 AM UTC to JUNE 25, 11: 59:59 PM UTC
# CSV file name would be generated out of the from date in query feild.
# in the above scenario file name would be `BillingStatistics_SITE_2017-06-25_00-00-00.csv`

from decimal import Decimal
from dateutil.parser import parse
from datetime import date, datetime, timedelta
import json


import pymssql
import argparse
import requests

from post_notification import notifier, JSONAttributeAction, PAYLOAD_PART_HELP

OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3


CSV_NAME = 'BillingStatistics_{SITEID}_{date_str}.csv'
SITEID_FILE = '/etc/siteid'


PROCEDURE_NAME = 'apIDMQueryBillingStatistics'

PROCEDURE_STMT = 'CREATE PROCEDURE {name} @FromDate DATETIME, @EndDate DATETIME AS BEGIN {qry_stmt} END'

QUERY_STMT = """SELECT R.StudyUID, R.ReceivedDateTime, R.SourceIP, S.aeTitle AS AETitle,
 R.StudyDateTime, S.StationName, S.modalityType AS ModalityType, R.BodyPart,\
    S.InstitutionName AS InstitutionName, S.departmentName AS DepartmentName, \
    SUM(R.numberOfImages) AS NumberOfImages, SUM(R.studySize)/1024.0/1024.0 AS 'StudySize (MB)',\
    R.ArchivedDateTime, R.DeletedDateTime, S.manufacturerModelName AS ManufacturerModelName, \
    R.Host, S.manufacturer AS Manufacturer, S.Ghost, S.SourceInternalId, R.ConsolidationDateTime, \
    R.ProcessorId, R.ProcessorLocationName, R.OrganizationId FROM DBO.Sources \
    AS S JOIN DBO.ReceivedStudies AS R on S.sourceInternalId = R.sourceInternalId \
    WHERE R.ReceivedDateTime >= @FromDate AND R.ReceivedDateTime < @EndDate \
    GROUP BY R.ProcessorLocationName, R.OrganizationId, R.StudyUID, \
    R.AccessionNumber, R.PatientID,R.ReceivedDateTime,R.ArchivedDateTime, \
    R.DeletedDateTime, R.StudyDateTime, R.ProcessorId,R.ConsolidationDateTime, \
    S.AETitle,S.ModalityType,S.manufacturerModelName, S.Manufacturer, \
    S.StationName,S.InstitutionName,S.DepartmentName, S.SourceIP,R.BodyPart, \
    R.SourceIP, S.Ghost, S.SourceInternalId, R.Host ORDER BY ReceivedDateTime
    """
# The CSV_HEADERS are going to be the column headers for the CSV.
# It has a dependency over the QUERY_STMT column selection
# The colums which are selected using the Query and this headers
# should always match.
# The 0 th field `Siteid` is not part of the Query, but it is required.
CSV_HEADERS = ['Siteid', 'StudyUID', 'ReceivedDateTime', 'SourceIP', 'AETitle', 'StudyDateTime', 'StationName', 'ModalityType', 'BodyPart', 'InstitutionName', 'DepartmentName', 'NumberOfImages',
               'StudySize (MB)', 'ArchivedDateTime', 'DeletedDateTime', 'ManufacturerModelName', 'Host', 'Manufacturer', 'Ghost', 'SourceInternalId', 'ConsolidationDateTime', 'ProcessorId', 'ProcessorLocationName', 'OrganizationId']


class SQLHandler:
    def __init__(self, conn, prcedure_stmt, procedure_name, qry_stmt):
        self._prcedure_stmt = prcedure_stmt
        self._qry_stmt = qry_stmt
        self.conn = conn
        self.cursor = conn.cursor()
        self.procedure_name = procedure_name

    def execute(self, query):
        return self.cursor.execute(query)

    def call_procedure(self, *args):
        return self.cursor.callproc(self.procedure_name, args)

    def create_procedure(self):
        self.execute(self._prcedure_stmt.format(
            name=self.procedure_name, qry_stmt=self._qry_stmt))

    def process(self, start_date, end_date):
        self.call_procedure(start_date, end_date)
        return [row for row in self.cursor]


def get_base_url(url):
    return url if url.endswith('/') else url + '/'


def get_url(base_url, site_id):
    route = 'status/' + site_id
    return base_url + route


def post_url(base_url):
    return base_url + 'upload'


def get_site_id(file_name):
    with open(file_name) as fd:
        return fd.read().strip()


def date_string(date_time, formt='%Y-%m-%d_%H-%M-%S'):
    return date_time.strftime(formt)


def csv_name(site_id, date_str):
    # CSV Name : BillingStatistics_TESTSITE_2017-06-21_12-15-33
    return CSV_NAME.format(SITEID=site_id, date_str=date_str)


def get_payload(studies, processing_date, csv_headers, billing_datetime, site_id):
    payload = {'studies': studies, 'csv_headers': csv_headers}
    payload['siteid'] = site_id
    payload['billing_datetime'] = billing_datetime.isoformat()
    payload['file_name'] = csv_name(
        payload['siteid'], date_string(processing_date))
    return payload


def generate_dates(processing_date):
    # Generate from and to dates for sql query
    end_date = processing_date - timedelta(hours=processing_date.hour, minutes=processing_date.minute,
                                           seconds=processing_date.second)
    from_date = end_date - timedelta(days=1)
    return from_date, end_date


def custom_serializer(obj):
    """JSON serializer for objects not serializable by default json code
        datetime, Decimal
    """
    if isinstance(obj, (datetime, date)):
        return str(obj)
    elif isinstance(obj, (Decimal,)):
        return float(obj)
    raise TypeError('Type %s not serializable' % type(obj))


def post_data(url, data, headers=None, timeout=180):
    params = dict(url=url, json=data, verify=False, timeout=timeout)
    if headers:
        params['headers'] = headers
    response = requests.post(**params)
    response.raise_for_status()
    return response


def verify_result(csv_headers, studies):
    # Verify the result and custom Headers by length
    # Siteid in csv headers won't be part of result row
    return len(csv_headers) - 1 == len(studies[0])


def iso_to_datetime(iso_str):
    # iso_str - datetime objects in ISO format
    return parse(iso_str)


def get_previous_billing_date(get_url):
    response = requests.get(get_url)
    response.raise_for_status()
    recent_success = response.json().get('billing_datetime')
    return iso_to_datetime(recent_success) if recent_success else {}


def get_end_date(date_time):
    delta_hour, delta_minute, = (23 - date_time.hour, 59 - date_time.minute)
    delta_second = 59 - date_time.second
    delta_microsec = 1000000 - date_time.microsecond
    return date_time + timedelta(hours=delta_hour, minutes=delta_minute,
                                 seconds=delta_second, microseconds=delta_microsec)


def get_begin_date(url, present_date):
    recent_billing_date = get_previous_billing_date(url)
    if not recent_billing_date:
        return present_date - timedelta(days=1)
    date_max_limit = present_date - timedelta(days=30)
    if recent_billing_date < date_max_limit:
        return date_max_limit
    return recent_billing_date


def billing_dates(begin_date, end_date):
    bill_date = begin_date + timedelta(days=1)
    while bill_date < end_date:
        yield bill_date
        bill_date = bill_date + timedelta(days=1)


def do_billing(sql_handler, billing_date, site_id, post_url):
    status = OK
    frm_date, end_date = generate_dates(billing_date)
    frm_date_str = date_string(frm_date, formt='%Y-%m-%d %H:%M:%S')
    end_date_str = date_string(end_date, formt='%Y-%m-%d %H:%M:%S')
    studies = sql_handler.process(frm_date_str, end_date_str)
    if studies and not verify_result(CSV_HEADERS, studies):
        message = 'CRITICAL - Header length does not match with result row\
         length for the date {date}'.format(date=frm_date.strftime('%Y-%b-%d'))
        status = CRITICAL
    else:
        json_payload = json.dumps(get_payload(
            studies, frm_date, CSV_HEADERS, billing_date, site_id), default=custom_serializer)
        post_data(post_url, json_payload)
        message = 'OK, Rows uploaded for {date} is - {rows}'.format(
            date=frm_date.strftime('%Y-%b-%d'), rows=len(studies))
    return status, message


def parse_args(args=None):
    parser = argparse.ArgumentParser(
        description='iSite Billing Statistics Plugin')
    parser.add_argument('-H', '--hostaddress',
                        help='The address of MSSQL DB node',
                        required=True)
    parser.add_argument('-u', '--username',
                        help='MSSQL user name',
                        required=True)
    parser.add_argument('-p', '--password',
                        help='MSSQL user password',
                        required=True)
    parser.add_argument('-n', '--notificationurl',
                        help='Post Notification url',
                        required=True)
    parser.add_argument('-a', '--hostname', required=True,
                        help='The name of the server originating the message')
    parser.add_argument('-desc', '--description',
                        help='The description of the entity for the event',
                        required=True)
    parser.add_argument('-d', '--database',
                        help='Database Name',
                        default='StentorStatistics')
    parser.add_argument('-url', '--appurl',
                        help='Billing App base url, eg - http://vigilant/billingapp/',
                        default='http://vigilant/billingapp/')
    parser.add_argument('-P', '--payload_part',
                        action=JSONAttributeAction, help=PAYLOAD_PART_HELP, nargs=2)
    # arg = parser.parse_args(args)
    return parser.parse_args(args)


@notifier
def main(cmd_args, *args, **kwargs):
    message_list, state, notification_type = [], CRITICAL, 'PROBLEM'
    try:
        base_url = get_base_url(cmd_args.appurl)
        site_id = get_site_id(SITEID_FILE)
        begin_date = get_begin_date(
            get_url(base_url, site_id), datetime.utcnow())
        with pymssql.connect(cmd_args.hostaddress, cmd_args.username,
                             cmd_args.password, cmd_args.database) as sql_con:
            sql_handler = SQLHandler(
                sql_con, PROCEDURE_STMT, PROCEDURE_NAME, QUERY_STMT)
            sql_handler.create_procedure()
            for billing_date in billing_dates(begin_date, get_end_date(datetime.utcnow())):
                state, msg = do_billing(
                    sql_handler, billing_date, site_id, post_url(base_url))
                message_list.append(msg)
                if not state == OK:
                    break
        if message_list:
            message = message_list
        else:
            message = 'OK - Billing already done for - {date} OR its a future date'.format(
                date=begin_date.strftime('%Y-%b-%d'))
            state = OK
        if state == OK:
            notification_type = 'RECOVERY'
    except pymssql.Error as ex:
        message = 'CRITICAL - Error - %s , Host -%s, DB - %s' % (
            str(ex), cmd_args.hostaddress, cmd_args.database)
    except requests.exceptions.RequestException as ex:
        msg = 'CRITICAL - Error accessing Billing-App: {url} : {ex_info}'
        message = msg.format(url=base_url, ex_info=ex)
    except Exception as e:
        message = 'CRITICAL - Error, {ex_info}'.format(ex_info=e)
    return state, notification_type, message


if __name__ == '__main__':
    main(parse_args())
