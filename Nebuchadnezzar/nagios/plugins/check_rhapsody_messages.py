#!/usr/bin/env python
import sys
import argparse
import requests
from scanline.utilities.http import HTTPRequester


def check_arg(args=None):
    parser = argparse.ArgumentParser(
        description='Script to get message counts in error and hold queues')
    parser.add_argument('-H', '--hostname', required=True,
                        help='The name of the Rahpsody server')
    parser.add_argument('-P', '--port', required=True,
                        help='The passowrd of the Rahpsody server')
    parser.add_argument('-u', '--username', required=True,
                        help='The username of the Rahpsody server')
    parser.add_argument('-p', '--password', required=True,
                        help='The passowrd of the Rahpsody server')
    parser.add_argument('-s', '--subpath', required=True,
                        help='The passowrd of the Rahpsody server')
    parser.add_argument('-w', '--warning_val', required=True,
                        help='The warning threshold for the queue')
    parser.add_argument('-c', '--critical_val', required=True,
                        help='The critical threshold for the queue')
    results = parser.parse_args(args)
    return (
        results.hostname,
        results.port,
        results.username,
        results.password,
        results.subpath,
        results.warning_val,
        results.critical_val)


def get_message_count(hostname, port, username, password, subpath, warning_val,
                      critical_val):
    url = 'https://{0}:{1}/api/{2}/count'.format(hostname, port, subpath)
    headers = {'Accept': 'application/json, text/html',}
    auth = (username, password)
    warning_val, critical_val = int(warning_val), int(critical_val)
    try:
        http_request = HTTPRequester()
        msg_str = http_request.suppressed_get_parsed(url, headers=headers,
                                                     auth=auth, section='div',
                                                     attrs={'class': 'section'})
        msg_count = int(str(msg_str).strip().split(':')[1])
        msg = 'Total message(s) found in the queue is: {0}'
        if msg_count < warning_val and msg_count < critical_val:
            return 0, msg.format(msg_count)
        elif msg_count >= warning_val and msg_count < critical_val:
            return 1, msg.format(msg_count)
        elif msg_count >= critical_val:
            return 2, msg.format(msg_count)
    except IndexError:
        return 2, 'Could not fetch attribute.'
    except Exception:
        return 2, 'Could not connect to server.'


def main():
    state, msg = get_message_count(*check_arg())
    print msg
    sys.exit(state)


if __name__ == '__main__':
    main()
