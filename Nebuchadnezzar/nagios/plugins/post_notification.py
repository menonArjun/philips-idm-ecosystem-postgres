#!/usr/bin/env python
# This script should always exit 0
# it may display problem information as output
# problems with this script are meant to be determined by the system expecting the post
from __future__ import print_function
import sys

import requests
import argparse
import linecache
from functools import wraps

from phimutils.message import Message
from phimutils.argument import from_json
from phimutils.collection import DictNoEmpty


PAYLOAD_PART_HELP = 'A label and JSON string to be used as part of the payload'
STATE_CHOICES = ('OK', 'WARNING', 'CRITICAL', 'UNKNOWN')
SITEID_FILE = '/etc/siteid'


def notifier(func):
    @wraps(func)
    def wrapper(cmd_args, *args, **kwargs):
        state, notification_type, message = func(cmd_args, *args, **kwargs)
        notify_args = {'state': STATE_CHOICES[state], 'notificationtype': notification_type,
                       'output': message, 'timestamp': None, 'perfdata': None,
                       'hostaddress': cmd_args.hostaddress, 'siteid': None,
                       'siteid_file': SITEID_FILE, 'url': cmd_args.notificationurl,
                       'hostaddress': cmd_args.hostaddress, 'hostname': cmd_args.hostname,
                       'description': cmd_args.description, 'payload_part': cmd_args.payload_part
                       }
        post_notification(**notify_args)
        print (message)
        sys.exit(state)
    return wrapper



class JSONAttributeAction(argparse.Action):
    # Needs values to be 2 in length
    def __call__(self, parser, namespace, values, option_string=None):
        if not (len(values) == 2):
            raise argparse.ArgumentError(self, '%s takes 2 values, %d given' % (option_string, len(values)))
        label, json_string = values
        try:
            value = from_json(json_string)
        except argparse.ArgumentTypeError:
            raise argparse.ArgumentError(self, '%s takes valid JSON, %s given' % (option_string, json_string))
        destination = getattr(namespace, self.dest) or {}
        destination[label] = value
        setattr(namespace, self.dest, destination)


# the following requires python-argparse to be installed
def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Script to send a message via HTTP post')
    parser.add_argument('-u', '--url', required=True, help='The URL address to post to')
    parser.add_argument('-H', '--hostaddress', required=True, help='The address of the server originating the message')
    parser.add_argument('-a', '--hostname', required=True, help='The name of the server originating the message')
    parser.add_argument('-d', '--description', required=True, help='The description of the entity for the message')
    parser.add_argument('-t', '--timestamp', help='The timestamp for the message, expected in UNIX epoch time')
    parser.add_argument('-o', '--output', help='The output for the message')
    parser.add_argument('-T', '--notificationtype', required=True, help='The notification type for the message')
    parser.add_argument('-s', '--state', choices=STATE_CHOICES, help='The state for the message')
    parser.add_argument('-p', '--perfdata', help='The performance data type for the message')
    parser.add_argument('-P', '--payload_part', action=JSONAttributeAction, help=PAYLOAD_PART_HELP, nargs=2)
    parser.add_argument('-k', '--insecure', help='do not verify ssl', action='store_true')
    group = parser.add_mutually_exclusive_group()
    group.add_argument('--siteid_file', help='A file containing the site id', default=SITEID_FILE)
    group.add_argument('--siteid', help='The site id')
    results = parser.parse_args(args)

    return (
        results.url,
        results.hostname,
        results.hostaddress,
        results.description,
        results.notificationtype,
        results.state,
        results.timestamp,
        results.output,
        results.perfdata,
        results.siteid,
        results.siteid_file,
        results.payload_part,
        results.insecure,
    )


def post_notification(
    url,
    hostname,
    hostaddress,
    description,
    notificationtype,
    state,
    timestamp,
    output,
    perfdata,
    siteid,
    siteid_file,
    payload_part=None,
    insecure=False):
    """Send a notification via http in json format"""
    if not siteid:
        siteid = linecache.getline(siteid_file, 1).strip()
        if not siteid:
            return 0, 'But could not post due to missing siteid'

    payload = DictNoEmpty(payload_part or {}, output=output)

    msg = Message(
        siteid=siteid,
        epoch_time=timestamp,
        hostname=hostname,
        hostaddress=hostaddress,
        service=description,
        type=notificationtype,
        state=state,
        payload=payload,
        perfdata=perfdata
    )
    
    try:
        r = requests.post(url, json=msg.to_dict(), verify=not insecure)
        r.raise_for_status()
    except requests.exceptions.RequestException:
        outmsg = 'But could not post due to request error'
    else:
        outmsg = 'Posted'
    return 0, outmsg

        
def main():
    state, msg = post_notification(*check_arg())
    print('OK - %s' % msg)


if __name__ == '__main__':
    main()
