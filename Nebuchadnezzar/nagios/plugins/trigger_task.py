#!/usr/bin/env python
# tasks called by this module are expected to accept hostname and service as a keyword parameters

import argparse
import sys
from celery import Celery
from phimutils.argument import from_json

STATES = ('OK', 'WARNING', 'CRITICAL', 'UNKNOWN')


# the following requires python-argparse to be installed
def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Script to trigger a celery task')
    parser.add_argument('-H', '--hostname', required=True, help='The name of the server to associate with the task')
    parser.add_argument('-s', '--service', required=True, help='The name of the service to associate with the task')
    parser.add_argument('-T', '--task', required=True, help='The task to trigger')
    parser.add_argument('-A', '--args', type=from_json, help='JSON array of arguments to be sent to the task')
    parser.add_argument('-K', '--kwargs', type=from_json, help='JSON object to be sent to the task as keyword arguments')
    parser.add_argument('-C', '--config-object', help='The object to use for configuration')
    parser.add_argument('-P', '--path', help='The path to add to find configuration')

    results = parser.parse_args(args)

    return (
        results.hostname,
        results.service,
        results.task,
        results.args,
        results.kwargs,
        results.config_object,
        results.path
    )


def get_celery(config_object, path):
    sys.path.append(path)
    celery = Celery()
    # Configure from module
    celery.config_from_object(config_object)
    return celery


def trigger_task(hostname, service, task, args, kwargs, config_object, path):
    in_kwargs = kwargs or {}
    in_kwargs.update({'hostname': hostname, 'service': service})
    celery = get_celery(config_object, path)
    args = args or []
    celery.send_task(task, args, kwargs=in_kwargs)
    return 'Task {0} sent'.format(task)


def main():
    state = 0
    try:
        msg = trigger_task(*check_arg())
    except Exception, e:
        msg = e
        state = 3
    print('{state} - {msg}'.format(state=STATES[state], msg=msg))
    sys.exit(state)


if __name__ == '__main__':
    main()
