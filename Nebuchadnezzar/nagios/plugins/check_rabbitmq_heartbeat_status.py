#!/usr/bin/env python
# This plugin is useful for checking heartbeat of RabbitMQ.
# It creates a queue, post a message to it.
# Checks if message is posted successfully and then fetches that message.
# It deletes the queue, if this complete workflow happens properly
# then this plugin returns 'OK' status with message.
# If anything wrong happens, if throws status code with relevent message.

import sys
import argparse
import json
import requests

PAYLOAD = """<?xml version="1.0"?>
<data xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://
www.w3.org/2001/XMLSchema">
  <transaction_data>
    <id>17f9aaad-bda3-ecbb-c051-39da7764b5c4</id>
    <type>Prefetch</type>
    <sub-type>EC</sub-type>
    <requesting_node>ingbtcpic5dtq43.code1.emi.philips.com</requesting_node>
  </transaction_data>
  <business_data>
    <patient id="609-26-000-57E8C0AF" patientKey="434f80d1-b283-e611-81d6-
    6451065d1d16" personKey="00000000-0000-0000-0000-000000000000" />
    <exam key="1ae307e9-b383-e611-81d6-6451065d1d16" scheduledDateTime=
    "2016-09-26T06:38:49" />
    <rule_execution>
      <request>
        <fact_attributes>
          <fact_attribute name="Modality">CT</fact_attribute>
          <fact_attribute name="BodyPart">HEAD</fact_attribute>
          <fact_attribute name="ExamCode">CTHEAD</fact_attribute>
          <fact_attribute name="OrganizationId">DEFAULT</fact_attribute>
          <fact_attribute name="ExamDescription" />
          <fact_attribute name="SubSpecialtyCode" />
          <fact_attribute name="TriggerType">NEWORDERARRIVAL</fact_attribute>
        </fact_attributes>
        <execution_specifications>
          <context>prefetch</context>
          <type>SINGLE_MATCH</type>
          <group>prefetch_general</group>
        </execution_specifications>
        <response_queue>
          <broker>rabbitMq</broker>
          <exchange>PreFetch.Topic</exchange>
          <queueName>PrefetchManagerQueue</queueName>
          <routingKey>PrefetchManagerQueue_RK</routingKey>
        </response_queue>
      </request>
    </rule_execution>
  </business_data>
</data>"""


def check_arg(args=None):
    parser = argparse.ArgumentParser(
        description='Script to check heartbeat status of RabbitMQ server')
    parser.add_argument('-H', '--hostname', required=True,
                        help='The address to post to')
    parser.add_argument('-P', '--port', required=True,
                        help='The port to post to')
    parser.add_argument('-u', '--username', required=True,
                        help='The username of the rabbitmq server')
    parser.add_argument('-p', '--password', required=True,
                        help='The password of the rabbitmq server')
    parser.add_argument('-q', '--queue_name', required=True,
                        help='The queue_name of the rabbitmq server to be created by IDM')
    parser.add_argument('-r', '--routing_key', required=True,
                        help='The routing_key of the rabbitmq server to be used')
    parser.add_argument('-s', '--source_exchange', required=True,
                        help='The source_exchange of the rabbitmq server to be used')
    parser.add_argument('-t', '--timeout', required=True,
                        help='The timeout of the rabbitmq server while fetching message')
    results = parser.parse_args(args)
    return (results.hostname,
            results.port,
            results.username,
            results.password,
            results.queue_name,
            results.routing_key,
            results.source_exchange,
            results.timeout)


def form_header(username, password):
    headers = {'content-type': 'application/json', }
    auth = (username, password)
    return headers, auth


def create_queue(headers, auth, hostname, port, queue_name):
    url = 'https://{0}:{1}/api/queues/%2F/{2}'.format(hostname, port,
                                                      queue_name)
    data = {'vhost': '/', 'name': queue_name, 'durable': 'true',
            'auto_delete': 'false', 'arguments': {}}
    try:
        response = requests.put(url, headers=headers, data=json.dumps(data),
                          auth=auth, verify=False)
        if response.status_code == 204:
            return 0, 'Queue created successfully.'
        else:
            return 2, 'Could not create queue: {0}, status code: {1}'.format(
                    queue_name, response.status_code)
    except Exception:
        return 2, 'Invalid request, could not create queue: {0}'.format(
            queue_name)


def bind_queue(headers, auth, hostname, port, source_exchange, queue_name,
               routing_key):
    url = 'https://{0}:{1}/api/bindings/%2F/e/{2}/q/{3}'.format(hostname, port,
                                                                source_exchange,
                                                                queue_name)
    data = {'vhost': '/', 'destination': queue_name, 'destination_type': 'q',
            'source': source_exchange, 'routing_key': routing_key,
            'arguments': {}}
    try:
        response = requests.post(url, headers=headers, auth=auth,
                                 data=json.dumps(data), verify=False)
        if response.status_code == 201:
            return 0, 'Queue binded successfully.'
        else:
            return 2, 'Could not bind {0} queue, status code: {1}'.format(
                queue_name, response.status_code)
    except Exception:
        return 2, 'Invalid post request for {0} queue.'.format(queue_name)


def post_message(headers, auth, hostname, port, source_exchange, routing_key,
                queue_name):
    url = 'https://{0}:{1}/api/exchanges/%2f/{2}/publish'.format(hostname, port,
                                                                 source_exchange)
    data = {'properties': {'delivery_mode': 2}, 'routing_key': routing_key,
            'payload_encoding': 'string'}
    data['payload'] = PAYLOAD
    try:
        response = requests.post(url, data=json.dumps(data), auth=auth,
                                 headers=headers, verify=False)
        if response.status_code == 200:
            return 0, 'Message posted successfully.'
        else:
            return 2, 'Could not post message to {0} queue, status code: {1}' \
                      ''.format(queue_name, response.status_code)
    except Exception:
        return 2, 'Invalid post request for {0} queue.'.format(queue_name)


def get_message(headers, auth, hostname, port, queue_name, timeout):
    url = 'https://{0}:{1}/api/queues/%2f/{2}/get'.format(hostname, port,
                                                          queue_name)
    data = {'count': 1, 'requeue': 'false', 'encoding': 'auto'}
    try:
        response = requests.post(url, headers=headers, auth=auth,
                                 data=json.dumps(data), timeout=float(timeout),
                                 verify=False)
        if response.status_code == 200:
            return 0, 'RabbitMQ Heartbeat is up.'
        else:
            return 2, 'Could not fetch message from {0} queue, status code: {1}' \
                      ''.format(queue_name, response.status_code)
    except Exception:
        return 2, 'Invalid post(get message) request for {0} queue.'.format(
            queue_name)


def main():
    hostname, port, username, password, queue_name, routing_key, source_exchange,\
                                timeout = check_arg()
    headers, auth = form_header(username, password)
    status, message = create_queue(headers, auth, hostname, port, queue_name)
    if status == 0:
        status, message = bind_queue(headers, auth, hostname, port,
                                     source_exchange, queue_name, routing_key)
    else:
        print message
        sys.exit(status)
    if status == 0:
        status, message = post_message(headers, auth, hostname, port,
                                       source_exchange, routing_key, queue_name)
    else:
        print message
        sys.exit(status)
    if status == 0:
        status, message = get_message(headers, auth, hostname, port, queue_name,
                                      timeout)
        print message
        sys.exit(status)


if __name__ == '__main__':
    main()
