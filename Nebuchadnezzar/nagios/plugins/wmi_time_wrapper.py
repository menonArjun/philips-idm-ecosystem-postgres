#!/usr/bin/env python

import argparse
import sys
from subprocess import call
from datetime import datetime, timedelta
from phimutils.timestamp import wmi_datetime


DELTAS = ('minutes', 'seconds', 'hours', 'days')

def check_arg(args=None):
    """Check the arguments passed"""
    parser = argparse.ArgumentParser(description='Nagios Plugin to wrap a command placing a wmi date as the last parameter')
    parser.add_argument('-T', '--delta', type=int, help='the time delta to add', required=True)
    parser.add_argument('-U', '--unit', help='The unit for the delta', choices=DELTAS, default='minutes')
    parser.add_argument('-C', '--command', help='the command to run', nargs=argparse.REMAINDER, required=True)
    results = parser.parse_args(args)
    return results.delta, results.unit, results.command


def wmi_time_wrapper(delta, unit, command):
    timestamp = wmi_datetime(datetime.utcnow() + timedelta(**{unit: delta}))
    command.append(timestamp)
    try:
        state = call(command)
        output = None
    except OSError:
        state = 3
        output = 'UNKNOWN - Error running command.'
    return state, output


def main():
    state, output = wmi_time_wrapper(*check_arg())
    if output:
        print(output)
    sys.exit(state)

if __name__ == '__main__':
    main()
