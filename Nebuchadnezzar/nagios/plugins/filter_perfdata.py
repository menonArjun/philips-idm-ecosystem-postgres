#!/usr/bin/env python
# from perfdata as input, returns metrics passed as argument.
# This is a filter for presentation purposes.
# This script should always exit 0
# it may display problem information as output

import argparse
from phimutils.perfdata import perfdata2dict, raw_metric

OUT_STRING = 'OK - {output} | {perfdata}'


def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Script to filter perfdata')
    parser.add_argument('-m', '--metrics',
                        required=True, help='Comma delimited list of the metrics to return')
    parser.add_argument('-p', '--perfdata',
                        required=True, help='The raw performance data')
    results = parser.parse_args(args)
    return results.metrics, results.perfdata


def filter_perfdata(metrics, perfdata):
    found_metrics = filtered_metrics(metrics.split(','), perfdata2dict(perfdata))
    message = OUT_STRING.format(output='No Data', perfdata='')
    if found_metrics:
        perf_text = ' '.join([format_metric(metric) for metric in found_metrics])
        perf_metrics = ' '.join([raw_metric(**metric) for metric in found_metrics])
        message = OUT_STRING.format(output=perf_text, perfdata=perf_metrics)
    print(message)


def format_metric(metric):
    return "'{label}' = {value}{uom}".format(label=metric['label'], value=metric['value'], uom=metric.get('uom', ''))


def filtered_metrics(metrics, perfdata):
    """
    Generates a nagios perfdata style strings of the items found in perfdata from metrics
    :param metrics: a list of metric names to retrieve from perfdata
    :param perfdata: a list of perfdata dictionaries
    :return: a list containing raw perfdata strings
    """
    return [metric for metric in perfdata if metric['label'] in metrics]


def main():
    filter_perfdata(*check_arg())


if __name__ == "__main__":
    main()