#!/usr/bin/env python
# This script should always exit 0
# it may display problem information as output
# problems with this script are meant to be determined by the system expecting
# the post.

import requests
import argparse
from lxml import etree


def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Script to check web services'
                                                 'status.')
    parser.add_argument('-H', '--hostname', required=True,
                        help='The address of the server.')
    parser.add_argument('-o', '--option', required=True,
                        help="pass 'disks' for disk status,"
                             "'process' for process, 'check_aws_service' to"
                             "check AWS services 'check_eventlog' to check"
                             "event log for errors  'check_wfl_webservice' to"
                             "check workflow web tools service, 'services' to"
                             "see list of services not running, 'version' to"
                             "check AWS version.")
    parser.add_argument('-p', '--params', help='argument for url',
                        default='GET')
    results = parser.parse_args(args)
    return (results.hostname, results.option, results.params)

def get_str_from_xml(text):
    enc = text.encode('utf-8')
    val = etree.fromstring(enc, parser=etree.XMLParser(encoding='utf-8'))
    for tag in val.iter():
        if not len(tag):
            return tag.text.split('<br>')[1]

def get_https_process(hostname, option, params):
    option_list = ['disks', 'process', 'check_aws_service', 'check_eventlog',
                   'check_wfl_webservice', 'services', 'version']
    if option not in option_list:
        return 2, 'Incorrect option.'
    sub_path = '/AdvancedWorkflowNagios/AdvancedWorkflowNagios.asmx/'
    url = ''.join(('https://', hostname, sub_path, option))
    querystring = {'server': hostname, 'param': params}
    try:
        response = requests.get(url, params=querystring, verify=False)
        if response.status_code == 200:
            if 'not running' in response.text:
                return 2, 'Process not running.'
            elif 'critical' in response.text.lower():
                return 2, get_str_from_xml(response.text)
            elif 'warning' in response.text.lower():
                return 1, get_str_from_xml(response.text)
            elif 'okay' in response.text.lower():
                return 0, get_str_from_xml(response.text)
        else:
            return 2, 'Could not connect to server, status code: {0}' \
                      ''.format(response.status_code)
    except Exception:
        return 2, 'Invalid Request.'


def main():
    state, msg = get_https_process(*check_arg())
    print msg
    exit(state)


if __name__ == '__main__':
    main()
