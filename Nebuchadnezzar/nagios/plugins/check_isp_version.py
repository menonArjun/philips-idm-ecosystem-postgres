#!/usr/bin/env python

import argparse
import sys
from scanline.product.isp import ISPProductScanner
import logging
from StringIO import StringIO
from phimutils.perfdata import INFO_MARKER


# the following requires python-argparse to be installed
def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Script to get ISP version from API')
    parser.add_argument('-H', '--hostaddress',
                        required=True, help='The address of the server originating the message')
    results = parser.parse_args(args)
    return results.hostaddress,


def check_isp_version(hostaddress):
    """get ISP version from API"""
    scanned = ISPProductScanner(scanner='ISP', address=hostaddress, username=None, password=None)
    if scanned:
        return scanned.isp.get_software_version()


def main():
    logger = logging.getLogger(__name__)
    out_file = StringIO()
    handler_console = logging.StreamHandler(out_file)
    logger.addHandler(handler_console)
    software_version = check_isp_version(*check_arg())
    if not software_version:
        print('UNKNOWN - ISP Version not found')
        sys.exit(3)

    out_template = 'OK - ISP Version {version} {info_marker} Version={version}'
    print(out_template.format(version=software_version, info_marker=INFO_MARKER))


if __name__ == '__main__':
    main()
