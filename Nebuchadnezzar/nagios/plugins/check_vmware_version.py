#!/usr/bin/python

import sys
import time
import pywbem
import re
import string
from optparse import OptionParser,OptionGroup


NS = 'root/cimv2'
hostname = ''
user = ''
password = ''

# define exit codes
ExitOK = 0
ExitWarning = 1
ExitCritical = 2
ExitUnknown = 3


def getopts() :
  global hosturl,user,password
  usage = "usage: %prog  https://hostname user password\n" \
    "example: %prog https://vmware-server root password\n\n" \
    "or, using new style options:\n\n" \
    "usage: %prog -H hostname -U username -P password\n" \
    "example: %prog -H vmware-server -U root -P password\n\n" \
    "usage: %prog --host=hostname --user=username --pass=password\n"

  parser = OptionParser(usage=usage, version="%prog "+version)
  group1 = OptionGroup(parser, 'Mandatory parameters')

  group1.add_option("-H", "--host", dest="host", help="report on HOST", metavar="HOST")
  group1.add_option("-U", "--user", dest="user", help="user to connect as", metavar="USER")
  group1.add_option("-P", "--pass", dest="password", \
      help="password, if password matches file:<path>, first line of given file will be used as password", metavar="PASS")

  parser.add_option_group(group1)

  # check input arguments
  if len(sys.argv) < 2:
    print "no parameters specified\n"
    parser.print_help()
    sys.exit(-1)
  # if first argument starts with 'https://' we have old-style parameters, so handle in old way
  if re.match("https://",sys.argv[1]):
    # check input arguments
    if len(sys.argv) < 4:
      print "too few parameters\n"
      parser.print_help()
      sys.exit(-1)
    if len(sys.argv) > 4 :
      if sys.argv[5] == "verbose" :
        verbose = True
    hosturl = sys.argv[1]
    user = sys.argv[2]
    password = sys.argv[3]
  else:
    # we're dealing with new-style parameters, so go get them!
    (options, args) = parser.parse_args()

    # Making sure all mandatory options appeared.
    mandatories = ['host', 'user', 'password']
    for m in mandatories:
      if not options.__dict__[m]:
        print "mandatory parameter '--" + m + "' is missing\n"
        parser.print_help()
        sys.exit(-1)

    hostname=options.host.lower()
    # if user has put "https://" in front of hostname out of habit, do the right thing
    # hosturl will end up as https://hostname
    if re.match('^https://',hostname):
      hosturl = hostname
    else:
      hosturl = 'https://' + hostname

    user=options.user
    password=options.password


getopts()

# connection to the host
client = pywbem.WBEMConnection(hosturl, (user, password), NS)

GlobalStatus = ExitUnknown

list = client.EnumerateInstances('VMware_HypervisorSoftwareIdentity')
if list is None:
   print 'Error: Unable to locate any instances of VMware_HypervisorSoftwareIdentity'
else:
   # We know there's only once instance, so we can skip looping for now
   print "OK - " + list[0]['VersionString']