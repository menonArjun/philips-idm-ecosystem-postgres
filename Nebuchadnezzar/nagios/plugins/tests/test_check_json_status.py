import unittest
from mock import MagicMock, patch


class CheckJSONInformationTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_requests = MagicMock(name='requests')
        self.mock_jsonpath_rw = MagicMock(name='jsonpath_rw')
        modules = {
            'requests': self.mock_requests,
            'jsonpath_rw': self.mock_jsonpath_rw
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_json_status
        self.module = check_json_status

        self.module.get_json = MagicMock(name='get_json')
        self.module.get_query = MagicMock(name='get_query')

        self.module.get_query.return_value.find.return_value = [
            MagicMock(value={'Status': 'OK', 'Message': 'everything is extremely well', 'ApplicationName': 'Happy.App'})
        ]
        self.find_return_value = self.module.get_query.return_value.find.return_value

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_check_json_status_one_value(self):
        # to get a mock object with a value field
        out = self.module.check_json_status('nourl', '', 'Status', 'Message', 'ApplicationName', False)
        self.assertEqual(out, (0, '[OK] Happy.App: everything is extremely well'))

    def test_check_json_status_one_value_no_message_label(self):
        # to get a mock object with a value field
        self.assertEqual(
            self.module.check_json_status('nourl', '', 'Status', '', '', False),
            (0, '[OK] : ')
        )

    def test_check_json_status_two_values(self):
        # to get a mock object with a value field
        self.find_return_value.append(
            MagicMock(value={'Status': 'OK', 'Message': 'no error', 'ApplicationName': 'NotErroredApp'})
        )
        self.assertEqual(
            self.module.check_json_status('nourl', '', 'Status', 'Message', 'ApplicationName', False),
            (0, '[OK] Happy.App: everything is extremely well; [OK] NotErroredApp: no error')
        )

    def test_check_json_status_ok_critical_and_unk(self):
        # to get a mock object with a value field
        self.find_return_value.append(
            MagicMock(value={'Status': 'CRITICAL', 'Message': 'error', 'ApplicationName': 'ErroredApp'})
        )
        self.find_return_value.append(
            MagicMock(value={'Status': 'UNKNOWN', 'Message': 'who am I?', 'ApplicationName': 'SentientApp'})
        )
        self.assertEqual(
            self.module.check_json_status('nourl', '', 'Status', 'Message', 'ApplicationName', False),
            (
                2,
                '[OK] Happy.App: everything is extremely well; '
                '[CRITICAL] ErroredApp: error; '
                '[UNKNOWN] SentientApp: who am I?'
            )
        )

    def test_check_json_status_warn_ok(self):
        # to get a mock object with a value field
        self.find_return_value.append(
            MagicMock(value={'Status': 'warning', 'Message': 'maybe error', 'ApplicationName': 'MaybeErroredApp'})
        )
        self.assertEqual(
            self.module.check_json_status('nourl', '', 'Status', 'Message', 'ApplicationName', False),
            (1, '[OK] Happy.App: everything is extremely well; [WARNING] MaybeErroredApp: maybe error')
        )

    def test_check_json_status_missing_state(self):
        # to get a mock object with a value field
        self.find_return_value.append(MagicMock(value={'Message': 'maybe error', 'ApplicationName': 'MaybeErroredApp'}))
        self.assertEqual(
            self.module.check_json_status('nourl', '', 'Status', 'Message', 'ApplicationName', False),
            (3, '[OK] Happy.App: everything is extremely well; [UNKNOWN] MaybeErroredApp: maybe error')
        )

    def test_check_json_status_missing_message_specified(self):
        # to get a mock object with a value field
        self.find_return_value.append(MagicMock(value={'Status': 'OK', 'ApplicationName': 'MaybeErroredApp'}))
        self.assertEqual(
            self.module.check_json_status('nourl', '', 'Status', 'Message', 'ApplicationName', False),
            (3, '[OK] Happy.App: everything is extremely well; [OK] : missing key')
        )

    def test_check_json_status_missing_message_specified_state_critical(self):
        # to get a mock object with a value field
        self.find_return_value.append(MagicMock(value={'Status': 'critical', 'ApplicationName': 'MaybeErroredApp'}))
        self.assertEqual(
            self.module.check_json_status('nourl', '', 'Status', 'Message', 'ApplicationName', False),
            (2, '[OK] Happy.App: everything is extremely well; [CRITICAL] : missing key')
        )


if __name__ == '__main__':
    unittest.main()
