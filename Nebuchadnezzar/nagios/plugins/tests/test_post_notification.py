from StringIO import StringIO

import unittest

from mock import MagicMock, patch


class PostNotificationBaseTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_phimutils = MagicMock(name='phimutils')
        self.mock_phimutils.collection.DictNoEmpty = dict
        modules = {
            'requests': self.mock_request,
            'phimutils': self.mock_phimutils,
            'phimutils.message': self.mock_phimutils.message,
            'phimutils.argument': self.mock_phimutils.argument,
            'phimutils.collection': self.mock_phimutils.collection,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import post_notification
        self.post_notification = post_notification

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()


class PostNotificationCommonTestCase(PostNotificationBaseTestCase):
    def setUp(self):
        PostNotificationBaseTestCase.setUp(self)

    @patch('post_notification.requests')
    def test_post_notification_request_exception(self, mock_requests):
        class MyException(Exception):
            pass
        mock_requests.exceptions.RequestException = MyException
        mock_requests.post.side_effect = MyException('some http error')
        self.assertEqual(
            self.post_notification.post_notification(
                'url',
                'hostname',
                'hostaddress',
                'description',
                'notificationtype',
                'state',
                'timestamp',
                'output',
                'perfdata',
                'siteid',
                'siteid_file'
            ),
            (0, 'But could not post due to request error')
        )
        self.post_notification.Message.assert_called_once_with(
            epoch_time='timestamp',
            hostaddress='hostaddress',
            hostname='hostname',
            payload={'output': 'output'},
            perfdata='perfdata',
            service='description',
            siteid='siteid',
            state='state',
            type='notificationtype'
        )
        mock_requests.post.assert_called_once_with(
            'url',
            json=self.post_notification.Message.return_value.to_dict.return_value,
            verify=True
        )

    @patch('post_notification.linecache')
    def test_post_notification_no_siteid(self, mock_linecache):
        mock_linecache.getline.return_value = ''
        self.assertEqual(
            self.post_notification.post_notification(
                'url',
                'hostname',
                'hostaddress',
                'description',
                'notificationtype',
                'state',
                'timestamp',
                'output',
                'perfdata',
                '',
                'siteid_file'
            ),
            (0, 'But could not post due to missing siteid')
        )

    @patch('post_notification.requests')
    def test_post_notification_post_ok(self, mock_requests):
        self.assertEqual(
            self.post_notification.post_notification(
                'url',
                'hostname',
                'hostaddress',
                'description',
                'notificationtype',
                'state',
                'timestamp',
                'output',
                'perfdata',
                'siteid',
                'siteid_file'
            ),
            (0, 'Posted')
        )
        self.post_notification.Message.assert_called_once_with(
            epoch_time='timestamp',
            hostaddress='hostaddress',
            hostname='hostname',
            payload={'output': 'output'},
            perfdata='perfdata',
            service='description',
            siteid='siteid',
            state='state',
            type='notificationtype'
        )
        mock_requests.post.assert_called_once_with(
            'url',
            json=self.post_notification.Message.return_value.to_dict.return_value,
            verify=True
        )


class PostNotificationNotifierTestCase(PostNotificationBaseTestCase):
    def setUp(self):
        PostNotificationBaseTestCase.setUp(self)
        self.cmd_args_mock = MagicMock(name="cmd_args")
        config = {'appurl': 'appurl', 'hostaddress': 'hostaddress', 'username': 'username',
                  'password': 'password', 'database': 'database', 'hostname': 'hostname',
                  'description': 'description', 'notificationurl': 'notificationurl',
                  'payload_part': 'payload_part'}
        self.cmd_args_mock.configure_mock(**config)
        self.sys_mock = MagicMock(name='sys')
        self.post_notification.sys = self.sys_mock

    @patch('sys.stdout', new_callable=StringIO)
    def test_notifier_decorator(self, std_out):
        def tester(cmd_args, *args, **kwargs):
            return 0, 'R', 'OK'
        post_notification_mock = MagicMock(name='post_notification')
        self.post_notification.post_notification = post_notification_mock
        decorated_tester = self.post_notification.notifier(tester)
        decorated_tester(self.cmd_args_mock)
        post_notification_mock.assert_called_once_with(description='description', hostaddress='hostaddress',
                                                       hostname='hostname', notificationtype='R', output='OK',
                                                       payload_part='payload_part', perfdata=None, siteid=None,
                                                       siteid_file='/etc/siteid', state='OK', timestamp=None,
                                                       url='notificationurl')
        self.sys_mock.exit.assert_called_once_with(0)
        self.assertEqual(std_out.getvalue(), 'OK\n')


if __name__ == '__main__':
    unittest.main()
