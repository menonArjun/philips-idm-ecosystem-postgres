import unittest
from requests.exceptions import HTTPError
from mock import MagicMock, patch


class CheckTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_scanline = MagicMock(name='scanline')
        self.OBJHSDPMock = MagicMock(name='blah')
        self.OBJTIERMock = MagicMock(name='abc')
        self.UserMock = MagicMock(name='cde')
        modules = {
            'requests': self.mock_request,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.hsdp_obs': self.mock_scanline.hsdp_obs,
            'scanline.utilities.iamauth': self.mock_scanline.iamauth,
            'scanline.utilities.encryption': self.mock_scanline.encryption,
            'scanline.utilities.hsdp_obs.HSDPConnection': self.OBJHSDPMock,
            'scanline.utilities.hsdp_obs.TIERConnection': self.OBJTIERMock,
            'scanline.utilities.iamauth.UserLogin': self.UserMock,
            'scanline.utilities.proxy': self.mock_scanline.proxy
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_hsdp_services
        self.check_hsdp_services = check_hsdp_services
        from check_hsdp_services import get_hsdp_result
        self.get_hsdp_result = get_hsdp_result
        from scanline.utilities.hsdp_obs import HSDPConnection, TIERConnection
        self.HSDPConnection = HSDPConnection()
        self.TIERConnection = TIERConnection()
        from scanline.utilities.iamauth import UserLogin
        self.UserLogin = UserLogin('http://test', 'test', 'auth', 'test')
        from scanline.utilities.proxy import get_proxy
        self.proxy = get_proxy

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_hsdp_result_iam(self):
        self.UserLogin.get_status.return_value = 200
        self.UserLogin.get_headers.return_value = 'test'
        self.UserLogin.get_response.return_value = ''
        self.assertEqual(self.get_hsdp_result('IAM'), (0, '200 - Service is up'))

    def test_get_hsdp_result_sap(self):
        self.check_hsdp_services.get_svc_status = MagicMock('get_svc_status', return_value= (200, 'test', ''))
        self.assertEqual(self.get_hsdp_result('SAP'), (0, '200 - Service is up'))

    def test_get_hsdp_result_backup(self):
        self.check_hsdp_services.get_svc_status = MagicMock('get_svc_status', return_value= (200, 'test', ''))
        self.assertEqual(self.get_hsdp_result('SAPBACKUP'), (0, '200 - Service is up'))

    def test_get_hsdp_result_ssm(self):
        self.check_hsdp_services.get_svc_status = MagicMock('get_svc_status', return_value= (200, 'test', ''))
        self.assertEqual(self.get_hsdp_result('SSM'), (0, '200 - Service is up'))

    def test_get_iam_status(self):
        rval = {'UserName': 'atul.shrivastav@philips.com',
                'Secretkey': 'UT/7eVRccYe9IOrna7EJdIwNSZZ6X55QLb+daeGcSA8=',
                'CredentialStoreURL': 'https://iam-integration.us-east.philips-healthsuite.com',
                'SharedKey': 'yluKLXeeKKfX/Xuce5g2P7Cpl/wSDjfcONbYiGFkZ38=',
                'PassKey': 'bzAgE73h3lwSji6VXtiGxQ==',
                'CredentialName': 'HSDP_Credential'}
        self.HSDPConnection.to_dict.return_value = rval
        self.UserLogin.get_status.return_value = 200
        self.UserLogin.get_headers.return_value = 'test'
        self.UserLogin.get_response.return_value = ''
        self.assertEqual(self.get_hsdp_result('IAM'), (0, '200 - Service is up'))

    def test_get_svc_status(self):
        session_mock = MagicMock()
        session_mock.status_code = 200
        session_mock.headers = ''
        session_mock.content = 'up'
        self.proxy.return_value = ''
        self.mock_request.session.return_value.options.return_value = session_mock
        self.assertEqual(self.check_hsdp_services.get_svc_status('IAM'), (200, '', 'up'))

    def test_get_aim_status(self):
        tierval = {'CFT':{'ProxyUrl': 'http://testproxy.com',
                'ServiceUrl': 'http://testsvc.com'}}
        hsdpval = {'UserName': 'atul.shrivastav@philips.com',
                'Secretkey': 'UT/7eVRccYe9IOrna7EJdIwNSZZ6X55QLb+daeGcSA8=',
                'CredentialStoreURL': 'https://iam-integration.us-east.philips-healthsuite.com',
                'SharedKey': 'yluKLXeeKKfX/Xuce5g2P7Cpl/wSDjfcONbYiGFkZ38=',
                'PassKey': 'bzAgE73h3lwSji6VXtiGxQ==',
                'CredentialName': 'HSDP_Credential'}
        self.HSDPConnection.to_dict.return_value = hsdpval
        self.UserLogin.get_status.return_value = 200
        self.UserLogin.get_headers.return_value = {'Content-Type': 'json', 'Accept': 'json'}
        self.UserLogin.get_response.return_value = {"exchange": {"authentication": {"accessToken": "c3de29c7-8578-414d-a1dd-24067e1868f1"}}}
        self.TIERConnection.to_dict.return_value = tierval
        self.mock_scanline.proxy.get_proxy =  MagicMock('get_proxy', return_value={'http': 'http://test'})
        session_mock = MagicMock()
        session_mock.status_code = 200
        session_mock.headers = ''
        session_mock.content = 'up'
        self.proxy.return_value = ''
        self.mock_request.session.return_value.delete.return_value = session_mock
        self.assertEqual(self.get_hsdp_result('S3'), (0, '200 - Service is up'))

if __name__ == '__main__':
    unittest.main()
