import unittest
from mock import MagicMock, patch


class PostNotificationTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_response = MagicMock(name='response')
        self.mock_phimutils = MagicMock(name='phimutils')
        self.mock_json = MagicMock(name='json')
        self.mock_phimutils.collection.DictNoEmpty = dict
        modules = {
            'requests': self.mock_request,
            'response': self.mock_response,
            'json': self.mock_json,
            'phimutils': self.mock_phimutils,
            'phimutils.message': self.mock_phimutils.message,
            'phimutils.argument': self.mock_phimutils.argument,
            'phimutils.collection': self.mock_phimutils.collection,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import post_alarm
        self.post_alarm = post_alarm
        from post_alarm import get_keystone_token
        from post_alarm import get_alarm_json
        from post_alarm import get_alarm_count
        from post_alarm import get_alarm_id_description
        self.get_keystone_token = get_keystone_token
        self.get_alarm_json = get_alarm_json
        self.get_alarm_count = get_alarm_count
        self.get_alarm_id_description = get_alarm_id_description

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    @patch('post_alarm.requests')
    @patch('post_alarm.json')
    def test_get_keystone_token(self, mock_json, mock_response):
        mock_response.request.return_value.text = '{"access":{"token": {"id": "12345"}}}'
        mock_json.loads.return_value ={"access":{"token": {"id": "12345"}}}
        self.assertEqual(self.get_keystone_token('127.0.0.1', 'admin', 'admin', 'admin', '5000', 'v2.0'), "12345")

    @patch('post_alarm.requests')
    @patch('post_alarm.json')
    def test_get_alarm_json(self, mock_json, mock_response):
        alarms = {u'elements': [{u'lifecycle_state': None, u'links':
            [{u'href': u'https://192.168.237.13:8070/v2.0/alarms/8bdd0e6c-afdb-49c0-aafd-fcc73b341ece',
              u'rel': u'self'}], u'updated_timestamp': u'2017-04-18T06:22:30.026Z',
                                 u'state_updated_timestamp': u'2017-04-18T06:22:30.026Z',
                                 u'metrics': [{u'dimensions': {u'hostname': u'phcblr-udm01-cp1-swpac1-m3-mgmt',
                                                               u'service': u'monasca-transform',
                                                               u'cloud_name': u'phcblr-udm01',
                                                               u'process_name': u'pyspark',
                                                               u'component': u'pyspark',
                                                               u'control_plane': u'control-plane-1',
                                                               u'cluster': u'swpac'},
                                               u'id': None, u'name': u'process.pid_count'}],
                                 u'state': u'ALARM', u'link': None,
                                 u'alarm_definition': {u'severity': u'HIGH',
                                                       u'id': u'15ccf16b-0b55-4649-aa5e-931464229d2d',
                                                       u'links': [{u'href': u'http://192.168.237.13:8070/v2.0/'
                                                                            u'alarm-definitions/15ccf16b-0b55-4649-aa5e-931464229d2d',
                                                                   u'rel': u'self'}], u'name': u'Process Check'},
                                 u'created_timestamp': u'2017-02-23T16:24:07.482Z', u'id': u'8bdd0e6c-afdb-49c0-aafd-fcc73b341ece'}]}
        mock_response.request.return_value.text = '{"counts":2}'
        mock_json.loads.return_value = alarms
        self.assertEqual(self.get_alarm_json('127.0.0.1', '13ad9e4b-073f-4283-80ff-da953ertfd', '8070', 'v2.0', '2'), alarms['elements'])

    def test_get_alarm_count(self):
        alarms=[{'id': '13ad9e4b-073f-4283-80ff-da953eb0f540', 'alarm_definition': {'name':'Disk usage percentage'}}]
        self.assertEqual(self.get_alarm_count(alarms), 1)

    def test_get_alarm_id_description(self):
        alarms=[{'id': '13ad9e4b-073f-4283-80ff-da953eb0f540', 'alarm_definition': {'name':'Disk usage percentage'}}]
        self.assertEqual(self.get_alarm_id_description(alarms), {'13ad9e4b-073f-4283-80ff-da953eb0f540': 'Disk usage percentage'})

    def test_post_notification_no_siteid(self):
        self.assertEqual(
            self.post_alarm.post_notification(
                'url',
                'hostname',
                'op_console_host',
                'op_console_username',
                'op_console_password',
                'op_console_tenant',
                'description',
                'notificationtype',
                'state',
                'timestamp',
                'output',
                'perfdata',
                '',
                'siteid_file'
            ),
            (2, 'But could not post due to missing siteid')
        )

    @patch('post_alarm.json')
    def test_post_notification_alarm_fetch_error(self, mock_json):
        mock_json.loads.return_value ={"access_key":{"token": {"id": "12345"}}}
        self.assertEqual(
            self.post_alarm.post_notification(
                'url',
                'hostname',
                'op_console_host',
                'op_console_username',
                'op_console_password',
                'op_console_tenant',
                'description',
                'notificationtype',
                'state',
                'timestamp',
                'output',
                'perfdata',
                'siteid',
                'siteid_file'
            ),
            (2, "Could not fetch alarm(s) due to request error")
        )

    @patch('post_alarm.requests')
    def test_post_notification_alarm_post_ok(self,  mock_requests):
        self.assertEqual(
            self.post_alarm.post_notification(
                'url',
                'hostname',
                '127.0.0.1',
                'op_console_username',
                'op_console_password',
                'op_console_tenant',
                'description',
                'notificationtype',
                'state',
                'timestamp',
                'output',
                'perfdata',
                'siteid',
                'siteid_file',
                dashboard_port = 9095,
                shinken_alarm_state = '2'
            ),
            (0, 'Everything looks good. Ops Console URL: https://127.0.0.1:9095/#/general/dashboard_alarms_summary')
        )
        self.post_alarm.Message.assert_called_once_with(
            epoch_time='timestamp',
            hostaddress='127.0.0.1',
            hostname='hostname',
            payload={'output': 'Everything looks good. Ops Console URL: https://127.0.0.1:9095/#/general/dashboard_alarms_summary'},
            perfdata='perfdata',
            service='description',
            siteid='siteid',
            state='state',
            type='notificationtype'
        )
        mock_requests.post.assert_called_once_with(
            'url',
            json=self.post_alarm.Message.return_value.to_dict.return_value,
            verify=True
        )


if __name__ == '__main__':
    unittest.main()
