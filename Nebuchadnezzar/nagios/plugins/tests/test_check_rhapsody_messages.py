import unittest
import requests
from mock import MagicMock, patch


class CheckRhapsodyMessagesTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        modules = {
            'requests': self.mock_request,
        }
        self.mock_scanline = MagicMock(name='scanline')
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        self.http_patch = patch('scanline.utilities.http.HTTPRequester')
        self.http_patcher = self.http_patch.start()
        import check_rhapsody_messages
        self.check_rhapsody_messages = check_rhapsody_messages
        from check_rhapsody_messages import get_message_count
        self.get_message_count = get_message_count


    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()


    def test_get_message_count_ok(self):
        self.http_patcher.return_value.suppressed_get_parsed.return_value =\
            'Queue Size:2'
        self.assertEqual(self.get_message_count('localhost', 8544, 'guest',
                                                'guest', 'errorqueue', 5, 10),
                         (0, 'Total message(s) found in the queue is: 2'))


    def test_get_message_count_critical(self):
        self.http_patcher.return_value.suppressed_get_parsed.return_value =\
            'Queue Size:2'
        self.assertEqual(self.get_message_count('localhost', 8544, 'guest',
                                                'guest', 'errorqueue', 0, 0),
                         (2, 'Total message(s) found in the queue is: 2'))


    def test_get_message_count_warning(self):
        self.http_patcher.return_value.suppressed_get_parsed.return_value =\
            'Queue Size:2'
        self.assertEqual(self.get_message_count('localhost', 8544, 'guest',
                                                'guest', 'errorqueue', 2, 5),
                         (1, 'Total message(s) found in the queue is: 2'))


    def test_get_message_count_indexerror(self):
        self.http_patcher.return_value.suppressed_get_parsed.side_effect =\
            IndexError
        self.assertEqual(self.get_message_count('localhost', 8544, 'guest',
                                                'guest', 'errorqueue', 0, 0),
                         (2, 'Could not fetch attribute.'))


    def test_get_message_count_exception(self):
        self.http_patcher.return_value.suppressed_get_parsed.side_effect =\
            requests.exceptions.ConnectionError
        self.assertEqual(self.get_message_count('localhost', 8544, 'guest',
                                                'guest', 'errorqueue', 0, 0),
                         (2, 'Could not connect to server.'))


if __name__ == '__main__':
    unittest.main()
