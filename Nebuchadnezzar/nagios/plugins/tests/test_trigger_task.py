import unittest
from mock import MagicMock, patch


class TriggerTaskTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_celery = MagicMock(name='celery')
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_phimutils = MagicMock(name='phimutils')
        modules = {
            'celery': self.mock_celery,
            'argparse': self.mock_argparse,
            'phimutils': self.mock_phimutils,
            'phimutils.argument': self.mock_phimutils.argument,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        import trigger_task
        self.module = trigger_task

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_trigger_task(self):
        self.module.get_celery = MagicMock(name='get_celery')
        self.assertEqual(
            self.module.trigger_task(
                'host01',
                'service_01',
                'tasks.task01',
                ['arg1', 'arg2'],
                {'kwarg1': 'val1', 'kwarg2': 'val2'},
                {'config1': 'valc1'},
                '/tmp/path'
            ),
            'Task tasks.task01 sent'
        )
        self.module.get_celery.assert_called_once_with({'config1': 'valc1'}, '/tmp/path')
        self.module.get_celery.return_value.send_task.assert_called_once_with(
            'tasks.task01',
            ['arg1', 'arg2'],
            kwargs={'service': 'service_01', 'hostname': 'host01', 'kwarg1': 'val1', 'kwarg2': 'val2'}
        )


if __name__ == '__main__':
    unittest.main()
