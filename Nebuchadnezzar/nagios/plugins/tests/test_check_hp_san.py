import unittest
import requests
from mock import MagicMock, patch, Mock


class CheckHPSANTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_scanline = MagicMock(name='scanline')
        self.mock_phimutils = MagicMock(name='phimutils')
        self.mock_json = MagicMock(name='json')
        self.mock_phimutils.collection.DictNoEmpty = dict
        modules = {
            'requests': self.mock_request,
            'phimutils': self.mock_phimutils,
            'json': self.mock_json,
            'phimutils.message': self.mock_phimutils.message,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()



if __name__ == '__main__':
    unittest.main()
