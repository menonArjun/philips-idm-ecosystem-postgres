import unittest

from mock import MagicMock, patch, mock_open, call

import pymssql
import requests


def fake_notifier(func):
    def wrap(*args, **kwargs):
        return func(*args, **kwargs)
    return wrap


class iSiteBillingBaseTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_decimal = MagicMock(name='decimal')
        self.mock_parse = MagicMock(name='parse')
        self.mock_datetime = MagicMock(name='datetime')
        self.mock_json = MagicMock(name='json')
        self.mock_pymssql = MagicMock(name='pymssql')
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_post_notification = MagicMock(name='post_notification')
        self.mock_post_notification.notifier = fake_notifier
        modules = {
            'requests': self.mock_request,
            'decimal': self.mock_decimal,
            'json': self.mock_json,
            'datetime': self.mock_datetime,
            'pymssql': self.mock_pymssql,
            'argparse': self.mock_argparse,
            'dateutil.parser': self.mock_parse,
            'post_notification': self.mock_post_notification,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import isite_billing
        self.isite_billing = isite_billing

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()


class iSiteBillingSQLHandlerTestCase(iSiteBillingBaseTestCase):
    def setUp(self):
        iSiteBillingBaseTestCase.setUp(self)
        self.conn = MagicMock(name='conn')

    def test_sql_handler_execute(self):
        sqlh_obj = self.isite_billing.SQLHandler(self.conn, 'ps', 'p_n', 'q_s')
        sqlh_obj.execute('query')
        self.conn.cursor.assert_called_once_with()
        self.conn.cursor().execute.assert_called_once_with('query')

    def test_sql_handler_call_procedure(self):
        sqlh_obj = self.isite_billing.SQLHandler(self.conn, 'ps', 'p_n', 'q_s')
        sqlh_obj.call_procedure('a', 'b')
        self.conn.cursor.assert_called_once_with()
        self.conn.cursor().callproc.assert_called_once_with('p_n', ('a', 'b'))

    def test_sql_handler_create_procedure(self):
        execute_mock = MagicMock(name='execute')
        proc_stmt = 'ps.{name}.{qry_stmt}'
        sqlh_obj = self.isite_billing.SQLHandler(
            self.conn, proc_stmt, 'p_n', 'q_s')
        sqlh_obj.execute = execute_mock
        sqlh_obj.create_procedure()
        self.conn.cursor.assert_called_once_with()
        execute_mock.assert_called_once_with('ps.p_n.q_s')

    def test_sql_handler_process(self):
        cal_procedure_mock = MagicMock(name='call_procedure')
        proc_stmt = 'ps.{name}.{qry_stmt}'
        sqlh_obj = self.isite_billing.SQLHandler(
            self.conn, proc_stmt, 'p_n', 'q_s')
        sqlh_obj.call_procedure = cal_procedure_mock
        sqlh_obj.process('sd', 'ed')
        self.conn.cursor.assert_called_once_with()
        # cr_procedure_mock.assert_called_once()
        cal_procedure_mock.assert_called_once_with('sd', 'ed')


class iSiteBillingCommonTestCase(iSiteBillingBaseTestCase):
    def setUp(self):
        iSiteBillingBaseTestCase.setUp(self)

    def test_siteid(self):
        mock_opn = mock_open(read_data='\n SITEID \n\r')
        with patch('isite_billing.open', mock_opn, create=True):
            site_id = self.isite_billing.get_site_id('test.txt')
            mock_opn.assert_called_once_with('test.txt')
            self.assertEqual(site_id, 'SITEID')

    def test_date_string_without_format(self):
        date_mock = MagicMock(name='datetime')
        date_mock.strftime.return_value = 'abcd'
        default_format = '%Y-%m-%d_%H-%M-%S'
        date_string = self.isite_billing.date_string(date_mock)
        date_mock.strftime.assert_called_once_with(default_format)
        self.assertEqual(date_string, 'abcd')

    def test_date_string_with_format(self):
        date_mock = MagicMock(name='datetime')
        date_mock.strftime.return_value = 'abcd'
        fmt = '%Y-%m'
        date_string = self.isite_billing.date_string(date_mock, fmt)
        date_mock.strftime.assert_called_once_with(fmt)
        self.assertEqual(date_string, 'abcd')

    def test_csv_name(self):
        csv_name_mock = MagicMock(name='csv_name')
        csv_name_mock.format.return_value = 'test.csv'
        self.isite_billing.CSV_NAME = csv_name_mock
        csv_name = self.isite_billing.csv_name('site_id', 'date_string')
        csv_name_mock.format.assert_called_once_with(
            SITEID='site_id', date_str='date_string')
        self.assertEqual(csv_name, 'test.csv')

    def test_csv_get_payload(self):
        csv_name_mock = MagicMock(name='csv_name')
        csv_name_mock.return_value = 'file1.csv'
        date_string_mock = MagicMock(name='date_string')
        date_string_mock.return_value = 'date_string'
        self.isite_billing.csv_name = csv_name_mock
        self.isite_billing.date_string = date_string_mock
        expected_payload = {'studies': [
            's1', 's2'], 'csv_headers': ['h1', 'h2']}
        expected_payload['siteid'] = 'site_id'
        expected_payload['file_name'] = 'file1.csv'
        expected_payload['billing_datetime'] = '2100'
        billing_dtime_mock = MagicMock(name='billing_datetime')
        billing_dtime_mock.isoformat.return_value = '2100'
        resp_pay_load = self.isite_billing.get_payload(
            ['s1', 's2'], 'dt_obj', ['h1', 'h2'], billing_dtime_mock, 'site_id')
        csv_name_mock.assert_called_once_with('site_id', 'date_string')
        date_string_mock.assert_called_once_with('dt_obj')
        billing_dtime_mock.isoformat.assert_called_once_with()
        self.assertEqual(resp_pay_load, expected_payload)

    def test_post_data_defaults(self):
        resp_mock = MagicMock(name='response')
        self.mock_request.post.return_value = resp_mock
        resp = self.isite_billing.post_data('url', 'data')
        self.mock_request.post.assert_called_once_with(
            json='data', timeout=180, url='url', verify=False)
        resp_mock.raise_for_status.assert_called_once_with()
        self.assertEqual(resp, resp_mock)

    def test_post_data_non_defaults(self):
        resp_mock = MagicMock(name='response')
        self.mock_request.post.return_value = resp_mock
        resp = self.isite_billing.post_data('url', 'data', 'headers', 10)
        self.mock_request.post.assert_called_once_with(
            json='data', timeout=10, headers='headers', url='url', verify=False)
        resp_mock.raise_for_status.assert_called_once_with()
        self.assertEqual(resp, resp_mock)

    def test_verify_result_true(self):
        result = self.isite_billing.verify_result(
            ['h1', 'h2', 'h3'], [('v1', 'v2')])
        self.assertEqual(result, True)

    def test_verify_result_false(self):
        result = self.isite_billing.verify_result(['h1', 'h2'], [('v1', 'v2')])
        self.assertEqual(result, False)

    def test_generates_dates(self):
        end_date_mock = MagicMock(name='end_date')
        end_date_mock.__sub__.return_value = 'end_date'
        date_mock_obj = MagicMock(name='dateobj')
        date_mock_obj.hour = 1
        date_mock_obj.minute = 2
        date_mock_obj.second = 3
        date_mock_obj.__sub__.return_value = end_date_mock
        dates = self.isite_billing.generate_dates(date_mock_obj)
        self.assertEqual(dates, ('end_date', end_date_mock))
        timedelta_exp_call = [
            call(hours=1, minutes=2, seconds=3), call(days=1)]
        self.assertEqual(timedelta_exp_call,
                         self.mock_datetime.timedelta.mock_calls)

    def test_get_base_url(self):
        self.assertEqual(self.isite_billing.get_base_url(
            'http://baseurl'), 'http://baseurl/')
        self.assertEqual(self.isite_billing.get_base_url(
            'http:/baseurl/'), 'http:/baseurl/')

    def test_get_url(self):
        get_url = self.isite_billing.get_url('http://base/', 'site123')
        self.assertEqual(get_url, 'http://base/status/site123')

    def test_post_url(self):
        self.assertEqual(self.isite_billing.post_url(
            'http://base/'), 'http://base/upload')

    def test_iso_to_datetime(self):
        self.mock_parse.parse.return_value = 'datetimeobject'
        self.assertEqual(self.isite_billing.iso_to_datetime(
            'iso_str'), 'datetimeobject')
        self.mock_parse.parse.assert_called_once_with('iso_str')

    def test_get_end_date(self):
        date_time_mock = MagicMock(name='date_time')
        date_time_mock.hour, date_time_mock.minute = 12, 34
        date_time_mock.second, date_time_mock.microsecond = 45, 900000
        self.mock_datetime.timedelta.return_value = 'time_delta'
        date_time_mock.__add__.return_value = 'end_date'
        self.assertEqual(self.isite_billing.get_end_date(
            date_time_mock), 'end_date')
        self.mock_datetime.timedelta.assert_called_once_with(
            hours=11, microseconds=100000, minutes=25, seconds=14)
        date_time_mock.__add__.assert_called_once_with('time_delta')

    def test_billing_dates(self):
        resp = [True, False]

        def _side_effect(*args, **kwars):
            return resp.pop(0)

        begin_date_mock = MagicMock(name='begin_date')
        end_date_mock = MagicMock(name='end_date')
        bill_date_mock = MagicMock(name='bill_date')
        bill_date_mock.__lt__.side_effect = _side_effect
        begin_date_mock.__add__.return_value = bill_date_mock
        self.mock_datetime.timedelta.return_value = 'time_delta'
        timedelta_exp_call = [call(days=1), call(days=1)]
        bill_dates = self.isite_billing.billing_dates(
            begin_date_mock, end_date_mock)
        self.assertEqual(bill_dates.next(), bill_date_mock)
        self.assertRaises(StopIteration, bill_dates.next)
        self.assertEqual(timedelta_exp_call,
                         self.mock_datetime.timedelta.mock_calls)


class iSiteBillingGetPreviousBillingDateTestCase(iSiteBillingBaseTestCase):
    def setUp(self):
        iSiteBillingBaseTestCase.setUp(self)
        self.get_response_mock = MagicMock(name='get_response')
        self.iso_to_datetime_mock = MagicMock(name='get_response')
        self.iso_to_datetime_mock.return_value = 'recent_date'
        self.isite_billing.iso_to_datetime = self.iso_to_datetime_mock

    def test_get_previous_billing_date(self):
        self.get_response_mock.json.return_value = {
            'billing_datetime': 'bill_date'}
        self.mock_request.get.return_value = self.get_response_mock
        response = self.isite_billing.get_previous_billing_date('get_url')
        self.assertEqual(response, 'recent_date')
        self.get_response_mock.raise_for_status.assert_called_once_with()
        self.get_response_mock.json.assert_called_once_with()
        self.iso_to_datetime_mock.assert_called_once_with('bill_date')
        self.mock_request.get.assert_called_once_with('get_url')

    def test_get_previous_billing_date_empty_response(self):
        self.get_response_mock.json.return_value = {}
        self.mock_request.get.return_value = self.get_response_mock
        response = self.isite_billing.get_previous_billing_date('get_url')
        self.assertEqual(response, {})


class iSiteBillingGetBeginDate(iSiteBillingBaseTestCase):
    def setUp(self):
        iSiteBillingBaseTestCase.setUp(self)
        self.previous_billing_mock = MagicMock(
            name='get_previous_billing_date')
        self.isite_billing.get_previous_billing_date = self.previous_billing_mock
        self.present_date_mock = MagicMock(name='present_date')

    def test_begin_date_empty_previous_date(self):
        self.previous_billing_mock.return_value = {}
        self.present_date_mock.__sub__.return_value = 'subtracted_val'
        self.mock_datetime.timedelta.return_value = 'timedelta'
        result = self.isite_billing.get_begin_date(
            'url', self.present_date_mock)
        self.assertEqual(result, 'subtracted_val')
        self.mock_datetime.timedelta.assert_called_once_with(days=1)
        self.previous_billing_mock.assert_called_once_with('url')
        self.present_date_mock.__sub__.assert_called_once_with('timedelta')

    def test_begin_date_lesser_previous_date(self):
        previous_respone_mock = MagicMock()
        previous_respone_mock.__lt__.return_value = True
        self.previous_billing_mock.return_value = previous_respone_mock
        self.present_date_mock.__sub__.return_value = 'subtracted_val'
        result = self.isite_billing.get_begin_date(
            'url', self.present_date_mock)
        self.assertEqual(result, 'subtracted_val')
        self.mock_datetime.timedelta.assert_called_once_with(days=30)
        previous_respone_mock.__lt__.assert_called_once_with('subtracted_val')

    def test_begin_date_grater_previous_date(self):
        previous_respone_mock = MagicMock()
        previous_respone_mock.__lt__.return_value = False
        self.previous_billing_mock.return_value = previous_respone_mock
        self.present_date_mock.__sub__.return_value = 'subtracted_val'
        result = self.isite_billing.get_begin_date(
            'url', self.present_date_mock)
        self.assertEqual(result, previous_respone_mock)
        self.mock_datetime.timedelta.assert_called_once_with(days=30)
        previous_respone_mock.__lt__.assert_called_once_with('subtracted_val')


class iSiteBillingCustomSerializerTestCase(iSiteBillingBaseTestCase):
    def setUp(self):
        iSiteBillingBaseTestCase.setUp(self)
        self.instance_mock = MagicMock(name='isinstance')
        self.str_mock = MagicMock(name='str', return_value='sss')
        self.isite_billing.str = self.str_mock
        self.float_mock = MagicMock(name='float', return_value=1.0)
        self.isite_billing.float = self.float_mock
        self.isite_billing.isinstance = self.instance_mock

    def test_custom_serializer_date(self):
        def _side_effect(*args, **kwars):
            s_dict = {'date': True, 'decimal': False}
            return s_dict[args[0]]
        self.instance_mock.side_effect = _side_effect
        return_value = self.isite_billing.custom_serializer('date')
        self.str_mock.assert_called_once_with('date')
        self.assertEqual(return_value, 'sss')
        self.assertEqual(self.instance_mock.call_count, 1)
        self.assertEqual(self.float_mock.call_count, 0)

    def test_custom_serializer_decimal(self):
        resp = [False, True]

        def _side_effect(*args, **kwars):
            return resp.pop(0)

        self.instance_mock.side_effect = _side_effect
        return_value = self.isite_billing.custom_serializer('decimal')
        self.float_mock.assert_called_once_with('decimal')
        self.assertEqual(return_value, 1.0)
        self.assertEqual(self.instance_mock.call_count, 2)
        self.assertEqual(self.str_mock.call_count, 0)

    def test_custom_serializer_exception(self):
        resp = [False, False]

        def _side_effect(*args, **kwars):
            return resp.pop(0)
        self.instance_mock.side_effect = _side_effect
        self.assertRaises(
            TypeError, self.isite_billing.custom_serializer, 'arg1')
        self.assertEqual(self.instance_mock.call_count, 2)
        self.assertEqual(self.str_mock.call_count, 0)
        self.assertEqual(self.float_mock.call_count, 0)


class iSiteBillingDoBillingTestCase(iSiteBillingBaseTestCase):
    def setUp(self):
        iSiteBillingBaseTestCase.setUp(self)
        self.sql_handler_mock = MagicMock(name='SQLHandler')
        self.sql_handler_mock.process.return_value = ['s1', 's2']
        self.isite_billing.CSV_HEADERS = ['h1', 'h2']
        self.frm_date_mock = MagicMock(name='from_date')
        self.frm_date_mock.strftime.return_value = 'strftime'
        self.end_date_mock = MagicMock(name='end_date')
        self.generate_dates_mock = MagicMock(name='generate_dates')
        self.generate_dates_mock.return_value = self.frm_date_mock, self.end_date_mock
        self.isite_billing.generate_dates = self.generate_dates_mock
        self.date_string_mock = MagicMock(name='date_string')
        self.isite_billing.date_string = self.date_string_mock
        self.verify_result_mock = MagicMock(name='verify_result')
        self.verify_result_mock.return_value = True
        self.isite_billing.verify_result = self.verify_result_mock
        self.payload_mock = MagicMock(name='get_payload')
        self.payload_mock.return_value = 'payload'
        self.isite_billing.get_payload = self.payload_mock
        self.isite_billing.custom_serializer = 'custom_serializer'
        self.postdata_mock = MagicMock(name='post_data')
        self.isite_billing.post_data = self.postdata_mock

    def test_do_billing_verify_result_true(self):
        resp = ['frm_date_str', 'end_date_str']

        def _side_effect(*args, **kwars):
            return resp.pop(0)
        self.date_string_mock.side_effect = _side_effect
        date_mock_exp_call = [call(self.frm_date_mock, formt='%Y-%m-%d %H:%M:%S'),
                              call(self.end_date_mock, formt='%Y-%m-%d %H:%M:%S')]
        self.mock_json.dumps.return_value = 'json_dumps'
        status, message = self.isite_billing.do_billing(
            self.sql_handler_mock, 'billing_date', 'site_id', 'post_url')
        self.generate_dates_mock('billing_date')
        self.assertEqual(self.date_string_mock.mock_calls, date_mock_exp_call)
        self.sql_handler_mock.process.assert_called_once_with(
            'frm_date_str', 'end_date_str')
        self.verify_result_mock.assert_called_once_with(
            ['h1', 'h2'], ['s1', 's2'])
        self.mock_json.dumps.assert_called_once_with(
            'payload', default='custom_serializer')
        self.postdata_mock.assert_called_once_with('post_url', 'json_dumps')
        self.frm_date_mock.strftime.assert_called_once_with('%Y-%b-%d')
        self.assertEqual((status, message),
                         (0, 'OK, Rows uploaded for strftime is - 2'))

    def test_do_billing_verify_result_false(self):
        resp = ['frm_date_str', 'end_date_str']

        def _side_effect(*args, **kwars):
            return resp.pop(0)
        self.verify_result_mock.return_value = False
        self.date_string_mock.side_effect = _side_effect
        date_mock_exp_call = [call(self.frm_date_mock, formt='%Y-%m-%d %H:%M:%S'),
                              call(self.end_date_mock, formt='%Y-%m-%d %H:%M:%S')]
        self.mock_json.dumps.return_value = 'json_dumps'
        status, message = self.isite_billing.do_billing(
            self.sql_handler_mock, 'billing_date', 'site_id', 'post_url')
        self.generate_dates_mock('billing_date')
        self.assertEqual(self.date_string_mock.mock_calls, date_mock_exp_call)
        self.sql_handler_mock.process.assert_called_once_with(
            'frm_date_str', 'end_date_str')
        self.verify_result_mock.assert_called_once_with(
            ['h1', 'h2'], ['s1', 's2'])
        self.frm_date_mock.strftime.assert_called_once_with('%Y-%b-%d')
        self.assertEqual((status, message),
                         (2, 'CRITICAL - Header length does not match with result row\
         length for the date strftime'))


class iSiteBillingMainTestCase(iSiteBillingBaseTestCase):
    def setUp(self):
        iSiteBillingBaseTestCase.setUp(self)
        self.sql_handler_mock = MagicMock(name='SQLHandler')
        self.isite_billing.SQLHandler = self.sql_handler_mock
        self.base_url_mock = MagicMock(name='get_base_url')
        self.base_url_mock.return_value = 'base_url'
        self.isite_billing.get_base_url = self.base_url_mock
        self.site_id_mock = MagicMock(name='get_site_id')
        self.site_id_mock.return_value = 'site123'
        self.isite_billing.get_site_id = self.site_id_mock
        self.get_url_mock = MagicMock(name='get_url')
        self.get_url_mock.return_value = 'http://url'
        self.isite_billing.get_url = self.get_url_mock
        self.begin_date_mock = MagicMock(name='get_begin_date')
        self.begin_date_mock.return_value = 'datetime'
        self.isite_billing.get_begin_date = self.begin_date_mock
        self.end_date_mock = MagicMock(name='get_end_date')
        self.end_date_mock.return_value = 'end_date'
        self.isite_billing.get_end_date = self.end_date_mock
        self.billing_dates_mock = MagicMock(name='billing_dates')
        self.billing_dates_mock.return_value = ['dt1']
        self.isite_billing.billing_dates = self.billing_dates_mock
        self.post_url_mock = MagicMock(name='post_url')
        self.post_url_mock.return_value = 'post_url'
        self.isite_billing.post_url = self.post_url_mock
        self.do_billing_mock = MagicMock(name='do_billing')
        self.do_billing_mock.return_value = 0, 'message'
        self.isite_billing.do_billing = self.do_billing_mock
        self.mock_datetime.datetime.utcnow.return_value = 'utcnow'
        self.cmd_args_mock = MagicMock(name="cmd_args")
        config = {'appurl': 'appurl', 'hostaddress': 'hostaddress', 'username': 'username',
                  'password': 'password', 'database': 'database'}
        self.cmd_args_mock.configure_mock(**config)

    # @patch('sys.stdout', new_callable=StringIO)
    def test_main_normal_flow(self):
        self.mock_pymssql.connect.return_value.__enter__.return_value = 'sqlcon'
        self.isite_billing.PROCEDURE_STMT = "proc_s"
        self.isite_billing.PROCEDURE_NAME = "proc_n"
        self.isite_billing.QUERY_STMT = "query_s"
        response = self.isite_billing.main(self.cmd_args_mock)
        self.assertEqual((0, 'RECOVERY', ['message']), response)
        self.base_url_mock.assert_called_once_with('appurl')
        self.site_id_mock.assert_called_once_with('/etc/siteid')
        self.get_url_mock.assert_called_once_with('base_url', 'site123')
        self.begin_date_mock.assert_called_once_with('http://url', 'utcnow')
        self.mock_pymssql.connect.assert_called_once_with(
            'hostaddress', 'username', 'password', 'database')
        self.sql_handler_mock.assert_called_once_with(
            'sqlcon', 'proc_s', 'proc_n', 'query_s')
        self.sql_handler_mock().create_procedure.assert_called_once_with()
        self.end_date_mock.assert_called_once_with('utcnow')
        self.assertEqual(self.mock_datetime.datetime.utcnow.call_count, 2)
        self.billing_dates_mock.assert_called_once_with('datetime', 'end_date')
        self.post_url_mock.assert_called_once_with('base_url')
        self.do_billing_mock.assert_called_once_with(
            self.sql_handler_mock(), 'dt1', 'site123', 'post_url')

    def test_main_no_billing_date(self):
        recent_succes_date_mock = MagicMock(name='recent_succes_date')
        recent_succes_date_mock.strftime.return_value = 'today'
        self.begin_date_mock.return_value = recent_succes_date_mock
        self.billing_dates_mock.return_value = []
        self.mock_pymssql.connect.return_value.__enter__.return_value = 'sqlcon'
        self.isite_billing.PROCEDURE_STMT = "proc_s"
        self.isite_billing.PROCEDURE_NAME = "proc_n"
        self.isite_billing.QUERY_STMT = "query_s"
        exp_respone = (
            0, 'RECOVERY', 'OK - Billing already done for - today OR its a future date')
        self.assertEqual(self.isite_billing.main(
            self.cmd_args_mock), exp_respone)
        self.base_url_mock.assert_called_once_with('appurl')
        self.site_id_mock.assert_called_once_with('/etc/siteid')
        self.get_url_mock.assert_called_once_with('base_url', 'site123')
        self.begin_date_mock.assert_called_once_with('http://url', 'utcnow')
        self.mock_pymssql.connect.assert_called_once_with(
            'hostaddress', 'username', 'password', 'database')
        self.sql_handler_mock.assert_called_once_with(
            'sqlcon', 'proc_s', 'proc_n', 'query_s')
        self.sql_handler_mock().create_procedure.assert_called_once_with()
        self.end_date_mock.assert_called_once_with('utcnow')
        self.assertEqual(self.mock_datetime.datetime.utcnow.call_count, 2)
        self.billing_dates_mock.assert_called_once_with(
            recent_succes_date_mock, 'end_date')
        self.assertEqual(self.do_billing_mock.call_count, 0)

    # @patch('sys.stdout', new_callable=StringIO)
    def test_main_pymssql_error(self):
        self.mock_pymssql.connect.side_effect = pymssql.Error('Error')
        self.assertEqual(self.isite_billing.main(self.cmd_args_mock),
                         (2, 'PROBLEM', 'CRITICAL - Error, Error'))

    # @patch('sys.stdout', new_callable=StringIO)
    def test_main_requests_error(self):
        self.mock_pymssql.connect.return_value.__enter__.return_value = 'sqlcon'
        self.isite_billing.PROCEDURE_STMT = "proc_s"
        self.isite_billing.PROCEDURE_NAME = "proc_n"
        self.isite_billing.QUERY_STMT = "query_s"
        self.do_billing_mock.side_effect = requests.exceptions.RequestException(
            'Request Error')
        self.assertEqual(self.isite_billing.main(self.cmd_args_mock),
                         (2, 'PROBLEM', 'CRITICAL - Error, Request Error'))

    # @patch('sys.stdout', new_callable=StringIO)
    def test_main_exception(self):
        self.base_url_mock.side_effect = Exception('Exception')
        self.assertEqual(self.isite_billing.main(self.cmd_args_mock),
                         (2, 'PROBLEM', 'CRITICAL - Error, Exception'))


if __name__ == '__main__':
    unittest.main()
