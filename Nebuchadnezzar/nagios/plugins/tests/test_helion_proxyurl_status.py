import unittest
import requests
from mock import MagicMock, patch


class HelionProxyURLStatusTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_phimutils = MagicMock(name='phimutils')
        self.mock_scanline = MagicMock(name='scanline')
        self.mock_phimutils.collection.DictNoEmpty = dict
        self.OBJSTMock = MagicMock(name='blah')
        modules = {
            'requests': self.mock_request,
            'phimutils': self.mock_phimutils,
            'phimutils.message': self.mock_phimutils.message,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.hsdp_obs': self.mock_scanline.utilities.hsdp_obs,
            'scanline.utilities.hsdp_obs.OBJSTConnection': self.OBJSTMock,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_helion_proxyurl_status
        from scanline.utilities import hsdp_obs
        self.OBJSTConnection = hsdp_obs.OBJSTConnection()
        self.check_helion_proxyurl_status = check_helion_proxyurl_status
        from check_helion_proxyurl_status import get_stored_data
        from check_helion_proxyurl_status import get_helion_proxy_url_status
        self.get_helion_proxy_url_status = get_helion_proxy_url_status
        self.get_stored_data = get_stored_data

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_stored_data_ok(self):
        self.OBJSTConnection.to_dict.return_value = {1:'hello'}
        self.assertEqual(self.get_stored_data(), {1:'hello'})

    def test_stored_data_exception(self):
        self.OBJSTConnection.to_dict.return_value = {}
        self.assertEqual(self.get_stored_data(), {})

    @patch('check_helion_proxyurl_status.requests.get')
    def test_get_keystone_server_status_ok(self, mock_get):
        mock_get.return_value.status_code = 200
        stored_data = {'token_issued_at': u'2017-08-03T11:11:20.850448Z',
                       'token_id': u'db55f21b592f4c56b671039ed2841625',
                       'keystone_url': u'https://localhost:5000/v2.0/tokens',
                       'obs_url': u'https://localhost:8080/v1/AUTH_4c2bc62d02ef42efbc9a27036113a1f9',
                       'payload': u"{'auth': {'passwordCredentials':{'username"
                                  u"' : 'username', 'password' : 'password'},"
                                  u"'tenantName':'tenent1'}}", 'token_expires_at': u'2017-08-03T15:11:20Z'}
        self.assertEqual(self.get_helion_proxy_url_status(stored_data),
                         (0, 'Proxy URL is reachable.'))

    @patch('check_helion_proxyurl_status.requests.get')
    def test_get_keystone_server_status_unrechable(self, mock_get):
        mock_get.return_value.status_code = 404
        stored_data = {'token_issued_at': u'2017-08-03T11:11:20.850448Z',
                       'token_id': u'db55f21b592f4c56b671039ed2841625',
                       'keystone_url': u'https://localhost:5000/v2.0/tokens',
                       'obs_url': u'https://localhost:8080/v1/AUTH_4c2bc62d02ef42efbc9a27036113a1f9',
                       'payload': u"{'auth': {'passwordCredentials':{'username"
                                  u"' : 'username', 'password' : 'password'},"
                                  u"'tenantName':'tenent1'}}", 'token_expires_at': u'2017-08-03T15:11:20Z'}
        self.assertEqual(self.get_helion_proxy_url_status(stored_data),
                         (2, 'Proxy URL is not reachable, status code: 404'))

    @patch('check_helion_proxyurl_status.requests.get')
    def test_get_keystone_server_status_exception(self, mock_post):
        stored_data = {'token_issued_at': u'2017-08-03T11:11:20.850448Z',
                       'token_id': u'db55f21b592f4c56b671039ed2841625',
                       'keystone_url': u'https://localhost:5000/v2.0/tokens',
                       'obs_url': u'https://localhost:8080/v1/AUTH_4c2bc62d02ef42efbc9a27036113a1f9',
                       'payload': u"{'auth': {'passwordCredentials':{'username"
                                  u"' : 'username', 'password' : 'password'},"
                                  u"'tenantName':'tenent1'}}",
                       'token_expires_at': u'2017-08-03T15:11:20Z'}
        mock_post.side_effect = requests.exceptions.ConnectionError
        self.assertEqual(self.get_helion_proxy_url_status(stored_data),
                         (2, 'Invalid get request.'))


if __name__ == '__main__':
    unittest.main()
