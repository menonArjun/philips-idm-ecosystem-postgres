import unittest
from requests.exceptions import HTTPError
from mock import MagicMock, patch


class CheckTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        modules = {
            'requests': self.mock_request,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_http_auth
        self.check_http_auth = check_http_auth
        from check_http_auth import get_http_with_auth
        self.get_http_with_auth = get_http_with_auth

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    @patch('check_http_auth.requests')
    def test_get_http_with_auth(self, mock_request):
        mock_request.get.return_value.status_code = 200
        self.assertEqual(self.get_http_with_auth('127.0.0.1', '/admin/'), (0, '200 - Service is up'))

    @patch('requests.get')
    def test_get_http_with_auth_with(self, mock_get):
        mock_resp = MagicMock()
        mock_resp.raise_for_status = MagicMock()
        mock_resp.raise_for_status.side_effect = HTTPError("google is down")
        mock_resp.status_code = 500
        mock_resp.content = ('unable to connect')
        mock_get.return_value = mock_resp
        self.assertEqual(self.get_http_with_auth('127.0.0.1', '/admin/'), (2, 'could not post due to request error'))

if __name__ == '__main__':
    unittest.main()
