import unittest
from mock import MagicMock, patch


class HelionKeystoneServerStatusTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_phimutils = MagicMock(name='phimutils')
        self.mock_scanline = MagicMock(name='scanline')
        self.mock_phimutils.collection.DictNoEmpty = dict
        modules = {
            'requests': self.mock_request,
            'phimutils': self.mock_phimutils,
            'phimutils.message': self.mock_phimutils.message,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_rabbitmq_heartbeat_status
        self.check_rabbitmq_heartbeat_status = check_rabbitmq_heartbeat_status
        from check_rabbitmq_heartbeat_status import form_header
        from check_rabbitmq_heartbeat_status import create_queue
        from check_rabbitmq_heartbeat_status import bind_queue
        from check_rabbitmq_heartbeat_status import post_message
        from check_rabbitmq_heartbeat_status import get_message
        self.form_header = form_header
        self.create_queue = create_queue
        self.bind_queue = bind_queue
        self.post_message = post_message
        self.get_message = get_message


    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_form_header(self):
        username, password = 'guest', 'guest'
        self.assertEqual(self.form_header(username, password), 
                         ({'content-type': 'application/json'}, ('guest', 'guest')))

    @patch('check_rabbitmq_heartbeat_status.requests.put')
    def test_create_queue_ok(self, mock_put):
        mock_put.return_value.status_code = 204
        headers = {'content-type': 'application/json'}
        auth = ('guest', 'guest')
        hostname = 'rabbitmq.isyntax.net'
        port = '15671'
        queue_name = 'idm_test_q'
        self.assertEqual(self.create_queue(headers, auth, hostname, port,
                                           queue_name), (0, 'Queue created successfully.'))

    @patch('check_rabbitmq_heartbeat_status.requests.put')
    def test_create_queue_404(self, mock_put):
        mock_put.return_value.status_code = 404
        headers = {'content-type': 'application/json'}
        auth = ('guest', 'guest')
        hostname = 'rabbitmq.isyntax.net'
        port = '15671'
        queue_name = 'idm_test_q'
        self.assertEqual(self.create_queue(headers, auth, hostname, port,
                                           queue_name),
                         (2, 'Could not create queue: idm_test_q, status code: 404'))

    @patch('check_rabbitmq_heartbeat_status.requests.put')
    def test_create_queue_exception(self, mock_put):
        headers = {'content-type': 'application/json'}
        auth = ('guest', 'guest')
        hostname = 'rabbitmq.isyntax.net'
        port = '15671'
        queue_name = 'idm_test_q'
        mock_put.side_effect = Exception
        self.assertEqual(self.create_queue(headers, auth, hostname, port,
                                           queue_name),
                         (2, 'Invalid request, could not create queue: idm_test_q'))

    @patch('check_rabbitmq_heartbeat_status.requests.post')
    def test_bind_queue_ok(self, mock_post):
        mock_post.return_value.status_code = 201
        headers = {'content-type': 'application/json'}
        auth = ('guest', 'guest')
        hostname = 'rabbitmq.isyntax.net'
        port = '15671'
        source_exchange = 'PreFetch.Topic'
        queue_name = 'idm_test_q'
        routing_key = 'Key1'
        self.assertEqual(self.bind_queue(headers, auth, hostname, port,
                                         source_exchange, queue_name,
                                         routing_key), (0, 'Queue binded successfully.'))

    @patch('check_rabbitmq_heartbeat_status.requests.post')
    def test_bind_queue_404(self, mock_post):
        mock_post.return_value.status_code = 404
        headers = {'content-type': 'application/json'}
        auth = ('guest', 'guest')
        hostname = 'rabbitmq.isyntax.net'
        port = '15671'
        source_exchange = 'PreFetch.Topic'
        queue_name = 'idm_test_q'
        routing_key = 'Key1'
        self.assertEqual(self.bind_queue(headers, auth, hostname, port,
                                         source_exchange, queue_name,
                                         routing_key),
                         (2, 'Could not bind idm_test_q queue, status code: 404'))

    @patch('check_rabbitmq_heartbeat_status.requests.post')
    def test_bind_queue_exception(self, mock_post):
        headers = {'content-type': 'application/json'}
        auth = ('guest', 'guest')
        hostname = 'rabbitmq.isyntax.net'
        port = '15671'
        source_exchange = 'PreFetch.Topic'
        queue_name = 'idm_test_q'
        routing_key = 'Key1'
        mock_post.side_effect = Exception
        self.assertEqual(self.bind_queue(headers, auth, hostname, port,
                                         source_exchange, queue_name,
                                         routing_key),
                         (2, 'Invalid post request for idm_test_q queue.'))

    @patch('check_rabbitmq_heartbeat_status.requests.post')
    def test_post_message_ok(self, mock_post):
        mock_post.return_value.status_code = 200
        headers = {'content-type': 'application/json'}
        auth = ('guest', 'guest')
        hostname = 'rabbitmq.isyntax.net'
        port = '15671'
        source_exchange = 'PreFetch.Topic'
        queue_name = 'idm_test_q'
        routing_key = 'Key1'
        self.assertEqual(self.post_message(headers, auth, hostname, port,
                                           source_exchange, routing_key,
                                           queue_name), (0, 'Message posted successfully.'))

    @patch('check_rabbitmq_heartbeat_status.requests.post')
    def test_post_message_404(self, mock_post):
        mock_post.return_value.status_code = 404
        headers = {'content-type': 'application/json'}
        auth = ('guest', 'guest')
        hostname = 'rabbitmq.isyntax.net'
        port = '15671'
        source_exchange = 'PreFetch.Topic'
        queue_name = 'idm_test_q'
        routing_key = 'Key1'
        self.assertEqual(self.post_message(headers, auth, hostname, port,
                                           source_exchange, routing_key,
                                           queue_name),
                         (2, 'Could not post message to idm_test_q queue, status code: 404'))

    @patch('check_rabbitmq_heartbeat_status.requests.post')
    def test_post_message_exception(self, mock_post):
        headers = {'content-type': 'application/json'}
        auth = ('guest', 'guest')
        hostname = 'rabbitmq.isyntax.net'
        port = '15671'
        source_exchange = 'PreFetch.Topic'
        queue_name = 'idm_test_q'
        routing_key = 'Key1'
        mock_post.side_effect = Exception
        self.assertEqual(self.post_message(headers, auth, hostname, port,
                                           source_exchange, routing_key,
                                           queue_name),
                         (2, 'Invalid post request for idm_test_q queue.'))

    @patch('check_rabbitmq_heartbeat_status.requests.post')
    def test_get_message_ok(self, mock_post):
        mock_post.return_value.status_code = 200
        headers = {'content-type': 'application/json'}
        auth = ('guest', 'guest')
        hostname = 'rabbitmq.isyntax.net'
        port = '15671'
        queue_name = 'idm_test_q'
        timeout = 3
        self.assertEqual(self.get_message(headers, auth, hostname, port,
                                          queue_name, timeout),
                         (0, 'RabbitMQ Heartbeat is up.'))

    @patch('check_rabbitmq_heartbeat_status.requests.post')
    def test_get_message_404(self, mock_post):
        mock_post.return_value.status_code = 404
        headers = {'content-type': 'application/json'}
        auth = ('guest', 'guest')
        hostname = 'rabbitmq.isyntax.net'
        port = '15671'
        queue_name = 'idm_test_q'
        timeout = 3
        self.assertEqual(self.get_message(headers, auth, hostname, port,
                                          queue_name, timeout),
                         (2, 'Could not fetch message from idm_test_q queue, status code: 404'))

    @patch('check_rabbitmq_heartbeat_status.requests.post')
    def test_get_message_exception(self, mock_post):
        mock_post.return_value.status_code = 404
        headers = {'content-type': 'application/json'}
        auth = ('guest', 'guest')
        hostname = 'rabbitmq.isyntax.net'
        port = '15671'
        queue_name = 'idm_test_q'
        timeout = 3
        mock_post.side_effect = Exception
        self.assertEqual(self.get_message(headers, auth, hostname, port,
                                          queue_name, timeout),
                         (2, 'Invalid post(get message) request for idm_test_q queue.'))


if __name__ == '__main__':
    unittest.main()
