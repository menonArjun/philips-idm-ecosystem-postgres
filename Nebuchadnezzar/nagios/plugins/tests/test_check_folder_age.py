import unittest
from mock import MagicMock, patch


class CheckTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_phimutils = MagicMock(name='phimutils')
        self.mock_scanline = MagicMock(name='scanline')
        self.datetime = MagicMock(name='datetime')
        modules = {
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.wmi': self.mock_scanline.wmi,
            'scanline.utilities.dns': self.mock_scanline.dns,
            'phimutils.timestamp': self.mock_phimutils.timestamp,
            
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_folder_age
        self.check_folder_age = check_folder_age
        from check_folder_age import wmi_folder_age
        self.wmi_folder_age = wmi_folder_age
        from scanline.utilities.wmi import WMIHostDriller
        self.WMIHostDriller = WMIHostDriller(host='127.0.0.1', user='user', password='pass', domain='Domain')
        from phimutils.timestamp import wmi_datetime
        self.wmi_datetime = wmi_datetime

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_wmi_folder_age(self):
        self.wmi_datetime.return_value = '1234567'
        self.mock_scanline.dns.get_address = MagicMock('get_address', return_value='127.0.0.1')
        self.WMIHostDriller.query_wmi.return_value = iter([1,2,3])
        self.assertEqual(self.wmi_folder_age(720, 'minutes', 'Domain', '127.0.0.1', 'user', 'pass', 'C:', '/test/'),\
                         (0, 'OK - 3 Backup found compared to timestamp 1234567')
                        )

    def test_wmi_folder_age_okstatus(self):
        self.wmi_datetime.return_value = '1234567'
        self.mock_scanline.dns.get_address = MagicMock('get_address', return_value='127.0.0.1')
        self.WMIHostDriller.query_wmi.return_value = iter([])
        self.assertEqual(self.wmi_folder_age(720, 'minutes', 'Domain', '127.0.0.1', 'user', 'pass', 'C:', '/test/'),\
                         (2, 'CRITICAL - 0 Backup found compared to timestamp 1234567')
                        )


if __name__ == '__main__':
    unittest.main()
