import sys
import unittest
from mock import MagicMock, patch


sys.modules['requests'] = MagicMock(name='requests')
sys.modules['phimutils.perfdata'] = MagicMock(name='phimutils.perfdata')
sys.modules['phimutils.perfdata'].INFO_MARKER = '++I#'
sys.modules['phimutils'] = MagicMock(name='phimutils')

sys.modules['scanline.component.isp'] = MagicMock(name='scanline.component.isp')
sys.modules['scanline.component'] = MagicMock(name='scanline.component')
sys.modules['scanline'] = MagicMock(name='scanline')

import check_anywhere


class CheckAnywhereTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.patch_anywhere = patch('check_anywhere.AnywhereComponent')
        self.anywhere_mock = self.patch_anywhere.start()

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.patch_anywhere.stop()

    def test_check_anywhere_ok(self):
        self.anywhere_mock.return_value.scan.return_value = '1.0.2.3'
        self.assertEqual(check_anywhere.check_anywhere('host1'), (0, 'Found ++I# Version = 1.0.2.3'))

    def test_check_anywhere_version_not_found(self):
        self.anywhere_mock.return_value.scan.return_value = None
        self.assertEqual(check_anywhere.check_anywhere('host1'),
                         (2, "Could not get version from host 'host1'"))


if __name__ == '__main__':
    unittest.main()
