import unittest
import requests
from mock import MagicMock, patch


class CheckHttpsWSCCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        modules = {
            'requests': self.mock_request,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_https_wsc
        self.check_https_wsc = check_https_wsc
        from check_https_wsc import get_https_process
        from check_https_wsc import get_str_from_xml
        self.get_https_process = get_https_process
        self.get_str_from_xml = get_str_from_xml


    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_str_from_xml(self):
        text = '<?xml version="1.0" encoding="utf-8"?><string xmlns="http:' \
               '//tempuri.org/">AWSNagios-wsc v1.0 Result: Disks Okay&lt;br&gt;' \
               'C:23.7GB/59.9GB (39.6% free)&lt;br&gt;S:39.3GB/40.0GB (98.3% ' \
               'free)</string>'
        self.assertEqual(self.get_str_from_xml(text), 'C:23.7GB/59.9GB (39.6% free)')

    def test_get_https_process_incorrect_option(self):
        self.assertEqual(self.get_https_process('localhost', 'option',
                                                'params'),
                         (2, 'Incorrect option.'))

    @patch('check_https_wsc.requests.get')
    def test_get_https_process_okay(self, mock_get):
        response = '<?xml version="1.0" encoding="utf-8"?>\r\n<string ' \
                   'xmlns="http://tempuri.org/">AWSNagios-wsc v1.0 Result: ' \
                   'Disks Okay&lt;br&gt;C:23.7GB/59.9GB (39.6% free)&lt;br&gt;' \
                   'S:39.3GB/40.0GB (98.3% free)</string>'
        mock_get.return_value.status_code = 200
        mock_get.return_value.text = response
        self.assertEqual(self.get_https_process('localhost', 'disks', '20,5'),
                         (0, 'C:23.7GB/59.9GB (39.6% free)'))

    @patch('check_https_wsc.requests.get')
    def test_get_https_process_critical(self, mock_get):
        response = '<?xml version="1.0" encoding="utf-8"?>\r\n<string xmlns=' \
                   '"http://tempuri.org/">AWSNagios-wsc v1.0 Result: Disks ' \
                   'critical&lt;br&gt;C:23.7GB/59.9GB (9.6% free)&lt;br&gt;S:' \
                   '39.3GB/40.0GB (9.3% free)</string>'
        mock_get.return_value.status_code = 200
        mock_get.return_value.text = response
        self.assertEqual(self.get_https_process('localhost', 'disks', '20,5'),
                         (2, 'C:23.7GB/59.9GB (9.6% free)'))

    @patch('check_https_wsc.requests.get')
    def test_get_https_process_warning(self, mock_get):
        response = '<?xml version="1.0" encoding="utf-8"?>\r\n<string xmlns=' \
                   '"http://tempuri.org/">AWSNagios-wsc v1.0 Result: Disks ' \
                   'warning&lt;br&gt;C:23.7GB/59.9GB (9.6% free)&lt;br&gt;' \
                   'S:39.3GB/40.0GB (9.3% free)</string>'
        mock_get.return_value.status_code = 200
        mock_get.return_value.text = response
        self.assertEqual(self.get_https_process('localhost', 'disks', '20,5'),
                         (1, 'C:23.7GB/59.9GB (9.6% free)'))

    @patch('check_https_wsc.requests.get')
    def test_get_https_process_not_running(self, mock_get):
        response = '<?xml version="1.0" encoding="utf-8"?>\r\n<string xmlns=' \
                   '"http://tempuri.org/">AWSNagios-wsc v1.0 Result: Disks not ' \
                   'running&lt;br&gt;C:23.7GB/59.9GB (9.6% free)&lt;br&gt;' \
                   'S:39.3GB/40.0GB (9.3% free)</string>'
        mock_get.return_value.status_code = 200
        mock_get.return_value.text = response
        self.assertEqual(self.get_https_process('localhost', 'disks', '20,5'),
                         (2, 'Process not running.'))

    @patch('check_https_wsc.requests.get')
    def test_get_https_process_not_running(self, mock_get):
        mock_get.side_effect = requests.exceptions.ConnectionError
        self.assertEqual(self.get_https_process('localhost', 'disks', '20,5'),
                         (2, 'Invalid Request.'))

    @patch('check_https_wsc.requests.get')
    def test_get_https_process_unrechable(self, mock_get):
        mock_get.return_value.status_code = 404
        self.assertEqual(self.get_https_process('localhost', 'disks', '20,5'),
                         (2, 'Could not connect to server, status code: 404'))


if __name__ == '__main__':
    unittest.main()
