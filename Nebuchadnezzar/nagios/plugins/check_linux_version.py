#!/usr/bin/env python

import argparse
import sys
import platform
from phimutils.perfdata import INFO_MARKER

STATES = {0: 'OK', 3: 'UNKNOWN'}


def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Script to get Linux OS version')
    results = parser.parse_args(args)
    return


def check_linux_version():
    """get OS version"""
    try:
        distname, version, id = platform.linux_distribution()
        version_out = '{distname} {version} ({id})'.format(**locals())
        state = 0
    except Exception:
        version_out = 'UNKNOWN'
        state = 3
    return state, version_out


def main():
    check_arg()
    state, os_version = check_linux_version()
    vv = {'state': STATES[state], 'version': os_version, 'info_marker': INFO_MARKER}
    out_template = "%(state)s - OS Version %(version)s %(info_marker)s OS_Version='%(version)s'"
    print(out_template % vv)
    sys.exit(state)


if __name__ == "__main__":
    main()
