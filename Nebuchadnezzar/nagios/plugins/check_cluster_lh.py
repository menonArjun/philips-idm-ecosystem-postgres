#!/usr/bin/env python

import argparse
import sys
from collections import defaultdict

STATES = ('OK', 'WARNING', 'CRITICAL', 'UNKNOWN')

DATA_DESCRIPTION = 'The status codes of the hosts or services in the cluster followed by a label to identify the node'

LEVEL_DESCRIPTION = 'The level to consider a problem 1-Warning 2-Critical'

CRITICAL_DESCRIPTION = 'The CRITICAL threshold. Number of hosts or services in cluster that must be in a non-OK state'

WARNING_DESCRIPTION = 'The WARNING threshold. Number of hosts or services in cluster that must be in a non-OK state'


class LevelListAction(argparse.Action):
    LEVELS = map(str, range(4))
    def __call__(self, parser, namespace, values, option_string=None):
        if not (len(values) == 2):
            raise argparse.ArgumentError(self, '%s takes 2 values, %d given' % (option_string, len(values)))
        level, label = values
        if level in self.LEVELS:
            destination = getattr(namespace, self.dest) or []
            destination.append((int(level), label))
            setattr(namespace, self.dest, destination)


def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Service Cluster Plugin for Nagios')
    parser.add_argument('-l', '--label', help='Optional prepended text output (i.e. "Host cluster")')
    parser.add_argument('-w', '--warning', type=int, help=WARNING_DESCRIPTION)
    parser.add_argument('-c', '--critical', type=int, help=CRITICAL_DESCRIPTION)
    parser.add_argument('-r', '--level', type=int, choices=(1,2), default=2, help=LEVEL_DESCRIPTION)
    parser.add_argument('-d', '--data', required=True, action=LevelListAction, help=DATA_DESCRIPTION, nargs=2)
    results = parser.parse_args(args)
    return (results.label, results.warning, results.critical, results.level, results.data)


def over_level(items, level):
    """Returns count and a string representation for items in or above the level"""
    items_over = items[2][:]
    if level == 1:
        items_over += items[1]
    items_msg = '[ {0} ]'.format(', '.join(items_over)) if items_over else ''
    return len(items_over), items_msg


def overview_msg(items):
    om = '{0} ok, {1} warning, {2} unknown, {3} critical'
    return om.format(len(items[0]), len(items[1]), len(items[3]), len(items[2]))


def level_groups_from_list(data):
    level_groups = defaultdict(list)
    for status, label in data:
        level_groups[status].append(label)
    return level_groups


def check_cluster_lh(label, warning, critical, level, data):
    state = 0
    level_groups = level_groups_from_list(data)
    over_count, over_msg = over_level(level_groups, level)
    if critical is not None and critical < over_count:
        state = 2
    elif warning is not None and warning < over_count:
        state = 1
    label_txt = '{0}: '.format(label) if label else ''
    msg = '{label}{overview} {msg}'.format(label=label_txt, overview=overview_msg(level_groups), msg=over_msg)
    return state, msg.strip()

        
def main():
    state, msg = check_cluster_lh(*check_arg())
    print('{state} - {msg}'.format(state=STATES[state], msg=msg))
    sys.exit(state)


if __name__ == '__main__':
    main()
