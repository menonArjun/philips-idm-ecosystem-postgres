#!/usr/bin/env python

import argparse
import sys
import os
import stat
from subprocess import call


WMIC = '/usr/local/bin/wmic'
# Example: wmic -U domain/administrator%psswrd //10.0.0.2 'Select Caption From Win32_OperatingSystem'
QUERY = 'Select Caption From Win32_OperatingSystem'
USER_HELP = 'The username for the server domain may be included Eg: user@domain.com'


def get_command(ntlmv2auth=False, user_pass='', address_argument=''):
    cmd = '{wmic_command} {option} {user} {ip} {qry}'
    if ntlmv2auth:
        return cmd.format(wmic_command=WMIC,
                    option='--option="client ntlmv2 auth=Yes"',
                    user='-U ' + user_pass,
                    ip=address_argument,
                    qry='"' + QUERY + '"')
    else:
        return cmd.format(wmic_command=WMIC,
                    option='',
                    user='-U ' + user_pass,
                    ip=address_argument,
                    qry='"' + QUERY + '"')


def write_to_file(command):
    os.remove('wmi_cmd.sh') if os.path.exists('wmi_cmd.sh') else None
    f = open('wmi_cmd.sh', 'w+')
    f.writelines('#! /bin/sh' + '\n')
    f.writelines(command + '\n')
    f.close()
    os.chmod('wmi_cmd.sh', stat.S_IEXEC)


def check_arg(args=None):
    """Check the arguments passed"""
    parser = argparse.ArgumentParser(description='Nagios Plugin to check WMI accessibility')
    parser.add_argument('-H', '--hostaddress', required=True, help='The address of the server')
    parser.add_argument('-u', '--username', required=True, help=USER_HELP)
    parser.add_argument('-p', '--password', required=True, help='The password for the server')
    parser.add_argument('--ntlmv2auth', help='use ntlm v2 auth', action='store_true')
    results = parser.parse_args(args)
    return results.hostaddress, results.username, results.password, results.ntlmv2auth


def check_wmi_access(hostaddress, username, password, ntlmv2auth):
    state = 3
    user, _, domain = username.partition('@')
    user_prefix = '{0}/'.format(domain) if domain else ''
    user_pass = '{prefix}{user}%{password}'.format(prefix=user_prefix, user=user, password=password)
    address_argument = '//{0}'.format(hostaddress)
    command = get_command(ntlmv2auth, user_pass, address_argument)
    write_to_file(command)
    try:
        with open(os.devnull, 'w') as devnull:
            state = call(['/bin/sh', 'wmi_cmd.sh'], stdout=devnull, stderr=devnull)
        if state:
            output = 'CRITICAL - Could not execute wmi query'
            state = 2
        else:
            output = 'OK - wmi query successful'
    except OSError:
        output = 'UNKNOWN - wmic execution problem'
    return state, output


def main():
    state, output = check_wmi_access(*check_arg())
    print(output)
    sys.exit(state)


if __name__ == '__main__':
    main()
