# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Name of the plugin
%global plugin hb_message

%global nagiospluginsdir %{_libdir}/nagios/plugins

# Basic Information
Name:      nagios-plugins-hb_message
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   Nagios plugin to send messages to HeartBeat (Email with formatting).

Group:     none
License:   GPL
URL:       http://www.philips.com/healthcare
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: i386 x86_64

# Source Information
Source0:   %{plugin}-%{version}.tar.gz
#Patch0:

# Owns the nagios plugins directory
Requires:  nagios-plugins 
Requires:  python >= 2.6
Requires:  python-argparse
Requires:  libxml2-python

Provides:  nagios-plugins-hb_message = %{version}-%{release}

%description
Nagios plugin to send messages to HeartBeat (Email with formatting).
Looks at %{nagiospluginsdir}/nagios-hb-services.xml to find relevant error codes

%prep
%setup -q -n %{plugin}-%{version}

%build
echo OK

%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{nagiospluginsdir}
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/hb_nagios.py %{buildroot}%{nagiospluginsdir}/
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/hb_keepalived.py %{buildroot}%{nagiospluginsdir}/
%{__install} -Dp -m 0644 %{_builddir}/%{plugin}-%{version}/nagios-hb-services.xml %{buildroot}%{nagiospluginsdir}/
%{__sed} -i 's#/usr/lib/nagios/plugins#%{nagiospluginsdir}#g' %{buildroot}%{nagiospluginsdir}/hb_nagios.py

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%attr(0755,root,root) %{nagiospluginsdir}/hb_nagios.py
%attr(0755,root,root) %{nagiospluginsdir}/hb_keepalived.py
%attr(0644,root,root) %{nagiospluginsdir}/nagios-hb-services.xml

%changelog
* Tue Sep 10 2013 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Added I am alive script
* Wed Jun 26 2013 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Update to version 0.3
- Added libxml2 requirement
* Tue Jun 18 2013 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File