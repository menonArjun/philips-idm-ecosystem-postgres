# spec file for package 'nagios-plugins-available_ports' (version '0.1')
#
# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Name of the plugin
%global plugin check_available_ports

%global nagiospluginsdir %{_libdir}/nagios/plugins

# Basic Information
Name:      nagios-plugins-available_ports
Version:   0.1
Release:   1%{?dist}
Summary:   Nagios plugin to report switch port availability.

Group:     none
License:   GPL
URL:       http://www.philips.com/healthcare
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: i386 x86_64

# Source Information
Source0:   %{plugin}-%{version}.tar.gz
#Patch0:

# Owns the nagios plugins directory
Requires:  nagios-plugins 
# Rpmbuild doesn't find these perl dependencies
Requires:  perl(Net::SNMP)
Requires:  perl(Getopt::Long)

Provides:  nagios-plugins-check-available_ports = %{version}-%{release}

%description
The purpose of this plugin is simply to count the number of unused switch ports
on the monitored device. When the number of available ports dips below the
configured thresholds we will alert ops staff via Nagios so additional
switch chassis can be ordered as required.


%prep
%setup -q -n %{plugin}-%{version}

%build
echo OK

%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{nagiospluginsdir}
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/check_available_ports.pl %{buildroot}%{nagiospluginsdir}/

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%attr(0755,root,root) %{nagiospluginsdir}/check_available_ports.pl

%changelog
* Wed Jul 25 2012 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File