# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Name of the plugin
%global plugin check_msa_hardware

%global nagiospluginsdir %{_libdir}/nagios/plugins

# Basic Information
Name:      nagios-plugins-msa_hardware
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   Nagios - script to monitor HP MSA (Modular Smart Array) sensors via SNMP.

Group:     none
License:   GPL
URL:       https://exchange.nagios.org/directory/Plugins/Hardware/Storage-Systems/SAN-and-NAS/Check-HP-MSA-(Modular-Smart-Array)-sensors/details
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: i386 x86_64

# Source Information
Source0:   %{plugin}-%{version}.tar.gz
#Patch0:

# Owns the nagios plugins directory
Requires:  nagios-plugins 
# Rpmbuild doesn't find these perl dependencies
Requires:  perl(Net::SNMP)

Provides:  nagios-plugins-check-msa_hardware = %{version}-%{release}

%description
This plugin checks all sensors of an HP MSA array.
Currently successfully tested with :

HP MSA2312i
HP MSA2012i
HP MSA2012fc
HP MSA2324i
HP P2000 G3 MSA (iSCSI & FC)


%prep
%setup -q -n %{plugin}-%{version}

%build
echo OK

%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{nagiospluginsdir}
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/check_msa_hardware.pl %{buildroot}%{nagiospluginsdir}/

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%attr(0755,root,root) %{nagiospluginsdir}/check_msa_hardware.pl

%changelog
* Tue Nov 17 2015 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File