# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Name of the plugin
%global plugin check_ctdb

%global nagiospluginsdir %{_libdir}/nagios/plugins

# Basic Information
Name:      nagios-plugins-ctdb
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   Nagios plugin to monitor CTDB (Clustered Trivial Database)

Group:     none
License:   GPL
URL:       http://exchange.nagios.org/directory/Plugins/Clustering-and-High-2DAvailability/check_ctdb/details
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: i386 x86_64

# Source Information
Source0:   %{plugin}-%{version}.tar.gz
#Patch0:

# Owns the nagios plugins directory
Requires:  nagios-plugins 
# Rpmbuild doesn't find these perl dependencies
Requires:  perl(Nagios::Plugin) perl(File::Basename)

Provides:  nagios-plugins-check-ctdb = %{version}-%{release}

%description
Nagios plugin to monitor CTDB (Clustered Trivial Database)


%prep
%setup -q -n %{plugin}-%{version}

%build
echo OK

%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{nagiospluginsdir}
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/check_ctdb.pl %{buildroot}%{nagiospluginsdir}/

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%attr(0755,root,root) %{nagiospluginsdir}/check_ctdb.pl

%changelog
* Tue Jun 18 2013 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File