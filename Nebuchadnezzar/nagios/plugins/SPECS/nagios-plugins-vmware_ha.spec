# spec file for package 'nagios-plugins-vmware_ha' (version '0')
#
# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Name of the plugin
%global plugin check_vmware_ha

%global nagiospluginsdir %{_libdir}/nagios/plugins

# Basic Information
Name:      nagios-plugins-vmware_ha
Version:   0
Release:   1%{?dist}
Summary:   Nagios plugin to query VMWare vCenter server for HA cluster status

Group:     none
License:   GPL
URL:       http://www.philips.com/healthcare
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: i386 x86_64

# Source Information
Source0:   %{plugin}-%{version}.tar.gz
#Patch0:

# Owns the nagios plugins directory
Requires:  nagios-plugins 
Requires:  VMware-vSphere-CLI

Provides:  nagios-plugins-check-vmware_ha

%description
Nagios plugin to query VMWare vCenter server for HA cluster status


%prep
%setup -q -n %{plugin}-%{version}

%build
echo OK

%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{nagiospluginsdir}
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/check_vmware_ha.pl %{buildroot}%{nagiospluginsdir}/

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%attr(0755,root,root) %{nagiospluginsdir}/check_vmware_ha.pl

%changelog
* Thu Jul 26 2012 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File