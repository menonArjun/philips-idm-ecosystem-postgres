# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Name of the plugin
%global plugin check_ibm_v7000

%global nagiospluginsdir %{_libdir}/nagios/plugins

# Basic Information
Name:      nagios-plugins-ibm_v7000
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   Nagios plugin to monitor IBM v7000.

Group:     none
License:   GPL
URL:       http://www.philips.com/healthcare
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: i386 x86_64

# Source Information
Source0:   %{plugin}-%{version}.tar.gz
#Patch0:

# Owns the nagios plugins directory
Requires:  nagios-plugins 
Requires:  openssh-clients
Requires:  gawk
Requires:  grep

Provides:  nagios-plugins-check-ibm_v7000 = %{version}-%{release}

%description
This plugin check the HW status of IBM Storwize v7000.
To use this script you need to create a linux user with his ssh certificate.
Create the same user on the v7000, member of monitor group and upload the public ssh key.
Try to log from linux machine to the v7000 without password, if it function you can use the plugin.
I use the 'nagios' user to check the remote system.
The help is included into the script.

%prep
%setup -q -n %{plugin}-%{version}

%build
echo OK

%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{nagiospluginsdir}
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/check_ibm_v7000.sh %{buildroot}%{nagiospluginsdir}/

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%attr(0755,root,root) %{nagiospluginsdir}/check_ibm_v7000.sh

%changelog
* Fri Jun 25 2013 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Updated to version 1.4
* Wed Jul 25 2012 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File