# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Name of the plugin
%global plugin check_wmi_plus

%global nagiospluginsdir %{_libdir}/nagios/plugins

# Basic Information
Name:      nagios-plugins-wmi_plus
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   Nagios plugin to monitor Windows servers via WMI.

Group:     none
License:   GPL
URL:       http://www.edcint.co.nz/checkwmiplus/
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: i386 x86_64

# Source Information
Source0:   %{plugin}.v%{version}.tar.gz
#Patch0:

# Owns the nagios plugins directory
Requires:  nagios-plugins wmic
Requires:  perl(Config::IniFiles) >= 2.58
Requires:  perl >= 5.01
Requires:  perl(Getopt::Long) >= 2.38
Requires:  perl(Data::Dumper) >= 2.125
Requires:  perl(Storable) >= 2.22
Requires:  perl(Scalar::Util) >= 1.22

# This was added as perl(Storable) >= 2.22 seems to be satisfied with perl(Storable) = 2.20
Requires:  local-perl-Storable >= 2.22
Requires:  perl(DateTime) >= 0.66

Provides:  nagios-plugins-check-wmi_plus = %{version}-%{release}

%description
Check WMI Plus is a client-less Nagios plugin for checking Windows systems.
No more need to install any software on any of your Windows machines. 
Monitor Microsoft Windows systems directly from your Nagios server.
Check WMI Plus uses the Windows Management Interface (WMI) to check for common 
services (cpu, disk, sevices, eventlog...) on Windows machines. It requires 
the open source wmi client for Linux.

Besides the built in checks, Check WMI Plus functionality can be easily extended 
through the use of an ini file. Check WMI Plus comes with a release version of the 
ini file, but you are free to add your own checks to your own ini file.

%prep
%setup -q -c -n %{plugin}-%{version}

%build
echo OK

%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{nagiospluginsdir}
%{__install} -d -m 0755 %{buildroot}%{nagiospluginsdir}/eventhandlers
%{__cp} -R %{_builddir}/%{plugin}-%{version}/check_wmi_plus.d  %{buildroot}%{nagiospluginsdir}/
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/check_wmi_plus.pl %{buildroot}%{nagiospluginsdir}/
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/event_generic.pl %{buildroot}%{nagiospluginsdir}/eventhandlers/
%{__install} -Dp -m 0644 %{_builddir}/%{plugin}-%{version}/check_wmi_plus.conf.sample %{buildroot}%{nagiospluginsdir}/
%{__install} -Dp -m 0644 %{_builddir}/%{plugin}-%{version}/check_wmi_plus.README.txt %{buildroot}%{nagiospluginsdir}/
%{__sed} -i 's#\(use\s*lib\).*plugins[^;]*#\1 "%{nagiospluginsdir}"#g' %{buildroot}%{nagiospluginsdir}/check_wmi_plus.pl

%clean
rm -rf %{buildroot}

%post
if [ $1 -eq 1 ] ; then
    # Initial installation
    #setup a link to the directory, this is the directory where the plugin looks for the config
    #doing this avoids changing the configuration
    %{__mkdir} -p /opt/nagios/bin/
    %{__ln_s} %{nagiospluginsdir} /opt/nagios/bin/plugins
    %{__cp} %{nagiospluginsdir}/check_wmi_plus.conf.sample %{nagiospluginsdir}/check_wmi_plus.conf
fi

%preun
if [ $1 -eq 0 ] ; then
    # Package removal, not upgrade
    #no macro for unlink
    /bin/unlink /opt/nagios/bin/plugins
    #no macro for rmdir
    /bin/rmdir /opt/nagios/bin
    /bin/rmdir /opt/nagios
    %{__rm} -f %{nagiospluginsdir}/check_wmi_plus.conf
fi


%files
%defattr(-, root, root, -)
%doc %{nagiospluginsdir}/check_wmi_plus.README.txt
%attr(0755,root,root) %{nagiospluginsdir}/check_wmi_plus.pl
%attr(0755,root,root) %{nagiospluginsdir}/eventhandlers/event_generic.pl
%config %{nagiospluginsdir}/check_wmi_plus.conf.sample
%{nagiospluginsdir}/check_wmi_plus.d

%changelog
* Tue Apr 15 2014 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Changed to version 1.59
* Tue Apr 15 2014 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Changed to version 1.58
* Tue Oct 15 2013 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Changed to version 1.57
* Tue Jun 03 2013 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File
