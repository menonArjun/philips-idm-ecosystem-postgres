#!/usr/bin/env python
# This script should always exit 0
# it may display problem information as output
# problems with this script are meant to be determined by the system expecting the post
# This plugin is uesful to get status of proxy url server of Helion.

import requests
import sys
from scanline.utilities.hsdp_obs import OBJSTConnection


def get_stored_data():
    try:
        obj = OBJSTConnection()
        return obj.to_dict()
    except Exception:
        return {}


def get_helion_proxy_url_status(stored_data):
    headers = {
    'x-auth-token': stored_data['token_id'],
    'content-type': "application/json",
    }
    try:
        response = requests.get(stored_data['obs_url'], data=stored_data['payload'],
                                headers=headers, verify=False)
        if response.status_code == 200:
            return 0, 'Proxy URL is reachable.'
        else:
            return 2, 'Proxy URL is not reachable, status code: ' \
                      '{0}'.format(response.status_code)
    except Exception:
        return 2, 'Invalid get request.'


def main():
    stored_data = get_stored_data()
    if stored_data:
        state, msg = get_helion_proxy_url_status(stored_data)
        print msg
        sys.exit(state)
    else:
        print 'Could not connect UDM Database.'
        sys.exit(2)


if __name__ == '__main__':
    main()
