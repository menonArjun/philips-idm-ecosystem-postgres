import unittest
from mock import MagicMock, patch, call, mock_open


class F5PoolHandlerAndNotifyTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_post_notification = MagicMock(name='post_notification')
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_phimutils = MagicMock(name='phimutils')
        self.mock_phimutils.collection = MagicMock(name='phimutils.collection')
        self.mock_phimutils.message = MagicMock(name='phimutils.message')
        self.mock_request = MagicMock(name='requests')
        self.mock_json = MagicMock(name='json')
        self.mock_yaml = MagicMock(name='yaml')
        modules = {
            'post_notification': self.mock_post_notification,
            'argparse': self.mock_argparse,
            'phimutils': self.mock_phimutils,
            'phimutils.argument': self.mock_phimutils.argument,
            'phimutils.collection': self.mock_phimutils.collection,
            'phimutils.message': self.mock_phimutils.message,
            'requests': self.mock_request,
            'json': self.mock_json,
            'yaml': self.mock_yaml,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import f5_pool_handler_and_notify
        self.module = f5_pool_handler_and_notify

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_poolmemeber_enable(self):
        pool = self.module.PoolMemeber('address', 'name', 'state')
        pool.UP = 'state'
        resp_mock = MagicMock(name='response')
        self.mock_request.put.return_value = resp_mock
        self.mock_json.dumps.return_value = 'data'
        resp = pool.enable('url', 'user', 'password', 'data', 'headers')
        self.mock_request.put.assert_called_once_with(
            'url', auth=('user', 'password'), verify=False, data=self.mock_json.dumps.return_value,
            headers='headers')
        resp_mock.raise_for_status.assert_called_once_with()
        self.assertEqual(resp, resp_mock)

    def test_poolmemeber_disable(self):
        pool = self.module.PoolMemeber('address', 'name', 'state')
        pool.DOWN = 'state'
        resp_mock = MagicMock(name='response')
        self.mock_request.put.return_value = resp_mock
        resp = pool.disable('url', 'user', 'password', 'data', 'headers')
        self.mock_request.put.assert_called_once_with(
            'url', auth=('user', 'password'), verify=False, data=self.mock_json.dumps.return_value,
            headers='headers')
        resp_mock.raise_for_status.assert_called_once_with()
        self.assertEqual(resp, resp_mock)

    def test_get_f5_credential_when_f5value_present(self):
        f5_dict = {'address': '1.2.3.4',
                   'username': 'user', 'password': 'pass'}
        mk_open = mock_open(read_data='f5')
        self.mock_yaml.load.return_value = [{'f5': f5_dict}]
        with patch('f5_pool_handler_and_notify.open', mk_open, create=True):
            f5_credentials = self.module.get_f5_credentials('discovery.yml')
            mk_open.assert_called_once_with('discovery.yml', 'r')
            self.assertEqual(f5_credentials, ('1.2.3.4', 'user', 'pass'))
            self.mock_yaml.load.assert_called_once_with(mk_open())

    def test_get_f5_credential_f5value_absent(self):
        f5_dict = {'address': '1.2.3.4',
                   'username': 'user', 'password': 'pass'}
        mk_open = mock_open(read_data='f5')
        self.mock_yaml.load.return_value = [{'f6': f5_dict}]
        with patch('f5_pool_handler_and_notify.open', mk_open, create=True):
            f5_credentials = self.module.get_f5_credentials('discovery.yml')
            mk_open.assert_called_once_with('discovery.yml', 'r')
            self.assertEqual(f5_credentials, (None, None, None))
            self.mock_yaml.load.assert_called_once_with(mk_open())

    def test_get_f5_credential_exception(self):
        mk_open = mock_open()
        mk_open.side_effect = IOError()
        with patch('f5_pool_handler_and_notify.open', mk_open, create=True):
            f5_credentials = self.module.get_f5_credentials('discovery.yml')
            mk_open.assert_called_once_with('discovery.yml', 'r')
            self.assertEqual(f5_credentials, (None, None, None))

    def test_f5loadbalancer_STATUS_URL(self):
        f5_obj = self.module.F5LoadBalancer('1.1', 'user', 'pass', '12.45')
        member_mock = MagicMock(name='member')
        member_mock.name = 'node1'
        f5_obj.member = member_mock
        status_url = f5_obj._STATUS_URL()
        expected_url = 'https://1.1/mgmt/tm/ltm/pool/~Common~DICOM_IN_104/members/~Common~node1'
        self.assertEqual(status_url, expected_url)

    def test_f5loadbalancer_enable_member(self):
        f5_obj = self.module.F5LoadBalancer('1.1', 'user', 'pass', '12.45')
        member_mock = MagicMock(name='member')
        _STATUS_URL_mock = MagicMock(name='_STATUS_URL')
        _STATUS_URL_mock.return_value = 'url'
        f5_obj.member = member_mock
        f5_obj._STATUS_URL = _STATUS_URL_mock
        ENABLE = {'session': 'user-enabled', 'state': 'user-up'}
        HEADERS = {'Content-Type': 'application/json'}
        expected_call = call('url', 'user', 'pass', ENABLE, HEADERS)
        f5_obj.enable_member()
        _STATUS_URL_mock.assert_called_once_with()
        self.assertEqual(member_mock.enable.call_args, expected_call)

    def test_verify_poolmember_state_only_one_member(self):
        f5_obj = self.module.F5LoadBalancer('1.1', 'user', 'pass', '1.1.1.1')
        pool_member = [{'address': '1.1.1.1', 'name': 'n1', 'state': 'up'}]
        f5_obj.pool_members = pool_member
        member_mock = MagicMock(name='member')
        member_mock.address = '1.1.1.1'
        f5_obj.member = member_mock
        self.assertEqual(f5_obj.verify_poolmember_state(), False)

    def test_verify_poolmember_state_one_up(self):
        f5_obj = self.module.F5LoadBalancer('1.1', 'user', 'pass', '1.1.1.1')
        pool_member = [{'address': '1.1.1.1', 'name': 'n1', 'state': 'up'},
                       {'address': '1.1.1.2', 'name': 'n2', 'state': 'up'},
                       {'address': '1.1.1.3', 'name': 'n2', 'state': 'user-down'}]
        f5_obj.pool_members = pool_member
        member_mock = MagicMock(name='member')
        member_mock.address = '1.1.1.1'
        f5_obj.member = member_mock
        self.assertEqual(f5_obj.verify_poolmember_state(), True)

    def test_verify_poolmember_state_other_down(self):
        f5_obj = self.module.F5LoadBalancer('1.1', 'user', 'pass', '1.1.1.1')
        pool_member = [{'address': '1.1.1.1', 'name': 'n1', 'state': 'up'},
                       {'address': '1.1.1.2', 'name': 'n2', 'state': 'user-down'},
                       {'address': '1.1.1.3', 'name': 'n2', 'state': 'user-down'}]
        f5_obj.pool_members = pool_member
        member_mock = MagicMock(name='member')
        member_mock.address = '1.1.1.1'
        f5_obj.member = member_mock
        self.assertEqual(f5_obj.verify_poolmember_state(), False)

    def test_f5loadbalancer_disable_member(self):
        f5_obj = self.module.F5LoadBalancer('1.1', 'user', 'pass', '12.45')
        member_mock = MagicMock(name='member')
        _STATUS_URL_mock = MagicMock(name='_STATUS_URL')
        _STATUS_URL_mock.return_value = 'url'
        f5_obj.member = member_mock
        f5_obj._STATUS_URL = _STATUS_URL_mock
        DISABLE = {'state': 'user-down', 'session': 'user-disabled'}
        HEADERS = {'Content-Type': 'application/json'}
        expected_call = call('url', 'user', 'pass', DISABLE, HEADERS)
        f5_obj.disable_member()
        _STATUS_URL_mock.assert_called_once_with()
        self.assertEqual(member_mock.disable.call_args, expected_call)

    def test_f5loadbalancer_nos_nodes(self):
        f5_obj = self.module.F5LoadBalancer('1.1', 'user', 'pass', '12.45')
        pool_members_mock = MagicMock(name='pool_members')
        pool_members_mock.__len__.return_value = 3
        f5_obj.pool_members = pool_members_mock
        self.assertEqual(f5_obj.nos_nodes, 3)

    def test_f5loadbalancer_member_nos_nodes_gt_1(self):
        f5_obj = self.module.F5LoadBalancer('1.1', 'user', 'pass', '1.2.4.5')
        f5_obj.nos_nodes = 2
        pool_member = [{'address': '1.2.4.5', 'name': 'n1', 'state': 'up'},
                       {'address': '1.2.3.6', 'name': 'n2', 'state': 'up'}]
        f5_obj.pool_members = pool_member
        import f5_pool_handler_and_notify
        PoolMemeber_mk = MagicMock(
            name='PoolMemeber', spec=f5_pool_handler_and_notify.PoolMemeber)
        PoolMemeber_mk.return_value = 'm1'
        self.module.PoolMemeber = PoolMemeber_mk
        PM_expected_call = [call(address='1.2.4.5', name='n1', state='up')]
        self.assertEqual(f5_obj.member, 'm1')
        self.assertEqual(PoolMemeber_mk.call_args_list, PM_expected_call)

    def test_f5loadbalancer_member_nos_nodes_member_not_found(self):
        f5_obj = self.module.F5LoadBalancer('1.1', 'user', 'pass', '1.2.4.5.6')
        f5_obj.nos_nodes = 2
        pool_member = [{'address': '1.2.4.5', 'name': 'n1', 'state': 'up'},
                       {'address': '1.2.3.6', 'name': 'n2', 'state': 'up'}]
        f5_obj.pool_members = pool_member
        import f5_pool_handler_and_notify
        PoolMemeber_mk = MagicMock(
            name='PoolMemeber', spec=f5_pool_handler_and_notify.PoolMemeber)
        PoolMemeber_mk.return_value = 'm1'
        self.module.PoolMemeber = PoolMemeber_mk
        self.assertRaises(Exception, getattr, f5_obj, 'member')
        self.assertEqual(PoolMemeber_mk.call_count, 0)

    def test_f5loadbalancer_member_nos_nodes_is_1(self):
        f5_obj = self.module.F5LoadBalancer('1.1', 'user', 'pass', '1.2.4.5')
        f5_obj.nos_nodes = 1
        pool_member = [{'address': '1.2.4.5', 'name': 'n1', 'state': 'up'},
                       {'address': '1.2.3.6', 'name': 'n2', 'state': 'up'}]
        f5_obj.pool_members = pool_member
        import f5_pool_handler_and_notify
        PoolMemeber_mk = MagicMock(
            name='PoolMemeber', spec=f5_pool_handler_and_notify.PoolMemeber)
        PoolMemeber_mk.return_value = 'm1'
        self.module.PoolMemeber = PoolMemeber_mk
        self.assertRaises(Exception, getattr, f5_obj, 'member')
        self.assertEqual(PoolMemeber_mk.call_count, 0)

    def test_f5loadbalancer_member_nos_nodes_less_than_1(self):
        f5_obj = self.module.F5LoadBalancer('1.1', 'user', 'pass', '1.2.4.5')
        f5_obj.nos_nodes = 1
        pool_member = [{'address': '1.2.4.5', 'name': 'n1', 'state': 'up'},
                       {'address': '1.2.3.6', 'name': 'n2', 'state': 'up'}]
        f5_obj.pool_members = pool_member
        import f5_pool_handler_and_notify
        PoolMemeber_mk = MagicMock(
            name='PoolMemeber', spec=f5_pool_handler_and_notify.PoolMemeber)
        PoolMemeber_mk.return_value = 'm1'
        self.module.PoolMemeber = PoolMemeber_mk
        self.assertRaises(Exception, getattr, f5_obj, 'member')
        self.assertEqual(PoolMemeber_mk.call_count, 0)

    def test_f5loadbalancer_pool_members_items_present(self):
        json_mk = MagicMock(name='response_mock')
        json_mk.json.return_value = {'items': ['items1', 'items2']}
        self.mock_request.get.return_value = json_mk
        f5_obj = self.module.F5LoadBalancer('1.1', 'user', 'pass', '1.2.4.5.6')
        self.assertEqual(f5_obj.pool_members, ['items1', 'items2'])
        exp_get_call = [call(
            'https://1.1/mgmt/tm/ltm/pool/~Common~DICOM_IN_104/members', auth=('user', 'pass'), verify=False)]
        self.assertEqual(self.mock_request.get.call_args_list, exp_get_call)
        exp_json_mk_call = [call.raise_for_status(), call.json()]
        self.assertEqual(json_mk.mock_calls, exp_json_mk_call)

    def test_f5loadbalancer_pool_members_items_not_present(self):
        json_mk = MagicMock(name='response_mock')
        json_mk.json.return_value = {}
        self.mock_request.get.return_value = json_mk
        f5_obj = self.module.F5LoadBalancer('1.1', 'user', 'pass', '1.2.4.5.6')
        self.assertEqual(f5_obj.pool_members, [])
        exp_get_call = [call(
            'https://1.1/mgmt/tm/ltm/pool/~Common~DICOM_IN_104/members', auth=('user', 'pass'), verify=False)]
        self.assertEqual(self.mock_request.get.call_args_list, exp_get_call)
        exp_json_mk_call = [call.raise_for_status(), call.json()]
        self.assertEqual(json_mk.mock_calls, exp_json_mk_call)

    def test_verify_state_OK(self):
        status = self.module.verify_state('OK', None)
        self.assertEqual(status, True)

    def test_verify_state_CRITICAL_HARD(self):
        status = self.module.verify_state('CRITICAL', 'HARD')
        self.assertEqual(status, True)

    def test_verify_state_CRITICAL_SOFT(self):
        status = self.module.verify_state('CRITICAL', 'SOFT')
        self.assertEqual(status, False)

    def test_post_notification_enable_member(self):
        args = ('url', 'hostaddress', 'hostname', 'statetype',
                'description', 'notificationtype', 'OK',
                'timestamp', 'output', 'perfdata', 'siteid', 'siteid_file')
        verifyst_mock = MagicMock(name='verify_state')
        verifyst_mock.return_value = True
        self.module.verify_state = verifyst_mock
        get_f5_mock = MagicMock(name='get_f5_mock')
        get_f5_mock.return_value = 'f5_address', 'f5_user', 'f5_pwd'
        self.module.get_f5_credentials = get_f5_mock
        F5LB_mock = MagicMock(name='F5LoadBalancer')
        f5loadb_mock = MagicMock(name='lbobj')
        f5loadb_mock.nos_nodes = 2
        F5LB_mock.return_value = f5loadb_mock
        self.module.F5LoadBalancer = F5LB_mock
        self.mock_phimutils.collection.DictNoEmpty.return_value = {
            'payload': 'p1'}
        msg_mock = MagicMock(name='Message')
        msg_mock.to_dict.return_value = {'test': 't1'}
        self.mock_phimutils.message.Message.return_value = msg_mock
        self.module.post_notification(*args)
        verifyst_mock.assert_called_once_with('OK', 'statetype')
        get_f5_mock.assert_called_once_with(self.module.DISCOVERY_FILE)
        self.assertEqual(f5loadb_mock.enable_member.call_count, 1)
        self.assertEqual(f5loadb_mock.disable_member.call_count, 0)
        F5LB_mock.assert_called_once_with(
            'f5_address', 'f5_user', 'f5_pwd', 'hostaddress')
        self.mock_phimutils.collection.DictNoEmpty.assert_called_once_with(
            {}, output='Node Enabled successfully in F5 LB Pool.')
        self.assertEqual(self.mock_phimutils.message.Message.call_count, 1)
        exp_post_call = [
            call('url', json={'test': 't1'}, verify=True), call().raise_for_status()]
        self.assertEqual(self.mock_request.post.mock_calls, exp_post_call)

    def test_post_notification_disable_member(self):
        args = ('url', 'hostaddress', 'hostname', 'HARD',
                'description', 'notificationtype', 'CRITICAL',
                'timestamp', 'output', 'perfdata', 'siteid', 'siteid_file')
        verifyst_mock = MagicMock(name='verify_state')
        verifyst_mock.return_value = True
        self.module.verify_state = verifyst_mock
        get_f5_mock = MagicMock(name='get_f5_mock')
        get_f5_mock.return_value = 'f5_address', 'f5_user', 'f5_pwd'
        self.module.get_f5_credentials = get_f5_mock
        F5LB_mock = MagicMock(name='F5LoadBalancer')
        f5loadb_mock = MagicMock(name='lbobj')
        f5loadb_mock.nos_nodes = 2
        F5LB_mock.return_value = f5loadb_mock
        self.module.F5LoadBalancer = F5LB_mock
        self.mock_phimutils.collection.DictNoEmpty.return_value = {
            'payload': 'p1'}
        msg_mock = MagicMock(name='Message')
        msg_mock.to_dict.return_value = {'test': 't1'}
        self.mock_phimutils.message.Message.return_value = msg_mock
        self.module.post_notification(*args)
        verifyst_mock.assert_called_once_with('CRITICAL', 'HARD')
        get_f5_mock.assert_called_once_with(self.module.DISCOVERY_FILE)
        self.assertEqual(f5loadb_mock.enable_member.call_count, 0)
        self.assertEqual(f5loadb_mock.disable_member.call_count, 1)
        F5LB_mock.assert_called_once_with(
            'f5_address', 'f5_user', 'f5_pwd', 'hostaddress')
        self.mock_phimutils.collection.DictNoEmpty.assert_called_once_with(
            {}, output='Node Disabled successfully in F5 LB Pool.')
        self.assertEqual(self.mock_phimutils.message.Message.call_count, 1)
        exp_post_call = [
            call('url', json={'test': 't1'}, verify=True), call().raise_for_status()]
        self.assertEqual(self.mock_request.post.mock_calls, exp_post_call)

if __name__ == '__main__':
    unittest.main()
