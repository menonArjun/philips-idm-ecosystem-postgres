#!/usr/bin/env python
# This script should always exit 0
# it may display problem information as output
# problems with this script are meant to be determined by the system expecting the post
# This plugin is uesful for get

import requests
import sys
import json
from scanline.utilities.hsdp_obs import OBJSTConnection


def get_stored_data():
    try:
        obj = OBJSTConnection()
        return obj.to_dict()
    except Exception:
        return {}


def get_keystone_server_status(stored_data):
    try:
        response = requests.post(stored_data['keystone_url'], 
                                 data=json.dumps(stored_data['payload']),
                                 verify=False)
        if response.status_code == 200:
            return 0, 'Keystone URL is reachable.'
        else:
            return 2, 'Keystone URL is not reachable, status code: ' \
                      '{0}'.format(response.status_code)
    except Exception:
        return 2, 'Invalid post request.'


def main():
    stored_data = get_stored_data()
    if stored_data:
        state, msg = get_keystone_server_status(stored_data)
        print msg
        sys.exit(state)
    else:
        print 'Could not connect UDM Database.'
        sys.exit(2)


if __name__ == '__main__':
    main()
