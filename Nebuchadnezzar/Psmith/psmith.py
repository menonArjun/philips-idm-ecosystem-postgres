import socket
import signal
import time
import os
import logging
from glob import glob
from lxml import etree
from blist import sortedset
from shutil import rmtree
from collections import defaultdict
from distutils.dir_util import create_tree
from itertools import chain
from subprocess import Popen,PIPE,CalledProcessError

spit_bucket = logging.getLogger('phim.psmith')
ZSYNC_PROCESS_NAME = '/usr/local/bin/zsync_curl'

class PSmith(object):
    def __init__(self, path, url, xml_files=None, xml_dirs=None):
        self.path = path
        self.url = url
        self.xml_files = xml_files if xml_files else []
        self.xml_dirs = xml_dirs if xml_dirs else []
        self._xmls = None

        self._listed_dirs = None
        self._listed_files = None
        self._present_dirs = None
        self._present_files = None
        self.sha1_files = None
        self.part_files = None

    @property
    def xmls(self):
        if not self._xmls:
            xmls_in_dirs = [glob(os.path.join(dir, '*.xml')) for dir in self.xml_dirs]
            self._xmls = chain(self.xml_files, *xmls_in_dirs)
        return self._xmls

    @staticmethod
    def _sortedset_from_xml(tree, element_type):
        result = sortedset()
        for element in tree.iter(element_type):
            path_parts = element.xpath('ancestor::dir/@name')
            path_parts.append(element.get('name'))
            result.add(os.path.join(*path_parts))
        return result

    def sortedsets_from_xml(self):
        dirs = sortedset()
        files = sortedset()
        for xml_file in self.xmls:
            spit_bucket.debug('Trying to parse XML: %s', xml_file)
            parsedtree = etree.parse(xml_file)
            dirs |= PSmith._sortedset_from_xml(parsedtree, 'dir')
            files |= PSmith._sortedset_from_xml(parsedtree, 'file')
        return dirs, files

    @staticmethod
    def _sortedsets_from_fs(path, munge=''):
        dirs = sortedset()
        files = sortedset()
        for item in os.listdir(path):
            item_path = os.path.join(path, item)
            relative_item_path = os.path.join(munge, item)
            if os.path.isdir(item_path):
                dirs.add(relative_item_path)
                d2, f2 = PSmith._sortedsets_from_fs(item_path, relative_item_path)
                dirs |= d2
                files |= f2
            elif os.path.isfile(item_path):
                files.add(relative_item_path)
        return dirs, files

    def sortedsets_from_fs(self):
        return self._sortedsets_from_fs(self.path)

    @staticmethod
    def delete_from_fs(root_path, items):
        for item in items:

            item = os.path.join(root_path, item)
            if not os.path.exists(item):
                spit_bucket.debug('Item %s does not exist to delete. Skip', item)
                continue

            if os.path.isdir(item):
                spit_bucket.debug('Trying to remove dir: %s', item)
                # remove_tree(item) #doesn't really ignore errors
                rmtree(item, ignore_errors=True)
            else:
                spit_bucket.debug('Trying to remove file: %s', item)
                try:
                    os.remove(item)
                except OSError:
                    spit_bucket.exception('Error removing item: %s', item)

    def cleanup_unlisted(self):
        self.sha1_files = sortedset(['{filename}{extension}'.format(filename=filename, extension=SHA1_SUFFIX)
                                     for filename in self.listed_files])
        self.part_files = sortedset(['{filename}{extension}'.format(filename=filename, extension=PART_SUFFIX)
                                     for filename in self.listed_files])
        unlisted_files = self.present_files - (self.listed_files | self.sha1_files | self.part_files )
        PSmith.delete_from_fs(self.path, unlisted_files)
        unlisted_dirs = self.present_dirs - self.listed_dirs
        PSmith.delete_from_fs(self.path, unlisted_dirs)
        return len(unlisted_files), len(unlisted_dirs)

    def generate_lists(self):
        try:
            dirs_from_xml, files_from_xml = self.sortedsets_from_xml()
            dirs_from_fs, files_from_fs = self.sortedsets_from_fs()
        except (IOError, OSError):
            spit_bucket.exception('Exception generating lists')
            raise
        except etree.XMLSyntaxError:
            spit_bucket.exception('Exception parsing xml')
            raise
        self._listed_dirs = dirs_from_xml
        self._listed_files = files_from_xml
        self._present_dirs = dirs_from_fs
        self._present_files = files_from_fs

    @property
    def listed_dirs(self):
        if self._listed_dirs is None:
            self.generate_lists()
        return self._listed_dirs

    @property
    def listed_files(self):
        if self._listed_files is None:
            self.generate_lists()
        return self._listed_files

    @property
    def present_files(self):
        if self._present_files is None:
            self.generate_lists()
        return self._present_files

    @property
    def present_dirs(self):
        if self._present_dirs is None:
            self.generate_lists()
        return self._present_dirs

    # lock so that script runs only once at a time (Linux only)
    @staticmethod
    def get_lock(process_name):
        # global lock_socket
        lock_socket = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
        try:
            lock_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            lock_socket.bind('\0' + process_name)
            spit_bucket.error('lock acquired for %s', process_name)
        except socket.error:
            spit_bucket.error('lock exists for %s,Package download is in progress for earlier SWD request,skipping this one', process_name)
            lock_socket.shutdown(socket.SHUT_RDWR)
            lock_socket.close()
            lock_socket = None
        return lock_socket

    def check_output(self, *popenargs, **kwargs):
        if 'stdout' in kwargs:
            raise ValueError('stdout argument not allowed, it will be overridden.')
        process = Popen(stdout=PIPE, *popenargs, **kwargs)
        output, unused_err = process.communicate()
        retcode = process.poll()
        if retcode:
            cmd = kwargs.get("args")
            if cmd is None:
                cmd = popenargs[0]
        #    raise CalledProcessError(retcode, cmd, output=output)
        return output


    def kill_orphan_zsync_process(self):
        try:
            proces_ids = map(int, self.check_output(["pidof", ZSYNC_PROCESS_NAME]).split())
        except CalledProcessError as e:
            spit_bucket.error("No Zsync process available")
        if proces_ids:
            for process_id in proces_ids:
                try:
                    os.kill(int(process_id), signal.SIGKILL)
                    time.sleep(90)
                except Exception as e:
                    spit_bucket.error("Error killing zsync orphan process")


    def sync(self):
        sync_results = defaultdict(int)
        sync_results['removed_files'], sync_results['removed_dirs'] = self.cleanup_unlisted()
        self.kill_orphan_zsync_process()
        create_tree(self.path, self.listed_files)
        sync_results['listed_files'] = len(self.listed_files)
        sync_results['listed_dirs'] = len(self.listed_dirs)
        for item in self.listed_files:
            try:
                lock = self.get_lock('psmith_running')
                if lock:
                    spit_bucket.debug('Attempting to download package %s',item)
                    zsync_file = ZSyncFile(self.path, self.url, item)
                    if not zsync_file.sync():
                        sync_results['errors'] += 1
            except Exception as e:
                spit_bucket.debug('Psmith failed due to %s'.format(e.message))
            finally:
                spit_bucket.debug('Attempting to close psmith lock')
                if lock:
                    lock.shutdown(socket.SHUT_RDWR)
                    lock.close()
        return sync_results


SHA1_SUFFIX = '.sha1'
PART_SUFFIX = '.part'
ZSYNC_SUFFIX = '.zsync'
TEMP_SUFFIXES = [ZSYNC_SUFFIX, '.zs-old']
ZSYNC_CMD = 'cd %(path)s; mkdir -p %(folder_path)s; cd %(folder_path)s;  /usr/local/bin/zsync_curl %(zsync_url)s -i %(item)s'
PERMISSIONS = 0644


class ZSyncFile(object):
    def __init__(self, path, url, name, permissions=PERMISSIONS):
        self.path = path
        self.url = url
        self.name = name
        self._sha1 = None
        self._filename = None
        self.permissions = permissions

    @property
    def filename(self):
        if not self._filename:
            self._filename = os.path.join(self.path, self.name)
        return self._filename

    def get_sha1_fom_zsync(self):
        zsync_file = '{filename}{extension}'.format(filename=self.filename, extension=ZSYNC_SUFFIX)
        spit_bucket.debug('Getting SHA1 from file: %s', zsync_file)
        with open(zsync_file) as infile:
            for line in infile:
                if line.startswith('SHA-1:'):
                    return line.split(':')[1].strip()
            else:
                spit_bucket.warning('Could not find SHA1 from file: %s', zsync_file)

    @property
    def sha1(self):
        if not self._sha1:
            self._sha1 = self.get_sha1_fom_zsync()
        return self._sha1

    def write_sha1(self):
        sha1_file = '{filename}{extension}'.format(filename=self.filename, extension=SHA1_SUFFIX)
        spit_bucket.debug('Writing SHA1 to file: %s' % sha1_file)
        with open(sha1_file, 'w') as outfile:
            outfile.write(self.sha1)

    def run_zsync(self):
        cmd_map = {
            'folder_path': os.path.dirname(self.filename),
            'path': self.path,
            'item': self.filename,
            'zsync_url': '{address}/{name}{extension}'.format(address=self.url, name=self.name, extension=ZSYNC_SUFFIX)
        }
        zsync_line = ZSYNC_CMD % cmd_map
        spit_bucket.info('Running ZSYNC command: %s', zsync_line)
        return_code = os.system(zsync_line)
        if return_code:
            spit_bucket.error('Failed to run ZSYNC command: %s', zsync_line)
            return False
        return True

    def sync(self):
        sync_result = self.run_zsync()
        if sync_result:
            os.chmod(self.filename, self.permissions)
            # try:
            #     self.write_sha1()
            # except IOError:
            #     spit_bucket.exception('Failed to generate sha1 for file %s', self.name)
            #     sync_result = False
        PSmith.delete_from_fs(self.path, ['{filename}{extension}'.format(filename=self.filename, extension=extension)
                               for extension in TEMP_SUFFIXES])
        return sync_result


def main():
    """Manifest xml files expected:
<manifest>
    <dir name="complex">
        <dir name="1">
            <dir name="e">
                <file name='co.txt' />
            </dir>
        </dir>
    </dir>
    <file name='etc.exe' />
    <dir name="Installers3.6">
        <file name='install.msi' />
        <file name='lang1.rsr' />
    </dir>
    <dir name="Installers4.4">
        <file name='install.msi' />
        <file name='patch1.exe' />
        <file name='patch2.exe' />
    </dir>
</manifest>
"""
    reposin = PSmith('/var/philips/repo/', 'http://repo.phim.isyntax.net/philips', ['/etc/philips/psmith/site/site-repo.xml'])
    reposin.sync()


if __name__ == '__main__':
    main()
