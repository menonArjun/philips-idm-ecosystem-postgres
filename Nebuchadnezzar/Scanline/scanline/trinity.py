import logging
import yaml
import pkgutil
import scanline.host
from scanline.host import (linux, analytics_publisher, windows,
                           i4, isportal, iscv, ibe, dwp, xperim, concerto,
                           advanced_workflow_services, san, switch, mssql, i4ev,
                           i4prep, i4viewer, udm, xperconnect, xperdatacenter,
                           iecg, helion, ibegd, advanced_workflow_services_archive,
                           isite, analytics_publisher_gd, iecggd, aws_hd,
                           aws_archive_hd, iscvgd, tivoli_utility, f5loadbalancer)
from scanline.product.analytics_publisher import AnalyticsPublisherProductScanner
from scanline.product.legacy_nagios import LegacyNagiosProductScanner
from scanline.product.isp import ISPProductScanner
from scanline.product.windows import WindowsBasedProductScanner
from scanline.product.vmware import VMwareProductScanner
from scanline.product import ProductScanner
from scanline.product.ibe import IBEProductScanner
from scanline.product.iecg import IECGProductScanner
from scanline.product.iscv import ISCVProductScanner

scanners = {
    'ISP': {'product': ISPProductScanner},
    'LN': {'product': LegacyNagiosProductScanner},
    'I4': {'host': i4.I4HostScanner},
    'XPERCONNECT': {'host': xperconnect.XPERConnectHostScanner},
    'XPERDATACENTER': {'host': xperdatacenter.XPERDataCenterHostScanner},
    'ISPortal': {'host': isportal.ISPortalHostScanner},
    'Concerto': {'host': concerto.ConcertoHostScanner},
    'ISCV': {'host': iscv.ISCVHostScanner},
    'IBE': {'host': ibe.IBEHostScanner},
    'IBEGD': {'host': ibegd.IBEGDHostScanner,
              'product': IBEProductScanner},
    'DWP': {'host': dwp.DWPHostScanner},
    'XPERIM': {'host': xperim.XPERIMHostScanner},
    'iECG': {'host': iecg.IECGHostScanner},
    'iSite': {'host': isite.ISiteHostScanner},
    'vCenter': {'product': VMwareProductScanner},
    'F5LoadBalancer': {'host': f5loadbalancer.F5LBHostScanner},
    'AnalyticsPublisher': {
        'host': analytics_publisher.AnalyticsPublisherHostScanner
    },
    'AdvancedWorkflowServices': {
        'host': advanced_workflow_services.AdvancedWorkflowServicesHostScanner
    },
    'Windows': {'host': windows.WindowsHostScanner},
    'Linux': {'host': linux.LinuxHostScanner},
    'HPMSASAN': {'host': san.HPMSASANHostScanner},
    'DellEqualLogicSAN': {'host': san.DellEqualLogicSANHostScanner},
    'IBMStorwizeSAN': {'host': san.IBMStorwizeSANHostScanner},
    'CiscoSwitch': {'host': switch.CiscoSwitchHostScanner},
    'HPSwitch': {'host': switch.HPSwitchHostScanner},
    'MSSQL': {'host': mssql.MSSQLHostScanner},
    'I4EV': {'host': i4ev.I4EVHostScanner},
    'I4Prep': {'host': i4prep.I4PrepHostScanner},
    'I4Viewer': {'host': i4viewer.I4ViewerHostScanner},
    'UDMRedis': {'host': udm.UDMRedisHostScanner},
    'Helion': {'host': helion.HelionHostScanner},
    'AdvancedWorkflowArchiveServices': {
        'host': advanced_workflow_services_archive.AdvancedWorkflowArchiveServicesHostScanner
    },
    'AnalyticsPublisherGD': {
        'product': AnalyticsPublisherProductScanner,
        'host': analytics_publisher_gd.AnalyticsPublisherGDHostScanner
    },
    'iECGGD': {'host': iecggd.IECGGDHostScanner,
            'product':IECGProductScanner},
    'AdvancedWorkflowHDServices': {
        'host': aws_hd.AWSHDHostScanner
    },
    'AdvancedWorkflowArchiveHDServices': {
        'host': aws_archive_hd.AWAHDSHostScanner
    },
    'ISCVGD': {'host': iscvgd.ISCVGDHostScanner,
            'product':ISCVProductScanner
    },
    'ISEEUtilityServer': {'host': tivoli_utility.ISEEUtilHostScanner},
}

logger = logging.getLogger(__name__)


def scanline_endpoints(discovery_manifest):
    with open(discovery_manifest) as manifest:
        discovery_config = yaml.load(manifest)
    return discovery_config


def scanline_scanner(discovery_endpoint):
    scanner_name = discovery_endpoint.get('scanner')
    scanner = scanners.get(scanner_name)
    if not scanner:
        logger.error('Unknown scanner "%s"', scanner_name)
        return
    product_scanner = scanner.get('product') or ProductScanner
    return product_scanner(host_scanner=scanner.get('host'), **discovery_endpoint)


def get_host_scanners():
    package = scanline.host
    prefix = package.__name__ + '.'
    # walking the package to import all modules
    for importer, modname, ispkg in pkgutil.walk_packages(path=package.__path__, prefix=prefix):
        logger.debug('loaded module %s', modname)
    return scanline.host.HostScanner.get_host_scanners()
