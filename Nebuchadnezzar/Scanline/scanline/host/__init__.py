import logging
from cached_property import cached_property
from scanline.utilities.dns import get_address


logger = logging.getLogger(__name__)


class RegisterHostScanners(type):
    def __init__(cls, name, bases, class_dict):
        super(RegisterHostScanners, cls).__init__(name, bases, class_dict)
        if not hasattr(cls, 'registry'):
            cls.registry = {}
        cls.registry[cls.module_name] = cls

    def __iter__(cls):
        return cls.registry.iteritems()

    def get_host_scanners(cls):
        return cls.registry.keys()


class HostScanner(object):
    __metaclass__ = RegisterHostScanners
    module_name = 'Host'
    general_properties = frozenset(
        ['address', 'product_id', 'product_name', 'product_version', 'role'])
    module_properties = frozenset(['tags', 'flags', 'endpoint'])

    def __init__(self, hostname, endpoint, tags=None, product_id='NA',
                 product_name='NA', product_version='NA', role='NA', **kwargs):
        self.hostname = hostname
        self.endpoint = endpoint
        self.tags = tags
        self.product_id = product_id
        self.product_name = product_name
        self.product_version = product_version
        self.role = role
        self._flags = set()
        self.kwargs = kwargs
        self.ipv6_enabled = kwargs.get('ipv6_enabled', False)

    @property
    def flags(self):
        self.get_component_flags()
        return list(self._flags)

    def get_component_flags(self):
        add_on_flags = [flag for flag, add_on in self.components.iteritems() if add_on.scan(self.hostname)]
        self._flags.update(add_on_flags)

    @cached_property
    def components(self):
        return self.get_components()

    def get_components(self):
        # dictionary of flag name and component scanner. 'anywhere': AnywhereComponent()
        # This is over-ridden in child classes to set up means to find components
        return {}

    @cached_property
    def address(self):
        return get_address(self.hostname, self.ipv6_enabled)

    def get_found_properties(self, properties):
        try:
            property_items = ((x, getattr(self, x, None)) for x in properties)
            return dict(filter(lambda x: x[1], property_items))
        except Exception, e:
            return {}

    def to_dict(self):
        result = self.get_found_properties(self.general_properties)
        result[self.module_name] = self.get_found_properties(self.module_properties)
        if 'no_access' in self.flags:
            logger.info('{host} is not accessible'.format(host=self.hostname))
            return {}
        return result

    def get_product_id(self):
        pass
