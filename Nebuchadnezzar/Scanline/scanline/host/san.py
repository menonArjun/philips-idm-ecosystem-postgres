import logging
from scanline.host import HostScanner


logger = logging.getLogger(__name__)


class HPMSASANHostScanner(HostScanner):
    module_name = 'HPMSASAN'


class DellEqualLogicSANHostScanner(HostScanner):
    module_name = 'DellEqualLogicSAN'


class IBMStorwizeSANHostScanner(HostScanner):
    module_name = 'IBMStorwizeSAN'
