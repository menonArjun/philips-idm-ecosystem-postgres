import logging
from cached_property import cached_property
from scanline.component.isp import VLCaptureComponent, AnywhereComponent, CCAComponent
from scanline.host import HostScanner


logger = logging.getLogger(__name__)


class ISPHostScanner(HostScanner):
    module_name = 'ISP'
    module_properties = HostScanner.module_properties.union(['module_type', 'identifier', 'version',
                                                             'input_folder', 'noncore_node'])

    def __init__(self, hostname, endpoint, isp, tags=None, **kwargs):
        super(ISPHostScanner, self).__init__(hostname, endpoint, tags=tags, **kwargs)
        self.isp = isp
        self.product_id = 'ISPACS'
        self.product_name = 'IntelliSpace PACS'
        self.product_version = self.isp.get_software_version()
        try:
            self._flags.update(self.isp.intrinsic_flags)
        except AttributeError:
            pass

    @cached_property
    def module_type(self):
        return self.isp.get_module_type(self.hostname)

    @cached_property
    def input_folder(self):
        return self.isp.get_input_folder(self.hostname)

    @cached_property
    def version(self):
        return self.isp.software_version

    @cached_property
    def identifier(self):
        return self.isp.identifier

    def get_components(self):
        if self.noncore_node:
            return {}
        if not self.address:
            logger.warning('No address for Host: %s, components not retrieved', self.hostname)
            return {}
        return {
            'anywhere': AnywhereComponent(),
            # Commenting VL Capture as for workaround VL Capture version is not responding for many sites as well as other sites its None
            # 'vlcapture': VLCaptureComponent(), 
            'cca': CCAComponent()
        }

    @cached_property
    def noncore_node(self):
        return self.isp.get_noncore_node()
