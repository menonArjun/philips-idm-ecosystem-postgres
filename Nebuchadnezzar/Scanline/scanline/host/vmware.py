import logging
from pysphere import VIProperty
from scanline.utilities.vmware import create_vi_server
from scanline.host import HostScanner

# from pysphere import MORTypes, VIServer, VITask, VIProperty, VIMor, VIException, FaultTypes, VIApiException
# from pysphere.vi_virtual_machine import VIVirtualMachine
# from pysphere.vi_task import VITask
# from pysphere.resources import VimService_services as VI
# from pysphere.resources import VimService_services_types as VIType


logger = logging.getLogger(__name__)


class vCenterHostScanner(HostScanner):
    module_name = 'vCenter'
    def __init__(self, hostname, endpoint, username, password, tags=None, **kwargs):
        super(vCenterHostScanner, self).__init__(hostname, endpoint, tags=tags, **kwargs)
        self.username = username
        self.password = password

    def get_facts(self):
        facts = {}
        try:
            with create_vi_server(self.hostname, self.username, self.password) as server:
                facts["datacenters"] = self.get_datacenters(server)
                facts["resource_pools"] = self.get_resource_pools(server)
                facts["clusters"] = self.get_clusters(server)
                facts["hosts"] = self.get_hosts(server)
                facts["vms"] = self.get_vms(server)
        except Exception as e:
            logger.error("Connection error on %s : %s", self.hostname, e)
        return facts

    @staticmethod
    def get_datacenters(server):
        logger.info("Gathering datacenters")
        datacenters = {}
        for ds_mor, name in server.get_datacenters().items():
            datacenters[ds_mor] = {"name": name, "datastores": [], "network": []}
            props = VIProperty(server, ds_mor)

            if hasattr(props, "datastore"):
                for datastore in props.datastore:
                    new_datastore = {"name": datastore.info.name, "freeSpace": datastore.info.freeSpace,
                                     "type": datastore.summary.type, "capacity": datastore.summary.capacity}
                    datacenters[ds_mor]["datastores"].append(new_datastore)

            if hasattr(props, "network"):
                for network in props.network:
                    datacenters[ds_mor]["network"].append(network.name)
        return datacenters

    @staticmethod
    def get_resource_pools(server):
        logger.info("Gathering resource pools")
        resource_pools = {}
        for ds_mor, name in server.get_resource_pools().items():
            resource_pools[ds_mor] = {"name": name, "cpuAllocation": {}, "memoryAllocation": {}}

            props = VIProperty(server, ds_mor)
            if hasattr(props.config.cpuAllocation, "limit"):
                resource_pools[ds_mor]["cpuAllocation"]["limit"] = props.config.cpuAllocation.limit

            if hasattr(props.config.cpuAllocation, "reservation"):
                resource_pools[ds_mor]["cpuAllocation"]["reservation"] = props.config.cpuAllocation.reservation

            if hasattr(props.config.memoryAllocation, "limit"):
                resource_pools[ds_mor]["memoryAllocation"]["limit"] = props.config.memoryAllocation.limit

            if hasattr(props.config.memoryAllocation, "reservation"):
                resource_pools[ds_mor]["memoryAllocation"]["reservation"] = props.config.memoryAllocation.reservation
        return resource_pools

    @staticmethod
    def get_clusters(server):
        logger.info("Gathering clusters")
        clusters = {}
        for ds_mor, name in server.get_clusters().items():
            clusters[ds_mor] = {"name": name, "dasConfig": {}, "drsConfig": {}}
            props = VIProperty(server, ds_mor)

            if hasattr(props.configuration, "dasConfig"):
                if hasattr(props.configuration.dasConfig, "admissionControlEnabled"):
                    clusters[ds_mor]["dasConfig"][
                        "admissionControlEnabled"] = props.configuration.dasConfig.admissionControlEnabled

                if hasattr(props.configuration.dasConfig, "enabled"):
                    clusters[ds_mor]["dasConfig"]["enabled"] = props.configuration.dasConfig.enabled

                if hasattr(props.configuration.dasConfig, "failoverLevel"):
                    clusters[ds_mor]["dasConfig"]["failoverLevel"] = props.configuration.dasConfig.failoverLevel

            if hasattr(props.configuration, "drsConfig"):
                if hasattr(props.configuration.drsConfig, "vmotionRate"):
                    clusters[ds_mor]["drsConfig"]["vmotionRate"] = props.configuration.drsConfig.vmotionRate

                if hasattr(props.configuration.drsConfig, "enabled"):
                    clusters[ds_mor]["drsConfig"]["enabled"] = props.configuration.drsConfig.enabled

                if hasattr(props.configuration.drsConfig, "defaultVmBehavior"):
                    clusters[ds_mor]["drsConfig"]["defaultVmBehavior"] = props.configuration.drsConfig.defaultVmBehavior
        return clusters

    @staticmethod
    def get_hosts(server):
        logger.info("Gathering hosts")
        hosts = {}
        for ds_mor, name in server.get_hosts().items():
            hosts[ds_mor] = {
                "name": name,
                "product": {},
                "hardware": {},
                "runtime": {},
                "network": [],
                "datastore": []}
            props = VIProperty(server, ds_mor)

            if hasattr(props.summary, "config"):
                if hasattr(props.summary.config, "product"):
                    hosts[ds_mor]["product"]["fullName"] = props.summary.config.product.fullName
                    hosts[ds_mor]["product"]["osType"] = props.summary.config.product.osType
                    hosts[ds_mor]["product"][
                        "productLineId"] = props.summary.config.product.productLineId
                    hosts[ds_mor]["product"]["vendor"] = props.summary.config.product.vendor
                    hosts[ds_mor]["product"]["version"] = props.summary.config.product.version

                    if hasattr(props.summary.config, "datastorePrincipal"):
                        hosts[ds_mor]["product"][
                            "datastorePrincipal"] = props.summary.config.datastorePrincipal

                    hosts[ds_mor]["product"]["hyperThread"] = {}
                    if hasattr(props.summary.config, "hyperThread"):
                        hosts[ds_mor]["product"]["hyperThread"][
                            "active"] = props.summary.config.hyperThread.active
                        hosts[ds_mor]["product"]["hyperThread"][
                            "available"] = props.summary.config.hyperThread.available
                        hosts[ds_mor]["product"]["hyperThread"][
                            "config"] = props.summary.config.hyperThread.config

                    hosts[ds_mor]["product"]["network"] = {"consoleIpRouteConfig": {},
                                                           "consoleVnic": [],
                                                           "dnsConfig": {},
                                                           "ipRouteConfig": {},
                                                           "pnic": [],
                                                           "portgroup": [],
                                                           "vnic": [],
                                                           "vswitch": []}
                    # if hasattr(props.summary.config, "network"):
                    #
                    #     if hasattr(props.summary.config.network, "consoleIpRouteConfig"):
                    #         if hasattr(props.summary.config.network.consoleIpRouteConfig, "defaultGateway"):
                    # hosts[ds_mor]["product"]["network"]["consoleIpRouteConfig"]["defaultGateway"] =
                    #            props.summary.config.network.consoleIpRouteConfig.defaultGateway
                    #         if hasattr(props.summary.config.network.consoleIpRouteConfig, "gatewayDevice"):
                    # hosts[ds_mor]["product"]["network"]["consoleIpRouteConfig"]["gatewayDevice"] =
                    # props.summary.config.network.consoleIpRouteConfig.gatewayDevice
                    #
                    # TODO Add the rest of the attributes if needed
                    hosts[ds_mor]["product"]["storageDevice"] = {}
                    hosts[ds_mor]["product"]["vMotion"] = {}

            if hasattr(props.summary, "hardware"):
                if hasattr(props.summary, "hardware"):
                    hosts[ds_mor]["hardware"]["cpuMhz"] = props.summary.hardware.cpuMhz
                    hosts[ds_mor]["hardware"]["cpuModel"] = props.summary.hardware.cpuModel
                    hosts[ds_mor]["hardware"]["memorySize"] = props.summary.hardware.memorySize
                    hosts[ds_mor]["hardware"]["model"] = props.summary.hardware.model
                    hosts[ds_mor]["hardware"]["numCpuCores"] = props.summary.hardware.numCpuCores
                    hosts[ds_mor]["hardware"]["numCpuPkgs"] = props.summary.hardware.numCpuPkgs
                    hosts[ds_mor]["hardware"]["numCpuThreads"] = props.summary.hardware.numCpuThreads
                    hosts[ds_mor]["hardware"]["numHBAs"] = props.summary.hardware.numHBAs
                    hosts[ds_mor]["hardware"]["numNics"] = props.summary.hardware.numNics
                    hosts[ds_mor]["hardware"]["uuid"] = props.summary.hardware.uuid
                    hosts[ds_mor]["hardware"]["vendor"] = props.summary.hardware.vendor

            if hasattr(props.summary, "runtime"):
                hosts[ds_mor]["runtime"]["connectionState"] = props.summary.runtime.connectionState
                hosts[ds_mor]["runtime"]["inMaintenanceMode"] = props.summary.runtime.inMaintenanceMode

                # if hasattr(props, "network"):
                #    for network in props.network:
                #        hosts[ds_mor]["network"].append(network.name)

                # if hasattr(props, "datastore"):
                #    for datastore in props.datastore:
                #        hosts[ds_mor]["datastore"].append(datastore.info.name)

        return hosts

    @staticmethod
    def get_vms(server):
        logger.info("Gathering vms")
        vms = []
        for name in server.get_registered_vms():
            new_vm = {}
            vm = server.get_vm_by_path(name)

            new_vm["overallStatus"] = vm.properties.summary.overallStatus

            # vm.properties.summary.config
            new_vm["name"] = vm.properties.summary.config.name
            new_vm["template"] = vm.properties.summary.config.template
            # new_vm["vmPathName"] = vm.properties.summary.config.vmPathName

            new_vm["connectionState"] = vm.properties.runtime.connectionState
            new_vm["powerState"] = vm.properties.runtime.powerState

            # if hasattr(vm.properties.summary.config, "annotation"): new_vm[
            #    "annotation"] = vm.properties.summary.config.annotation
            if hasattr(vm.properties.summary.config, "guestFullName"):
                new_vm["guestFullName"] = vm.properties.summary.config.guestFullName
            if hasattr(vm.properties.summary.config, "guestId"):
                new_vm["guestId"] = vm.properties.summary.config.guestId
            if hasattr(vm.properties.summary.config, "memorySizeMB"):
                new_vm["memorySizeMB"] = vm.properties.summary.config.memorySizeMB
            if hasattr(vm.properties.summary.config, "numCpu"):
                new_vm["numCpu"] = vm.properties.summary.config.numCpu
            if hasattr(vm.properties.summary.config, "numEthernetCards"):
                new_vm["numEthernetCards"] = vm.properties.summary.config.numEthernetCards
            if hasattr(vm.properties.summary.config, "numVirtualDisks"):
                new_vm["numVirtualDisks"] = vm.properties.summary.config.numVirtualDisks
            if hasattr(vm.properties.summary.config, "uuid"):
                new_vm["uuid"] = vm.properties.summary.config.uuid

            new_vm["nics"] = []
            new_vm["disks"] = []
            # for dev in vm_obj.properties.config.hardware.device
            for device in vm.get_property("devices").values():
                device_type = device.get('type')
                if device_type in ["VirtualE1000", "VirtualE1000e", "VirtualPCNet32", "VirtualVmxnet",
                                   "VirtualVmxnet3"]:
                    new_nic = {"type": device_type}
                    if device.get('label'):
                        new_nic["label"] = device.get('label')
                    if device.get('macAddress'):
                        new_nic["macAddress"] = device.get('macAddress')
                    if device.get('summary'):
                        new_nic["summary"] = device.get('summary')
                    new_vm["nics"].append(new_nic)

                if device_type in ["VirtualDisk"]:
                    new_disk = {}
                    if device.get('label'):
                        new_disk["label"] = device.get('label')
                    if device.get('capacityInKB'):
                        new_disk["capacityInKB"] = device.get('capacityInKB')
                    new_vm["disks"].append(new_disk)

            new_vm["guest"] = {}
            # vm.properties.vm.guest*
            if hasattr(vm.properties, "guest"):
                new_vm["guest"]["guestState"] = vm.properties.guest.guestState

                if hasattr(vm.properties.guest, "guestFamily"):
                    new_vm["guest"]["guestFamily"] = vm.properties.guest.guestFamily
                if hasattr(vm.properties.guest, "guestFullName"):
                    new_vm["guest"]["guestFullName"] = vm.properties.guest.guestFullName
                if hasattr(vm.properties.guest, "guestId"):
                    new_vm["guest"]["guestId"] = vm.properties.guest.guestId
                if hasattr(vm.properties.guest, "hostName"):
                    new_vm["guest"]["hostName"] = vm.properties.guest.hostName
                if hasattr(vm.properties.guest, "ipAddress"):
                    new_vm["guest"]["ipAddress"] = vm.properties.guest.ipAddress
                if hasattr(vm.properties.guest, "toolsStatus"):
                    new_vm["guest"]["toolsStatus"] = vm.properties.guest.toolsStatus
                if hasattr(vm.properties.guest, "toolsVersion"):
                    new_vm["guest"]["toolsVersion"] = vm.properties.guest.toolsVersion

                new_vm["guest"]["net"] = []
                if hasattr(vm.properties.guest, "net"):
                    for guest_nic in vm.properties.guest.net:
                        new_nic = {"connected": guest_nic.connected}
                        if hasattr(guest_nic, "ipAddress"):
                            new_nic["ipAddress"] = guest_nic.ipAddress  # actutally a string []
                        if hasattr(guest_nic, "macAddress"):
                            new_nic["macAddress"] = guest_nic.macAddress
                        if hasattr(guest_nic, "network"):
                            new_nic["network"] = guest_nic.network
                        new_vm["guest"]["net"].append(new_nic)

                new_vm["guest"]["disk"] = []
                if hasattr(vm.properties.guest, "disk"):
                    for guest_disk in vm.properties.guest.disk:
                        new_disk = {}
                        if hasattr(guest_disk, "capacity"):
                            new_disk["capacity"] = guest_disk.capacity
                        if hasattr(guest_disk, "freeSpace"):
                            new_disk["freeSpace"] = guest_disk.freeSpace
                        if hasattr(guest_disk, "diskPath"):
                            new_disk["diskPath"] = guest_disk.diskPath
                        new_vm["guest"]["disk"].append(new_disk)
            vms.append(new_vm)
        return vms

    def to_dict(self):
        logger.info('Gathering vCenter site facts from host %s', self.hostname)
        result = {'vCenter': self.get_facts()}

        if self.address:
            result['address'] = self.address

        result['vCenter']['endpoint'] = self.endpoint
        if self.tags:
            result['vCenter']['tags'] = self.tags

        return result
