import logging
from scanline.host import HostScanner


logger = logging.getLogger(__name__)


class IECGGDHostScanner(HostScanner):
    module_name = 'iECGGD'
