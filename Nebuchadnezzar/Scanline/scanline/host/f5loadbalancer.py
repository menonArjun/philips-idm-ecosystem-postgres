from scanline.host import HostScanner


class F5LBHostScanner(HostScanner):
    module_name = 'F5LoadBalancer'

    def __init__(self, hostname, endpoint, tags=None, product_id='F5LoadBalancer',
                 product_name='F5LoadBalancer', product_version='NA', **kwargs):
        super(F5LBHostScanner, self).__init__(
            hostname, endpoint, tags=tags, product_id=product_id,
            product_name=product_name, product_version=product_version, **kwargs)
