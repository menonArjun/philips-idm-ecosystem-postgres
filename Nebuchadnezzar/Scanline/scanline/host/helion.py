import logging
from scanline.host import HostScanner
#from scanline.host.windows import WindowsHostScanner


logger = logging.getLogger(__name__)


class HelionHostScanner(HostScanner):
    module_name = 'Helion'
