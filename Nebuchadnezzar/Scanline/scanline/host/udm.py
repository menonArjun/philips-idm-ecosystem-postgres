import logging
from scanline.host import HostScanner
from scanline.host.linux import LinuxHostScanner


logger = logging.getLogger(__name__)


class UDMRedisHostScanner(LinuxHostScanner):
    module_name = 'UDMRedis'
