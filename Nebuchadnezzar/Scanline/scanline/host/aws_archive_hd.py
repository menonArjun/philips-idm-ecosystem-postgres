import logging
import logging
from scanline.host import HostScanner
from scanline.host.windows import WindowsHostScanner


logger = logging.getLogger(__name__)


class AWAHDSHostScanner(WindowsHostScanner):
    module_name = 'AdvancedWorkflowArchiveHDServices'
    productid = 'AWS'
    productname = 'AdvancedWorkflowServices'
    productversion = ''
