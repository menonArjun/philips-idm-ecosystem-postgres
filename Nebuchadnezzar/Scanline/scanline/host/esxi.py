import logging
from cached_property import cached_property
from scanline.host import HostScanner


logger = logging.getLogger(__name__)


class ESXiHostScanner(HostScanner):
    module_name = 'ESXi'
    general_properties = HostScanner.general_properties.union(['manufacturer', 'model', 'servicetag'])

    def __init__(self, vmware_host, endpoint, tags=None, **kwargs):
        super(ESXiHostScanner, self).__init__(self.get_hostname(vmware_host), endpoint, tags=tags, **kwargs)
        self.vmware_host = vmware_host

    @staticmethod
    def get_hostname(vmware_host):
        return str(vmware_host.name)

    @cached_property
    def manufacturer(self):
        return self.vmware_host.summary.hardware.vendor

    @cached_property
    def model(self):
        return self.vmware_host.summary.hardware.model

    @cached_property
    def servicetag(self):
        service_tag = ''
        for host in self.vmware_host.summary.hardware.otherIdentifyingInfo:
            if host.identifierType.key == 'ServiceTag':
                service_tag = host.identifierValue
                break
        return service_tag
