import logging
from cached_property import cached_property
from scanline.utilities.wmi import WMIHostDriller
from scanline.host import HostScanner
from scanline.component.windows import WindowsServiceComponent


logger = logging.getLogger(__name__)


class WindowsHostScanner(HostScanner):
    module_name = 'Windows'
    general_properties = HostScanner.general_properties.union(['manufacturer', 'domain', 'uuid'])
    module_properties = HostScanner.module_properties.union(
        ['name', 'service_pack', 'language', 'serial_number', 'service_pack_version', 'version']
    )

    def __init__(self, hostname, endpoint, username, password, domain=None, tags=None, **kwargs):
        super(WindowsHostScanner, self).__init__(hostname, endpoint, tags=tags, **kwargs)
        self.user = username
        self.password = password
        self.domain = domain

    @cached_property
    def manufacturer(self):
        return self.get_from_driller('get_manufacturer')

    @cached_property
    def uuid(self):
        return self.get_from_driller('get_uuid')

    @cached_property
    def name(self):
        return self.windows_info.get('Name')

    @cached_property
    def service_pack(self):
        return self.windows_info.get('ServicePack')

    @cached_property
    def language(self):
        return self.windows_info.get('OSLanguage')

    @cached_property
    def serial_number(self):
        return self.windows_info.get('SerialNumber')

    @cached_property
    def service_pack_version(self):
        return self.windows_info.get('ServicePackVersion')

    @cached_property
    def version(self):
        return self.windows_info.get('Version')

    @cached_property
    def windows_info(self):
        return self.get_from_driller('get_windows_info', {})

    @cached_property
    def wmi_driller(self):
        return self.get_wmi_driller()

    def get_wmi_driller(self):
        if (not self.user) or (not self.address):
            logger.warning(
                'WMI Driller for Host: %s not possible with user: "%s" and address: "%s"',
                self.hostname,
                self.user,
                self.address
            )
            return

        # using hostname for host may cause reverse DNS lookup on the windows host, crashing wmic
        driller =  WMIHostDriller(host=self.address, user=self.user, password=self.password)

        domains = [None]
        if self.domain:
            domains.append(self.domain)

        for domain in domains:
            driller.domain = domain
            # get_manufacturer is used as probe for access, the manufacturer query seems to work on all Windows
            if driller.get_manufacturer():
                if domain:
                    self._flags.add('domain_needed')
                break
        else:
            self._flags.add('no_access')
            return
        return driller

    def get_from_driller(self, method, default=None):
        if not self.wmi_driller:
            return default
        return getattr(self.wmi_driller, method)()

    def get_components(self):
        if not self.wmi_driller:
            logger.warning('No WMI Driller for Host: %s, components not retrieved', self.hostname)
            return {}
        return {
            'MSMQ': WindowsServiceComponent(self.wmi_driller, 'MSMQ'),
            'MSSQLSERVER': WindowsServiceComponent(self.wmi_driller, 'MSSQLSERVER'),
            'W3SVC': WindowsServiceComponent(self.wmi_driller, 'W3SVC')
        }
