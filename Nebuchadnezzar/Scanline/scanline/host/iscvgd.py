import logging
from scanline.host import HostScanner


logger = logging.getLogger(__name__)


class ISCVGDHostScanner(HostScanner):
    module_name = 'ISCVGD'
