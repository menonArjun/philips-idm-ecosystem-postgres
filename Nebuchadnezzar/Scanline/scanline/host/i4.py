import logging
from scanline.host import HostScanner
from scanline.host.windows import WindowsHostScanner


logger = logging.getLogger(__name__)


class I4HostScanner(WindowsHostScanner):
    module_name = 'I4'
