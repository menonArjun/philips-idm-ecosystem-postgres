import logging
import requests
from lxml import etree
from scanline.host.windows import WindowsHostScanner
from scanline.utilities.http import HTTPRequester


logger = logging.getLogger(__name__)


class AWSHDHostScanner(WindowsHostScanner):
    module_name = 'AdvancedWorkflowServices'

    def __init__(self, hostname, endpoint, tags=None, product_id='AWS',
                 product_name='AdvancedWorkflowServices', product_version='NA', **kwargs):
        super(AWSHDHostScanner, self).__init__(
            hostname, endpoint, tags=tags, product_id=product_id,
            product_name=product_name, product_version=product_version, **kwargs)
        self.HTTPRequester = HTTPRequester()

        version_url = ''.join(['https://',
                               self.hostname,
                               '/AdvancedWorkflowNagios/AdvancedWorkflowNagios.asmx/version'
                               ])
        querystring = {'server': self.hostname, 'param': 'version'}
        try:
            response = self.HTTPRequester.suppressed_get(url=version_url, data=querystring)
            if 'okay' in response.text.lower():
                response = response.text.encode('utf-8')
                val = etree.fromstring(response, parser=etree.XMLParser(encoding='utf-8'))
                for tag in val.iter():
                    if not len(tag):
                        self.product_version = tag.text.split(':')[-1].strip()
        except Exception:
            logger.warning('version information not available for host {host}'.format(host=self.hostname))
