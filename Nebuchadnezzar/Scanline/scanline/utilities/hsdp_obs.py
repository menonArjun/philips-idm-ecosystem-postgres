import requests
import json

from scanline.utilities.db import QueryMgr
from scanline.utilities.encryption import AESCipher
from lxml import objectify, etree

KEY_HEX_ARY = [0xA6, 0x46, 0x74, 0xE2, 0xFD, 0xDE, 0x3A, 0x85,
               0x74, 0x2C, 0x18, 0x11, 0xFC, 0xC3, 0xFB, 0x10,
               0x48, 0x0E, 0xC3, 0xC6, 0xE8, 0x4C, 0x93, 0x3B,
               0x6D, 0x8D, 0x1C, 0x68, 0xF9, 0x5A, 0x77, 0xA0]

IV_HEX_ARY = [0x36, 0x1B, 0x9F, 0x53, 0xB2, 0x4D, 0x60, 0x56,
              0xE1, 0x32, 0x2D, 0xE6, 0x26, 0x58, 0x02, 0x4B]


class HSDPConnection(QueryMgr):
    where_cdn = "CONFIGTYPECD = 'CREDENTIALS'"
    key_fields = ['CredentialName', 'CredentialStoreURL',
                  'UserName', 'PassKey',
                  'SharedKey', 'Secretkey' ]

    def to_dict(self):
        data_obj = super(HSDPConnection, self).execute()
        ret_dict = dict()
        xml_obj = objectify.fromstring(str(data_obj[0]))
        for kf in self.key_fields:
            ret_dict[kf] = xml_obj.xpath('//' + kf)[0].text
        return ret_dict


class TIERConnection(QueryMgr):
    where_cdn = "CONFIGTYPECD = 'TIERCONFIG'"
    config = ['OBS', 'CFT', 'CDT', 'CBT']

    def to_dict(self):
        data_obj = super(TIERConnection, self).execute()
        ret_dict = dict()
        xml_obj = objectify.fromstring(str(data_obj[0]))
        for tier in xml_obj.findall('TierConfig'):
            if not tier.find('TierType'):
                continue
            tier_type = tier.find('TierType').text
            if tier_type in self.config:
                tier_item = {tier_type: self.get_data_dict(tier)}
                ret_dict.update(tier_item)
        return ret_dict

    def get_data_dict(self, tier):
        return {
            'ProxyUrl': tier.find('ProxyUrl').text,
            'ServiceUrl': tier.find('ServiceUrl').text,
        }


class OBJSTConnection(QueryMgr):
    where_cdn = "CONFIGTYPECD = 'OBJJSONMULTI'"
    key_fields = ['SecuritySchemaType',
                  'ServiceUrl',
                  'ServicePort',
                  'Tenant',
                  'AccessKey',
                  'PrivateKey']

    def to_dict(self):
        data_obj = super(OBJSTConnection, self).execute()
        ret_dict = dict()
        json_obj = json.loads(data_obj[0])
        redis_config_dict = self.get_redis_config(json_obj)
        data_dict = self.get_data_dict(json_obj)
        ks_url = self.get_keystone_url(data_dict)
        ks_json, payload = self.get_keystone_json(data_dict, ks_url)
        if 'access' in ks_json.keys():
            ret_dict['payload'] = payload
            ret_dict['keystone_url']  = ks_url
            ret_dict['token_issued_at'] = ks_json.get('access').get('token').get('issued_at')
            ret_dict['token_expired_at'] = ks_json.get('access').get('token').get('expires')
            ret_dict['token_id'] = ks_json.get('access').get('token').get('id')
            ret_dict['obs_url'] = ks_json.get('access').get('serviceCatalog')[4].get('endpoints')[0].get('publicURL')
            ret_dict['RedisConfig'] = redis_config_dict
        return ret_dict

    def get_data_dict(self, json_obj):
        return dict((x,y)
                    for x,y in json_obj.iteritems()
                    if x in self.key_fields
                    )

    def get_keystone_url(self, data_dict):
        url = '{0}://{1}:{2}/'.format(data_dict['SecuritySchemaType'].lower(),
                                      data_dict['ServiceUrl'],
                                      data_dict['ServicePort'])
        res = requests.get(url, verify=False)
        return json.loads(res.text)['versions']['values'][1]['links'][0]['href'] + 'tokens'

    def get_keystone_json(self, data_dict, url):
        aes_obj = AESCipher(AESCipher.hex_ary_to_byte_str(KEY_HEX_ARY),
                            AESCipher.hex_ary_to_byte_str(IV_HEX_ARY))
        payload = {'auth': {'passwordCredentials': {'username': aes_obj.
            decrypt(data_dict['AccessKey']),'password': aes_obj.
            decrypt(data_dict['PrivateKey'])},'tenantName': data_dict['Tenant']}}
        headers = {'content_type': 'application/json'}
        res = requests.post(url, data=json.dumps(payload), headers=headers,
                            verify=False)
        return json.loads(res.text), payload

    def get_redis_config(self, json_obj):
        return json_obj['RedisConfig']
