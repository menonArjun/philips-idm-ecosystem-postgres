import logging
from contextlib import closing
import paramiko


logger = logging.getLogger(__name__)


class SSHConnection(object):
    def __init__(self, host, username, password):
        self.host = host
        self.username = username
        self.password = password

    def get_connected_client(self):
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(self.host, username=self.username, password=self.password, timeout=5)
        return client

    def exec_output(self, command):
        logger.debug('Connecting to %s', self.host)
        try:
            with closing(self.get_connected_client()) as conn:
                stdin, stdout, stderr = conn.exec_command(command)
                error = stderr.read()
                output = stdout.read()
        except Exception:
            logger.exception('Could not execute command %s on host %s', command, self.host)
        else:
            if error:
                logger.error('Command %s on host %s returned error %s', command, self.host, error)
                return
            return output