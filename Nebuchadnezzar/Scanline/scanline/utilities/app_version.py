import logging
import re
import json
from HTMLParser import HTMLParser, HTMLParseError
from scanline.utilities.http import HTTPRequester


logger = logging.getLogger(__name__)


class AppVersionHTTPScanner(HTTPRequester):
    PROTOCOLS = ['https', 'http']

    def __init__(self, host, application_name, targets):
        super(AppVersionHTTPScanner, self).__init__()
        self.host = host
        self.application_name = application_name
        self.targets = targets

    def get_page_text(self, path):
        for protocol in self.PROTOCOLS:
            url = '{protocol}://{host}/{path}'.format(protocol=protocol, host=self.host, path=path)
            logger.info('Trying to detect %s at address: %s', self.application_name, url)
            response = self.suppressed_get(url)
            if response:
                logger.info('%s detected at address: %s', self.application_name, url)
                return response.text
            logger.info('%s NOT found at address: %s', self.application_name, url)

    def get_version(self):
        for path, parser_class in self.targets:
            text = self.get_page_text(path)
            if not text:
                continue
            parser = parser_class(text)
            parser.application_name = self.application_name
            version = parser.get_appversion()
            if version:
                return version


class AppVersionHTMLParser(HTMLParser):
    def __init__(self, content):
        HTMLParser.__init__(self)
        self.in_appversion = False
        self.appversion_content = ''
        self.attempt_feed(content)
        self.application_name = ''

    def attempt_feed(self, content):
        try:
            self.feed(content)
        except HTMLParseError:
            pass

    def handle_starttag(self, tag, attrs):
        attributes = dict(map(lambda x: x.upper(), attr) for attr in attrs)
        if attributes.get('ID') == 'APP_VERSION':
            self.in_appversion = True

    def handle_endtag(self, tag):
        if self.in_appversion:
            self.in_appversion = False

    def handle_data(self, data):
        if self.in_appversion:
            self.appversion_content = data.strip()

    def get_appversion(self):
        if self.appversion_content:
            m = re.search('\d+\.\d+\.\d+\.\d+', self.appversion_content)
            if m:
                return m.group()


class AppVersionJSONRetriever(object):
    def __init__(self, content):
        self.content = content
        self.data = None
        self.attempt_load()
        self.application_name = ''

    def attempt_load(self):
        try:
            self.data = json.loads(self.content)
        except ValueError:
            self.data = {}

    def get_appversion(self):
        try:
            matches = (app for app in self.data['Applications'] if app['ApplicationName'] == self.application_name)
            application = next(matches, None)
        except KeyError:
            return
        if application:
            return application.get('Version')
