import redis

from scanline.utilities.db import HSDPConnection
from scanline.utilities.db import TIERConnection
from scanline.utilities.db import OBJSTConnection

class CacheMgr(object):
	url = 'localhost'
	server = redis.Redis(url)

	@classmethod
	def set(cls, key, value):
		if type(value) == dict:
		    cls.server.hmset(key, value)
		else:
		    cls.server.set(key , value)

	@classmethod
	def get(cls, key, multi=False):
		if multi:
		    return cls.server.hgetall(key)
		else:
		    return cls.server.get(key)

	@classmethod
	def remove(cls, key):
		cls.server.delete(key)



class CacheUpdater():
    def execute(self):
        if CacheMgr.get('HSDP') != None:
            CacheMgr.set('HSDP',HSDPConnection().to_dict())
        if CacheMgr.get('TIER') != None:
            CacheMgr.set('TIER',TIERConnection().to_dict())
        if CacheMgr.get('OBST') != None:
            CacheMgr.set('OBST', OBJSTConnection().to_dict())


