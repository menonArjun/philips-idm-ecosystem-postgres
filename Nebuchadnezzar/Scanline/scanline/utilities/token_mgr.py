import hmac
import hashlib
import base64
import linecache
import logging

from datetime import datetime
from phimutils.resource import NagiosResourcer
from lxml import objectify, etree
from scanline.utilities.http import HTTPRequester
from StringIO import StringIO

logger = logging.getLogger(__name__)

DISCOVERY = {
    'NAGIOS_RESOURCE_FILE': '/etc/philips/shinken/resource.d/resource.cfg',
    'SECRET_FILE': '/etc/philips/secret'
}

HMAC_IDENTIFIERS = {
    'TOKEN': 'iSiteWebApplication',  # Not required
    'TIMESTAMP_LABEL': 'Timestamp',
    'HASH_LABEL': 'Hash',
    'APPLICATIONIDENTIFIER': 'iSiteRadiology',  # Not Required
    'APPID_LABEL': 'AppId',
    'APPID': 'IDM',
    'SECRET_KEY': '',  # Loaded from resource.cfg
    'SERVERTIMESTAMP_LABEL': 'ServerTimeStamp',
    'RETRIES': 3,  # Possibly part of the protocol
}


config_scheme = 'http'
config_path = 'InfrastructureServices/ConfigurationService/ConfigurationService.ashx'
auth_scheme = 'https'
auth_path = 'InfrastructureServices/AuthenticationService/v1_0/authenticationservice.ashx'
auth_header_syntax = 'HMAC {TIMESTAMP_LABEL}={ISODATE};{HASH_LABEL}={HASH};{APPID_LABEL}={APPID}'
url_format = '{scheme}://{server}/{path}'


def get_auth_url(server):
    return url_format.format(scheme=auth_scheme, server=server, path=auth_path)


def get_config_url(server):
    return url_format.format(scheme=config_scheme, server=server, path=config_path)


def get_hashkey(key, timestamp):
    hashkey = hmac.new(key, msg=timestamp, digestmod=hashlib.sha256).digest()
    return base64.b64encode(hashkey).decode()


def get_auth_header(token):
    return dict(Authorization=token)


def get_iso_date():
    return datetime.utcnow()


def get_server_info_query():
    """Generate <GetServerInfo/>"""
    el_maker = objectify.ElementMaker(annotate=False)
    return etree.tostring(el_maker.GetServerInfo())


def get_secret_key():
    secret = linecache.getline(DISCOVERY['SECRET_FILE'], 1).strip()
    nr = NagiosResourcer(DISCOVERY['NAGIOS_RESOURCE_FILE'], secret)
    secret_key = nr.get_resource("$USER113$")
    return secret_key


def get_auth_header_str(isodate):
    iso_formatted_date = isodate.isoformat()
    return auth_header_syntax.format(
        ISODATE=iso_formatted_date,
        HASH=get_hashkey(HMAC_IDENTIFIERS['SECRET_KEY'], iso_formatted_date),
        **HMAC_IDENTIFIERS
    )


def get_formatted_token():
    '''
    Format: 'HMAC Timestamp=2016-07-14T16:40:21.9763242+05:30;Hash=7zFZTBpbmmJvEPDry7b7XdB3LMl5tNRpwb5UBINpyT0=;AppId=IDM'
    '''
    HMAC_IDENTIFIERS["SECRET_KEY"] = get_secret_key()

    isodate = get_iso_date()
    auth_header = get_auth_header_str(isodate)
    return auth_header


def valid_version(in_xml):
    f_xml = StringIO(in_xml)
    root = etree.parse(f_xml)
    version = root.xpath('//SoftwareVersion')[0].text
    return int(version.split(',')[2]) >= 550


def authenticate_token(auth_token, auth_server):
    hr = HTTPRequester()
    response = hr.suppressed_post(
        get_config_url(auth_server),
        get_server_info_query(),
        get_auth_header(auth_token),
        ok_only=False
    )
    return valid_version(response.text)

if __name__ == '__main__':
    ft = get_formatted_token()
    print ft
    print authenticate_token(ft, 'PAC55IF1.PAC55.iSyntax.net')
