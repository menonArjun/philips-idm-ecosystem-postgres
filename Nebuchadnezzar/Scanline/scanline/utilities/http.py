import logging
import requests
from bs4 import BeautifulSoup


logger = logging.getLogger(__name__)
requests.packages.urllib3.disable_warnings()


class HTTPRequester(object):
    POST_TIMEOUT = 5

    def __init__(self):
        self._session = None

    @property
    def get(self):
        return self._session.get if self._session else requests.get

    @property
    def post(self):
        return self._session.post if self._session else requests.post

    def suppressed_get(self, url, data=None):
        logger.info('Trying to get %s', url)
        try:
            params = dict(url=url, params=data, verify=False)
            response = self.get(**params)
            response.raise_for_status()
            return response
        except requests.exceptions.RequestException as ex:
            logger.info('Exception getting: %s : %s', url, ex)

    def suppressed_post(self, url, data, headers=None, ok_only=True):
        logger.info('Posting to %s', url)
        try:
            params = dict(url=url, data=data, verify=False, timeout=self.POST_TIMEOUT)
            if headers:
                params['headers'] = headers
            response = self.post(**params)
            if ok_only:
                response.raise_for_status()
            logger.debug('Response content received: %s', response.content)
            return response
        except requests.exceptions.RequestException as ex:
            logger.info('Error posting to: %s : %s', url, ex)

    def is_ok_response(self, response):
        return response.status_code == requests.codes.ok

    def suppressed_get_parsed(self, url, headers=None, auth=None, section=None,
                              attrs={}):
        logger.info('Trying to get %s', url)
        try:
            params = dict(url=url, headers=headers, auth=auth, verify=False)
            response = self.get(**params)
            response.raise_for_status()
            parsed_html = BeautifulSoup(response.content)
            message = parsed_html.body.find(section, attrs=attrs).text
            return message.encode('ascii', 'ignore').decode('ascii')
        except Exception as ex:
            logger.info('Exception getting: %s : %s', url, ex)
