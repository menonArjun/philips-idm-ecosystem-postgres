import hmac
import hashlib
import base64


def get_hashkey(key, timestamp):
    '''
    Generate HMAC key from secret key and timestamp (used as msg)
    :param timestamp:
    :return: base64 encoded HMAC key
    '''
    hashkey = hmac.new(key, msg=timestamp, digestmod=hashlib.sha256).digest()
    return base64.b64encode(hashkey).decode()
