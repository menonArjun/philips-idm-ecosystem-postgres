import logging
from scanline.product import ProductScanner
from scanline.product.esxi import ESXiProductScanner
from scanline.host.vmware import vCenterHostScanner


logger = logging.getLogger(__name__)


class VMwareProductScanner(ProductScanner):
    def __init__(self, scanner, address, username, password, tags=None, host_scanner=None, product_id=None):
        super(VMwareProductScanner, self).__init__(scanner, address, tags=tags, host_scanner=host_scanner, product_id=None)
        self.username = username
        self.password = password
        self.product_id = product_id
        self.esxi_scanner = ESXiProductScanner(
            scanner=scanner,
            address=self.address,
            username=self.username,
            password=self.password,
            tags=self.tags
        )
        self.vcenter_scanner = ProductScanner(
            host_scanner=vCenterHostScanner,
            scanner=scanner,
            address=self.address,
            username=self.username,
            password=self.password,
            tags=self.tags
        )

    def site_facts(self):
        logger.info('Gathering site facts from endpoint %s', self.endpoint)
        result = self.esxi_scanner.site_facts()
        vcenter_data = self.vcenter_scanner.site_facts()
        vcenter_product_id = vcenter_data[self.endpoint['address']]['product_id']
        if vcenter_product_id == 'vCenter':
            vcenter_data[self.endpoint['address']]['product_id'] = 'NA'
        result.update(vcenter_data)
        result.update({"product_id": self.product_id})
        return result
