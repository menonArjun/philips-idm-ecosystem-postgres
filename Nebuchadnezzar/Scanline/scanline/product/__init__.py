import logging
from scanline.host import HostScanner
from scanline.utilities.http import HTTPRequester
from cached_property import cached_property


logger = logging.getLogger(__name__)


class ProductScanner(object):
    def __init__(self, scanner, address, tags=None, host_scanner=None, **kwargs):
        self.address = address
        self.tags = tags
        self.host_scanner = host_scanner
        self.endpoint = {'scanner': scanner, 'address': address}
        self.kwargs = kwargs
        self.ipv6_enabled = kwargs.get('ipv6_enabled', False)

    def get_hosts(self):
        yield self.address

    def get_hostname(self, host):
        return host

    def scan_host(self, host):
        return self.host_scanner(host, self.endpoint, tags=self.tags, **self.kwargs).to_dict()

    def get_present_modules(self, host_facts):
        return list(set(host_facts.keys()).intersection(HostScanner.get_host_scanners()))

    def get_host_facts(self, host):
        result = self.scan_host(host)
        if not result:
            return
        if 'address' not in result:
            result['address'] = self.address
        result['modules'] = self.get_present_modules(result)
        if self.host_scanner:
            host_scanner = self.host_scanner(host, self.endpoint, tags=self.tags, **self.kwargs)
        result['product_id'] = result['product_id'] if 'product_id' in result and result['product_id'] not in [ 'NA'] else (
            self.kwargs.get('product_id', None) or host_scanner.get_product_id() if self.host_scanner else None)
        return result

    def site_facts(self):
        logger.info('Gathering site facts from endpoint %s', self.endpoint)
        return dict((self.get_hostname(host), self.get_host_facts(host)) for host in self.get_hosts() if self.get_host_facts(host))


class GenericProductScanner(ProductScanner):

    def __init__(self, scanner, address, tags=None, host_scanner=None, **kwargs):
        super(GenericProductScanner, self).__init__(scanner, address, tags=tags, host_scanner=host_scanner, **kwargs)
        self.uri = self.kwargs.get('discovery_url', '')
        self._session = None
        self.roles = {}
        self.role = set()
        self.HTTPRequester = HTTPRequester()

    @cached_property
    def discovery_info(self):
        _discovery_info = {}
        if self.uri:
            response = self.HTTPRequester.suppressed_get(self.uri)
            if response:
                _discovery_info = response.json()
        else:
            msg = 'Discovery URL Missing for scanner - {scanner}, Address - {address}'
            logger.info(msg.format(**self.endpoint))
        return _discovery_info

    def get_hosts(self):
        host_info = self.discovery_info.get('hostinfo', [])
        if not host_info:
            msg = 'Host information is missing in json fetched from {uri}'
            logger.info(msg.format(uri=self.uri))
        return host_info

    def get_hostname(self, host):
        return host.get('address', '')

    def get_host_role(self, host):
        if host['address'] not in self.roles.keys():
            self.role = set()
        self.role.add(host['role'])
        self.roles[host['address']] = self.role
        return list(self.roles[host['address']])

    def scan_host(self, host):
        host['address'] = host['address'] if 'address' in host else host.pop('name')
        logger.info('Gathering host attributes from {address}'.format(
            address=host['address']))
        _scanner_obj = self.host_scanner(host['address'], self.endpoint, tags=self.tags,
                                         product_id=self.discovery_info.get('productid', ''),
                                         product_name=self.discovery_info.get('productname', ''),
                                         product_version=self.discovery_info.get('productversion', ''),
                                         role=self.get_host_role(host), **self.kwargs)
        result = _scanner_obj.to_dict()
        if not result:
            return
        logger.info('final result is {result} from {address}'.format(
            result=result, address=host['address']))
        return result

    def site_facts(self):
        logger.info('Gathering site facts from endpoint %s', self.endpoint)
        return dict((self.get_hostname(host), self.get_host_facts(host)) for host in self.get_hosts() if self.get_host_facts(host))
