import logging

from scanline.product import GenericProductScanner


logger = logging.getLogger(__name__)


class IECGProductScanner(GenericProductScanner):
    pass