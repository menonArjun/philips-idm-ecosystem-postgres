import zlib
import struct
import logging
import requests
import re
import linecache
from lxml import objectify, etree
from operator import attrgetter
from datetime import datetime, timedelta
from dateutil import parser
from cached_property import cached_property
from scanline.utilities.http import HTTPRequester
from scanline.utilities import token_mgr
from scanline.product import ProductScanner
from scanline.host.isp import ISPHostScanner
from scanline.host.windows import WindowsHostScanner
from scanline.host.udm import UDMRedisHostScanner
from scanline.utilities.hsdp_obs import OBJSTConnection
from scanline.utilities.auth import get_hashkey
from phimutils.resource import NagiosResourcer

logger = logging.getLogger(__name__)


DISCOVERY = {
    'NAGIOS_RESOURCE_FILE': '/etc/philips/shinken/resource.d/resource.cfg',
    'SECRET_FILE': '/etc/philips/secret'
}


class ISPProductScanner(ProductScanner):
    def __init__(self, scanner, address, username, password, domain=None, tags=None, host_scanner=None, product_id=None, **kwargs):
        super(ISPProductScanner, self).__init__(scanner, address, tags=tags, host_scanner=host_scanner, product_id=product_id, **kwargs)
        self.username = username
        self.password = password
        self.domain = domain
        self.product_id = product_id
        self.ispdb = ISPDBConfiguration(self.address)
        self.ipv6_enabled = kwargs.get('ipv6_enabled', False)
        self.product_version = ''

    @cached_property
    def isp(self):
        return self.create_isp()

    def create_isp(self):
        isp_classes = [ISP4_1Configuration, ISP4XHiSecConfiguration,
                       ISP4XConfiguration, ISP3XConfiguration]
        while isp_classes:
            isp_class = isp_classes.pop()
            result = isp_class(self.address)
            logger.info('Trying %s as %s', self.address,
                        result.__class__.__name__)
            try:
                if result.get_software_version():
                    self.product_version = result.get_software_version()
                    return result
            except ISPAuthenticationError:
                logger.debug('Could not authenticate to %s as %s',
                             self.address, result.__class__.__name__)
        logger.info('Could not create ISP for %s', self.address)

    def get_hosts(self):
        hosts = self.isp.get_hosts() if self.isp else []
        return iter(hosts)

    def get_dbhosts(self):
        hosts = self.ispdb.get_hosts() if self.ispdb else []
        return iter(hosts)

    def get_redis_hosts(self):
        redis_nodes = []
        try:
            db_obj = OBJSTConnection()
            json_obj = db_obj.to_dict()
            redis_config = json_obj['RedisConfig']
            if redis_config['Enabled']:
                for redis_node in redis_config['RedisEndPoints']:
                    redis_nodes.append(redis_node['ClusterIP'])
        except Exception:
            pass
        return iter(redis_nodes)

    def get_hostname(self, host):
        return str(host)

    def scan_host(self, host):
        hostname = self.get_hostname(host)
        isp_scanner = ISPHostScanner(
            hostname=hostname,
            endpoint=self.endpoint,
            isp=self.isp,
            tags=self.tags,
            ipv6_enabled=self.ipv6_enabled
        )
        result = isp_scanner.to_dict()
        params = dict(
            hostname=hostname,
            endpoint=self.endpoint,
            username=self.username,
            password=self.password,
            domain=self.domain,
            tags=self.tags,
            ipv6_enabled=self.ipv6_enabled
        )
        windows_scanner = WindowsHostScanner(**params)
        windows_facts = windows_scanner.to_dict()
        windows_facts.update(result)
        result = windows_facts
        if self.host_scanner is not None:
            other_scanner = self.host_scanner(**params)
            result.update(other_scanner.to_dict())
        return result

    def scan_dbhost(self, host):
        hostname = self.get_hostname(host)
        isp_scanner = ISPHostScanner(
            hostname=hostname,
            endpoint=self.endpoint,
            isp=self.ispdb,
            tags=self.tags
        )
        result = isp_scanner.to_dict()
        result['product_version'] = self.product_version
        params = dict(
            hostname=hostname,
            endpoint=self.endpoint,
            username=self.username,
            password=self.password,
            domain=self.domain,
            tags=self.tags
        )
        windows_scanner = WindowsHostScanner(**params)
        windows_facts = windows_scanner.to_dict()
        windows_facts.update(result)
        result = windows_facts
        if self.host_scanner is not None:
            other_scanner = self.host_scanner(**params)
            result.update(other_scanner.to_dict())
        return result

    def scan_redis_host(self, host):
        hostname = self.get_hostname(host)
        redis_scanner = UDMRedisHostScanner(
            hostname=hostname,
            endpoint=self.endpoint,
            tags=self.tags
        )
        result = redis_scanner.to_dict()
        result['product_version'] = self.product_version
        params = dict(
            hostname=hostname,
            endpoint=self.endpoint,
            username=self.username,
            password=self.password,
            domain=self.domain,
            tags=self.tags
        )
        result.update({"product_id": self.product_id})
        if self.host_scanner is not None:
            other_scanner = self.host_scanner(**params)
            result.update(other_scanner.to_dict())
        return result

    def get_dbhost_facts(self, host):
        result = self.scan_dbhost(host)
        result['modules'] = self.get_present_modules(result)
        return result

    def get_redis_host_facts(self, host):
        result = self.scan_redis_host(host)
        result['modules'] = self.get_present_modules(result)
        return result

    def site_facts(self):
        result = super(ISPProductScanner, self).site_facts()
        result = dict(
            (hostname.lower(), attribute) for hostname, attribute in
            result.iteritems())
        if hasattr(self,
                   'isp') and self.isp.__class__.__name__ == 'ISP4XHiSecConfiguration':
            noncore_dict = dict((self.get_hostname(host),
                                 self.get_dbhost_facts(host))
                                for host in self.get_dbhosts())
            noncore_dict = dict(
                (hostname.lower(), attribute) for hostname, attribute in
                noncore_dict.iteritems())
            for node in noncore_dict.keys():
                noncore_dict[node]['ISP']['identifier'] = '4x'
                if node in result.keys():
                    noncore_isp = noncore_dict[node]['ISP']
                    result_isp = result[node]['ISP']
                    noncore_isp['ex_module_type'] = noncore_isp['module_type']
                    noncore_isp['module_type'] = result_isp['module_type']
                    noncore_isp['version'] = result_isp['version']
                    noncore_isp['input_folder'] = result_isp['input_folder']
            result.update(noncore_dict)
        result.update(dict((self.get_hostname(host),
                            self.get_redis_host_facts(host))
                           for host in self.get_redis_hosts()))
        return result


class ISPAuthenticationError(Exception):
    pass


class ISPConfiguration(HTTPRequester):
    auth_scheme = 'https'
    config_scheme = 'http'
    url_format = '{scheme}://{server}/{path}'

    def __init__(self, server):
        super(ISPConfiguration, self).__init__()
        self.server = server
        self.E = objectify.ElementMaker(annotate=False)

    @property
    def config_url(self):
        return self.url_format.format(scheme=self.config_scheme, server=self.server, path=self.config_path)

    @property
    def auth_url(self):
        return self.url_format.format(scheme=self.auth_scheme, server=self.server, path=self.auth_path)

    @cached_property
    def software_version(self):
        return self.get_software_version()

    def get_list_hosts_query(self):
        """Generate
    <ListHosts/>"""
        return etree.tostring(self.E.ListHosts())

    def get_server_info_query(self):
        """Generate
    <GetServerInfo/>"""
        return etree.tostring(self.E.GetServerInfo())

    def get_host_config_query(self, hostname, config):
        """Generate
    <Retrieve>
        <Host>%s</Host>
        <ConfigurationName>%s</ConfigurationName>
    </Retrieve>"""
        root = self.E.Retrieve(
            self.E.Host(hostname),
            self.E.ConfigurationName(config)
        )
        return etree.tostring(root)

    def get_auth_header(self, token):
        return dict(Cookie='iSiteWebApplication={token}'.format(token=token))

    def auth_session(self):
        if not self._session:
            token = self.get_auth_token()
            if not token:
                raise ISPAuthenticationError
            self._session = requests.session()
            self._session.headers.update(self.get_auth_header(token))
        return self._session

    def query_xml(self, url, query):
        response = self.suppressed_post(url=url, data=query)
        if response:
            return objectify.fromstring(response.content)

    def get_module_type(self, hostname):
        process_and_services_config = self.query_host_config(
            hostname, 'iSyntaxServer\ProcessesAndServices')
        return int(process_and_services_config.get('ModuleType'))

    def get_input_folder(self, hostname):
        system_config = self.query_host_config(
            hostname, 'iSyntaxServer\iSiteSystem')
        result = system_config.get_configuration_attribute(
            'Stack', 'DICOMInputDirectory')
        if result is not None:
            return str(result)

    def query_host_config(self, hostname, config):
        query = self.get_host_config_query(hostname, config)
        root = self.query_xml(self.config_url, query)
        return ISPHostConfiguration(data=root['Configuration'], host=hostname)

    def get_software_version(self):
        self.auth_session()
        server_info = self.query_xml(
            self.config_url, self.get_server_info_query())
        try:
            return str(server_info.ServerInfoBlob.ServerInfo.SoftwareVersion)
        except AttributeError:
            pass

    def get_hosts(self):
        self.auth_session()
        hosts_xml = self.query_xml(
            self.config_url, self.get_list_hosts_query())
        return hosts_xml.xpath('//HostName')

    def get_noncore_node(self):
        return None

class ISP4XConfiguration(ISPConfiguration):
    auth_path = 'InfrastructureServices/AuthenticationService/v1_0/authenticationservice.ashx'
    config_path = 'InfrastructureServices/ConfigurationService/ConfigurationService.ashx'
    identifier = '4x'

    TICKET_XPATH = '//*[ local-name() = "Ticket" ]'
    CREDENTIALS = {}

    def __init__(self, server):
        super(ISP4XConfiguration, self).__init__(server)
        self.auth_login_request_ns = 'uri://stentor.com/iSite/Authentication/Messages'
        self.auth_authentication_source = 'ISITE'
        #self.auth_username = 'Administrator'
        #self.auth_password = 'changeme'
        self.auth_hostname = 'ingbtcpic5dt9ah.code1.emi.philips.com'
        self.auth_ip_address = '161.85.27.158'
        self.auth_application_name = 'iSiteServer'
        self.auth_application_version = '1.0.0.0'
        self.auth_culture = '1033'
        self.auth_uiculture = '1033'
        self.load_credentials()

    @staticmethod
    def load_credentials():
        """ Load the username(USER111) and password(USER112) from the resource.cfg file
            Which will in turn use for accessing the API end point
        """
        secret = linecache.getline(DISCOVERY['SECRET_FILE'], 1).strip()
        nr = NagiosResourcer(DISCOVERY['NAGIOS_RESOURCE_FILE'], secret)
        username, password = nr.get_resources("$USER111$", "$USER112$")
        ISP4XConfiguration.CREDENTIALS["username"] = username
        ISP4XConfiguration.CREDENTIALS["password"] = password

    @property
    def auth_username(self):
        return self.CREDENTIALS["username"]

    @property
    def auth_password(self):
        return self.CREDENTIALS["password"]

    def get_authentication_query(self):
        """generate
    <Message><Login><loginRequest xmlns="uri://stentor.com/iSite/Authentication/Messages">
    <AuthenticationSource>ISITE</AuthenticationSource>
    <UserName>Administrator</UserName>
    <Password>changeme</Password>
    <HostName>ingbtcpic5dt9ah.code1.emi.philips.com</HostName>
    <IpAddress>161.85.27.158</IpAddress>
    <ApplicationName>iSiteServer</ApplicationName>
    <ApplicationVersion>1.0.0.0</ApplicationVersion>
    <Culture>1033</Culture>
    <UICulture>1033</UICulture>
    </loginRequest>
    </Login>
    </Message>"""
        E = self.E
        E2 = objectify.ElementMaker(annotate=False,
                                    namespace=self.auth_login_request_ns,
                                    nsmap={None: self.auth_login_request_ns})
        message = E.Message(
            E.Login(
                E2.loginRequest(
                    E.AuthenticationSource(self.auth_authentication_source),
                    E.UserName(self.auth_username),
                    E.Password(self.auth_password),
                    E.HostName(self.auth_hostname),
                    E.IpAddress(self.auth_ip_address),
                    E.ApplicationName(self.auth_application_name),
                    E.ApplicationVersion(self.auth_application_version),
                    E.Culture(self.auth_culture),
                    E.UICulture(self.auth_uiculture)
                )
            )
        )
        result = etree.tostring(message)
        logger.debug('Query message xml: %s', result)
        return result

    def get_auth_token(self):
        logger.info('Getting ticket for server %s', self.server)
        root = self.query_xml(self.auth_url, self.get_authentication_query())
        if root is not None:
            try:
                return root.xpath(self.TICKET_XPATH)[0]
            except IndexError:
                logger.exception('Error getting Ticket from %s', self.auth_url)


class ISP4XHiSecConfiguration(ISPConfiguration):
    auth_path = ''
    config_scheme = 'https'
    config_path = 'InfrastructureServices/ConfigurationService/ConfigurationService.ashx'
    identifier = '4x'
    intrinsic_flags = ['hisec']
    HMAC_IDENTIFIERS = {
        'TOKEN': 'iSiteWebApplication',  # Not required
        'TIMESTAMP_LABEL': 'Timestamp',
        'HASH_LABEL': 'Hash',
        'APPLICATIONIDENTIFIER': 'iSiteRadiology',  # Not Required
        'APPID_LABEL': 'AppId',
        'APPID': 'IDM',
        'SECRET_KEY': '',  # Loaded from resource.cfg
        'SERVERTIMESTAMP_LABEL': 'ServerTimeStamp',
        'RETRIES': 3,  # Possibly part of the protocol
    }
    auth_header_syntax = 'HMAC {TIMESTAMP_LABEL}={ISODATE};{HASH_LABEL}={HASH};{APPID_LABEL}={APPID}'

    def __init__(self, server):
        super(ISP4XHiSecConfiguration, self).__init__(server)
        self._auth_timestamp = None
        self.load_credentials()

    @staticmethod
    def load_credentials():
        """ Load the secretkey(USER113) from the resource.cfg file
            Which will in turn use for accessing the API end point
        """
        secret = linecache.getline(DISCOVERY['SECRET_FILE'], 1).strip()
        nr = NagiosResourcer(DISCOVERY['NAGIOS_RESOURCE_FILE'], secret)
        secret_key = nr.get_resource("$USER113$")
        ISP4XHiSecConfiguration.HMAC_IDENTIFIERS["SECRET_KEY"] = secret_key

    @property
    def auth_timestamp(self):
        if self._auth_timestamp is None:
            return datetime.utcnow()
        return self._auth_timestamp

    def set_auth_timestamp(self, value, latency):
        self._auth_timestamp = parser.parse(
            value) + timedelta(seconds=(latency / 2)) if value else None

    def get_auth_header(self, token):
        return dict(Authorization=token)

    def get_formatted_token(self):
        '''
        This method return HMAC token required to auth iSite Server2Server communication
        sample token: 'HMAC Timestamp=2016-07-14T16:40:21.9763242+05:30;Hash=7zFZTBpbmmJvEPDry7b7XdB3LMl5tNRpwb5UBINpyT0=;AppId=IDM'
        :param timestamp:
        :return: token string
        '''
        isodate = self.auth_timestamp.isoformat()
        auth_header = self.auth_header_syntax.format(
            ISODATE=isodate,
            HASH=get_hashkey(self.HMAC_IDENTIFIERS['SECRET_KEY'], isodate),
            **self.HMAC_IDENTIFIERS
        )
        return auth_header

    def get_auth_token(self):
        for retry in range(self.HMAC_IDENTIFIERS['RETRIES']):
            auth_token = self.attempt_authorization()
            if auth_token:
                return auth_token

    def attempt_authorization(self):
        auth_token = self.get_formatted_token()
        response, latency = self.post_auth_request(auth_token)
        if not response:
            raise ISPAuthenticationError
        if self.is_ok_response(response):
            return auth_token
        self.set_auth_timestamp(response.headers.get(
            self.HMAC_IDENTIFIERS['SERVERTIMESTAMP_LABEL']), latency)

    def post_auth_request(self, token):
        start_time = datetime.utcnow()
        response = self.suppressed_post(
            self.config_url,
            self.get_server_info_query(),
            self.get_auth_header(token),
            ok_only=False
        )
        latency = (datetime.utcnow() - start_time).seconds
        return response, latency


class ISP3XConfiguration(ISPConfiguration):
    auth_path = 'iSiteWeb/Authentication/Authentication.ashx'
    config_path = 'iSiteWeb/Configuration/ConfigurationService.ashx'
    identifier = '3x'

    def __init__(self, server):
        super(ISP3XConfiguration, self).__init__(server)
        self.auth_user_login_id = 'rgomez@stentor.com'
        self.auth_password = '2dc33c7'
        self.auth_source = '__STENTOR_iSiteSimple_'
        self.auth_login_type = 'Administrative'
        self.auth_system_dsn = 'ISITE'
        self.auth_machine_name = 'deveid2k4-1'
        self.auth_ip_address = '192.168.43.50'
        self.auth_application_name = 'iSiteServer'
        self.auth_application_version = '3.6.0.0'

    def get_authentication_query(self):
        """generate
    <Message><LoginRequest>
    <UserLoginID>rgomez@stentor.com</UserLoginID>
    <Password>2dc33c7</Password>
    <AuthSource>__STENTOR_iSiteSimple_</AuthSource>
    <LoginType>Administrative</LoginType>
    <SystemDSN>ISITE</SystemDSN>
    <MachineName>deveid2k4-1</MachineName>
    <IpAddress>192.168.43.50</IpAddress>
    <ApplicationName>iSiteServer</ApplicationName>
    <ApplicationVersion>3.6.0.0</ApplicationVersion>
    </LoginRequest>
    </Message>"""
        E = self.E
        message = E.Message(
            E.LoginRequest(
                E.UserLoginID(self.auth_user_login_id),
                E.Password(self.auth_password),
                E.AuthSource(self.auth_source),
                E.LoginType(self.auth_login_type),
                E.SystemDSN(self.auth_system_dsn),
                E.MachineName(self.auth_machine_name),
                E.IpAddress(self.auth_ip_address),
                E.ApplicationName(self.auth_application_name),
                E.ApplicationVersion(self.auth_application_version),
            )
        )
        result = etree.tostring(message)
        logger.debug('Query message xml: %s', result)
        return result

    def get_auth_token(self):
        logger.info('Getting cookie for server %s', self.server)
        response = self.suppressed_post(
            url=self.auth_url, data=self.get_authentication_query())
        if response:
            return response.cookies.get('iSiteWebApplication')


class ISP4_1Configuration(ISP4XConfiguration):
    auth_path = '%2fiSiteWeb%2fAuthentication%2fv1_0%2fauthenticationservice.ashx'
    config_path = '%2fiSiteWeb%2fConfiguration%2fConfigurationService.ashx'
    identifier = '4_1'


class ISPHostConfiguration(object):
    XML_DESCRIPTOR_RE = '^<\?\s*xml.*\?>\r?\n'

    def __init__(self, data, host):
        self.data = data
        self.host = host

    @staticmethod
    def decode_inflate(stream):
        stream_decoded = repr(stream).decode('base64')
        # the first four bytes are the length (Compression.cs)
        uncompressed_length = struct.unpack('<I', stream_decoded[:4])[0]
        try:
            result = zlib.decompress(stream_decoded[4:])
        except zlib.error:
            result = ''
            logger.exception('Could not decompress')
        return result

    def remove_xml_descriptor(self, data):
        return re.sub(self.XML_DESCRIPTOR_RE, '', data, 1)

    @cached_property
    def config_blob(self):
        logger.debug('Decompressing config for host %s', self.host)
        return self.remove_xml_descriptor(self.decode_inflate(self.data))

    @cached_property
    def config_root(self):
        logger.debug('Parsing config for host %s', self.host)
        try:
            result = objectify.fromstring(self.config_blob)
        except etree.XMLSyntaxError:
            logger.exception(
                'Configuration for host %s could not be parsed', self.host)
            raise
        return result

    def get(self, attr, default=None):
        return getattr(self.config_root, attr, default)

    def get_configuration_attribute(self, config, attr):
        attribute_getter = attrgetter(attr)
        try:
            return attribute_getter(self.get(config))
        except AttributeError:
            logger.info(
                'Host %s could not find attribute %s from configuration %s', self.host, attr, config)


class ISPDBConfiguration(ISP4XHiSecConfiguration):
    config_path = 'InfrastructureServices/RegistryService/RegistryService.ashx'

    def __init__(self, server):
        super(ISPDBConfiguration, self).__init__(server)
        self._auth_timestamp = None
        self.load_credentials()
        self.token_mgr = token_mgr

    def attempt_authorization(self):
        auth_token = self.token_mgr.get_formatted_token()
        response, latency = self.post_auth_request(auth_token)
        if not response:
            raise ISPAuthenticationError
        if self.is_ok_response(response):
            return auth_token
        self.set_auth_timestamp(response.headers.get(
            self.HMAC_IDENTIFIERS['SERVERTIMESTAMP_LABEL']), latency)

    def get_server_info_query(self):
        """ Generate
            <Message>
                <ListNonCoreNodes/>
            </Message>
        """
        result = objectify.ElementMaker(annotate=False)
        return etree.tostring(self.E.Message(result.ListNonCoreNodes()))

    def get_software_version(self):
        self.auth_session()
        logger.info('Fetching information from {url}'.format(url=self.config_url))
        server_info = self.query_xml(
            self.config_url, self.get_server_info_query())
        try:
            result = etree.tostring(server_info)
            if result:
                return result
        except AttributeError:
            pass

    def get_hosts(self):
        try:
            self.auth_session()
        except ISPAuthenticationError:
            logger.warning('Raised ISPAuthenticationError, ISPACS extended discovery failed. Check for discovery endpoint URL.')
            return []
        hosts_xml = self.query_xml(
            self.config_url, self.get_server_info_query())
        logger.info('Fethed host list is {list_host}'.format(list_host=hosts_xml.xpath('//HostName')))
        return hosts_xml.xpath('//HostName')

    def get_module_type(self, hostname):
        self.auth_session()
        server_info = self.query_xml(
            self.config_url, self.get_server_info_query())
        try:
            result = etree.tostring(server_info)
            if result:
                nodes = server_info.ListNonCoreNodesResponse
                nodes = nodes.ArrayOfNode
                for node in nodes.iterchildren():
                    if node.HostName == hostname:
                        logger.info('Fetched {node} from {url}'.format(node=node.NodeType, url=self.config_url))
                        return node.NodeType.text.title()
        except AttributeError:
            logger.warning('Module type information is not available'.format(url=self.config_url))
            pass

    def get_input_folder(self, hostname=None):
        mount_path = None
        self.auth_session()
        logger.info('Fetching information from {url} for nodes'.format(url=self.config_url))
        server_info = self.query_xml(
            self.config_url, self.get_server_info_query())
        try:
            result = etree.tostring(server_info)
            if result:
                nodes = server_info.ListNonCoreNodesResponse
                nodes = nodes.ArrayOfNode
                for node in nodes.iterchildren():
                    if node.NodeType == 'Database' and node.HostName == hostname:
                        mount_path = node.FilePath.string
                        logger.info('Database mount path from {url} is {mount_path}'.format(url=self.config_url,
                                                                                            mount_path=mount_path))
                return mount_path.text
        except AttributeError:
            logger.warning('Database mount path from {url} is not available'.format(url=self.config_url))
            pass

    @cached_property
    def software_version(self):
        return None

    @cached_property
    def identifier(self):
        return None

    def get_noncore_node(self):
        return "DB"
