import unittest
from mock import MagicMock, patch


class ScanlineWMIHostDrillerTest(object):
    class TestCase(unittest.TestCase):
        def setUp(self):
            unittest.TestCase.setUp(self)
            modules = {
                'logging': MagicMock(name='logging'),
            }
            self.module_patcher = patch.dict('sys.modules', modules)
            self.module_patcher.start()
            from scanline.utilities.wmi import WMIHostDriller
            self.WMIHostDriller = WMIHostDriller
            self.wmi_driller = self.WMIHostDriller('host01', 'admin', 'secret', 'itest.com')

        def tearDown(self):
            unittest.TestCase.tearDown(self)
            self.module_patcher.stop()


class ScanlineWMIDrillerTestCase(ScanlineWMIHostDrillerTest.TestCase):
    def setUp(self):
        ScanlineWMIHostDrillerTest.TestCase.setUp(self)

    def tearDown(self):
        ScanlineWMIHostDrillerTest.TestCase.tearDown(self)

    def test_get_command_with_domain(self):
        self.assertEqual(
            self.wmi_driller.get_command('SELECT * FROM TABLE'),
            [
                '/usr/local/bin/wmic',
                '--delimiter=\x01',
                '-U',
                'itest.com/admin%secret',
                '//host01',
                'SELECT * FROM TABLE',
                '--option=client ntlmv2 auth=Yes'
            ]
        )

    def test_get_command_without_domain(self):
        self.wmi_driller.domain = None
        self.assertEqual(
            self.wmi_driller.get_command('SELECT * FROM TABLE'),
            [
                '/usr/local/bin/wmic',
                '--delimiter=\x01',
                '-U',
                'admin%secret',
                '//host01',
                'SELECT * FROM TABLE',
                '--option=client ntlmv2 auth=Yes'
            ])


class ScanlineWMIDrillerFetchReaderTestCase(ScanlineWMIHostDrillerTest.TestCase):
    def setUp(self):
        ScanlineWMIHostDrillerTest.TestCase.setUp(self)
        self.csv_patch = patch('scanline.utilities.wmi.csv')
        self.mock_csv = self.csv_patch.start()

        def m_reader(first_arg, delimiter):
            return list(first_arg)

        self.mock_csv.DictReader.side_effect = m_reader

    def tearDown(self):
        ScanlineWMIHostDrillerTest.TestCase.tearDown(self)
        self.csv_patch.stop()

    def test_empty(self):
        self.assertEqual(self.wmi_driller.fetch_reader([]), [])
        self.assertEqual(self.mock_csv.DictReader.call_count, 1)

    def test_no_class(self):
        self.assertEqual(self.wmi_driller.fetch_reader(['something', 'more']), [])
        self.assertEqual(self.mock_csv.DictReader.call_count, 1)

    def test_class_no_data(self):
        self.assertEqual(self.wmi_driller.fetch_reader(['something', 'more', 'CLASS:']), [])
        self.assertEqual(self.mock_csv.DictReader.call_count, 1)

    def test_ok(self):
        self.assertEqual(self.wmi_driller.fetch_reader(['something', 'more', 'CLASS:', 'a|b', 'c|d']), ['a|b', 'c|d'])
        self.assertEqual(self.mock_csv.DictReader.call_count, 1)


class ScanlineWMIDrillerQueryWMITestCase(ScanlineWMIHostDrillerTest.TestCase):
    def setUp(self):
        ScanlineWMIHostDrillerTest.TestCase.setUp(self)
        self.popen_patch = patch('scanline.utilities.wmi.Popen')
        self.mock_popen = self.popen_patch.start()
        self.wmi_driller.fetch_reader = MagicMock(name='fetch_reader')
        self.wmi_driller.get_command = MagicMock(name='get_command')

    def tearDown(self):
        ScanlineWMIHostDrillerTest.TestCase.tearDown(self)
        self.popen_patch.stop()

    def test_os_error(self):
        self.mock_popen.side_effect = OSError
        self.assertEqual(self.wmi_driller.query_wmi('query_here'), None)

    def test_failed_query(self):
        self.mock_popen.pp.returncode = 1
        self.mock_popen.pp.communicate.return_value = 'Somevalue1', ''
        self.mock_popen.return_value = self.mock_popen.pp
        self.assertEqual(self.wmi_driller.query_wmi('query_here'), None)

    def test_query_ok(self):
        self.mock_popen.pp.returncode = 0
        self.mock_popen.pp.communicate.return_value = 'Somevalue1', ''
        self.mock_popen.return_value = self.mock_popen.pp
        self.assertEqual(self.wmi_driller.query_wmi('query_here'), self.wmi_driller.fetch_reader.return_value)
        self.wmi_driller.fetch_reader.assert_called_once_with(['Somevalue1'])

    def test_query_ok_multiline(self):
        self.mock_popen.pp.returncode = 0
        self.mock_popen.pp.communicate.return_value = 'Somevalue1 here\nand more here', ''
        self.mock_popen.return_value = self.mock_popen.pp
        self.assertEqual(self.wmi_driller.query_wmi('query_here'), self.wmi_driller.fetch_reader.return_value)
        self.wmi_driller.fetch_reader.assert_called_once_with(['Somevalue1 here', 'and more here'])

    def test_parse_single_line_empty(self):
        self.wmi_driller.query_wmi = MagicMock(name='query_wmi', return_value=iter([]))
        self.assertEqual(self.wmi_driller.parse_single_line('SOME QUERY'), None)
        self.wmi_driller.query_wmi.assert_called_once_with('SOME QUERY')

    def test_parse_single_line_query_failure(self):
        self.wmi_driller.query_wmi = MagicMock(name='query_wmi', return_value=None)
        self.assertEqual(self.wmi_driller.parse_single_line('SOME QUERY'), None)
        self.wmi_driller.query_wmi.assert_called_once_with('SOME QUERY')

    def test_parse_single_line_ok(self):
        self.wmi_driller.query_wmi = MagicMock(name='query_wmi', return_value=iter([{'Manufacturer': 'Philips'}]))
        self.assertEqual(self.wmi_driller.parse_single_line('SOME QUERY'), {'Manufacturer': 'Philips'})

    def test_get_field_from_query_without_proper_field_value(self):
        self.wmi_driller.parse_single_line = MagicMock(return_value={"a":"b"})
        self.assertEqual(self.wmi_driller.get_field_from_query('SOME QUERY', 'Philips'), None)

    def test_get_field_from_query_no_query_result(self):
        self.wmi_driller.parse_single_line = MagicMock(return_value=None)
        self.assertEqual(self.wmi_driller.get_field_from_query('SOME QUERY', 'Philips'), None)

    def test_get_field_from_query_no_field(self):
        self.wmi_driller.parse_single_line = MagicMock(return_value={})
        self.assertEqual(self.wmi_driller.get_field_from_query('SOME QUERY', 'Philips'), None)

    def test_get_field_from_query_with_field_value(self):
        self.wmi_driller.parse_single_line = MagicMock(return_value={"Philips":"yes"})
        self.assertEqual(self.wmi_driller.get_field_from_query('SOME QUERY', 'Philips'), "yes")

    def test_get_field_from_query_with_list_value(self):
        self.wmi_driller.parse_single_line = MagicMock(return_value=[])
        self.assertEqual(self.wmi_driller.get_field_from_query('SOME QUERY', 'Philips'), None)

    def test_b_ok(self):
        self.wmi_driller.parse_single_line = MagicMock(return_value={'Philips': 'someval'})
        self.assertEqual(self.wmi_driller.get_field_from_query('SOME QUERY', 'Philips'), 'someval')


class ScanlineWMIDrillerInformationGettersTestCase(ScanlineWMIHostDrillerTest.TestCase):
    def setUp(self):
        ScanlineWMIHostDrillerTest.TestCase.setUp(self)
        self.wmi_driller.get_field_from_query = MagicMock(name='get_field_from_query')

    def tearDown(self):
        ScanlineWMIHostDrillerTest.TestCase.tearDown(self)

    def test_get_manufacturer(self):
        self.assertEqual(self.wmi_driller.get_manufacturer(), self.wmi_driller.get_field_from_query.return_value)
        self.wmi_driller.get_field_from_query.assert_called_once_with(
            self.wmi_driller.MANUFACTURER_QUERY, 'Manufacturer'
        )

    def test_get_uuid_invalid(self):
        self.wmi_driller.get_field_from_query.return_value = 'hellow'
        self.assertEqual(self.wmi_driller.get_uuid(), None)
        self.wmi_driller.get_field_from_query.assert_called_once_with(
            self.wmi_driller.UUID_QUERY, 'UUID'
        )

    def test_get_uuid(self):
        self.wmi_driller.get_field_from_query.return_value = 'f47ac10b-58cc-4372-a567-0e02b2c3d479'
        self.assertEqual(self.wmi_driller.get_uuid(), '0bc17af4-cc58-7243-a567-0e02b2c3d479')
        self.wmi_driller.get_field_from_query.assert_called_once_with(
            self.wmi_driller.UUID_QUERY, 'UUID'
        )

    def test_is_service_auto_start_empty(self):
        self.wmi_driller.get_field_from_query.return_value = None
        self.assertEqual(self.wmi_driller.is_service_auto_start('Foo'), False)
        self.wmi_driller.get_field_from_query.assert_called_once_with(
            "SELECT Name, Started, State, StartMode FROM Win32_Service WHERE Name LIKE 'Foo'", 'StartMode'
        )

    def test_is_service_auto_start_ok(self):
        self.wmi_driller.get_field_from_query.return_value = 'Auto'
        self.assertEqual(self.wmi_driller.is_service_auto_start('Foo'), True)
        self.wmi_driller.get_field_from_query.assert_called_once_with(
            "SELECT Name, Started, State, StartMode FROM Win32_Service WHERE Name LIKE 'Foo'", 'StartMode'
        )

    def test_is_service_auto_start_not_auto(self):
        self.wmi_driller.get_field_from_query.return_value = 'Manual'
        self.assertEqual(self.wmi_driller.is_service_auto_start('Foo'), False)
        self.wmi_driller.get_field_from_query.assert_called_once_with(
            "SELECT Name, Started, State, StartMode FROM Win32_Service WHERE Name LIKE 'Foo'", 'StartMode'
        )

    def test_get_windows_info_empty(self):
        self.wmi_driller.parse_single_line = MagicMock(return_value=None)
        self.assertEqual(self.wmi_driller.get_windows_info(), None)

    def test_get_windows_info_ok(self):
        parse_return = {
            'Caption': 'Microsoft Windows Server 2008 R2 Standard',
            'CSDVersion': 'Service Pack 1',
            'OSLanguage': '1033',
            'SerialNumber': '55041-266-0070725-84774',
            'ServicePackMajorVersion': '1',
            'ServicePackMinorVersion': '0',
            'Version': '6.1.7601'
        }
        self.wmi_driller.parse_single_line = MagicMock(return_value=parse_return)
        expected = {
            'ServicePack': 'Service Pack 1',
            'Name': 'Microsoft Windows Server 2008 R2 Standard',
            'SerialNumber': '55041-266-0070725-84774',
            'OSLanguage': '1033',
            'Version': '6.1.7601',
            'ServicePackVersion': '1.0'
        }
        self.assertEqual(self.wmi_driller.get_windows_info(), expected)

    def test_get_windows_info_no_service_pack(self):
        parse_return = {
            'Caption': 'Microsoft Windows Server 2008 R2 Standard',
            'CSDVersion': 'Service Pack 1',
            'OSLanguage': '1033',
            'SerialNumber': '55041-266-0070725-84774',
            'Version': '6.1.7601'
        }
        self.wmi_driller.parse_single_line = MagicMock(return_value=parse_return)
        expected = {
            'ServicePack': 'Service Pack 1',
            'Name': 'Microsoft Windows Server 2008 R2 Standard',
            'SerialNumber': '55041-266-0070725-84774',
            'OSLanguage': '1033',
            'Version': '6.1.7601',
            'ServicePackVersion': 'None.None'
        }
        self.assertEqual(self.wmi_driller.get_windows_info(), expected)


if __name__ == '__main__':
    unittest.main()
