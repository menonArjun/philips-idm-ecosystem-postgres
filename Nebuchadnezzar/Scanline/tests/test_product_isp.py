import unittest
from mock import MagicMock, patch, PropertyMock, DEFAULT, create_autospec
from lxml import objectify, etree
from datetime import datetime, timedelta
from phimutils.resource import NagiosResourcer
from StringIO import StringIO


class ScanlineISPTest(object):
    class TestCase(unittest.TestCase):
        def setUp(self):
            unittest.TestCase.setUp(self)
            self.mock_cached_property = MagicMock(
                name='cached_property', cached_property=property)
            self.mock_requests = MagicMock(name='requests')
            self.mock_datetime = MagicMock(name='datetime')
            self.mock_dateutil = MagicMock(name='dateutil')
            self.mock_pytz = MagicMock(name='pytz')
            self.mock_host = MagicMock()
            self.mock_utilities = MagicMock()
            modules = {
                'cached_property': self.mock_cached_property,
                'logging': MagicMock(name='logging'),
                'requests': self.mock_requests,
                'datetime': self.mock_datetime,
                'dateutil': self.mock_dateutil,
                'pytz': self.mock_pytz,
                'scanline.host.isp': self.mock_host.isp,
                'scanline.host.windows': self.mock_host.windows,
                'scanline.utilities.auth': self.mock_utilities.auth,
                'scanline.host.udm': self.mock_host.udm,
            }
            self.module_patcher = patch.dict('sys.modules', modules)
            self.module_patcher.start()

        def tearDown(self):
            unittest.TestCase.tearDown(self)
            self.module_patcher.stop()


class ScanlineISPConfigurationTestCase(ScanlineISPTest.TestCase):
    def setUp(self):
        ScanlineISPTest.TestCase.setUp(self)
        import scanline.product.isp
        self.module = scanline.product.isp
        self.ISPAuthenticationError = scanline.product.isp.ISPAuthenticationError
        self.ISPConfiguration = scanline.product.isp.ISPConfiguration
        self.isp = self.ISPConfiguration('10.55.66.1')

    def tearDown(self):
        ScanlineISPTest.TestCase.tearDown(self)

    def test_get_list_hosts_query(self):
        self.assertEqual(self.isp.get_list_hosts_query(), '<ListHosts/>')

    def test_get_server_info_query(self):
        self.assertEqual(self.isp.get_server_info_query(), '<GetServerInfo/>')

    def test_get_host_config_query(self):
        self.assertEqual(
            self.isp.get_host_config_query('host01', 'Test/config'),
            '<Retrieve><Host>host01</Host><ConfigurationName>Test/config</ConfigurationName></Retrieve>'
        )

    def test_get_auth_header(self):
        self.assertEqual(self.isp.get_auth_header('chuck'), {
                         'Cookie': 'iSiteWebApplication=chuck'})

    def test_auth_session(self):
        self.isp.get_auth_header = MagicMock(name='get_auth_header')
        self.isp.get_auth_token = MagicMock(name='get_auth_token')
        self.isp.auth_session()
        self.module.requests.session.assert_called_once_with()
        self.isp.get_auth_header.assert_called_once_with(
            self.isp.get_auth_token.return_value)
        self.module.requests.session.return_value.headers.update.assert_called_once_with(
            self.isp.get_auth_header.return_value
        )

    def test_auth_session_auth_not_possible(self):
        self.isp.get_auth_token = MagicMock(
            name='get_auth_token', return_value=None)
        self.assertRaises(self.ISPAuthenticationError, self.isp.auth_session)

    @patch('scanline.product.isp.objectify')
    def test_query_xml(self, objectify_mock):
        self.isp.suppressed_post = MagicMock(name='suppressed_post')
        result = self.isp.query_xml('http://host1/version', '</list>')
        self.isp.suppressed_post.assert_called_once_with(
            url='http://host1/version', data='</list>')
        objectify_mock.fromstring.assert_called_once_with(
            self.isp.suppressed_post.return_value.content)
        self.assertEqual(result, objectify_mock.fromstring.return_value)

    def test_query_xml_no_response(self):
        self.isp.suppressed_post = MagicMock(
            name='suppressed_post', return_value=None)
        self.assertEqual(self.isp.query_xml(
            'http://host1/version', '</list>'), None)
        self.isp.suppressed_post.assert_called_once_with(
            url='http://host1/version', data='</list>')

    def test_get_module_type(self):
        self.isp.query_host_config = MagicMock(name='query_host_config')
        self.isp.query_host_config.return_value.get.return_value = '4'
        result = self.isp.get_module_type('host1')
        self.isp.query_host_config.assert_called_once_with(
            'host1', 'iSyntaxServer\\ProcessesAndServices')
        self.assertEqual(result, 4)
        self.isp.query_host_config.return_value.get.assert_called_once_with(
            'ModuleType')

    def test_get_input_folder(self):
        self.isp.query_host_config = MagicMock(name='query_host_config')
        self.isp.query_host_config.return_value.get_configuration_attribute.return_value = {
            'key': 'value'}
        result = self.isp.get_input_folder('host1')
        self.isp.query_host_config.assert_called_once_with(
            'host1', 'iSyntaxServer\\iSiteSystem')
        self.assertEqual(result, "{'key': 'value'}")
        self.isp.query_host_config.return_value.get_configuration_attribute.assert_called_once_with(
            'Stack', 'DICOMInputDirectory'
        )

    def test_get_input_folder_none_found(self):
        self.isp.query_host_config = MagicMock(name='query_host_config')
        self.isp.query_host_config.return_value.get_configuration_attribute.return_value = None
        result = self.isp.get_input_folder('host1')
        self.isp.query_host_config.assert_called_once_with(
            'host1', 'iSyntaxServer\\iSiteSystem')
        self.assertEqual(result, None)
        self.isp.query_host_config.return_value.get_configuration_attribute.assert_called_once_with(
            'Stack', 'DICOMInputDirectory'
        )

    def test_noncore_node(self):
        result = self.isp.get_noncore_node()
        self.assertEqual(result, None)


class ScanlineISPConfigurationURLMethodsTestCase(ScanlineISPTest.TestCase):
    def setUp(self):
        ScanlineISPTest.TestCase.setUp(self)
        from scanline.product.isp import ISPConfiguration
        self.ISPConfiguration = ISPConfiguration
        self.config_url_patch = patch(
            'scanline.product.isp.ISPConfiguration.config_url', new_callable=PropertyMock)
        self.mock_config_url = self.config_url_patch.start()
        self.mock_config_url.return_value = 'http://host1/config'
        self.isp = self.ISPConfiguration('10.55.66.1')
        self.isp.auth_session = MagicMock(name='auth_session')
        self.isp.query_xml = MagicMock(name='query_xml')
        self.isp.get_server_info_query = MagicMock(
            name='get_server_info_query')
        self.isp.get_list_hosts_query = MagicMock(name='get_list_hosts_query')

    def tearDown(self):
        ScanlineISPTest.TestCase.tearDown(self)
        self.config_url_patch.stop()

    @patch('scanline.product.isp.ISPHostConfiguration')
    def test_query_host_config(self, mock_isphostconfiguration):
        self.isp.get_host_config_query = MagicMock(
            name='get_host_config_query')
        result = self.isp.query_host_config('host1', 'server/config')
        self.isp.get_host_config_query.assert_called_once_with(
            'host1', 'server/config')
        self.isp.query_xml.assert_called_once_with(
            'http://host1/config', self.isp.get_host_config_query.return_value)
        self.assertEqual(result, mock_isphostconfiguration.return_value)
        mock_isphostconfiguration.assert_called_once_with(data=self.isp.query_xml.return_value.__getitem__.return_value,
                                                          host='host1')

    def test_calls_for_get_software_version(self):
        result = self.isp.get_software_version()
        self.isp.auth_session.assert_called_once_with()
        self.isp.query_xml.assert_called_once_with(self.mock_config_url.return_value,
                                                   self.isp.get_server_info_query.return_value)

    def test_get_software_version_found(self):
        expected = '4,6,7,8'
        self.isp.query_xml.return_value.ServerInfoBlob.ServerInfo.SoftwareVersion = expected
        result = self.isp.get_software_version()
        self.assertEqual(result, expected)

    def test_get_software_version_not_found(self):
        self.isp.query_xml.return_value.mock_add_spec([])
        result = self.isp.get_software_version()
        self.assertEqual(result, None)

    def test_get_hosts(self):
        result = self.isp.get_hosts()
        self.assertEqual(
            result, self.isp.query_xml.return_value.xpath.return_value)
        self.isp.auth_session.assert_called_once_with()
        self.isp.query_xml.assert_called_once_with(self.mock_config_url.return_value,
                                                   self.isp.get_list_hosts_query.return_value)


class ScanlineISP4xConfigurationTestCase(ScanlineISPTest.TestCase):
    def setUp(self):
        ScanlineISPTest.TestCase.setUp(self)
        from scanline.product.isp import ISP4XConfiguration
        import scanline.product.isp as spi
        self.module =  spi
        self.ISP4XConfiguration = ISP4XConfiguration
        self.isp4x = self.ISP4XConfiguration('10.4.4.6')

    def tearDown(self):
        ScanlineISPTest.TestCase.tearDown(self)

    # @patch.object(NagiosResourcer,"get_resources",return_value=("abcd","pass"), autospec=True)
    # def test_load_credentials(self,ngr):
    # 	self.module.linecache = MagicMock(name="getline", return_value="abcdef")
    #     self.isp4x.load_credentials()
    # 	assert ngr.call_count == 1
    # 	self.module.linecache.getline.assert_called_once()

    def test_config_url(self):
        self.assertEqual(
            self.isp4x.config_url,
            'http://10.4.4.6/InfrastructureServices/ConfigurationService/ConfigurationService.ashx'
        )

    def test_auth_url(self):
        self.assertEqual(
            self.isp4x.auth_url,
            'https://10.4.4.6/InfrastructureServices/AuthenticationService/v1_0/authenticationservice.ashx'
        )

    def test_get_authentication_query(self):
        with patch('scanline.product.isp.ISP4XConfiguration.auth_username', new_callable=PropertyMock) as user_name:
            with patch('scanline.product.isp.ISP4XConfiguration.auth_password', new_callable=PropertyMock) as password:
                user_name.return_value = "username"
                password.return_value = "password"
                self.assertEqual(
                    self.isp4x.get_authentication_query(),
                    """<Message><Login><loginRequest xmlns="uri://stentor.com/iSite/Authentication/Messages"><AuthenticationSource>ISITE</AuthenticationSource><UserName>username</UserName><Password>password</Password><HostName>ingbtcpic5dt9ah.code1.emi.philips.com</HostName><IpAddress>161.85.27.158</IpAddress><ApplicationName>iSiteServer</ApplicationName><ApplicationVersion>1.0.0.0</ApplicationVersion><Culture>1033</Culture><UICulture>1033</UICulture></loginRequest></Login></Message>"""
                )

    def test_TICKET_XPATH_valid(self):
        root = objectify.fromstring(
            '<Message><Ticket>#WINNING</Ticket></Message>')
        result = root.xpath(self.isp4x.TICKET_XPATH)
        self.assertEqual(result, ['#WINNING'])

    def test_TICKET_XPATH_not_valid(self):
        root = objectify.fromstring('<Message></Message>')
        result = root.xpath(self.isp4x.TICKET_XPATH)
        self.assertEqual(result, [])

    def test_get_auth_token_valid(self):
        self.isp4x.query_xml = MagicMock(name='query_xml')
        self.assertEqual(self.isp4x.get_auth_token(),
                         self.isp4x.query_xml.return_value.xpath.return_value[0])

    def test_get_auth_no_root(self):
        self.isp4x.query_xml = MagicMock(name='query_xml', return_value=None)
        self.assertEqual(self.isp4x.get_auth_token(), None)

    def test_get_auth_token_invalid(self):
        self.isp4x.query_xml = MagicMock(name='query_xml')
        self.isp4x.query_xml.return_value.xpath.return_value = []
        self.assertEqual(self.isp4x.get_auth_token(), None)


class ScanlineISP4xHiSecConfigurationTestCase(ScanlineISPTest.TestCase):
    def setUp(self):
        ScanlineISPTest.TestCase.setUp(self)
        import scanline.product.isp
        self.module = scanline.product.isp
        self.ISP4XHiSecConfiguration = scanline.product.isp.ISP4XHiSecConfiguration
        self.isp4xHS = self.ISP4XHiSecConfiguration('10.4.4.6')

    def tearDown(self):
        ScanlineISPTest.TestCase.tearDown(self)

    def test_config_url(self):
        self.assertEqual(
            self.isp4xHS.config_url,
            'https://10.4.4.6/InfrastructureServices/ConfigurationService/ConfigurationService.ashx'
        )

    # @patch.object(NagiosResourcer,"get_resource",return_value=("abcd","pass"), autospec=True)
    # def test_load_credentials(self, ngr):
    #     self.module.linecache.getline = MagicMock(name="getline", return_value="abcdef")
    #     self.isp4xHS.load_credentials()
    #     assert ngr.call_count == 1
    #     self.module.linecache.getline.assert_called_once()


    def test_auth_timestamp_none(self):
        self.assertEqual(self.isp4xHS.auth_timestamp,
                         self.module.datetime.utcnow.return_value)

    def test_auth_timestamp_set(self):
        self.isp4xHS._auth_timestamp = 5678
        self.assertEqual(self.isp4xHS.auth_timestamp, 5678)

    def test_set_auth_timestamp_none(self):
        self.isp4xHS.set_auth_timestamp(None, None)
        self.assertEqual(self.isp4xHS._auth_timestamp, None)

    def test_set_auth_timestamp(self):
        self.isp4xHS.set_auth_timestamp('2016-09-15T02:45:58.317491', 9)
        self.assertEqual(
            self.isp4xHS._auth_timestamp,
            self.module.parser.parse.return_value.__add__.return_value
        )
        self.module.parser.parse.assert_called_once_with(
            '2016-09-15T02:45:58.317491')
        self.module.timedelta.assert_called_once_with(seconds=4)
        self.module.parser.parse.return_value.__add__.assert_called_once_with(
            self.module.timedelta.return_value
        )

    def test_get_auth_header(self):
        self.assertEqual(self.isp4xHS.get_auth_header(
            'chuck'), {'Authorization': 'chuck'})

    # def test_get_formatted_token(self):
    #     self.isp4xHS._auth_timestamp = MagicMock()
    #     self.isp4xHS._auth_timestamp.isoformat.return_value = '2016-09-15T02:45:58.317491'
    #     self.module.get_hashkey = MagicMock(
    #         name='get_hashkey', return_value='abc123')
    #     self.assertEqual(
    #         self.isp4xHS.get_formatted_token(),
    #         'HMAC Timestamp=2016-09-15T02:45:58.317491;Hash=abc123;AppId=IDM'
    #     )
    #     self.module.get_hashkey.assert_called_once()

    def test_get_auth_token_ok_on_first_try(self):
        self.isp4xHS.attempt_authorization = MagicMock(
            name='attempt_authorization')
        self.assertEqual(self.isp4xHS.get_auth_token(),
                         self.isp4xHS.attempt_authorization.return_value)

    def test_get_auth_token_ok_on_second_try(self):
        self.isp4xHS.attempt_authorization = MagicMock(
            name='attempt_authorization')
        self.isp4xHS.attempt_authorization.side_effect = [None, 'Token-1']
        self.assertEqual(self.isp4xHS.get_auth_token(), 'Token-1')

    def test_get_auth_token_not_retrieved(self):
        self.isp4xHS.attempt_authorization = MagicMock(
            name='attempt_authorization')
        self.isp4xHS.attempt_authorization.return_value = None
        self.assertEqual(self.isp4xHS.get_auth_token(), None)

    def test_attempt_authorization_ok_response(self):
        self.isp4xHS.is_ok_response = MagicMock(
            name='is_ok_response', return_value=True)
        self.isp4xHS.get_formatted_token = MagicMock(
            name='get_formatted_token')
        response = MagicMock()
        self.isp4xHS.post_auth_request = MagicMock(
            name='post_auth_request', return_value=(response, 5))
        self.assertEqual(self.isp4xHS.attempt_authorization(),
                         self.isp4xHS.get_formatted_token.return_value)
        self.isp4xHS.get_formatted_token.assert_called_once_with()
        self.isp4xHS.post_auth_request.assert_called_once_with(
            self.isp4xHS.get_formatted_token.return_value)

    def test_attempt_authorization_no_response(self):
        self.isp4xHS.get_formatted_token = MagicMock(
            name='get_formatted_token')
        self.isp4xHS.post_auth_request = MagicMock(
            name='post_auth_request', return_value=(None, 5))
        self.assertRaises(self.module.ISPAuthenticationError,
                          self.isp4xHS.attempt_authorization)
        self.isp4xHS.post_auth_request.assert_called_once_with(
            self.isp4xHS.get_formatted_token.return_value)

    def test_attempt_authorization_maybe_next_time(self):
        self.isp4xHS.is_ok_response = MagicMock(
            name='is_ok_response', return_value=False)
        self.isp4xHS.get_formatted_token = MagicMock(
            name='get_formatted_token')
        self.isp4xHS.set_auth_timestamp = MagicMock(name='set_auth_timestamp')
        response = MagicMock()
        self.isp4xHS.post_auth_request = MagicMock(
            name='post_auth_request', return_value=(response, 5))
        self.assertEqual(self.isp4xHS.attempt_authorization(), None)
        self.isp4xHS.get_formatted_token.assert_called_once_with()
        self.isp4xHS.post_auth_request.assert_called_once_with(
            self.isp4xHS.get_formatted_token.return_value)
        self.isp4xHS.set_auth_timestamp.assert_called_once_with(
            response.headers.get.return_value, 5)
        response.headers.get.assert_called_once_with('ServerTimeStamp')

    def test_post_auth_request(self):
        time1 = datetime.utcfromtimestamp(1473908934)
        time2 = time1 + timedelta(seconds=2)
        self.module.datetime.utcnow.side_effect = [time1, time2]
        self.isp4xHS.suppressed_post = MagicMock(name='suppressed_post')
        self.assertEqual(self.isp4xHS.post_auth_request(
            'some_token'), (self.isp4xHS.suppressed_post.return_value, 2))
        self.isp4xHS.suppressed_post.assert_called_once_with(
            self.isp4xHS.config_url, '<GetServerInfo/>', {'Authorization': 'some_token'}, ok_only=False
        )


class ScanlineISP3xConfigurationTestCase(ScanlineISPTest.TestCase):
    def setUp(self):
        ScanlineISPTest.TestCase.setUp(self)
        from scanline.product.isp import ISP3XConfiguration
        self.ISP3XConfiguration = ISP3XConfiguration
        self.isp3x = self.ISP3XConfiguration('10.3.6.1')

    def tearDown(self):
        ScanlineISPTest.TestCase.tearDown(self)

    def test_config_url(self):
        self.assertEqual(self.isp3x.config_url,
                         'http://10.3.6.1/iSiteWeb/Configuration/ConfigurationService.ashx')

    def test_auth_url(self):
        self.assertEqual(
            self.isp3x.auth_url, 'https://10.3.6.1/iSiteWeb/Authentication/Authentication.ashx')

    def test_get_authentication_query(self):
        self.assertEqual(
            self.isp3x.get_authentication_query(),
            """<Message><LoginRequest><UserLoginID>rgomez@stentor.com</UserLoginID><Password>2dc33c7</Password><AuthSource>__STENTOR_iSiteSimple_</AuthSource><LoginType>Administrative</LoginType><SystemDSN>ISITE</SystemDSN><MachineName>deveid2k4-1</MachineName><IpAddress>192.168.43.50</IpAddress><ApplicationName>iSiteServer</ApplicationName><ApplicationVersion>3.6.0.0</ApplicationVersion></LoginRequest></Message>"""
        )

    def test_calls_on_get_auth_token(self):
        self.isp3x.suppressed_post = MagicMock(name='suppressed_post')
        self.isp3x.get_authentication_query = MagicMock(
            name='get_authentication_query')
        self.isp3x.get_auth_token()
        self.isp3x.suppressed_post.assert_called_once_with(
            url='https://10.3.6.1/iSiteWeb/Authentication/Authentication.ashx',
            data=self.isp3x.get_authentication_query.return_value
        )

    def test_get_auth_token_invalid(self):
        self.isp3x.suppressed_post = MagicMock(
            name='suppressed_post', **{'return_value.cookies': {}})
        self.assertEqual(self.isp3x.get_auth_token(), None)

    def test_get_auth_token_valid(self):
        self.isp3x.suppressed_post = MagicMock(name='suppressed_post')
        self.isp3x.suppressed_post.return_value.cookies = {
            'iSiteWebApplication': 'noMilk?'}
        self.assertEqual(self.isp3x.get_auth_token(), 'noMilk?')


class ScanlineISP4_1ConfigurationTestCase(ScanlineISPTest.TestCase):
    def setUp(self):
        ScanlineISPTest.TestCase.setUp(self)
        from scanline.product.isp import ISP4_1Configuration
        self.ISP4_1Configuration = ISP4_1Configuration
        self.isp4_1 = self.ISP4_1Configuration('10.4.1.1')

    def tearDown(self):
        ScanlineISPTest.TestCase.tearDown(self)

    def test_config_url(self):
        self.assertEqual(
            self.isp4_1.config_url,
            'http://10.4.1.1/%2fiSiteWeb%2fConfiguration%2fConfigurationService.ashx'
        )

    def test_auth_url(self):
        self.assertEqual(
            self.isp4_1.auth_url,
            'https://10.4.1.1/%2fiSiteWeb%2fAuthentication%2fv1_0%2fauthenticationservice.ashx'
        )


class ScanlineISPHostConfigurationTestCase(ScanlineISPTest.TestCase):
    def setUp(self):
        ScanlineISPTest.TestCase.setUp(self)
        from scanline.product.isp import ISPHostConfiguration
        self.ISPHostConfiguration = ISPHostConfiguration
        self.config_root_patch = patch('scanline.product.isp.ISPHostConfiguration.config_root',
                                       new_callable=PropertyMock)
        self.mock_config_root = self.config_root_patch.start()
        self.mock_config_root.return_value.mock_add_spec(['key'])
        self.mock_config_root.return_value.key = 'value'
        self.isp_host_configuration = self.ISPHostConfiguration('', 'host1')

    def tearDown(self):
        ScanlineISPTest.TestCase.tearDown(self)
        self.config_root_patch.stop()

    def test_decode_inflate_not_valid(self):
        self.assertFalse(self.ISPHostConfiguration.decode_inflate('24f3rfgf'))

    def test_decode_inflate_valid(self):
        result = self.ISPHostConfiguration.decode_inflate(
            'GQAAAHicK8nILFYAovy8nEqFRIWS1OISAEXhBvg=\n')
        self.assertEqual(result, 'this is only a test')

    def test_get_attr_valid(self):
        self.assertEqual(self.isp_host_configuration.get('key'), 'value')

    def test_get_attr_invalid(self):
        self.assertEqual(self.isp_host_configuration.get('fail'), None)

    def test_get_attr_default_set(self):
        self.assertEqual(self.isp_host_configuration.get(
            'fail', 'DefaultVal'), 'DefaultVal')

    def test_remove_xml_descriptor_no_match(self):
        self.assertEqual(
            self.isp_host_configuration.remove_xml_descriptor('not xml'), 'not xml')

    def test_remove_xml_descriptor_removed(self):
        in_xml = '<? xml version="1.0" encoding="utf-16" ?>\r\n<iSiteSystem>\r\n</iSiteSystem>'
        self.assertEqual(self.isp_host_configuration.remove_xml_descriptor(
            in_xml), '<iSiteSystem>\r\n</iSiteSystem>')

    def test_remove_xml_descriptor_removed_compact(self):
        in_xml = '<?xml version="1.0" encoding="utf-8"?>\n<iSiteSystem>\r\n</iSiteSystem>'
        self.assertEqual(self.isp_host_configuration.remove_xml_descriptor(
            in_xml), '<iSiteSystem>\r\n</iSiteSystem>')


class ScanlineISPDBConfigurationTestCase(ScanlineISPTest.TestCase):
    def setUp(self):
        ScanlineISPTest.TestCase.setUp(self)
        import scanline.product.isp
        self.module = scanline.product.isp
        self.ISPDBConfiguration = scanline.product.isp.ISPDBConfiguration
        self.ispdb = self.ISPDBConfiguration('10.4.4.6')
        self.ispdb.auth_session = MagicMock(name='auth_session')

    def tearDown(self):
        ScanlineISPTest.TestCase.tearDown(self)

    def test_config_url(self):
        self.assertEqual(
            self.ispdb.config_url,
            'https://10.4.4.6/InfrastructureServices/RegistryService/RegistryService.ashx'
        )

    def test_get_auth_token_ok_on_first_try(self):
        self.ispdb.attempt_authorization = MagicMock(
            name='attempt_authorization')
        self.assertEqual(self.ispdb.get_auth_token(),
                         self.ispdb.attempt_authorization.return_value)

    def test_get_auth_token_ok_on_second_try(self):
        self.ispdb.attempt_authorization = MagicMock(
            name='attempt_authorization')
        self.ispdb.attempt_authorization.side_effect = [None, 'Token-1']
        self.assertEqual(self.ispdb.get_auth_token(), 'Token-1')

    def test_get_auth_token_not_retrieved(self):
        self.ispdb.attempt_authorization = MagicMock(
            name='attempt_authorization')
        self.ispdb.attempt_authorization.return_value = None
        self.assertEqual(self.ispdb.get_auth_token(), None)

    def test_get_server_info_query(self):
        self.assertEqual(self.ispdb.get_server_info_query(), '<Message><ListNonCoreNodes/></Message>')

    def test_get_hosts(self):
        f = StringIO("""<Message><ListNonCoreNodesResponse><ArrayOfNode
                     xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                     <Node><HostName>Host1</HostName><NodeType>Database</NodeType>
                     <Location>Main Location</Location><FilePath>
                     <string>s:\\Mount</string></FilePath></Node>
                     </ArrayOfNode></ListNonCoreNodesResponse></Message>""")
        atree = etree.parse(f)
        self.ispdb.query_xml = MagicMock(name='query_xml', return_value=atree)
        self.assertEqual(self.ispdb.get_hosts()[0].text, 'Host1')

    def test_get_noncore_node(self):
        result = self.ispdb.get_noncore_node()
        self.assertEqual(result, 'DB')


class ScanlineISPProductScannerTestCase(ScanlineISPTest.TestCase):
    def setUp(self):
        ScanlineISPTest.TestCase.setUp(self)
        from scanline.product.isp import ISPProductScanner
        from scanline.host.isp import ISPHostScanner
        from scanline.host.udm import UDMRedisHostScanner
        from scanline.host.windows import WindowsHostScanner
        self.ISPProductScanner = ISPProductScanner
        self.ISPHostScanner = ISPHostScanner
        self.UDMRedisHostScanner = UDMRedisHostScanner
        self.WindowsHostScanner = WindowsHostScanner
        self.ispcs = patch.multiple(
            'scanline.product.isp',
            ISP4_1Configuration=DEFAULT,
            ISP4XConfiguration=DEFAULT,
            ISP3XConfiguration=DEFAULT,
            ISP4XHiSecConfiguration=DEFAULT,
            ISPDBConfiguration=DEFAULT
        )
        self.mock_ispcs = self.ispcs.start()
        self.isp_scanner = self.ISPProductScanner(
            'ISP', '10.220.3.1', 'tester', 'CryptThis', tags=['Prod'])
        self.ISPHostScanner.to_dict = MagicMock(name='to_dict')
        self.WindowsHostScanner.to_dict = MagicMock(name='to_dict')
        self.UDMRedisHostScanner.to_dict = MagicMock(name='to_dict')

    def tearDown(self):
        ScanlineISPTest.TestCase.tearDown(self)
        self.ispcs.stop()

    # def test_create_isp_3x(self):
    #     self.assertEqual(self.isp_scanner.create_isp(), self.mock_ispcs[
    #                      'ISP3XConfiguration'].return_value)

    # def test_create_isp_4x(self):
    #     self.mock_ispcs[
    #         'ISP3XConfiguration'].return_value.get_software_version.return_value = None
    #     self.assertEqual(self.isp_scanner.create_isp(), self.mock_ispcs[
    #                      'ISP4XConfiguration'].return_value)

    # def test_create_isp_4xHS(self):
    #     self.mock_ispcs[
    #         'ISP3XConfiguration'].return_value.get_software_version.return_value = None
    #     self.mock_ispcs[
    #         'ISP4XConfiguration'].return_value.get_software_version.return_value = None
    #     self.assertEqual(self.isp_scanner.create_isp(), self.mock_ispcs[
    #                      'ISP4XHiSecConfiguration'].return_value)

    # def test_create_isp_4_1(self):
    #     self.mock_ispcs[
    #         'ISP3XConfiguration'].return_value.get_software_version.return_value = None
    #     self.mock_ispcs[
    #         'ISP4XConfiguration'].return_value.get_software_version.return_value = None
    #     self.mock_ispcs[
    #         'ISP4XHiSecConfiguration'].return_value.get_software_version.return_value = None
    #     self.assertEqual(self.isp_scanner.create_isp(), self.mock_ispcs[
    #                      'ISP4_1Configuration'].return_value)


    @patch('scanline.product.isp.ISPProductScanner.isp')
    def test_get_hosts(self, mock_isp):
        mock_isp.get_hosts.return_value = ['host1']
        self.assertEqual(list(self.isp_scanner.get_hosts()), ['host1'])

    @patch('scanline.product.isp.ISPProductScanner.isp', None)
    def test_get_hosts_no_isp(self):
        self.assertEqual(list(self.isp_scanner.get_hosts()), [])

    def test_get_hostname(self):
        self.assertEqual(self.isp_scanner.get_hostname(
            {'a': 'b'}), "{'a': 'b'}")

    @patch('scanline.product.isp.ISPHostScanner')
    @patch('scanline.product.isp.WindowsHostScanner')
    def test_scan_host(self, mock_windows_scanner, mock_isp_scanner):
        mock_windows_scanner.return_value.to_dict.return_value = {
            'address': '10.4.5.6',
            'Windows': {'version': 'Windows 2k', 'patch': 'please'}
        }
        mock_isp_scanner.return_value.to_dict.return_value = {
            'address': '10.4.5.6',
            'ISP': {'module_type': '4', 'version': '4,4,4,4'}
        }
        self.isp_scanner.host_scanner = MagicMock()
        self.isp_scanner.host_scanner.return_value.to_dict.return_value = {
            'ModR': {'key1': 'val1'}}
        self.assertEqual(
            self.isp_scanner.scan_host('host1'),
            {
                'Windows': {'version': 'Windows 2k', 'patch': 'please'},
                'ISP': {'version': '4,4,4,4', 'module_type': '4'},
                'ModR': {'key1': 'val1'},
                'address': '10.4.5.6',
            }
        )
        mock_windows_scanner.assert_called_once_with(
            domain=None,
            endpoint={'scanner': 'ISP', 'address': '10.220.3.1'},
            hostname='host1',
            ipv6_enabled=False,
            password='CryptThis',
            tags=['Prod'],
            username='tester'
        )
        mock_isp_scanner.assert_called_once_with(
            endpoint={'scanner': 'ISP', 'address': '10.220.3.1'},
            hostname='host1',
            ipv6_enabled=False,
            isp=self.isp_scanner.isp,
            tags=['Prod']
        )
        self.isp_scanner.host_scanner.assert_called_once_with(
            domain=None,
            endpoint={'scanner': 'ISP', 'address': '10.220.3.1'},
            hostname='host1',
            ipv6_enabled=False,
            password='CryptThis',
            tags=['Prod'],
            username='tester'
        )

    def test_scan_dbhost(self):
        self.ISPHostScanner.return_value.to_dict = MagicMock(name='to_dict',
                                                   return_value={'address': '10.4.5.6',
                                                                 'ISP': {'module_type': '4', 'version': '4,4,4,4'}
                                                                }
                                                )

        self.WindowsHostScanner.return_value.to_dict = MagicMock(name='to_dict',
                                                    return_value={'address': '10.4.5.6',
                                                                 'Windows': {'version': 'Windows 2k', 'patch': 'please'}
                                                                }
                                                    )
        self.isp_scanner.host_scanner = MagicMock()
        self.isp_scanner.host_scanner.return_value.to_dict.return_value = {
            'ModR': {'key1': 'val1'}}
        self.assertEqual(
            self.isp_scanner.scan_dbhost('host1'),
            {
                'Windows': {'version': 'Windows 2k', 'patch': 'please'},
                'ISP': {'version': '4,4,4,4', 'module_type': '4'},
                'ModR': {'key1': 'val1'},
                'product_version': '',
                'address': '10.4.5.6',
            }
        )

        self.WindowsHostScanner.assert_called_once_with(
            domain=None,
            endpoint={'scanner': 'ISP', 'address': '10.220.3.1'},
            hostname='host1',
            password='CryptThis',
            tags=['Prod'],
            username='tester'
        )

    def test_scan_redis_host(self):
        self.UDMRedisHostScanner.return_value.to_dict = MagicMock(name='to_dict',
                                                   return_value={'address': '10.4.5.6',
                                                                 'UDMRedis': {'module_type': '4', 'version': '4,4,4,4'}
                                                                }
                                                )

        self.isp_scanner.host_scanner = MagicMock()
        self.isp_scanner.host_scanner.return_value.to_dict.return_value = {
            'ModR': {'key1': 'val1'}}
        self.assertEqual(
            self.isp_scanner.scan_redis_host('host1'),
            {
                'UDMRedis': {'version': '4,4,4,4', 'module_type': '4'},
                'ModR': {'key1': 'val1'},
                'product_version': '',
                'address': '10.4.5.6',
                'product_id': None
            }
        )

    def test_get_redis_host_facts(self):
        self.isp_scanner.scan_redis_host = MagicMock(
            name='scan_redis_host',
            return_value={'Windows': {}, 'address': '10.0.0.1', 'ISP': {}, 'manufacturer': 'IBM'}
        )
        self.isp_scanner.get_present_modules = MagicMock(
            name='get_present_modules', return_value=['Windows', 'ISP']
        )
        self.assertEqual(
            self.isp_scanner.get_redis_host_facts({'hostname': 'host1'}),
            {'Windows': {}, 'ISP': {}, 'modules': ['Windows', 'ISP'], 'address': '10.0.0.1', 'manufacturer': 'IBM'}
        )

    def test_get_dbhost_facts(self):
        self.isp_scanner.scan_dbhost = MagicMock(
            name='scan_dbhost',
            return_value={'Windows': {}, 'address': '10.0.0.1', 'ISP': {}, 'manufacturer': 'IBM'}
        )
        self.isp_scanner.get_present_modules = MagicMock(
            name='get_present_modules', return_value=['Windows', 'ISP']
        )
        self.assertEqual(
            self.isp_scanner.get_dbhost_facts({'hostname': 'host1'}),
            {'Windows': {}, 'ISP': {}, 'modules': ['Windows', 'ISP'], 'address': '10.0.0.1', 'manufacturer': 'IBM'}
        )


    def test_site_facts(self):
        self.isp_scanner.get_dbhost_facts = MagicMock(name='get_dbhost_facts', return_value='167.81.183.99')
        self.isp_scanner.get_hostname = MagicMock(name='get_hostname', return_value='167.81.183.99')


        self.isp_scanner.isp.__class__.__name__ = 'ISP4XHiSecConfiguration'
        self.isp_scanner.site_facts = MagicMock(name='site_facts', return_value={'167.81.183.99':
                                                                                 self.isp_scanner.isp.get_dbhost_facts.return_value
                                                                                }
                                                )
        self.assertEqual(
            self.isp_scanner.site_facts(),
            {'167.81.183.99': self.isp_scanner.isp.get_dbhost_facts.return_value}
        )


    def test_site_facts_noncore(self):
        self.isp_scanner.get_dbhost_facts = MagicMock(name='get_dbhost_facts',
                                                      return_value='167.81.183.99')
        self.isp_scanner.get_hostname = MagicMock(name='get_hostname',
                                                  return_value='167.81.183.99')

        self.isp_scanner.isp.__class__.__name__ = 'ISP4XHiSecConfiguration'
        self.isp_scanner.site_facts = MagicMock(name='site_facts',
                                                return_value={'167.81.183.199':
                                self.isp_scanner.isp.get_dbhost_facts.return_value
                                                              }
                                                )
        self.assertEqual(
            self.isp_scanner.site_facts(),
            {'167.81.183.199': self.isp_scanner.isp.get_dbhost_facts.return_value}
        )


if __name__ == '__main__':
    unittest.main()
