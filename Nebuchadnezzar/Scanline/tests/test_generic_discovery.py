import unittest
from mock import MagicMock, patch, call


class ScanlineGenericProductTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(
            name='cached_property', cached_property=property)
        self.mock_scanline = MagicMock(name='scanline')
        self.mock_host = MagicMock()
        self.mock_http = MagicMock()
        modules = {
            'cached_property': self.mock_cached_property,
            'logging': MagicMock(name='logging')
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        self.http_patch = patch('scanline.product.HTTPRequester')
        self.http_patcher = self.http_patch.start()
        self.mock_host_scanner = MagicMock(name='host_scanner')
        from scanline.product import GenericProductScanner
        self.GPS_obj = GenericProductScanner(
            scanner='scan1',
            address='167.81.183.99',
            tags=['y', 'u'],
            host_scanner=self.mock_host_scanner,
            discovery_url='http://testurl.com'
        )

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()
        self.http_patch.stop()

    def test_discovery_info_empty_rsp(self):
        self.http_patcher.return_value.suppressed_get.return_value = None
        self.assertEqual(self.GPS_obj.discovery_info, {})
        self.http_patcher().suppressed_get.assert_called_once_with('http://testurl.com')

    def test_discovery_info_proper_rsp(self):
        mk_obj = MagicMock()
        mk_obj.json.return_value = {'test': 'ok'}
        self.http_patcher.return_value.suppressed_get.return_value = mk_obj
        self.assertEqual(self.GPS_obj.discovery_info, {'test': 'ok'})
        self.http_patcher().suppressed_get.assert_called_once_with('http://testurl.com')
        mk_obj.json.assert_called_once_with()

    def test_get_hosts_proper_rsp(self):
        mk_obj = MagicMock()
        mk_obj.json.return_value = {'hostinfo': ['h1', 'h2']}
        self.http_patcher.return_value.suppressed_get.return_value = mk_obj
        self.assertEqual(self.GPS_obj.get_hosts(), ['h1', 'h2'])
        self.http_patcher().suppressed_get.assert_called_once_with('http://testurl.com')

    # def test_get_hosts_exception_case1(self):
    #     mk_obj = MagicMock()
    #     mk_obj.json.return_value = {'hostinfo': []}
    #     self.http_patcher.return_value.suppressed_get.return_value = mk_obj
    #     self.assertRaises(Exception, self.GPS_obj.get_hosts)

    # def test_get_hosts_exception_case2(self):
    #     mk_obj = MagicMock()
    #     mk_obj.json.return_value = {}
    #     self.http_patcher.return_value.suppressed_get.return_value = mk_obj
    #     self.assertRaises(Exception, self.GPS_obj.get_hosts)

    def test_get_hostname(self):
        host = {'address': '1.0.1.1'}
        self.assertEqual(self.GPS_obj.get_hostname(host), '1.0.1.1')

    def test_get_hostname_empty_resp(self):
        self.assertEqual(self.GPS_obj.get_hostname({}), '')

    def test_scan_host(self):
        mk_obj = MagicMock()
        mk_obj.to_dict.return_value = {'test': 'ok'}
        self.mock_host_scanner.return_value = mk_obj
        discovery_info = {'productversion': '1.2',
                          'productname': 'prod', 'productid': 'productid'}
        mk_obj = MagicMock()
        mk_obj.json.return_value = discovery_info
        self.http_patcher.return_value.suppressed_get.return_value = mk_obj
        self.assertEqual(self.GPS_obj.scan_host(
            {'address': '1.2.3.4', 'role': 'application'}), {'test': 'ok'},)
        self.mock_host_scanner.assert_called_once_with('1.2.3.4',
                                                       {'scanner': 'scan1',
                                                           'address': '167.81.183.99'},
                                                       discovery_url='http://testurl.com', product_id='productid',
                                                       product_name='prod', product_version='1.2', role=['application'],
                                                       tags=['y', 'u'])
        # mk_obj.to_dict.assert_called_once()

    def test_site_facts(self):
        h_names = {'h1': '1.2.3.4', 'h2': '3.4.5.6'}
        h_facts = {'h1': ['f11', 'f12', 'f13'], 'h2': ['f21', 'f22', 'f23']}
        mk_name = MagicMock(side_effect=lambda host: h_names[host])
        mk_facts = MagicMock(side_effect=lambda host: h_facts[host])
        self.GPS_obj.get_hostname = mk_name
        self.GPS_obj.get_host_facts = mk_facts
        self.GPS_obj.get_hosts = MagicMock(
            return_value=['h1', 'h2'])
        self.assertEqual(self.GPS_obj.site_facts(), {'1.2.3.4': [
                         'f11', 'f12', 'f13'], '3.4.5.6': ['f21', 'f22', 'f23']})
        expected_call = [call('h1'), call('h2')]
        mk_expected_call = [call('h1'), call('h1'), call('h2'), call('h2')]
        self.assertEqual(mk_name.call_args_list, expected_call)
        self.assertEqual(mk_facts.call_args_list, mk_expected_call)

    def test_get_host_role(self):
        host = {'name':'ibe.cloudapp.net','address':'100.75.164.20','role':'application'}
        self.assertEqual(self.GPS_obj.get_host_role(host), ['application'])


if __name__ == '__main__':
    unittest.main()
