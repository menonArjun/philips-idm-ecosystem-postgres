import unittest
from mock import MagicMock, patch


class ISPComponentsTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_app_version = MagicMock(name='app_version')
        modules = {
            'scanline.utilities.app_version': self.mock_app_version,
            'logging': MagicMock(name='logging')
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.component import isp
        self.isp = isp
        self.mock_http_scanner = self.isp.AppVersionHTTPScanner

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_anywhere_get_version(self):
        self.assertEqual(
            self.isp.AnywhereComponent().scan('10.2.22.2'),
            self.mock_http_scanner.return_value.get_version.return_value
        )
        self.mock_http_scanner.assert_called_once_with(
            '10.2.22.2',
            'IntelliSpace Anywhere',
            [
                ('anywhere/version.html', self.isp.AppVersionHTMLParser),
                ('Anywhere/Version.json', self.isp.AppVersionJSONRetriever)
            ]
        )

    def test_anywhere_get_version_no_result(self):
        self.mock_http_scanner.return_value.get_version.return_value = 0
        self.assertEqual(self.isp.AnywhereComponent().scan('10.2.22.2'), None)
        self.mock_http_scanner.assert_called_once_with(
            '10.2.22.2',
            'IntelliSpace Anywhere',
            [
                ('anywhere/version.html', self.isp.AppVersionHTMLParser),
                ('Anywhere/Version.json', self.isp.AppVersionJSONRetriever)
            ]
        )

    def test_vl_get_version(self):
        self.assertEqual(
            self.isp.VLCaptureComponent().scan('10.2.22.2'),
            self.mock_http_scanner.return_value.get_version.return_value
        )
        self.mock_http_scanner.assert_called_once_with(
            '10.2.22.2', 'IntelliSpace VL Capture', [('VLCapture/version.json', self.isp.AppVersionJSONRetriever)])

    def test_vl_get_version_no_result(self):
        self.mock_http_scanner.return_value.get_version.return_value = 0
        self.assertEqual(self.isp.VLCaptureComponent().scan('10.2.22.2'), None)
        self.mock_http_scanner.assert_called_once_with(
            '10.2.22.2', 'IntelliSpace VL Capture', [('VLCapture/version.json', self.isp.AppVersionJSONRetriever)])

    def test_cca_get_version(self):
        self.assertEqual(
            self.isp.CCAComponent().scan('10.2.22.2'),
            self.mock_http_scanner.return_value.get_version.return_value
        )
        self.mock_http_scanner.assert_called_once_with(
            '10.2.22.2', 'CCA', [('CCA/version.json', self.isp.AppVersionJSONRetriever)])

    def test_cca_get_version_no_result(self):
        self.mock_http_scanner.return_value.get_version.return_value = 0
        self.assertEqual(self.isp.CCAComponent().scan('10.2.22.2'), None)
        self.mock_http_scanner.assert_called_once_with(
            '10.2.22.2', 'CCA', [('CCA/version.json', self.isp.AppVersionJSONRetriever)])


if __name__ == '__main__':
    unittest.main()
