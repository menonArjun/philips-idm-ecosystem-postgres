import unittest
from mock import MagicMock, patch


class ScanlineHostLegacyNagiosTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        modules = {
            'cached_property': self.mock_cached_property,
            'logging': MagicMock(name='logging'),
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.host.legacy_nagios import LegacyNagiosHostScanner
        self.LegacyNagiosHostScanner = LegacyNagiosHostScanner
        self.mock_host_config = {
            'host_name': 'host1.somep.net',
            'alias': 'host1',
            'use': 'some-servers',
            'address': '10.3.2.3'
        }
        self.lnhs = self.LegacyNagiosHostScanner(self.mock_host_config, '10.4.3.1', tags=['x', 'y'])

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_address(self):
        # DNS may not have address for legacy objects, get it from config
        self.assertEqual(self.lnhs.address, '10.3.2.3')

    def test_alias(self):
        self.assertEqual(self.lnhs.alias, 'host1')

    def test_use(self):
        self.assertEqual(self.lnhs.use, 'some-servers')

    def test_properties_present(self):
        mock_scanner = MagicMock(spec=self.lnhs)
        for prop in self.LegacyNagiosHostScanner.module_properties:
            self.assertTrue(getattr(mock_scanner, prop))
        for prop in self.LegacyNagiosHostScanner.general_properties:
            self.assertTrue(getattr(mock_scanner, prop))



if __name__ == '__main__':
    unittest.main()
