import unittest
from mock import MagicMock, patch


class WindowsComponentTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        modules = {'logging': MagicMock(name='logging')}
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.component.windows import WindowsServiceComponent
        self.WindowsServiceComponent = WindowsServiceComponent
        self.mock_driller = MagicMock(name='driller')
        self.service_component = self.WindowsServiceComponent(self.mock_driller, 'service1')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_service_is_auto_start(self):
        self.mock_driller.is_service_auto_start.return_value = True
        self.assertEqual(self.service_component.scan('10.6.88.61'), 'service1')

    def test_service_is_not_auto_start(self):
        self.mock_driller.is_service_auto_start.return_value = False
        self.assertEqual(self.service_component.scan('10.6.88.61'), None)


if __name__ == '__main__':
    unittest.main()
