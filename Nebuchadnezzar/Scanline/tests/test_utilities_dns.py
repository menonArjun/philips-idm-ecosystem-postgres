import unittest
from mock import patch, MagicMock


class FakeGAIError(Exception):
    pass


class ScanlineDNSTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        modules = {
            'logging': MagicMock(name='logging'),
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        self.socket_patcher = patch('scanline.utilities.dns.socket')
        self.mock_socket = self.socket_patcher.start()
        from scanline.utilities import dns
        self.dns = dns

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()
        self.socket_patcher.stop()

    def test_get_address_ok(self):
        self.assertEqual(self.dns.get_address('somehost'), self.mock_socket.gethostbyname.return_value)

    def test_get_address_exception(self):
        self.mock_socket.gaierror = FakeGAIError
        self.mock_socket.gethostbyname.side_effect = FakeGAIError
        self.assertEqual(self.dns.get_address('somehost'), None)


if __name__ == '__main__':
    unittest.main()
