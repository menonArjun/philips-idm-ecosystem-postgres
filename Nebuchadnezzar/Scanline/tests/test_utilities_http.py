import unittest
from mock import MagicMock, patch


class FakeRequestException(Exception):
    pass


class ScanlineHTTPRequesterTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_requests = MagicMock(name='requests')
        modules = {
            'logging': MagicMock(name='logging'),
            'requests': self.mock_requests,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.utilities import http
        self.http = http
        self.http_requester = self.http.HTTPRequester()
        self.http_requester._session = MagicMock(name='session')
        self.mock_requests.exceptions.RequestException = FakeRequestException

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_suppressed_get_ok(self):
        self.assertEqual(
            self.http_requester.suppressed_get('http://oktest.com/version.html'),
            self.http_requester._session.get.return_value
        )
        self.http_requester._session.get.assert_called_once_with(params=None,
                                                                 url='http://oktest.com/version.html',
                                                                 verify=False)

    def test_suppressed_get_exception(self):
        self.http_requester._session.get.side_effect = FakeRequestException
        self.assertEqual(
            self.http_requester.suppressed_get('http://oktest.com/version.html'),
            None
        )
        self.http_requester._session.get.assert_called_once_with(params=None,
                                                                 url='http://oktest.com/version.html',
                                                                 verify=False)

    def test_suppressed_post_ok(self):
        self.assertEqual(
            self.http_requester.suppressed_post('http://oktest.com/version.html', data={'akey': 'avalue'}),
            self.http_requester._session.post.return_value
        )
        self.http_requester._session.post.assert_called_once_with(
            data={'akey': 'avalue'}, timeout=5, url='http://oktest.com/version.html', verify=False
        )

    def test_suppressed_post_ok_with_headers(self):
        self.assertEqual(
            self.http_requester.suppressed_post(
                'http://oktest.com/version.html',
                data={'akey': 'avalue'},
                headers={'hkey': 'hval'}
            ),
            self.http_requester._session.post.return_value
        )
        self.http_requester._session.post.assert_called_once_with(
            data={'akey': 'avalue'},
            timeout=5,
            url='http://oktest.com/version.html',
            verify=False,
            headers={'hkey': 'hval'}
        )

    def test_suppressed_post_exception(self):
        self.http_requester._session.post.side_effect = FakeRequestException
        self.assertEqual(
            self.http_requester.suppressed_post('http://oktest.com/version.html', data={'akey': 'avalue'}),
            None
        )
        self.http_requester._session.post.assert_called_once_with(
            data={'akey': 'avalue'}, timeout=5, url='http://oktest.com/version.html', verify=False
        )

    def test_suppressed_post_exception_from_status(self):
        self.http_requester._session.post.return_value.raise_for_status.side_effect = FakeRequestException
        self.assertEqual(
            self.http_requester.suppressed_post('http://oktest.com/version.html', data={'akey': 'avalue'}),
            None
        )
        self.http_requester._session.post.assert_called_once_with(
            data={'akey': 'avalue'}, timeout=5, url='http://oktest.com/version.html', verify=False
        )
        self.assertTrue(self.http_requester._session.post.return_value.raise_for_status.called)

    def test_suppressed_post_not_ok_only(self):
        self.http_requester._session.post.return_value.raise_for_status.side_effect = FakeRequestException
        self.assertEqual(
            self.http_requester.suppressed_post('http://oktest.com/version.html', data={'a': 'value'}, ok_only=False),
            self.http_requester._session.post.return_value
        )
        self.http_requester._session.post.assert_called_once_with(
            data={'a': 'value'}, timeout=5, url='http://oktest.com/version.html', verify=False
        )
        self.assertFalse(self.http_requester._session.post.return_value.raise_for_status.called)

    def test_suppressed_get_parsed_ok(self):
        data = '<html><body><div class="section"><div>Queue Size&nbsp;:&nbsp;' \
               '2</div></div></body></html>'
        self.http_requester._session.get.return_value.status_code = 200
        self.http_requester._session.get.return_value.content = data
        url = 'http://oktest.com/version.html'
        headers = {'Accept': 'application/json, text/html',}
        auth = ('username', 'password')
        self.assertEqual(self.http_requester.suppressed_get_parsed(
            url, headers=headers, auth=auth, section='div', 
            attrs={'class': 'section'}), ('Queue Size:2'))

    def test_suppressed_get_parsed_exception(self):
        self.http_requester._session.get.return_value.side_effect = Exception
        url = 'http://oktest.com/version.html'
        headers = {'Accept': 'application/json, text/html',}
        auth = ('username', 'password')
        self.assertEqual(self.http_requester.suppressed_get_parsed(
            url, headers=headers, auth=auth, section='div',
            attrs={'class': 'section'}), None)

    def test_suppressed_get_parsed_404(self):
        self.http_requester._session.get.return_value.status_code = 404
        url = 'http://oktest.com/version.html'
        headers = {'Accept': 'application/json, text/html',}
        auth = ('username', 'password')
        self.assertEqual(self.http_requester.suppressed_get_parsed(
            url, headers=headers, auth=auth, section='div',
            attrs={'class': 'section'}), None)


if __name__ == '__main__':
    unittest.main()
