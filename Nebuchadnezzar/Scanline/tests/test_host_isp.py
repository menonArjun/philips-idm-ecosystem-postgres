import unittest
from mock import MagicMock, patch


class ScanlineISPHostScannerTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        self.mock_component = MagicMock()
        modules = {
            'cached_property': self.mock_cached_property,
            'logging': MagicMock(name='logging'),
            'scanline.component.isp': self.mock_component.isp,
            'scanline.component.windows': self.mock_component.windows
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.host.isp import ISPHostScanner
        self.ISPHostScanner = ISPHostScanner
        self.mock_isp = MagicMock(name='ISP', spec=['get_module_type', 'software_version', 'identifier', 'noncore_node', 'get_software_version'])
        self.isp_host_scanner = ISPHostScanner('host1', '167.81.183.99', self.mock_isp)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_module_type(self):
        self.assertEqual(self.isp_host_scanner.module_type, self.mock_isp.get_module_type.return_value)
        self.mock_isp.get_module_type.assert_called_once_with('host1')

    def test_version(self):
        self.assertEqual(self.isp_host_scanner.version, self.mock_isp.software_version)

    def test_identifier(self):
        self.assertEqual(self.isp_host_scanner.identifier, self.mock_isp.identifier)

    @patch('scanline.host.isp.ISPHostScanner.address', '10.2.3.4')
    @patch('scanline.host.isp.ISPHostScanner.noncore_node', None)
    def test_get_components(self):
        self.assertEqual(
            self.isp_host_scanner.get_components(),
            {
                'anywhere': self.mock_component.isp.AnywhereComponent.return_value,
                # Commenting VL Capture as for workaround VL Capture version is not responding for many sites as well as other sites its None
                # 'vlcapture': self.mock_component.isp.VLCaptureComponent.return_value,
                'cca': self.mock_component.isp.CCAComponent.return_value
            }
        )

    @patch('scanline.host.isp.ISPHostScanner.address', None)
    @patch('scanline.host.isp.ISPHostScanner.noncore_node', None)
    def test_get_components_no_address(self):
        self.assertEqual(self.isp_host_scanner.get_components(), {})

    def test_properties_present(self):
        mock_scanner = MagicMock(spec=self.isp_host_scanner)
        for prop in self.ISPHostScanner.module_properties:
            self.assertTrue(getattr(mock_scanner, prop))
        for prop in self.ISPHostScanner.general_properties:
            self.assertTrue(getattr(mock_scanner, prop))


if __name__ == '__main__':
    unittest.main()
