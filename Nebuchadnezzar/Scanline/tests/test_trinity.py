import unittest
from mock import MagicMock, patch


class TrinityTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_scanline_host = MagicMock(name='host')
        self.mock_scanline_product = MagicMock(name='product')
        modules = {
            'logging': MagicMock(name='logging'),
            'scanline.host': self.mock_scanline_host,
            'scanline.product': self.mock_scanline_product,
            'scanline.product.legacy_nagios': self.mock_scanline_product.legacy_nagios,
            'scanline.product.isp': self.mock_scanline_product.isp,
            'scanline.product.windows': self.mock_scanline_product.windows,
            'scanline.product.vmware': self.mock_scanline_product.vmware,
            'scanline.product.ibe': self.mock_scanline_product.ibe,
            'scanline.product.analytics_publisher': self.mock_scanline_product.analytics_publisher,
            'scanline.product.iecg': self.mock_scanline_product.iecg,
            'scanline.product.iscv': self.mock_scanline_product.iscv,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.trinity import scanline_endpoints, scanline_scanner
        self.scanline_endpoints = scanline_endpoints
        self.scanline_scanner = scanline_scanner
        self.blob = {
            'username': 'tester',
            'password': 'CryptThis',
            'scanner': 'ISP',
            'address': '167.81.183.99',
            'tags': ['Prod', 'Primary']
        }
        self.fake_scanners = {
           'ISP': {'product': MagicMock(name='isp_product')},
           'Windows': {'host': MagicMock(name='windows_host')},
           'Magic': {'product': MagicMock(name='magic_product'), 'host': MagicMock(name='magic_host')}
        }

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    @patch('scanline.trinity.yaml.load')
    def test_scanline_endpoints(self, mock_load):
        with patch('__builtin__.open'):
            scanners = self.scanline_endpoints('manifest')
        self.assertEqual(scanners, mock_load.return_value)

    def test_scanline_scanner_in_scanners_product(self):
        with patch.dict('scanline.trinity.scanners', self.fake_scanners, clear=True):
            self.assertEqual(self.scanline_scanner(self.blob), self.fake_scanners['ISP']['product'].return_value)
        self.fake_scanners['ISP']['product'].assert_called_once_with(
            address='167.81.183.99',
            host_scanner=None,
            password='CryptThis',
            tags=['Prod', 'Primary'],
            username='tester',
            scanner='ISP'
        )

    def test_scanline_scanner_in_scanners_product_and_host(self):
        self.blob['scanner'] = 'Magic'
        with patch.dict('scanline.trinity.scanners', self.fake_scanners, clear=True):
            self.assertEqual(self.scanline_scanner(self.blob), self.fake_scanners['Magic']['product'].return_value)
        self.fake_scanners['Magic']['product'].assert_called_once_with(
            address='167.81.183.99',
            host_scanner=self.fake_scanners['Magic']['host'],
            password='CryptThis',
            tags=['Prod', 'Primary'],
            username='tester',
            scanner='Magic'
        )

    @patch('scanline.trinity.ProductScanner')
    def test_scanline_scanner_in_scanners_host(self, mock_product_scanner):
        self.blob['scanner'] = 'Windows'
        with patch.dict('scanline.trinity.scanners', self.fake_scanners, clear=True):
            self.assertEqual(self.scanline_scanner(self.blob), mock_product_scanner.return_value)
        mock_product_scanner.assert_called_once_with(
            address='167.81.183.99',
            host_scanner=self.fake_scanners['Windows']['host'],
            password='CryptThis',
            tags=['Prod', 'Primary'],
            username='tester',
            scanner='Windows'
        )

    def test_scanline_scanner_not_in_scanners(self):
        self.blob['scanner'] = 'Unknown'
        with patch.dict('scanline.trinity.scanners', self.fake_scanners, clear=True):
            self.assertEqual(self.scanline_scanner(self.blob), None)


if __name__ == '__main__':
    unittest.main()
