import unittest
from mock import MagicMock, patch


class ScanlineHostESXiTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        modules = {
            'cached_property': self.mock_cached_property,
            'logging': MagicMock(name='logging'),
        }
        self.mock_host = MagicMock(name='host')
        self.mock_host.identifierValue = 'test_value'
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.host.esxi import ESXiHostScanner
        self.ESXiHostScanner = ESXiHostScanner
        self.mock_vmware_host = MagicMock(name='vmware_host')
        self.mock_vmware_host.summary.hardware.otherIdentifyingInfo = [self.mock_host]
        self.esxi_host_scanner = self.ESXiHostScanner(self.mock_vmware_host, '167.81.183.99',  tags=['x', 'y'])

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_hostname(self):
        self.mock_vmware_host.name = {'a': 'b'}
        self.assertEqual(self.esxi_host_scanner.get_hostname(self.mock_vmware_host), "{'a': 'b'}")

    def test_manufacturer(self):
        self.assertEqual(self.esxi_host_scanner.manufacturer, self.mock_vmware_host.summary.hardware.vendor)

    def test_model(self):
        self.assertEqual(self.esxi_host_scanner.model, self.mock_vmware_host.summary.hardware.model)

    def test_servicetag(self):
        self.mock_host.identifierType.key = 'ServiceTag'
        self.assertEqual(self.esxi_host_scanner.servicetag, 'test_value')

    def test_servicetag_when_servicetag_not_available(self):
        self.mock_host.identifierType.key = 'NoServiceTag'
        self.assertEqual(self.esxi_host_scanner.servicetag, '')

    def test_properties_present(self):
        mock_scanner = MagicMock(spec=self.esxi_host_scanner)
        for prop in self.ESXiHostScanner.module_properties:
            self.assertTrue(getattr(mock_scanner, prop))
        for prop in self.ESXiHostScanner.general_properties:
            self.assertTrue(getattr(mock_scanner, prop))


if __name__ == '__main__':
    unittest.main()
