import unittest
from mock import MagicMock, patch


class ScanlineProductTestCase(unittest.TestCase):

    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        self.mock_scanline = MagicMock(name='scanline')
        modules = {
            'cached_property': self.mock_cached_property,
            'logging': MagicMock(name='logging'),
            'scanline.host': self.mock_scanline.host,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline import product
        self.module = product
        self.ProductScanner = self.module.ProductScanner
        self.mock_host_scanner = MagicMock(name='host_scanner')
        self.product_scanner = self.ProductScanner(
            'scan1', '167.81.183.99', tags=['y', 'u'], host_scanner=self.mock_host_scanner, product_id='PID'
        )
        self.mock_host_scanner_mod_name = MagicMock(name='host_scanner')
        self.product_scanner_no_pid = self.ProductScanner(
            'scan1', '167.81.183.99', tags=['y', 'u'], host_scanner=self.mock_host_scanner_mod_name)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_hosts_returns_original_address(self):
        self.assertEqual(list(self.product_scanner.get_hosts()), ['167.81.183.99'])

    def test_scan_host(self):
        self.assertEqual(
            self.product_scanner.scan_host('host1'),
            self.mock_host_scanner.return_value.to_dict.return_value
        )
        self.mock_host_scanner.assert_called_once_with(
            'host1', {'scanner': 'scan1', 'address': '167.81.183.99'}, tags=['y', 'u'], product_id='PID'
        )

    def test_get_present_modules_none(self):
        self.module.HostScanner.get_host_scanners.return_value = ['Windows']
        self.assertEqual(self.product_scanner.get_present_modules({'address': '10.0.0.1'}), [])

    def test_get_present_modules(self):
        self.module.HostScanner.get_host_scanners.return_value = ['Windows', 'ISP', 'Host']
        self.assertEqual(self.product_scanner.get_present_modules(
            {'address': '10.0.0.1', 'Windows': {}, 'ISP': {}}),
            ['Windows', 'ISP']
        )

    def test_get_host_facts_with_pid(self):
        self.product_scanner.scan_host = MagicMock(
            name='scan_host',
            return_value={'Windows': {}, 'address': '10.0.0.1', 'ISP': {}, 'manufacturer': 'IBM', 'product_id': 'G_PID'}
        )
        self.product_scanner.get_present_modules = MagicMock(
            name='get_present_modules', return_value=['Windows', 'ISP']
        )
        self.assertEqual(
            self.product_scanner.get_host_facts({'hostname': 'host1'}),
            {'Windows': {}, 'ISP': {}, 'modules': ['Windows', 'ISP'], 'manufacturer': 'IBM', 'address': '10.0.0.1', 'product_id': 'G_PID'}
        )
        self.product_scanner.scan_host.assert_called_once_with({'hostname': 'host1'})
        self.product_scanner.get_present_modules.assert_called_once_with(self.product_scanner.scan_host.return_value)

    def test_get_host_facts_no_pid(self):
        self.product_scanner.scan_host = MagicMock(
            name='scan_host',
            return_value={'Windows': {}, 'address': '10.0.0.1', 'ISP': {}, 'manufacturer': 'IBM', 'product_id': 'PID'}
        )
        self.product_scanner.get_present_modules = MagicMock(
            name='get_present_modules', return_value=['Windows', 'ISP']
        )
        self.assertEqual(
            self.product_scanner.get_host_facts({'hostname': 'host1'}),
            {'Windows': {}, 'ISP': {}, 'modules': ['Windows', 'ISP'], 'manufacturer': 'IBM', 'address': '10.0.0.1', 'product_id': 'PID'}
        )
        self.product_scanner.scan_host.assert_called_once_with({'hostname': 'host1'})
        self.product_scanner.get_present_modules.assert_called_once_with(self.product_scanner.scan_host.return_value)

    def test_get_host_facts_without_data(self):
        self.product_scanner.scan_host = MagicMock(name='scan_host', return_value=None)
        self.assertEqual(self.product_scanner.get_host_facts({'hostname': 'host1'}), None)


    def test_get_host_facts_no_module_name(self):
        self.mock_host_scanner_mod_name.module_name = 'MODULE_NAME'
        self.mock_host_scanner_mod_name.return_value.get_product_id.return_value = 'MODULE_NAME'
        self.product_scanner_no_pid.scan_host = MagicMock(
            name='scan_host',
            return_value={'Windows': {}, 'address': '10.0.0.1', 'ISP': {}, 'manufacturer': 'IBM'})
        self.product_scanner_no_pid.get_present_modules = MagicMock(
            name='get_present_modules', return_value=['Windows', 'ISP']
        )
        self.assertEqual(
            self.product_scanner_no_pid.get_host_facts({'hostname': 'host1'}),
            {'product_id': 'MODULE_NAME', 'Windows': {}, 'ISP': {}, 'modules': ['Windows', 'ISP'],
             'address': '10.0.0.1', 'manufacturer': 'IBM'}
        )
        self.product_scanner_no_pid.scan_host.assert_called_once_with({'hostname': 'host1'})
        self.product_scanner_no_pid.get_present_modules.assert_called_once_with(
            {'product_id': 'MODULE_NAME', 'Windows': {}, 'ISP': {},
             'modules': ['Windows', 'ISP'], 'address': '10.0.0.1', 'manufacturer': 'IBM'})
        self.mock_host_scanner_mod_name.return_value.get_product_id.assert_called_once_with()

    def test_get_host_facts_with_result_product_id(self):
        self.product_scanner_no_pid.scan_host = MagicMock(
            name='scan_host',
            return_value={'Windows': {}, 'address': '10.0.0.1', 'ISP': {}, 'manufacturer': 'IBM', 'product_id': 'PID'})
        self.product_scanner_no_pid.get_present_modules = MagicMock(
            name='get_present_modules', return_value=['Windows', 'ISP']
        )
        self.assertEqual(
            self.product_scanner_no_pid.get_host_facts({'hostname': 'host1'}),
            {'product_id': 'PID', 'Windows': {}, 'ISP': {}, 'modules': ['Windows', 'ISP'],
             'address': '10.0.0.1', 'manufacturer': 'IBM'}
        )
        self.product_scanner_no_pid.scan_host.assert_called_once_with({'hostname': 'host1'})
        self.product_scanner_no_pid.get_present_modules.assert_called_once_with(
            {'product_id': 'PID', 'Windows': {}, 'ISP': {},
             'modules': ['Windows', 'ISP'], 'address': '10.0.0.1', 'manufacturer': 'IBM'})


    def test_site_facts(self):
        self.product_scanner.get_host_facts = MagicMock(name='get_host_facts')
        self.assertEqual(
            self.product_scanner.site_facts(),
            {'167.81.183.99': self.product_scanner.get_host_facts.return_value}
        )

    def test_site_facts_without_ip_address(self):
        self.product_scanner.get_host_facts = MagicMock(name='get_host_facts')
        self.assertEqual(
            self.product_scanner.site_facts(),
            {'167.81.183.99': self.product_scanner.get_host_facts.return_value}
        )

    def test_site_facts_no_hosts(self):
        self.product_scanner.get_hosts = MagicMock(return_value=iter([]))
        self.assertEqual(self.product_scanner.site_facts(), {})

    def test_site_facts_without_facts(self):
        self.product_scanner.get_host_facts = MagicMock(name='get_host_facts', return_value=None)
        self.assertEqual(self.product_scanner.site_facts(), {})

    def test_endpoint(self):
        self.assertEqual(self.product_scanner.endpoint, {'scanner': 'scan1', 'address': '167.81.183.99'})


if __name__ == '__main__':
    unittest.main()
