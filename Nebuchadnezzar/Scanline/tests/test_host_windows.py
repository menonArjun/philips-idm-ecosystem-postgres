import unittest
from mock import MagicMock, patch, call, PropertyMock


class ScanlineHostWindowsTest(object):
    class TestCase(unittest.TestCase):
        def setUp(self):
            unittest.TestCase.setUp(self)
            self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
            self.mock_utilities = MagicMock()
            self.mock_component = MagicMock()
            modules = {
                'cached_property': self.mock_cached_property,
                'logging': MagicMock(name='logging'),
                'scanline.component.windows': self.mock_component.windows,
                'scanline.utilities.wmi': self.mock_utilities.wmi
            }
            self.module_patcher = patch.dict('sys.modules', modules)
            self.module_patcher.start()
            from scanline.host.windows import WindowsHostScanner
            self.WindowsHostScanner = WindowsHostScanner
            self.windows_host_scanner = self.WindowsHostScanner(
                'host1', '167.81.183.99', 'tester', 'CryptThis', 'isomething.net', tags=['x', 'w']
            )

        def tearDown(self):
            unittest.TestCase.tearDown(self)
            self.module_patcher.stop()


class ScanlineHostWindowsWMIDrillerTestCase(ScanlineHostWindowsTest.TestCase):
    def setUp(self):
        ScanlineHostWindowsTest.TestCase.setUp(self)
        self.address_patch = patch('scanline.host.windows.WindowsHostScanner.address', new_callable=PropertyMock)
        self.mock_address = self.address_patch.start()
        self.mock_address.return_value = '10.2.3.4'

    def tearDown(self):
        ScanlineHostWindowsTest.TestCase.tearDown(self)
        self.address_patch.stop()

    def test_get_wmi_driller_ok(self):
        self.assertEqual(
            self.windows_host_scanner.get_wmi_driller(),
            self.mock_utilities.wmi.WMIHostDriller.return_value
        )
        self.mock_utilities.wmi.WMIHostDriller.assert_called_once_with(
            host='10.2.3.4', password='CryptThis', user='tester'
        )

    def test_get_wmi_driller_no_user(self):
        self.windows_host_scanner.user = None
        self.assertEqual(self.windows_host_scanner.get_wmi_driller(), None)

    def test_get_wmi_driller_no_address(self):
        self.mock_address.return_value = None
        self.assertEqual(self.windows_host_scanner.get_wmi_driller(), None)

    def test_get_wmi_driller_no_domain_needed(self):
        self.mock_utilities.wmi.WMIHostDriller.return_value.get_manufacturer.side_effect = ['VM', None]
        result = self.windows_host_scanner.get_wmi_driller()
        self.assertEqual(result, self.mock_utilities.wmi.WMIHostDriller.return_value)
        self.assertEqual(result.domain, None)
        self.assertEqual(self.windows_host_scanner._flags, set())

    def test_get_wmi_driller_domain_needed(self):
        self.mock_utilities.wmi.WMIHostDriller.return_value.get_manufacturer.side_effect = [None, 'VM']
        result = self.windows_host_scanner.get_wmi_driller()
        self.assertEqual(result, self.mock_utilities.wmi.WMIHostDriller.return_value)
        self.assertEqual(result.domain, 'isomething.net')
        self.assertEqual(self.windows_host_scanner._flags, set(['domain_needed']))

    def test_get_wmi_driller_no_access(self):
        self.mock_utilities.wmi.WMIHostDriller.return_value.get_manufacturer.side_effect = [None, None]
        self.assertEqual(self.windows_host_scanner.get_wmi_driller(), None)
        self.assertEqual(self.windows_host_scanner._flags, set(['no_access']))


class ScanlineHostWindowsTestCase(ScanlineHostWindowsTest.TestCase):
    def setUp(self):
        ScanlineHostWindowsTest.TestCase.setUp(self)
        self.driller_patch = patch('scanline.host.windows.WindowsHostScanner.wmi_driller', new_callable=PropertyMock)
        self.mock_driller = self.driller_patch.start()

    def tearDown(self):
        ScanlineHostWindowsTest.TestCase.tearDown(self)
        self.driller_patch.stop()

    def test_get_from_driller_ok(self):
        self.assertEqual(
            self.windows_host_scanner.get_from_driller('method1'),
            self.mock_driller.return_value.method1.return_value
        )

    def test_get_from_driller_no_driller_no_default(self):
        self.mock_driller.return_value = None
        self.assertEqual(self.windows_host_scanner.get_from_driller('method1'), None)

    def test_get_from_driller_no_driller_default(self):
        self.mock_driller.return_value = None
        self.assertEqual(self.windows_host_scanner.get_from_driller('method1', {}), {})

    def test_windows_info(self):
        self.windows_host_scanner.get_from_driller = MagicMock(name='get_from_driller')
        self.assertEqual(
            self.windows_host_scanner.windows_info,
            self.windows_host_scanner.get_from_driller.return_value
        )
        self.windows_host_scanner.get_from_driller.assert_called_once_with('get_windows_info', {})

    def test_manufacturer(self):
        self.windows_host_scanner.get_from_driller = MagicMock(name='get_from_driller')
        self.assertEqual(
            self.windows_host_scanner.manufacturer,
            self.windows_host_scanner.get_from_driller.return_value
        )
        self.windows_host_scanner.get_from_driller.assert_called_once_with('get_manufacturer')

    def test_uuid(self):
        self.windows_host_scanner.get_from_driller = MagicMock(name='get_from_driller')
        self.assertEqual(
            self.windows_host_scanner.uuid,
            self.windows_host_scanner.get_from_driller.return_value
        )
        self.windows_host_scanner.get_from_driller.assert_called_once_with('get_uuid')

    def test_get_components(self):
        mock_driller = self.mock_driller.return_value
        self.assertEqual(
            self.windows_host_scanner.get_components(),
            {
                'MSMQ': self.mock_component.windows.WindowsServiceComponent.return_value,
                'MSSQLSERVER': self.mock_component.windows.WindowsServiceComponent.return_value,
                'W3SVC': self.mock_component.windows.WindowsServiceComponent.return_value
            }
        )
        self.mock_component.windows.WindowsServiceComponent.assert_has_calls(
            [
                call(mock_driller, 'MSMQ'),
                call(mock_driller, 'MSSQLSERVER'),
                call(mock_driller, 'W3SVC'),
            ]
        )

    def test_get_components_no_driller(self):
        self.mock_driller.return_value = None
        self.assertEqual(self.windows_host_scanner.get_components(), {})

    def test_properties_present(self):
        mock_scanner = MagicMock(spec=self.windows_host_scanner)
        for prop in self.WindowsHostScanner.module_properties:
            self.assertTrue(getattr(mock_scanner, prop))
        for prop in self.WindowsHostScanner.general_properties:
            self.assertTrue(getattr(mock_scanner, prop))


if __name__ == '__main__':
    unittest.main()
