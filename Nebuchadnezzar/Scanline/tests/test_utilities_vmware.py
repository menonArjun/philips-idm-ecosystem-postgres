import unittest
from mock import MagicMock, patch


class ScanlineVMWareTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_pysphere = MagicMock(name='pysphere')
        modules = {
            'logging': MagicMock(name='logging'),
            'pysphere': self.mock_pysphere,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.utilities import vmware
        self.vmware = vmware

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    @patch('pysphere.VIServer.connect')
    def test_vi_server(self, mock_connect):
        mock_connect.return_value = MagicMock()
        with self.vmware.create_vi_server('127.0.0.1', 'Foo', 'Bar') as server:
            self.assertTrue(server)


if __name__ == '__main__':
    unittest.main()
