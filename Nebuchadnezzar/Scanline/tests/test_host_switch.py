import unittest
from mock import MagicMock, patch


class ScanlineHostSwitchTest(object):
    class TestCase(unittest.TestCase):
        def setUp(self):
            self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
            modules = {
                'cached_property': self.mock_cached_property,
                'logging': MagicMock(name='logging'),
            }
            self.module_patcher = patch.dict('sys.modules', modules)
            self.module_patcher.start()

        def tearDown(self):
            unittest.TestCase.tearDown(self)
            self.module_patcher.stop()

        def test_properties_present(self):
            mock_scanner = MagicMock(spec=self.scanner_instance)
            for prop in self.scanner.module_properties:
                self.assertTrue(getattr(mock_scanner, prop))
            for prop in self.scanner.general_properties:
                self.assertTrue(getattr(mock_scanner, prop))


class ScanlineHostCiscoSwitchTestCase(ScanlineHostSwitchTest.TestCase):
    def setUp(self):
        ScanlineHostSwitchTest.TestCase.setUp(self)
        from scanline.host.switch import CiscoSwitchHostScanner
        self.scanner = CiscoSwitchHostScanner
        self.scanner_instance = self.scanner(
            'host1', '167.81.18.99', username='user', password='secret', tags=['x', 'y']
        )

    def tearDown(self):
        ScanlineHostSwitchTest.TestCase.tearDown(self)


class ScanlineHostHPSwitchTestCase(ScanlineHostSwitchTest.TestCase):
    def setUp(self):
        ScanlineHostSwitchTest.TestCase.setUp(self)
        from scanline.host.switch import HPSwitchHostScanner
        self.scanner = HPSwitchHostScanner
        self.scanner_instance = self.scanner(
            'host1', '167.81.18.99', 5500
        )

    def tearDown(self):
        ScanlineHostSwitchTest.TestCase.tearDown(self)


if __name__ == '__main__':
    unittest.main()
