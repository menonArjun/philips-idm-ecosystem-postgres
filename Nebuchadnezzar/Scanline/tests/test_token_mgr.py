import unittest
from mock import MagicMock, patch


class TokenTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        modules = {
            'cached_property': self.mock_cached_property,
            'logging': MagicMock(name='logging'),
            'hmac': MagicMock(name='hmac'),
            'base64': MagicMock(name='base64'),
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
	from scanline.utilities import token_mgr
	self.token_mgr = token_mgr


    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    # @patch('scanline.utilities.token_mgr.get_auth_header_str',
    #         return_value="HMAC Timestamp=2017-03-28T03:50:11.273808+00:00;Hash=O+fIubG/gC0omYwDY2i7I31OHcpWR96E1Q1wONf67og=;AppId=IDM")
    # @patch('scanline.utilities.token_mgr.get_iso_date', return_value="2017-03-28T03:50:11.273808+00:00")
    # def test_get_token_from_current_time(self, mock_get_iso_date, mock_get_auth_header_str):
    #     self.assertEquals(
    #         self.token_mgr.get_formatted_token(),
    #         'HMAC Timestamp=2017-03-28T03:50:11.273808+00:00;Hash=O+fIubG/gC0omYwDY2i7I31OHcpWR96E1Q1wONf67og=;AppId=IDM'
    #     )
    #     mock_get_iso_date.assert_called()
    #     mock_get_iso_date.assert_called_once_with()



if __name__ == '__main__':
    unittest.main()
