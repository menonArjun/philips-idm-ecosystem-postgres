from distutils.core import setup
import os

phiversion = os.environ['phiversion']
setup(name='scanline',
      version=phiversion,
      description='Philips Monitoring Discovery',
      author='IDM Ecosystem',
      author_email='idm@philips.com',
      # py_modules=['phimutils.timestamp'],
      packages=['scanline', 'scanline.component', 'scanline.host', 'scanline.product', 'scanline.utilities'],
      # These do not work with the RPM build
      requires=['pynag', 'requests', 'pysphere', 'paramiko', 'lxml']
      )
