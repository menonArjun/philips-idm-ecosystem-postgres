#!/usr/bin/env python
import json
from bottle import Bottle, request, BaseRequest

BaseRequest.MEMFILE_MAX = 1024 * 1024 * 2
app = Bottle()

import logging
import time
import requests
from phimutils.plogging import LOG_DATE_FORMAT, LOG_FORMAT
from phimutils.decider import Decider
from phimutils.heartbeat import send_message
from jinja2 import Environment, FileSystemLoader

from swconfig import (
    FROM_ADDRESS,
    SMTP_SERVER,
    SMTP_PORT,
    SMTP_USER,
    SMTP_PASSWORD,
    CONTACT_RULES_FILE,
    CONTACTS_CONFIG_FILE,
    NOTIFICATION_TEMPLATES,
    EMAIL_BODY_TEMPLATE,
    EMAIL_SUBJECT_TEMPLATE,
    SLACK_BODY_TEMPLATE
)

log = logging.getLogger('phim.shortway')
log.setLevel(logging.INFO)
logging.Formatter.converter = time.gmtime
formatter = logging.Formatter(fmt=LOG_FORMAT, datefmt=LOG_DATE_FORMAT)
handler_console = logging.StreamHandler()
handler_console.setFormatter(formatter)
log.addHandler(handler_console)

decider = Decider()
decider.load_from_file(CONTACT_RULES_FILE)

jinja_env = Environment(loader=FileSystemLoader(NOTIFICATION_TEMPLATES))
email_template = jinja_env.get_template(EMAIL_BODY_TEMPLATE)
subject_template = jinja_env.get_template(EMAIL_SUBJECT_TEMPLATE)
slack_template = jinja_env.get_template(SLACK_BODY_TEMPLATE)


def load_json_object_from_file(filename):
    try:
        with open(filename) as se_config:
            result = json.load(se_config)
    except (ValueError, IOError):
        result = {}
    return result


contacts = load_json_object_from_file(CONTACTS_CONFIG_FILE)


def send_email(address, message):
    try:
        send_message(message=email_template.render(**message),
                     smtpfrom=FROM_ADDRESS,
                     smtpto=address,
                     smtpserver=SMTP_SERVER,
                     subject=subject_template.render(**message),
                     user=SMTP_USER,
                     password=SMTP_PASSWORD,
                     port=SMTP_PORT)
    except Exception:
        log.error('Error sending email')
    else:
        log.debug('email successful')


def post_http(address, message):
    headers = {'content-type': 'application/json'}
    try:
        r = requests.post(address, data=json.dumps(message), headers=headers)
        r.raise_for_status()
    except requests.exceptions.RequestException:
        log.error('could not post due to request error')
    else:
        log.debug('http post successful')


def slack_http(address, message):
    headers = {'content-type': 'application/json'}
    contents = slack_template.render(**message)

    try:
        r = requests.post(address, data=json.dumps({"text": contents}), headers=headers)
        r.raise_for_status()
    except requests.exceptions.RequestException:
        log.error('could not post due to request error')
    else:
        log.debug('http post successful')


TYPE_MAP = {
    'email': send_email,
    'http': post_http,
    'slack': slack_http
}


def send_notification(contact, message):
    contact_detail = contacts.get(contact)
    if not contact_detail:
        log.error('Contact %s could not be found in config.', contact)
        return
    notification_type = contact_detail['action']
    address = contact_detail['address']
    types_to_contact = contact_detail.get('types')
    notification_function = TYPE_MAP.get(notification_type)
    if not notification_function:
        log.error('Could not find notification function for %s', notification_type)
        return
    if not address:
        log.error('Address not provided in %s', contact_detail)
        return
    if types_to_contact:
        if not (message['type'] in types_to_contact):
            log.debug('Message type %s for contact %s is not set to be sent', message['type'], contact)
            return
    log.debug('Notifying via function: %s to address: %s message: %s', notification_function, address, message)
    notification_function(address, message)


@app.post('/notification')
def notification():
    log.info('Notification! %s', request.path)
    if not request.json:
        log.error('no json')
        log.debug('%s', request.body.read())
        return
    log.debug('%s', request.json)
    notification_list = decider.decide(request.json['siteid'], request.json['service'])
    log.debug('contacts to notify %s', notification_list)
    for contact in notification_list:
        send_notification(contact, request.json)

if __name__ == '__main__':
    #run(host='0.0.0.0', port=8080, debug=True, server='tornado')
    log.info('MemFileMax set to %s', BaseRequest.MEMFILE_MAX)
    app.run()
