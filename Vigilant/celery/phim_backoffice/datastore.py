from __future__ import absolute_import

from celery.utils.log import get_logger

from taskbase.mongotask import create_mongo_task
from phim_backoffice.celery import app
from tbconfig import COLLECTIONS, MONGODB


mongodb_task = create_mongo_task(MONGODB['URL'], MONGODB['DB_NAME'])
retries = MONGODB['RETRIES']
retry_delay = MONGODB['RETRY_DELAY']

logger = get_logger(__name__)


@app.task(base=mongodb_task, bind=True, ignore_result=True, max_retries=retries, default_retry_delay=retry_delay)
def mongodb(self, collection, method, **kwargs):
    try:
        getattr(get_collection(collection), method)(**kwargs)
    except self.errors.AutoReconnect as exc:
        raise self.retry(exc=exc)


def get_collection(collection):
    return mongodb.db[COLLECTIONS[collection]]


def id_exists(collection, identifier):
    return bool(get_collection(collection).count({'_id': identifier}, limit=1))


def find_one(collection, filter_or_id, projection=None):
    return get_collection(collection).find_one(filter_or_id, projection=projection)


def find(collection, filter, projection=None):
    return get_collection(collection).find(filter, projection=projection)


def distinct(collection, field, query=None):
    return get_collection(collection).distinct(field, query=query)


def aggregate(collection, pipeline):
    return get_collection(collection).aggregate(pipeline)


def delete_one_by_id(collection, identifier):
    mongodb.delay(collection, 'delete_one', filter={'_id': identifier})


def delete_many(collection, filter):
    mongodb.delay(collection, 'delete_many', filter=filter)


def update_many(collection, filter, update):
    mongodb.delay(collection, 'update_many', filter=filter, update=update)


def update_one_by_id(collection, identifier, data=None, update=None, upsert=False):
    update = update if update else {'$set': data}
    mongodb.delay(collection, 'update_one', filter={
                  '_id': identifier}, update=update, upsert=upsert)


def upsert_one_by_id(collection, identifier, data=None, update=None):
    update_one_by_id(collection, identifier, data=data,
                     update=update, upsert=True)


def insert_one(collection, data):
    mongodb.delay(collection, 'insert_one', document=data)


def update_doc(collection, query_filter, update, upsert):
    mongodb.delay(collection, 'update_one', filter=query_filter,
                  update=update, upsert=upsert)
