from __future__ import absolute_import

from taskbase.elastictask import create_elastic_task
from phim_backoffice.celery import app
from phimutils.namespace import cleanup_key
from phimutils.timestamp import dt_fromiso
from celery.utils.log import get_logger

from tbconfig import PERFDATA, METRICSDATA


logger = get_logger(__name__)


@app.task(base=create_elastic_task(PERFDATA['ELASTICS_URLS']), ignore_result=True)
def push2elasticsearch(data, index=PERFDATA['ELASTIC_INDEX'], doc_type=PERFDATA['ELASTIC_TYPE']):
    push2elasticsearch.es.index(index=index, doc_type=doc_type, body=data)


@app.task(base=create_elastic_task(METRICSDATA['ELASTICS_URLS']), ignore_result=True)
def push_metrics_data(data, index=METRICSDATA['ELASTIC_INDEX'], doc_type=METRICSDATA['ELASTIC_TYPE']):
    push_metrics_data.es.index(index=index, doc_type=doc_type, body=data)


@app.task(base=create_elastic_task(PERFDATA['ELASTICS_URLS']), ignore_result=True)
def bulk_elasticsearch_insert(siteid, data, index=PERFDATA['ELASTIC_INDEX'], doc_type=PERFDATA['ELASTIC_TYPE']):
    denormalized_perfdata = get_perfdata_records(siteid, data)
    bulk_elasticsearch_insert.bulk(
        {'_index': index, '_type': doc_type, '_source': perfdata_record}
        for perfdata_record in denormalized_perfdata
    )


def get_perfdata_records(siteid, raw_perfdata):
    for entry in raw_perfdata:
        try:
            timestamp = dt_fromiso(entry['timestamp'])
            hostname = entry['hostname']
            service = entry['service']
            perfdata = entry['perfdata']
        except KeyError:
            continue

        for perfdatum in perfdata:
            denormalized_perfdata = get_record(siteid, timestamp, hostname, service, perfdatum)
            if denormalized_perfdata:
                yield denormalized_perfdata


def get_record(siteid, timestamp, hostname, service, perfdata):
    try:
        label = cleanup_key(perfdata.pop('label'))
    except KeyError:
        return
    result = dict(siteid=siteid, timestamp=timestamp, hostname=hostname, service=service, label=label)
    result.update(perfdata)
    return result
