from __future__ import absolute_import

from datetime import datetime, timedelta
from functools import wraps

from celery.utils.log import get_logger

import phim_backoffice.event
import phim_backoffice.fact
import phim_backoffice.audit
import phim_backoffice.transport
from phim_backoffice.celery import app
from phim_backoffice.datastore import upsert_one_by_id
from phim_backoffice.perfdata import bulk_elasticsearch_insert, push_metrics_data
from phim_backoffice.state import update_state
from tbconfig import ALIVE, IN_TASK_MAP
from phim_backoffice.billing.statistics import billing_task

logger = get_logger(__name__)


def handle_no_data(func):
    @wraps(func)
    def no_dater(data):
        if not data:
            logger.warning('no data')
            return
        return func(data)
    return no_dater


@app.task(ignore_result=True)
@handle_no_data
def do_notification(data):
    if data.type == 'ALIVE':
        do_alive.delay(data)
    else:
        phim_backoffice.event.handle_event.delay(data)


@app.task(ignore_result=True)
@handle_no_data
def do_alive(data):
    alive_expiration = datetime.utcnow() + timedelta(minutes=ALIVE['CHECK_GAP'])
    alive_info = {
        'alive_expiration': alive_expiration,
        'hostname': getattr(data, 'hostname', ALIVE['DEFAULT_HOSTNAME'])
    }
    upsert_one_by_id('ALIVES', data.siteid, alive_info)
    expiration_check_time = alive_expiration + timedelta(minutes=1)
    phim_backoffice.event.stale_alive.apply_async((data.siteid,), eta=expiration_check_time)


@app.task(ignore_result=True)
@handle_no_data
def do_perfdata(data):
    bulk_elasticsearch_insert.delay(data.siteid, data.payload)


@app.task(ignore_result=True)
@handle_no_data
def do_metricsdata(data):
    push_metrics_data.delay(data)


@app.task(ignore_result=True)
@handle_no_data
def do_state(data):
    update_state.delay(data.siteid, data.payload)


@app.task(ignore_result=True)
@handle_no_data
def do_discovery(data):
    phim_backoffice.fact.discovery_collect.delay(data.siteid, **data.payload)
    phim_backoffice.audit.update_package_status.delay(data.siteid, **data.payload)


@app.task(ignore_results=True)
@handle_no_data
def hbmessage2hbdashboard(data):
    phim_backoffice.transport.send_site_email_msg.delay(data.siteid, data.payload['message'])


@app.task(ignore_result=True)
def inbound(resource, action, payload):
    app.send_task(IN_TASK_MAP[resource][action], [], kwargs=payload)


@app.task(ignore_result=True)
def deployment_status(data):
    phim_backoffice.audit.deployment_status.delay(**data.payload)


@app.task(ignore_result=True)
@handle_no_data
def isite_billing(data):
    import json
    billing_task.delay(json.loads(data))
