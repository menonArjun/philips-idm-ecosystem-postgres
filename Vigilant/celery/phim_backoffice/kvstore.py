from __future__ import absolute_import

from celery.utils.log import get_logger

from taskbase.redistask import create_redis_task
from phim_backoffice.celery import app
from tbconfig import REDIS


redis_task = create_redis_task(REDIS['URL'])

logger = get_logger(__name__)


@app.task(base=redis_task, ignore_result=True)
def redis(collection, method, **kwargs):
    client = redis.rc # only for other functions to use the connection pool


def get_client():
    return redis.rc


def add_to_set(set_name, *values):
    if values:
        get_client().sadd(set_name, *values)


def remove_from_set(set_name, *values):
    if values:
        get_client().srem(set_name, *values)


def is_member_of_set(set_name, value):
    return get_client().sismember(set_name, value)


def get_members_of_set(set_name):
    return get_client().smembers(set_name)


def delete_key(key):
    get_client().delete(key)
