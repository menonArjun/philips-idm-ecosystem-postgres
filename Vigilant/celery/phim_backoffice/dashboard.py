from __future__ import absolute_import

from phim_backoffice.celery import app
from phim_backoffice.datastore import (
    delete_one_by_id,
    update_one_by_id,
    upsert_one_by_id,
    id_exists,
    find_one,
    distinct,
    update_many,
    aggregate
)
from phim_backoffice.kvstore import (add_to_set,
                                     is_member_of_set,
                                     delete_key,
                                     remove_from_set,
                                     get_members_of_set)
from celery.utils.log import get_logger
from tbconfig import DASHBOARD


UNREACHABLE_KEYS = [DASHBOARD['UNREACHABLE_SITE_KEY'], DASHBOARD['UNREACHABLE_HOST_KEY']]


logger = get_logger(__name__)


def get_identifier(data):
    return '{siteid}-{hostname}-{service}'.format(**data)


@app.task(ignore_result=True)
def handle_dashboard(data):
    if data['type'] == 'RECOVERY':
        handle_recovery(data)
    elif data['type'] == 'PROBLEM':
        handle_problem(data)


def handle_recovery(data):
    identifier = get_identifier(data)
    logger.debug('Dashboard Handling RECOVERY for ID %s', identifier)
    delete_one_by_id('DASHBOARD', identifier)
    remove_from_problems(data['siteid'], data['hostname'], data['service'])


def handle_problem(data):
    identifier = get_identifier(data)
    logger.debug('Dashboard Handling PROBLEM for ID %s', identifier)
    add_to_problems(data['siteid'], data['hostname'], data['service'])
    document = get_dashboard_document(identifier, data)
    logger.debug('Inserting document -- %s -- with ID %s', document, identifier)
    upsert_one_by_id('DASHBOARD', identifier, document)


def guess_component_version(siteid, component):
    records = aggregate(
        'FACTS',
        [
            {'$match': {'Components.name': component, 'siteid': siteid}},
            {'$limit': 1},
            {'$unwind': '$Components'},
            {'$match': {'Components.name': component}},
            {'$sort': {'Components.timestamp': -1 }},
            {'$project': {'_id': 0, 'version': '$Components.version'}}
        ]
    )
    record = next(records, {})
    return record.get('version')


def get_product_version(siteid, hostname):
    records = aggregate(
        'FACTS',
        [
            {'$match': {'_id': siteid + '-' + hostname}},
            {'$project': {'_id': 0, 'version': '$product_version'}}
        ]
    )
    record = next(records, {})
    return record.get('version')


def get_site_info(siteid, hostname):
    result = notifying_get_data('site', 'SITES', siteid, {'name': DASHBOARD['DEFAULT_SITENAME']})
    # Type will be dynamic after product identification
    component = 'IntelliSpace PACS'
    pacs_version = guess_component_version(siteid, component)\
                   or get_product_version(siteid, hostname)
    if pacs_version:
        result['pacs_version'] = pacs_version
    return result


def get_dashboard_document(identifier, data):
    result = data.copy()
    result['site'] = get_site_info(data['siteid'], data['hostname'])
    if not id_exists('DASHBOARD', identifier):
        result['initial_timestamp'] = data.get('timestamp')
    if is_unreachable_service(data['siteid'], data['hostname'], data['service']):
        result['unreachable'] = True
    return result


def create_exception(exception_type, data):
    upsert_one_by_id('EXCEPTIONS', data, {exception_type: data})


def notifying_get_data(exception_type, collection, identifier, default=None):
    result = find_one(collection, identifier, {'_id': 0})
    if not result:
        logger.info('Data for %s with ID %s not found. Creating exception.', exception_type, identifier)
        create_exception(exception_type, identifier)
        result = default
    return result


def get_unreachable_hosts_key(siteid):
    return '{prefix}-{siteid}'.format(prefix=DASHBOARD['UNREACHABLE_HOSTS'], siteid=siteid)


def is_problem_site(siteid):
    return is_member_of_set(DASHBOARD['PROBLEM_SITES'], siteid)


def is_unreachable_service(siteid, hostname, service):
    if service in UNREACHABLE_KEYS:
        return False
    return (
        is_member_of_set(DASHBOARD['UNREACHABLE_SITES'], siteid)
        or
        is_member_of_set(get_unreachable_hosts_key(siteid), hostname)
    )


def set_services_unreachable(siteid, **kwargs):
    update_many('DASHBOARD', dict(siteid=siteid, **kwargs), {'$set': {'unreachable': True}})


def set_host_services_unreachable(siteid, hostname):
    set_services_unreachable(siteid=siteid, service={'$nin': UNREACHABLE_KEYS}, hostname=hostname)


def set_site_services_unreachable(siteid):
    set_services_unreachable(siteid=siteid, service={'$ne': DASHBOARD['UNREACHABLE_SITE_KEY']})


def set_services_reachable(siteid, **kwargs):
    update_many('DASHBOARD', dict(siteid=siteid, **kwargs), {'$unset': {'unreachable': None}})


def set_host_services_reachable(siteid, hostname):
    set_services_reachable(siteid=siteid, hostname=hostname)


def set_site_services_reachable(siteid):
    set_services_reachable(siteid=siteid, service=DASHBOARD['UNREACHABLE_HOST_KEY'])
    unreachable_hosts = list(get_members_of_set(get_unreachable_hosts_key(siteid)))
    set_services_reachable(siteid=siteid, hostname={'$nin': unreachable_hosts})


def rebuild_problem_sites():
    delete_key(DASHBOARD['PROBLEM_SITES'])
    sites = distinct('DASHBOARD', 'siteid')
    if sites:
        add_to_set(DASHBOARD['PROBLEM_SITES'], *sites)


def add_to_problems(siteid, hostname, service):
    add_to_set(DASHBOARD['PROBLEM_SITES'], siteid)
    if service == DASHBOARD['UNREACHABLE_SITE_KEY']:
        logger.info('Unreachable siteid %s received, marking dependant services', siteid)
        add_to_set(DASHBOARD['UNREACHABLE_SITES'], siteid)
        set_site_services_unreachable(siteid)
    elif service == DASHBOARD['UNREACHABLE_HOST_KEY']:
        logger.info('Unreachable host %s at siteid %s received, marking dependant services', hostname, siteid)
        add_to_set(get_unreachable_hosts_key(siteid), hostname)
        set_host_services_unreachable(siteid, hostname)


def remove_from_problems(siteid, hostname, service):
    rebuild_problem_sites()
    if service == DASHBOARD['UNREACHABLE_SITE_KEY']:
        logger.info('Reachable siteid %s received, unmarking dependant services', siteid)
        remove_from_set(DASHBOARD['UNREACHABLE_SITES'], siteid)
        set_site_services_reachable(siteid)
    elif service == DASHBOARD['UNREACHABLE_HOST_KEY']:
        logger.info('Reachable host %s at siteid %s received, unmarking dependant services', hostname, siteid)
        remove_from_set(get_unreachable_hosts_key(siteid), hostname)
        set_host_services_reachable(siteid, hostname)


def get_problem_services_by_host(siteid):
    return aggregate(
        'DASHBOARD',
        [
            {'$match': {'siteid': siteid}},
            {'$group': {'_id': '$hostname', 'services': {'$push': '$service'}}},
            {'$project': {'_id': 0, 'hostname': '$_id', 'services': 1}}
        ]
    )


@app.task(ignore_result=True)
def update_case_number(siteid, hostname, service, case_number):
    identifier = get_identifier(locals())
    update_one_by_id('DASHBOARD', identifier, {'case_number': case_number})
