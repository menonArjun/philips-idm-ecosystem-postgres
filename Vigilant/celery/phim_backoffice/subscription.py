from __future__ import absolute_import


from phim_backoffice.datastore import upsert_one_by_id, update_one_by_id
from phim_backoffice.helpers import get_validated_fields
from celery.utils.log import get_logger
from phim_backoffice.celery import app
from tbconfig import SUBSCRIPTION


logger = get_logger(__name__)


SUBSCRIPTION_FIELDS = frozenset(['path', 'countries', 'rules'])


@app.task(ignore_result=True)
def create_subscription(subscription, **kwargs):
    data = get_validated_fields(SUBSCRIPTION_FIELDS, **kwargs)
    if 'path' not in data:
        data['path'] = '/'
    if 'rules' not in data:
        data['rules'] = SUBSCRIPTION['DEFAULT_RULES']
    logger.debug('Creating subscription %s data: %s', subscription, data)
    upsert_one_by_id('SUBSCRIPTIONS', subscription, data)


@app.task(ignore_result=True)
def update_subscription(subscription, **kwargs):
    data = get_validated_fields(SUBSCRIPTION_FIELDS, **kwargs)
    logger.debug('Updating subscription %s data: %s', subscription, data)
    update_one_by_id('SUBSCRIPTIONS', subscription, data)
