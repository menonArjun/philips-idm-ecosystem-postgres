from __future__ import absolute_import

from celery.utils.log import get_logger
from datetime import datetime

from taskbase.elastictask import create_elastic_task
from elasticsearch import ElasticsearchException
from phim_backoffice.celery import app
from phim_backoffice.dashboard import handle_dashboard
from phim_backoffice.datastore import find_one, upsert_one_by_id, insert_one
from phimutils.message import Message
from phimutils.namespace import ns2dict
from tbconfig import ALIVE, EVENTDATA


logger = get_logger(__name__)

@app.task(ignore_result=True)
def stale_alive(siteid):
    stored_alive = find_one('ALIVES', siteid)
    if not stored_alive:
        return

    current_time = datetime.utcnow()
    if stored_alive['alive_expiration'] < current_time:
        logger.info('Site alive expired siteid %s', siteid)
        state = 'CRITICAL'
        type = 'PROBLEM'
    else:
        state = 'OK'
        type = 'RECOVERY'

    last_state = stored_alive.get('last_state', 'OK')
    if last_state == state:
        logger.debug('State already present siteid', siteid)
        return

    upsert_one_by_id('ALIVES', siteid, {'last_state': state})
    hostname = stored_alive.get('hostname', ALIVE['DEFAULT_HOSTNAME'])
    send_event(siteid=siteid, hostname=hostname, type=type, hostaddress=ALIVE['ADDRESS'], service=ALIVE['SERVICE'],
               state=state, datetime=current_time)


def send_event(siteid, hostname, service, type, state, datetime=None, hostaddress=None, payload=None):
    msg = Message(
        siteid=siteid,
        hostname=hostname,
        service=service,
        type=type,
        state=state,
        datetime=datetime,
        hostaddress=hostaddress,
        payload=payload
    )
    handle_event.delay(msg)

@app.task(ignore_results=True)
def handle_event(data):
    event_data = data.to_dict()
    payload = event_data.pop('payload', {})
    if isinstance(payload, basestring):
        # for backwards compatibility. To be removed after all nodes are sending messages in new format.
        event_data['output'] = payload
        event_data['components'] = []
        event_data['modules'] = []
    else:
        event_data['output'] = payload.get('output', '')
        event_data['components'] = payload.get('components', [])
        event_data['modules'] = payload.get('modules', [])
    event_data['timestamp'] = data.datetime
    event_data['namespace'] = ns2dict(data.service)
    identifier = '{siteid}-{hostname}'.format(**event_data)
    product_id = get_productid(identifier)
    event_data['product_id'] = product_id
    logger.debug('Handling event data %s', event_data)
    if EVENTDATA['PUSH_ENABLE']:
        push_events_consumer('ELASTIC', event_data)
    handle_dashboard.delay(event_data)
    insert_one('EVENTS', event_data)


def get_productid(identifier):
    fact = find_one('FACTS', {'_id': identifier})
    if fact:
        return fact.pop('product_id',None)

@app.task(base=create_elastic_task(EVENTDATA['ELASTICS_URLS']), ignore_result=True)
def push_event_elasticsearch(data, index=EVENTDATA['ELASTIC_INDEX'], doc_type=EVENTDATA['ELASTIC_TYPE']):
    try:
        timestamp = data.get('timestamp', datetime.utcnow())
        service_type = data.pop('service', {})
        data['siteid-hostname'] = data['siteid'] + '-' + data['hostname']
        push_event_elasticsearch.es.index(index=timestamp.strftime(index + '-' + '%Y.%m.%d'), doc_type=service_type, body=data)
    except ElasticsearchException:
        logger.error("push_event_elasticsearch - index error")
    except Exception:
        logger.error("push_event_elasticsearch - event preparation error")

events_consumers = {
    'ELASTIC': push_event_elasticsearch
}

def push_events_consumer (consumer='ELASTIC', data={}):
    events_consumers[consumer].delay(data)
