from __future__ import absolute_import

import os
import csv

from datetime import datetime
from dateutil.parser import parse

from celery.utils.log import get_logger
from celery.exceptions import SoftTimeLimitExceeded

from phim_backoffice.celery import app
from phim_backoffice.billing.s3_wrapper import S3Wrapper
from phim_backoffice.billing.task import BillingTask
from tbconfig import BILLING
from phim_backoffice.datastore import update_doc


logger = get_logger(__name__)


_CSVFILE_PATH = '/tmp/{fname}'


def _file_path(file_name):
    return _CSVFILE_PATH.format(fname=file_name)


def _create_csv(studies, ab_file_path, site_id, headers):
    with open(ab_file_path, 'w') as file_obj:
        csv_writer = csv.writer(file_obj)
        # Writing the header in to csv file
        csv_writer.writerow(headers)
        for study in studies:
            value_list = [site_id]
            value_list.extend(study)
            try:
                csv_writer.writerow(value_list)
            except UnicodeEncodeError:
                encoded_list = [item.encode('utf-8') if isinstance(item, unicode)
                                else item for item in value_list]
                csv_writer.writerow(encoded_list)


def _mongo_filter(key, value):
    return {'query_filter': {key: value}}


def _mongo_set_on_insert(key, value):
    return {'$setOnInsert': {key: value}}


def _mongo_set(key, value):
    return {'$set': {key: value}}


def _mongo_push(key, value):
    return {'$push': {key: value}}


def _iso_to_datetime(iso_str):
    # iso_str - datetime objects in ISO format
    return parse(iso_str)


def _get_upload_status(file_name, upload_status, billing_datetime, upload_size=None):
    # @ upload_status - boolean
    #          :True - uploaded, False - Not uploaded
    # @ upload_size - type int, size of data uploaded to s3
    # @ billing_datetime - billing processed datetime
    status = {'file_name': file_name, 'upload_status': upload_status,
              'timestamp': datetime.utcnow(), 'billing_datetime': _iso_to_datetime(billing_datetime)}
    if upload_size is not None:
        status['uploaded_bytes'] = upload_size
    return status


def _status_history_entry(status):
    # status_history field  is an array with history of statuses
    # and only 60 recent status history will be maintained
    # in the array sorted by timestamp
    status_history = {'$each': [status],
                      '$sort': {'billing_datetime': 1}, '$slice': -60}
    return status_history


def get_query_stmt(site_id, file_name, upload_status, billing_datetime, upload_size=None):
    query_stmt = _mongo_filter('site_id', site_id)
    update_stmt = _mongo_set_on_insert('creation_time', datetime.utcnow())
    _upload_status = _get_upload_status(
        file_name, upload_status, billing_datetime, upload_size)
    update_stmt.update(
        _mongo_push('status_history', _status_history_entry(_upload_status)))
    if upload_status:  # set recent_upload only when upload
        update_stmt.update(_mongo_set('recent_upload', _upload_status))
    query_stmt['update'] = update_stmt
    query_stmt['upsert'] = True
    return query_stmt


@app.task(base=BillingTask, bind=True, ignore_result=True, soft_time_limit=590, time_limit=600)
def billing_task(self, data):
    try:
        file_name = data["file_name"]
        file_path = _file_path(file_name)
        logger.info(
            "Billing : Writing iSite Billing studies to - %s file", file_name)
        _create_csv(data['studies'], file_path,
                    data['siteid'], data['csv_headers'])
        update_doc(
            'BILLING', **get_query_stmt(data['siteid'], file_name, False, data['billing_datetime']))
        logger.info("Billing : Uploading %s file to s3 ", file_name)
        s3_obj = S3Wrapper(BILLING['ACCESS_KEY'],
                           BILLING['SECRET_KEY'], BILLING['BUCKET'])
        _uploaded_size = s3_obj.upload(
            file_name, file_path, BILLING['S3_FOLDER'])
        os.remove(file_path)
        self.uploaded_mb = (_uploaded_size or 0) / 1048576.00
        logger.info("Billing : Uploaded the file - %s  to S3, Size - %sMB",
                    file_name, str(self.uploaded_mb))
        _upload_status = True if _uploaded_size > 0 else False
        update_doc(
            'BILLING', **get_query_stmt(data['siteid'], file_name, _upload_status, data['billing_datetime'], _uploaded_size))
    except SoftTimeLimitExceeded as e:
        if os.path.exists(file_path or ''):
            os.remove(file_path)
        logger.error('Billing : Time Limit Exceeded, while uploading the file {file}'.format(
            file=file_name or ''))
        raise e
    except Exception as exc:
        logger.error('Billing : {err}, while uploading the file {file}, Retrying...'.format(
            err=exc, file=file_name or ''))
        self.retry(countdown=60, exc=exc, max_retries=3)
