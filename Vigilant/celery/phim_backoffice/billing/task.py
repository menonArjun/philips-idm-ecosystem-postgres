from celery import Task

import phim_backoffice.event as event
from phim_backoffice.billing.event import event_message


class BillingTask(Task):

    def submit_event(self, data):
        event.handle_event.delay(data)

    def on_success(self, retval, task_id, args, kwargs):
        payload = {'output': 'Upload to S3 Succeeded, size {0:.4f}MB'.format(
            self.uploaded_mb)}
        self.submit_event(
            event_message(args[0]['siteid'], state='OK',
                          notification_type='RECOVERY', payload=payload))
        super(BillingTask, self).on_success(retval, task_id, args, kwargs)

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        payload = {'output': str(exc)}
        self.submit_event(
            event_message(args[0]['siteid'], state='CRITICAL',
                          notification_type='PROBLEM', payload=payload))
        super(BillingTask, self).on_failure(exc, task_id, args, kwargs, einfo)
