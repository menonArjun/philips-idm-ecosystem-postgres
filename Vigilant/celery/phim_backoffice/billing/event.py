import socket

from phimutils.message import Message


HOST_NAME = socket.gethostname()
HOST_ADDRESS = socket.gethostbyname(HOST_NAME)
SERVICE = 'Administrative__Philips__ISPACSBilling__Upload__Status'


def event_message(siteid, notification_type, state, payload):
    return Message(
        siteid=siteid,
        hostname=HOST_NAME,
        hostaddress=HOST_ADDRESS,
        service=SERVICE,
        type=notification_type,
        state=state,
        payload=payload,
    )
