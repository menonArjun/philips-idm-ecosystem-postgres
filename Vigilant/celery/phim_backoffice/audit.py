from __future__ import absolute_import


from phim_backoffice.datastore import insert_one, find_one, update_one_by_id
from phim_backoffice.helpers import get_validated_fields
from celery.utils.log import get_logger
from phim_backoffice.celery import app
from phim_backoffice.persist import poperator_del
from datetime import datetime


logger = get_logger(__name__)

DISTRIBUTION_FIELDS = frozenset(['subscription', 'siteid', 'status', 'user', 'version'])
DEPLOYMENT_FIELDS = frozenset(['package', 'siteid', 'hosts', 'status', 'user', 'version', 'deployment_id', 'output'])
DEPLOYMENT_UPDATE_FIELDS = frozenset(['status', 'deployment_id', 'output'])


def insert_dated_audit(type, data):
    data['type'] = type
    data['timestamp'] = datetime.utcnow()
    insert_one('AUDIT', data)


@app.task(ignore_result=True)
def insert_distribution(**kwargs):
    data = get_validated_fields(DISTRIBUTION_FIELDS, **kwargs)
    logger.debug('Inserting distribution audit log data: %s', data)
    insert_dated_audit('distribution', data)


@app.task(ignore_result=True)
def insert_deployment(**kwargs):
    data = get_validated_fields(DEPLOYMENT_FIELDS, **kwargs)
    logger.debug('Inserting deployment audit log data: %s', data)
    insert_dated_audit('deployment', data)


@app.task(ignore_result=True)
def update_package_status(siteid, facts, endpoint=None):
    host_components = facts.get('localhost') if facts.get('localhost') else {}
    pcm_components = host_components.get('PCM', [])
    for component in pcm_components:
        results = find_one('AUDIT', {'type': 'distribution',
                                     'subscription': component.get('name'),
                                     'version': component.get('version'),
                                     'siteid': siteid,
                                     'status': 'Available'})

        if not results:
            data = {
                'subscription': component.get('name'),
                'siteid': siteid,
                'status': 'Available',
                'user': 'IDM Portal',
                'version': component.get('version')
            }
            insert_distribution(**data)


@app.task(ignore_result=True)
def deployment_status(**kwargs):
    data = get_validated_fields(DEPLOYMENT_UPDATE_FIELDS, **kwargs)
    logger.debug('Updating deployment status in audit log data: %s', data)
    results = find_one('AUDIT', {'type': 'deployment', 'deployment_id': data.get('deployment_id')})
    if results:
        for field in ['timestamp', 'type', '_id']:
            results.pop(field)
        results.update(data)
        insert_deployment(**results)
        if results.get('status') == 'Success':
            poperator_del(results.get('siteid'), 'PCM_Manifest', data)
