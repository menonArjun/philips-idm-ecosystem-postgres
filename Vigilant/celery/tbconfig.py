from tchelp import load_config


## RabbitMQ EXCHANGES
ANALYTICS_CLINICAL_EXCHANGE = 'phim.analytics.clinicaldata.inbound'
ANALYTICS_DICOM_EXCHANGE = 'phim.analytics.dicomdata.inbound'

## RabbitMQ QUEUES
ANALYTICS_CLINICAL_QUEUE = 'phim.analytics.clinicaldata.inbound'
ANALYTICS_DICOM_QUEUE = 'phim.analytics.dicomdata.inbound'

EXCHANGE_QUEUE_MAP = {
    ANALYTICS_CLINICAL_EXCHANGE: [ANALYTICS_CLINICAL_QUEUE],
    ANALYTICS_DICOM_EXCHANGE: [ANALYTICS_DICOM_QUEUE],
}

## QUEUE TRANSPORT
QUEUE_TRANSPORT_MAP = {
    'phim.analytics.clinicaldata.outbound': ANALYTICS_CLINICAL_EXCHANGE,
    'phim.analytics.dicomdata.outbound': ANALYTICS_DICOM_EXCHANGE,
}

## Poperator Type Class Map
POPERATOR_TYPE_CLASS_MAP = {
    'Nagios': {
        'module': 'poperator.configgenNagios',
        'class': 'ConfiggenNagios'
    },
    'NagiosNull': {
        'module': 'poperator.configgenNagiosNull',
        'class': 'ConfiggenNagiosNull'
    },
    'SWD': {
        'module': 'poperator.configgenSWD',
        'class': 'ConfiggenSWD'
    },
    'PCM_Manifest': {
        'module': 'poperator.configgenPCM',
        'class': 'ConfiggenPCM'
    },
    'SiteInfo': {
        'module': 'poperator.configgenSiteInfo',
        'class': 'ConfiggenSiteInfo'
    }
}

IN_TASK_MAP = {
    'softwaredistribution': {
        'subscriptions': 'phim_backoffice.persist.swd_subscribe'
    },
    'softwaredeployment': {
        'manifest': 'phim_backoffice.fact.pcm_manifest_collect'
    },
    'descriptor': {
        'site': 'phim_backoffice.descriptor.update_site_descriptors'
    },
    'dashboard': {
        'case_number': 'phim_backoffice.dashboard.update_case_number',
    },
    'subscription': {
        'create': 'phim_backoffice.subscription.create_subscription',
        'update': 'phim_backoffice.subscription.update_subscription'
    },
    'audit': {
        'distribution': 'phim_backoffice.audit.insert_distribution',
        'deployment': 'phim_backoffice.audit.insert_deployment'
    }
}

ALIVE_SERVICE = 'Product__IDM__Nebuchadnezzar__Alive__Status'

DEFAULT = {
    'RABBITMQ': {'URL': 'amqp://sb.phim.isyntax.net'},
    'REDIS': {
        #'URL': 'redis://username:psswrd@localhost:6379/0'
        'URL': 'redis://redis.phim.isyntax.net:6379'
    },
    ## ALIVE message configuration
    'ALIVE': {
        'SERVICE': ALIVE_SERVICE,
        'DEFAULT_HOSTNAME': 'localhost',
        'ADDRESS': '127.0.0.1',
        'CHECK_GAP': 6
    },
    ## MONGODB / Collections
    'MONGODB': {
        'URL': 'mongodb://mongo.phim.isyntax.net:27017/',
        'RETRIES': 2,
        'RETRY_DELAY': 10,
        'DB_NAME': 'somedb'
    },
    'COLLECTIONS': {
        'ALIVES': 'alives',
        'AUDIT': 'audit',
        'DASHBOARD': 'dashboard',
        'EVENTS': 'events',
        'EXCEPTIONS': 'exceptions',
        'FACTS': 'facts',
        'NAMESPACES': 'namespaces',
        'SITES': 'sites',
        'STATES': 'states',
        'SUBSCRIPTIONS': 'subscriptions',
        'BILLING': 'billing',
    },
    'FACT_MODULES': ['ISP', 'LN', 'SWD', 'Windows', 'Components', 'PCM', 'vCenter'],
    'DASHBOARD': {
        'DEFAULT_SITENAME': 'new_site',
        'PROBLEM_SITES': 'problem_sites',
        'UNREACHABLE_SITES': 'unreachable_sites',
        'UNREACHABLE_SITE_KEY': ALIVE_SERVICE,
        'UNREACHABLE_HOSTS': 'unreachable_hosts',
        'UNREACHABLE_HOST_KEY': 'Administrative__Philips__Host__Reachability__Status',
    },
    'STATE': {
        'HOSTADDRESS_KEY': 'Administrative__Philips__Host__Information__IPAddress'
    },
    'SUBSCRIPTION': {
        'DEFAULT_RULES': ['singlesitedeployment']
    },
    ## IBC (InstallBase Config)
    'IBC': {
        'SVN_URL': 'http://sc.phim.isyntax.net/svn/ibc/sites/',
        'SVN_USERNAME': 'operator',
        'SVN_PASSWORD': 'st3nt0r',
        'WORKING_DIRECTORY': '/var/philips/operator'
    },
    'POPERATOR_NAGIOS_DELAY_AFTER_COLLECTION': 2 * 60,
    ## ELASTICSEARCH
    # perf is an alias pointing to latest rolling index
    'PERFDATA': {
        'ELASTIC_INDEX': 'perf',
        'ELASTIC_TYPE': 'perfdata',
        'ELASTICS_URLS': ['el.phim.isyntax.net:9200']
    },
    'METRICSDATA': {
                'ELASTIC_INDEX': 'metrics',
                'ELASTIC_TYPE': 'metricsdata',
                'ELASTICS_URLS': ['el.phim.isyntax.net:9200']
    },
    'EVENTDATA': {
        'ELASTIC_INDEX': 'event',
        'ELASTIC_TYPE': 'eventdata',
        'ELASTICS_URLS': ['el.phim.isyntax.net:9200'],
        'PUSH_ENABLE': True
    },
    ## HeartBeat
    # SITE_EMAIL_MAP_FILE contains a single JSON object mapping siteid to email address
    'HB': {
        'TO_EMAIL': 'hbeat@stentor.com',
        'FROM_EMAIL': 'isyntaxserver@stentor.com',
        'SMTP_SERVER': 'smtp.phim.isyntax.net',
        'CODE_MAP_FILE': '/etc/philips/hb_code_map.conf',
        'SITE_EMAIL_MAP_FILE': '/etc/philips/emailmap.conf'
    },
    'BILLING': {
        'ACCESS_KEY': 'AKIAJZTXY6O6MCXBOO5SQ',
        'SECRET_KEY': 't435RkTU56S+nL4QbHjyL8Mr+xKSPIblx7q02Sc8w',
        'BUCKET': 'itgs-hiss-reporting-prod',
        'S3_FOLDER': 'hiss/ei-idm/dropin/'  # In S3 bucket, files will go inside this folder.

    }
}

TBCONFIG_FILE = '/etc/philips/task/tbconfig.yml'


globals().update(load_config(DEFAULT, TBCONFIG_FILE))
