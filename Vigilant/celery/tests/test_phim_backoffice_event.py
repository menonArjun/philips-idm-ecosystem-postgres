import unittest
from mock import MagicMock, patch, DEFAULT
from elasticsearch import ElasticsearchException

# Parameterized decorator to mock celery.task
# It returns the function undecorated
def fakeorator_arg(*args, **kwargs):
    def fakeorator(func):
        return func
    return fakeorator


class EventTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tbconfig = MagicMock(name='tbconfig')
        self.mock_taskbase = MagicMock(name='taskbase')
        self.mock_pb = MagicMock(name='phim_backoffice')
        self.mock_pb.celery.app.task = fakeorator_arg
        self.mock_celery = MagicMock(name='celery')
        self.mock_phimutils = MagicMock(name='phimutils')
        modules = {
            'tbconfig': self.mock_tbconfig,
            'phim_backoffice.datastore': self.mock_pb.datastore,
            'phim_backoffice.dashboard': self.mock_pb.dashboard,
            'taskbase': self.mock_taskbase,
            'taskbase.elastictask': self.mock_taskbase.elastictask,
            'phim_backoffice.celery': self.mock_pb.celery,
            'celery': self.mock_celery,
            'celery.utils': self.mock_celery.utils,
            'celery.utils.log': self.mock_celery.utils.log,
            'phimutils': self.mock_phimutils,
            'phimutils.message': self.mock_phimutils.message,
            'phimutils.namespace': self.mock_phimutils.namespace
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import phim_backoffice.event
        self.event = phim_backoffice.event
        self.event.ALIVE = {'SERVICE': 'servicename1', 'ADDRESS': '10.3.4.5', 'DEFAULT_HOSTNAME': 'host1'}
        self.event.datetime = MagicMock(name='datetime')
        self.datastore_patch = patch.multiple(
            'phim_backoffice.event',
            find_one=DEFAULT,
            upsert_one_by_id=DEFAULT,
            insert_one=DEFAULT
        )
        self.mock_datastore = self.datastore_patch.start()

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()
        self.datastore_patch.stop()

    def test_push_es_raise_exception(self):
        mock_index_exception = MagicMock(side_effect=Exception('Generic Exception'))
        mock_index_esexception = MagicMock(side_effect=ElasticsearchException('Elasticsearch Exception'))
    	self.event.push_event_elasticsearch.es = MagicMock('es')
    	data = MagicMock(name='data')
    	data.to_dict.return_value = {
            'hostname': 'host1',
            'state': 'OK',
            'siteid': 'XYY00',
    	    'service': 'ispacs',
    	    'timestamp': data.datetime
            }
        self.event.push_event_elasticsearch.es.index = mock_index_exception
        self.event.push_event_elasticsearch(data)
        self.assertRaises(Exception, self.event.push_event_elasticsearch.es.index)

        self.event.push_event_elasticsearch.es.index = mock_index_esexception
        self.event.push_event_elasticsearch(data)
        self.assertRaises(ElasticsearchException, self.event.push_event_elasticsearch.es.index)

    def test_push_es_success(self):
        mock_index = MagicMock(name='es-index')
        self.event.push_event_elasticsearch.es = MagicMock('es')
        self.event.push_event_elasticsearch.es.index = mock_index
	from datetime import datetime
        data = MagicMock(name='data')
        data.to_dict.return_value = {
            'hostname': 'host1',
            'state': 'OK',
            'siteid': 'XYY00',
            'service': 'ispacs',
            'timestamp': data.datetime
            }
        self.event.push_event_elasticsearch(data)
        self.event.push_event_elasticsearch.es.index.assert_called_once_with(index=data.get('timestamp', datetime).strftime('event' + '-' + '%Y.%m.%d'), doc_type=data.pop('service', {}), body=data)

    def test_stale_alive_not_stored(self):
        self.event.send_event = MagicMock(name='send_event')
        self.mock_datastore['find_one'].return_value = None
        self.event.stale_alive('site1')
        self.mock_datastore['find_one'].assert_called_once_with('ALIVES', 'site1')
        self.mock_datastore['upsert_one_by_id'].assert_not_called()
        self.event.send_event.assert_not_called()

    def test_stale_alive_problem(self):
        self.event.send_event = MagicMock(name='send_event')
        self.mock_datastore['find_one'].return_value = {
            'alive_expiration': 1,
            'last_state': 'OK',
            'hostname': 'site1-somehost'
        }
        self.event.datetime.utcnow.return_value = 2
        self.event.stale_alive('site1')
        self.mock_datastore['upsert_one_by_id'].assert_called_once_with(
            'ALIVES', 'site1', {'last_state': 'CRITICAL'}
        )
        self.event.send_event.assert_called_once_with(
            datetime=2,
            hostaddress='10.3.4.5',
            hostname='site1-somehost',
            service='servicename1',
            siteid='site1',
            state='CRITICAL',
            type='PROBLEM'
        )

    def test_stale_alive_same_last_state(self):
        self.event.send_event = MagicMock(name='send_event')
        self.mock_datastore['find_one'].return_value = {
            'alive_expiration': 1,
            'last_state': 'CRITICAL',
            'hostname': 'site1-somehost'
        }
        self.event.datetime.utcnow.return_value = 2
        self.event.stale_alive('site1')
        self.mock_datastore['upsert_one_by_id'].assert_not_called()
        self.event.send_event.assert_not_called()

    def test_stale_alive_recovery(self):
        self.event.send_event = MagicMock(name='send_event')
        self.mock_datastore['find_one'].return_value = {
            'alive_expiration': 3,
            'last_state': 'CRITICAL',
            'hostname': 'site1-somehost'
        }
        self.event.datetime.utcnow.return_value = 2
        self.event.stale_alive('site1')
        self.mock_datastore['upsert_one_by_id'].assert_called_once_with(
            'ALIVES', 'site1', {'last_state': 'OK'}
        )
        self.event.send_event.assert_called_once_with(
            datetime=2,
            hostaddress='10.3.4.5',
            hostname='site1-somehost',
            service='servicename1',
            siteid='site1',
            state='OK',
            type='RECOVERY'
        )

    def test_send_event(self):
        self.event.handle_event = MagicMock(name='handle_event')
        self.event.send_event(
            siteid='XYZ01',
            hostname='host01',
            service='service__one',
            type='RECOVERY',
            state='OK',
            datetime=None,
            hostaddress='10.5.6.7',
            payload='output here'
        )
        self.event.handle_event.delay.assert_called_once_with(self.event.Message.return_value)
        self.event.Message.assert_called_once_with(
            datetime=None,
            hostaddress='10.5.6.7',
            hostname='host01',
            payload='output here',
            service='service__one',
            siteid='XYZ01',
            state='OK',
            type='RECOVERY'
        )

    def test_handle_event(self):
        self.event.handle_dashboard = MagicMock(name='handle_dashboard')
        self.event.events_consumers['ELASTIC'] = MagicMock(name='push_event_elasticsearch')
        self.mock_datastore['find_one'].return_value = {'product_id': 'PID'}
        data = MagicMock(name='data')
        data.to_dict.return_value = {
            'hostname': 'host1',
            'state': 'OK',
            'siteid': 'XYY00',
            'payload': {
                'output': 'something wrong',
                'components': [{'name': 'xx', 'version': '5.6'}],
                'modules': ['xx', 'Windows']
            }
        }
        self.event.handle_event(data)
        expected_event = {
            'siteid': 'XYY00',
            'output': 'something wrong',
            'state': 'OK',
            'hostname': 'host1',
            'timestamp': data.datetime,
            'namespace': self.event.ns2dict.return_value,
            'components': [{'version': '5.6', 'name': 'xx'}],
            'modules': ['xx', 'Windows'],
            'product_id': 'PID'
        }
        self.event.handle_dashboard.delay.assert_called_once_with(expected_event)
        self.event.events_consumers['ELASTIC'].delay.assert_called_once_with(expected_event)
        self.mock_datastore['insert_one'].assert_called_once_with('EVENTS', expected_event)
        self.event.ns2dict.assert_called_once_with(data.service)

    def test_handle_event_backwards_comp(self):
        self.event.handle_dashboard = MagicMock(name='handle_dashboard')
        self.event.events_consumers['ELASTIC'] = MagicMock(name='push_event_elasticsearch')
        self.mock_datastore['find_one'].return_value = {'product_id': 'PID'}
        data = MagicMock(name='data')
        data.to_dict.return_value = {
            'hostname': 'host1',
            'state': 'OK',
            'siteid': 'XYY00',
            'payload': 'something is not ok'
        }
        self.event.handle_event(data)
        expected_event = {
            'siteid': 'XYY00',
            'output': 'something is not ok',
            'state': 'OK',
            'hostname': 'host1',
            'timestamp': data.datetime,
            'namespace': self.event.ns2dict.return_value,
            'components': [],
            'modules': [],
            'product_id': 'PID'
        }
        self.mock_datastore['find_one'].assert_called_once_with('FACTS', {'_id': 'XYY00-host1'})
        self.event.handle_dashboard.delay.assert_called_once_with(expected_event)
        self.event.events_consumers['ELASTIC'].delay.assert_called_once_with(expected_event)
        self.mock_datastore['insert_one'].assert_called_once_with('EVENTS', expected_event)
        self.event.ns2dict.assert_called_once_with(data.service)


if __name__ == '__main__':
    unittest.main()
