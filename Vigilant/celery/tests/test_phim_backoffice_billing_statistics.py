import unittest
from mock import MagicMock, patch, call, mock_open


# Parameterized decorator to mock celery.task
# It returns the function undecorated
def fakeorator_arg(*args, **kwargs):
    def fakeorator(func):
        return func
    return fakeorator


class BillingStatisticsBaseTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tbconfig = MagicMock(name='tbconfig')
        self.mock_phim_backoffice = MagicMock(name='phim_backoffice')
        self.mock_phim_backoffice.celery.app.task = fakeorator_arg
        self.mock_celery = MagicMock(name='celery')
        self.mock_os = MagicMock(name='os')
        self.mock_csv = MagicMock(name='csv')
        self.mock_date = MagicMock(name='datetime')
        self.mock_dateutil = MagicMock(name='dateutil')
        modules = {
            'tbconfig': self.mock_tbconfig,
            'phim_backoffice.billing.s3_wrapper': self.mock_phim_backoffice.s3_wrapper,
            'phim_backoffice.billing.task': self.mock_phim_backoffice.task,
            'phim_backoffice.datastore': self.mock_phim_backoffice.datastore,
            'phim_backoffice.celery': self.mock_phim_backoffice.celery,
            'dateutil.parser': self.mock_dateutil,
            'datetime': self.mock_date,
            'celery': self.mock_celery,
            'celery.utils': self.mock_celery.utils,
            'celery.exceptions': self.mock_celery.exceptions,
            'celery.utils.log': self.mock_celery.utils.log,
            'os': self.mock_os,
            'csv': self.mock_csv
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import phim_backoffice.billing.statistics
        self.billing_statistics = phim_backoffice.billing.statistics
        self.billing_statistics.BILLING = {}
        self.billing_statistics.BILLING['ACCESS_KEY'] = 'access_key'
        self.billing_statistics.BILLING['SECRET_KEY'] = 'secret_key'
        self.billing_statistics.BILLING['BUCKET'] = 'bucket'
        self.billing_statistics.BILLING['S3_FOLDER'] = 'folder'

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()


class BillingStatisticsCommonTestCase(BillingStatisticsBaseTestCase):
    def setUp(self):
        BillingStatisticsBaseTestCase.setUp(self)

    def test_file_path(self):
        fname = self.billing_statistics._file_path('test_file.csv')
        self.assertEqual(fname, '/tmp/test_file.csv')

    def test_create_csv(self):
        mock_opn = mock_open()
        mock_writerow = MagicMock(name='writerow')
        self.mock_csv.writer.return_value = mock_writerow
        expected_call = [call.writerow(
            ['h1', 'h2']), call.writerow(['SITE1', 'v1', 'v2'])]
        with patch('phim_backoffice.billing.statistics.open', mock_opn, create=True):
            studies = [('v1', 'v2')]
            ab_file_path = '/tmp/test.csv'
            self.billing_statistics._create_csv(
                studies, ab_file_path, 'SITE1', ['h1', 'h2'])
            mock_opn.assert_called_once_with('/tmp/test.csv', 'w')
            # self.mock_csv.writer.assert_called_once()
            self.assertEqual(self.mock_csv.writer.call_count, 1)
            self.assertEqual(expected_call, mock_writerow.method_calls)

    def test_create_csv_unicode_encode_error(self):
        def _side_effect(*args, **kwars):
            if 'headers' in args:
                return None
            for item in args[0]:
                if isinstance(item, MagicMock):
                    raise UnicodeEncodeError(
                        'error', u"", 42, 43, 'unicode error')

        mock_opn = mock_open()
        mock_isinstance = MagicMock(name='isinstance')
        mock_study1 = MagicMock(name='study1')
        mock_study2 = MagicMock(name='study2')
        mock_study1.encode.return_value = 's1'
        mock_study2.encode.return_value = 's2'
        self.billing_statistics.isinstance = mock_isinstance
        mock_writerow = MagicMock(name='writerow')
        mock_writerow.writerow.side_effect = _side_effect
        self.mock_csv.writer.return_value = mock_writerow
        expected_call = [call.writerow('headers'), call.writerow(['SITE1', mock_study1, mock_study2]),
                         call.writerow(['SITE1', 's1', 's2'])]
        with patch('phim_backoffice.billing.statistics.open', mock_opn, create=True):
            studies = [(mock_study1, mock_study2)]
            ab_file_path = '/tmp/test.csv'
            self.billing_statistics._create_csv(
                studies, ab_file_path, 'SITE1', 'headers')
            mock_opn.assert_called_once_with('/tmp/test.csv', 'w')
            self.assertEqual(self.mock_csv.writer.call_count, 1)
            self.assertEqual(expected_call, mock_writerow.method_calls)
            mock_study1.encode.assert_called_once_with('utf-8')
            mock_study2.encode.assert_called_once_with('utf-8')
            self.assertEqual(mock_isinstance.call_count, 3)

    def test_mongofilter(self):
        self.assertEqual({'query_filter': {'k': 'v'}},
                         self.billing_statistics._mongo_filter('k', 'v'))

    def test_mongo_set_on_insert(self):
        self.assertEqual({'$setOnInsert': {'k': 'v'}},
                         self.billing_statistics._mongo_set_on_insert('k', 'v'))

    def test_mongo_set(self):
        self.assertEqual({'$set': {'k': 'v'}},
                         self.billing_statistics._mongo_set('k', 'v'))

    def test_mongo_push(self):
        self.assertEqual({'$push': {'k': 'v'}},
                         self.billing_statistics._mongo_push('k', 'v'))

    def test_iso_to_datetime(self):
        self.mock_dateutil.parse.return_value = 'sss'
        dt_str = self.billing_statistics._iso_to_datetime('test_date')
        self.assertEqual(dt_str, 'sss')
        self.mock_dateutil.parse.assert_called_once_with('test_date')

    def test_status_history_entry(self):
        st_history = self.billing_statistics._status_history_entry('status')
        exp_history = {'$each': ['status'],
                       '$sort': {'billing_datetime': 1}, '$slice': -60}
        self.assertEqual(exp_history, st_history)


class BillingStatisticsGetQueryStmtTestCase(BillingStatisticsBaseTestCase):
    def setUp(self):
        BillingStatisticsBaseTestCase.setUp(self)
        self.mock_date.datetime.utcnow.return_value = 'utcnow'
        self.filter_mock = MagicMock(name='mongofilter')
        self.filter_mock.return_value = {'filter_mock': 'v'}
        self.billing_statistics._mongo_filter = self.filter_mock
        self.mongoseton_mock = MagicMock(name='mongoset_on_insert')
        self.mongoseton_mock.return_value = {'mongoseton_mock': 'v'}
        self.billing_statistics._mongo_set_on_insert = self.mongoseton_mock
        self.get_upload_status_mock = MagicMock(name='status')
        self.get_upload_status_mock.return_value = {
            'get_upload_status_mock': 'v'}
        self.billing_statistics._get_upload_status = self.get_upload_status_mock
        self.mongopush_mock = MagicMock(name='mongopush')
        self.mongopush_mock.return_value = {'mongopush_mock': 'v'}
        self.billing_statistics._mongo_push = self.mongopush_mock
        self.status_history_entry_mock = MagicMock(name='st_history_entry')
        self.status_history_entry_mock.return_value = {
            'status_history_entry_mock': 'v'}
        self.billing_statistics._status_history_entry = self.status_history_entry_mock
        self.mongoset_mock = MagicMock(name='mongoset')
        self.mongoset_mock.return_value = {'mongoset_mock': 'v'}
        self.billing_statistics._mongo_set = self.mongoset_mock

    def test_get_query_stmt_with_upload_size(self):
        exp_query = {'filter_mock': 'v', 'update': {'mongoset_mock': 'v',
                                                    'mongoseton_mock': 'v', 'mongopush_mock': 'v'}, 'upsert': True}
        qurery = self.billing_statistics.get_query_stmt(
            'site_id', 'file_name', 'upload_status', 'billing_datetime', 'upload_size')
        self.assertEqual(qurery, exp_query)
        self.mock_date.datetime.utcnow.assert_called_once_with()
        self.filter_mock.assert_called_once_with('site_id', 'site_id')
        self.mongoseton_mock.assert_called_once_with('creation_time', 'utcnow')
        self.get_upload_status_mock.assert_called_once_with(
            'file_name', 'upload_status', 'billing_datetime', 'upload_size')
        self.status_history_entry_mock.assert_called_once_with(
            {'get_upload_status_mock': 'v'})
        self.mongopush_mock.assert_called_once_with(
            'status_history', {'status_history_entry_mock': 'v'})
        self.mongoset_mock.assert_called_once_with(
            'recent_upload', {'get_upload_status_mock': 'v'})

    def test_get_query_stmt_without_upload_size(self):
        exp_query = {'filter_mock': 'v', 'update': {'mongoset_mock': 'v',
                                                    'mongoseton_mock': 'v', 'mongopush_mock': 'v'}, 'upsert': True}
        qurery = self.billing_statistics.get_query_stmt(
            'site_id', 'file_name', 'upload_status', 'billing_datetime')
        self.assertEqual(qurery, exp_query)
        self.mock_date.datetime.utcnow.assert_called_once_with()
        self.filter_mock.assert_called_once_with('site_id', 'site_id')
        self.mongoseton_mock.assert_called_once_with('creation_time', 'utcnow')
        self.get_upload_status_mock.assert_called_once_with(
            'file_name', 'upload_status', 'billing_datetime', None)
        self.status_history_entry_mock.assert_called_once_with(
            {'get_upload_status_mock': 'v'})
        self.mongopush_mock.assert_called_once_with(
            'status_history', {'status_history_entry_mock': 'v'})
        self.mongoset_mock.assert_called_once_with(
            'recent_upload', {'get_upload_status_mock': 'v'})

    # def test_s3_upload_task(self):
    #     self.billing_statistics.s3_upload_task('access_key', 'secret_key', 'bucket_name', 'file_name', 'file_path', 'folder')
    #     self.mock_phim_backoffice.s3_wrapper.S3Wrapper.assert_called_with('access_key', 'secret_key', 'bucket_name')
    #     self.mock_phim_backoffice.s3_wrapper.S3Wrapper().upload.assert_called_with('file_name', 'file_path', 'folder')
    #     self.mock_os.remove.assert_called_with('file_path')

    def test_get_query_stmt_with_upload_status_False(self):
        exp_query = {'filter_mock': 'v', 'update': {
            'mongoseton_mock': 'v', 'mongopush_mock': 'v'}, 'upsert': True}
        qurery = self.billing_statistics.get_query_stmt(
            'site_id', 'file_name', False, 'billing_dt')
        self.assertEqual(qurery, exp_query)
        self.mock_date.datetime.utcnow.assert_called_once_with()
        self.filter_mock.assert_called_once_with('site_id', 'site_id')
        self.mongoseton_mock.assert_called_once_with('creation_time', 'utcnow')
        self.get_upload_status_mock.assert_called_once_with(
            'file_name', False, 'billing_dt', None)
        self.status_history_entry_mock.assert_called_once_with(
            {'get_upload_status_mock': 'v'})
        self.mongopush_mock.assert_called_once_with(
            'status_history', {'status_history_entry_mock': 'v'})
        self.assertEqual(self.mongoset_mock.call_count, 0)


class BillingStatisticsBillingTaskTestCase(BillingStatisticsBaseTestCase):
    def setUp(self):
        BillingStatisticsBaseTestCase.setUp(self)
        self.file_path_mock = MagicMock(name='file_path')
        self.task_self_mock = MagicMock(name='task_self')
        self.file_path_mock.return_value = 'file_path'
        self.billing_statistics._file_path = self.file_path_mock
        self.create_csv_mock = MagicMock(name='create_csv')
        self.billing_statistics._create_csv = self.create_csv_mock
        self.get_query_stmt_mock = MagicMock(name='get_query_stmt')
        self.get_query_stmt_mock.side_effect = [{'q': 'v'}, {'q1': 'v1'}]
        self.billing_statistics.get_query_stmt = self.get_query_stmt_mock
        self.data = {'file_name': 'file_name', 'studies': ['s1', 's2', 's3']}
        self.data['siteid'] = 'SITE1'
        self.data['csv_headers'] = ['h1', 'h2']
        self.data['billing_datetime'] = 'b_dtime'

    def test_billing_task(self):
        self.mock_phim_backoffice.s3_wrapper.S3Wrapper().upload.return_value = 2097152
        self.billing_statistics.billing_task(self.task_self_mock, self.data)
        self.mock_phim_backoffice.s3_wrapper.S3Wrapper.assert_called_with(
            'access_key', 'secret_key', 'bucket')
        self.mock_phim_backoffice.s3_wrapper.S3Wrapper().upload.assert_called_with(
            'file_name', 'file_path', 'folder')
        exp_get_query_stmt_call = [call('SITE1', 'file_name', False, 'b_dtime'), call(
            'SITE1', 'file_name', True, 'b_dtime', 2097152)]
        self.assertEqual(self.get_query_stmt_mock.mock_calls,
                         exp_get_query_stmt_call)
        self.assertEqual(self.task_self_mock.uploaded_mb, 2.0)
        self.mock_os.remove.assert_called_with('file_path')
        self.file_path_mock.assert_called_with('file_name')
        self.create_csv_mock.assert_called_with(
            ['s1', 's2', 's3'], 'file_path', 'SITE1', ['h1', 'h2'])
        exp_datastore_call = [call.update_doc(
            'BILLING', q='v'), call.update_doc('BILLING', q1='v1')]
        self.assertEqual(
            self.mock_phim_backoffice.datastore.mock_calls, exp_datastore_call)

    def test_billing_task_upload_size_eq_0(self):
        self.mock_phim_backoffice.s3_wrapper.S3Wrapper().upload.return_value = 0
        self.billing_statistics.billing_task(self.task_self_mock, self.data)
        self.mock_phim_backoffice.s3_wrapper.S3Wrapper.assert_called_with(
            'access_key', 'secret_key', 'bucket')
        self.mock_phim_backoffice.s3_wrapper.S3Wrapper().upload.assert_called_with(
            'file_name', 'file_path', 'folder')
        exp_get_query_stmt_call = [call('SITE1', 'file_name', False, 'b_dtime'), call(
            'SITE1', 'file_name', False, 'b_dtime', 0)]
        self.assertEqual(self.get_query_stmt_mock.mock_calls,
                         exp_get_query_stmt_call)
        self.mock_os.remove.assert_called_with('file_path')
        self.file_path_mock.assert_called_with('file_name')
        self.create_csv_mock.assert_called_with(
            ['s1', 's2', 's3'], 'file_path', 'SITE1', ['h1', 'h2'])
        exp_datastore_call = [call.update_doc(
            'BILLING', q='v'), call.update_doc('BILLING', q1='v1')]
        self.assertEqual(
            self.mock_phim_backoffice.datastore.mock_calls, exp_datastore_call)

    def test_billing_task_upload_size_lesthan_0(self):
        self.mock_phim_backoffice.s3_wrapper.S3Wrapper().upload.return_value = -1
        self.billing_statistics.billing_task(self.task_self_mock, self.data)
        self.mock_phim_backoffice.s3_wrapper.S3Wrapper.assert_called_with(
            'access_key', 'secret_key', 'bucket')
        self.mock_phim_backoffice.s3_wrapper.S3Wrapper().upload.assert_called_with(
            'file_name', 'file_path', 'folder')
        exp_get_query_stmt_call = [call('SITE1', 'file_name', False, 'b_dtime'), call(
            'SITE1', 'file_name', False, 'b_dtime', -1)]
        self.assertEqual(self.get_query_stmt_mock.mock_calls,
                         exp_get_query_stmt_call)
        self.mock_os.remove.assert_called_with('file_path')
        self.file_path_mock.assert_called_with('file_name')
        self.create_csv_mock.assert_called_with(
            ['s1', 's2', 's3'], 'file_path', 'SITE1', ['h1', 'h2'])
        exp_datastore_call = [call.update_doc(
            'BILLING', q='v'), call.update_doc('BILLING', q1='v1')]
        self.assertEqual(
            self.mock_phim_backoffice.datastore.mock_calls, exp_datastore_call)

    def test_billing_task_exception_retry(self):
        self.file_path_mock.side_effect = Exception
        self.billing_statistics.billing_task(self.task_self_mock, self.data)
        self.assertEqual(self.task_self_mock.retry.call_count, 1)


class BillingStatisticsGetUploadStatusTestCase(BillingStatisticsBaseTestCase):
    def setUp(self):
        BillingStatisticsBaseTestCase.setUp(self)
        self.iso_to_dtime_mock = MagicMock(name='iso_to_datetime')
        self.iso_to_dtime_mock.return_value = 'datetime'
        self.billing_statistics._iso_to_datetime = self.iso_to_dtime_mock
        self.mock_date.datetime.utcnow.return_value = 'utcnow'

    def test_get_upload_status_with_upload_size(self):
        status = self.billing_statistics._get_upload_status(
            'file_name', 'upload_status', 'billing_datetime', 'uploaded_bytes')
        exp_status = {'file_name': 'file_name', 'upload_status': 'upload_status',
                      'timestamp': 'utcnow', 'billing_datetime': 'datetime', 'uploaded_bytes': 'uploaded_bytes'}
        self.assertEqual(status, exp_status)
        self.mock_date.datetime.utcnow.assert_called_once_with()
        self.iso_to_dtime_mock.assert_called_once_with('billing_datetime')

    def test_get_upload_status_without_upload_size(self):
        status = self.billing_statistics._get_upload_status(
            'file_name', 'upload_status', 'billing_datetime')
        exp_status = {'file_name': 'file_name', 'upload_status': 'upload_status',
                      'timestamp': 'utcnow', 'billing_datetime': 'datetime'}
        self.assertEqual(status, exp_status)
        self.iso_to_dtime_mock.assert_called_once_with('billing_datetime')
        self.mock_date.datetime.utcnow.assert_called_once_with()
