import unittest
from mock import MagicMock, patch


# Parameterized decorator to mock celery.task
# It returns the function undecorated
def fakeorator_arg(*args, **kwargs):
    def fakeorator(func):
        return func
    return fakeorator


class KVstoreCTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tbconfig = MagicMock(name='tbconfig')
        self.mock_taskbase = MagicMock(name='taskbase')
        self.mock_pb = MagicMock(name='phim_backoffice')
        self.mock_pb.celery.app.task = fakeorator_arg
        self.mock_celery = MagicMock(name='celery')
        modules = {
            'tbconfig': self.mock_tbconfig,
            'taskbase': self.mock_taskbase,
            'taskbase.redistask': self.mock_taskbase.redistask,
            'phim_backoffice.celery': self.mock_pb.celery,
            'celery': self.mock_celery,
            'celery.utils': self.mock_celery.utils,
            'celery.utils.log': self.mock_celery.utils.log,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import phim_backoffice.kvstore
        self.kvstore = phim_backoffice.kvstore
        self.client_patcher = patch('phim_backoffice.kvstore.get_client')
        self.mock_client = self.client_patcher.start()

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()
        self.client_patcher.stop()

    def test_add_to_set_one_value(self):
        self.kvstore.add_to_set('set1', 'value1')
        self.mock_client.return_value.sadd.assert_called_once_with('set1', 'value1')

    def test_add_to_set_more_than_one_value(self):
        self.kvstore.add_to_set('set1', 'value1', 'value2')
        self.mock_client.return_value.sadd.assert_called_once_with('set1', 'value1', 'value2')

    def test_remove_from_set_one_value(self):
        self.kvstore.remove_from_set('set1', 'value1')
        self.mock_client.return_value.srem.assert_called_once_with('set1', 'value1')

    def test_remove_from_set_more_than_one_value(self):
        self.kvstore.remove_from_set('set1', 'value1', 'value2')
        self.mock_client.return_value.srem.assert_called_once_with('set1', 'value1', 'value2')

    def test_is_member_of_set(self):
        self.assertEqual(
            self.kvstore.is_member_of_set('set1', 'value1'),
            self.mock_client.return_value.sismember.return_value
        )
        self.mock_client.return_value.sismember.assert_called_once_with('set1', 'value1')

    def test_get_members_of_set(self):
        self.assertEqual(
            self.kvstore.get_members_of_set('set1'),
            self.mock_client.return_value.smembers.return_value
        )
        self.mock_client.return_value.smembers.assert_called_once_with('set1')

    def test_delete_key(self):
        self.kvstore.delete_key('set1')
        self.mock_client.return_value.delete.assert_called_once_with('set1')


if __name__ == '__main__':
    unittest.main()
