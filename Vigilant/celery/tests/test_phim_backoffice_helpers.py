import unittest
from mock import patch


class HelpersTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.module_patcher = patch.dict('sys.modules', {})
        self.module_patcher.start()
        import phim_backoffice.helpers
        self.helpers = phim_backoffice.helpers

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_identifier(self):
        self.assertEqual(self.helpers.get_identifier('abc00', 'somehost01'), 'abc00-somehost01')

    def test_get_identified_host_dictionaries(self):
        facts = {
            'host1': {
                'data1': 'value1'
            },
            'host2': '',
            'host3': {
                'data1': 'value2'
            },
        }
        self.assertEqual(
            sorted(self.helpers.get_identified_host_dictionaries('xyz00', facts)),
            [
             ('xyz00-host1', {'siteid': 'xyz00', 'hostname': 'host1', 'data1': 'value1'}),
             ('xyz00-host3', {'siteid': 'xyz00', 'hostname': 'host3', 'data1': 'value2'})
            ]
        )

    @patch('__builtin__.__import__')
    def test_get_class(self, mock_patch):
        result = self.helpers.get_class('module1', 'attribute1')
        mock_patch.assert_called_once_with('module1', fromlist=['attribute1'])
        self.assertEqual(result, mock_patch.return_value.attribute1)

    def test_get_validated_fields(self):
        fields = frozenset(['name', 'rules', 'two'])
        self.assertEqual(self.helpers.get_validated_fields(fields, name='one', rules=None), {'name': 'one'})

    def test_get_validated_fields_unexpected_arg(self):
        self.assertRaises(TypeError, self.helpers.get_validated_fields, frozenset(['name']), siteid='XYZ00')


if __name__ == '__main__':
    unittest.main()
