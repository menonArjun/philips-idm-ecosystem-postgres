import unittest
from mock import MagicMock, patch, DEFAULT


# Parameterized decorator to mock celery.task
# It returns the function undecorated
def fakeorator_arg(*args, **kwargs):
    def fakeorator(func):
        return func
    return fakeorator


class DescriptorTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_pb = MagicMock(name='phim_backoffice')
        self.mock_pb.celery.app.task = fakeorator_arg
        self.mock_celery = MagicMock(name='celery')
        modules = {
            'phim_backoffice.datastore': self.mock_pb.datastore,
            'phim_backoffice.helpers': self.mock_pb.helpers,
            'phim_backoffice.celery': self.mock_pb.celery,
            'celery': self.mock_celery,
            'celery.utils': self.mock_celery.utils,
            'celery.utils.log': self.mock_celery.utils.log
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import phim_backoffice.descriptor
        self.descriptor = phim_backoffice.descriptor

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_update_site_descriptors(self):
        self.descriptor.update_descriptors = MagicMock(name='update_descriptors')
        self.descriptor.update_site_descriptors(siteid='XYZ00', name='long name', pacs_version='9.8')
        self.descriptor.update_descriptors.assert_called_once_with(
            'SITES', 'XYZ00', self.descriptor.get_validated_fields.return_value
        )
        self.descriptor.get_validated_fields.assert_called_once_with(
            self.descriptor.SITE_FIELDS, pacs_version='9.8', name='long name'
        )

    def test_update_descriptors(self):
        self.descriptor.update_descriptors('COLLECTION', 'AXY01', {'val': 'key'})
        self.descriptor.upsert_one_by_id.assert_called_once_with('COLLECTION', 'AXY01', {'val': 'key'})
        self.descriptor.delete_one_by_id.assert_called_once_with('EXCEPTIONS', 'AXY01')


if __name__ == '__main__':
    unittest.main()
