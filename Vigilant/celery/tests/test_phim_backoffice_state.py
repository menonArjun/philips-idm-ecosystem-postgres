import unittest
from mock import MagicMock, patch, call


# Parameterized decorator to mock celery.task
# It returns the function undecorated
def fakeorator_arg(*args, **kwargs):
    def fakeorator(func):
        return func
    return fakeorator


class StateTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tbconfig = MagicMock(name='tbconfig')
        self.mock_pb = MagicMock(name='phim_backoffice')
        self.mock_pb.celery.app.task = fakeorator_arg
        self.mock_celery = MagicMock(name='celery')
        self.mock_phimutils = MagicMock(name='phimutils')
        modules = {
            'tbconfig': self.mock_tbconfig,
            'phim_backoffice.datastore': self.mock_pb.datastore,
            'phim_backoffice.dashboard': self.mock_pb.dashboard,
            'phim_backoffice.event': self.mock_pb.event,
            'phim_backoffice.helpers': self.mock_pb.helpers,
            'phim_backoffice.celery': self.mock_pb.celery,
            'celery': self.mock_celery,
            'celery.utils': self.mock_celery.utils,
            'celery.utils.log': self.mock_celery.utils.log,
            'phimutils': self.mock_phimutils,
            'phimutils.stateredis': self.mock_phimutils.stateredis,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import phim_backoffice.state
        self.state = phim_backoffice.state

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_update_state(self):
        self.state.update_problem_states = MagicMock(name='update_problem_states')
        self.state.get_identified_host_dictionaries.return_value = [('host1', {'x': 'y'}), ('host2', {'z': 'a'})]
        self.state.is_problem_site.return_value = True
        self.state.update_state('XYZ00', {'ee': 'val1'})
        self.state.get_identified_host_dictionaries.assert_called_once_with('XYZ00', {'ee': 'val1'})
        self.assertEqual(
            self.state.upsert_one_by_id.mock_calls,
            [call('STATES', 'host1', {'x': 'y'}), call('STATES', 'host2', {'z': 'a'})]
        )

    def test_update_problem_states(self):
        self.state.send_events_for_recovered_services = MagicMock(name='send_events_for_recovered_services')
        self.state.get_problem_services_by_host.return_value = [
            {'hostname': 'host01', 'services': ['service1', 'service2']},
            {'hostname': 'host02', 'services': ['service3']},
            {'hostname': 'host03', 'services': ['service3']}
        ]
        self.state.update_problem_states('ABX01', {'host01': {'zr': 'val2'}, 'host02': {'yx': 'val3'}})
        self.state.get_problem_services_by_host.assert_called_once_with('ABX01')
        self.assertEqual(
            self.state.send_events_for_recovered_services.mock_calls,
            [
                call('ABX01', 'host01', ['service1', 'service2'], {'zr': 'val2'}),
                call('ABX01', 'host02', ['service3'], {'yx': 'val3'}),
                call('ABX01', 'host03', ['service3'], None)
            ]
        )

    def test_get_hostaddress(self):
        self.state.STATE = {'HOSTADDRESS_KEY': 'address__service'}
        self.assertEqual(self.state.get_hostaddress({'address__service': '10.0.3.4'}), '10.0.3.4')

    def test_send_events_for_recovered_services(self):
        self.state.get_hostaddress = MagicMock(name='get_hostaddress')
        self.state.get_recovered_services = MagicMock(
            name='get_recovered_services',
            return_value=[('service1', 'it was ok'), ('service2', 'IDK')]
        )
        self.state.send_event = MagicMock(name='send_event')
        host_state = {'service1': 'OK'}
        self.state.send_events_for_recovered_services('XYZ00', 'host01', ['service1', 'service2'], host_state)
        self.state.get_hostaddress.assert_called_once_with(host_state)
        self.state.get_recovered_services.assert_called_once_with(host_state, ['service1', 'service2'])
        self.assertEqual(
            self.state.send_event.mock_calls,
            [
                call(
                    hostaddress=self.state.get_hostaddress.return_value,
                    hostname='host01',
                    payload={'output': 'OK - it was ok'},
                    service='service1',
                    siteid='XYZ00',
                    state='OK',
                    type='RECOVERY'
                ),
                call(
                    hostaddress=self.state.get_hostaddress.return_value,
                    hostname='host01',
                    payload={'output': 'OK - IDK'},
                    service='service2',
                    siteid='XYZ00',
                    state='OK',
                    type='RECOVERY'
                )
            ]
        )

    def test_get_recovered_services_no_host_state(self):
        self.assertEqual(
            list(self.state.get_recovered_services(None, ['service1', 'service2'])),
            [
                ('service1', 'Host not present in State data'),
                ('service2', 'Host not present in State data')
            ]
        )

    def test_get_recovered_services(self):
        self.state.StateRedis.get_state_key_name.side_effect = ['key1__State', 'key2__State', 'key3__State']
        host_state = {'key1__State': 'OK', 'key2__State': 'CRITICAL'}
        self.assertEqual(
            list(self.state.get_recovered_services(host_state, ['service1', 'service2', 'service3'])),
            [
                ('service1', 'From Service State data'),
                ('service3', 'Service not present in State data')
            ]
        )

    def test_purge_extra_hosts_state(self):
        self.state.purge_extra_hosts_state('XYZ00', ['host1', 'host2'])
        self.state.delete_many.assert_called_once_with(
            'STATES', {'hostname': {'$nin': ['host1', 'host2']}, 'siteid': 'XYZ00'}
        )


if __name__ == '__main__':
    unittest.main()
