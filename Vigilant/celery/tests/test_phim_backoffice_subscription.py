import unittest
from mock import MagicMock, patch, DEFAULT


# Parameterized decorator to mock celery.task
# It returns the function undecorated
def fakeorator_arg(*args, **kwargs):
    def fakeorator(func):
        return func
    return fakeorator


class SubscriptionTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tbconfig = MagicMock(name='tbconfig')
        self.mock_pb = MagicMock(name='phim_backoffice')
        self.mock_pb.celery.app.task = fakeorator_arg
        self.mock_celery = MagicMock(name='celery')
        modules = {
            'tbconfig': self.mock_tbconfig,
            'phim_backoffice.datastore': self.mock_pb.datastore,
            'phim_backoffice.helpers': self.mock_pb.helpers,
            'phim_backoffice.celery': self.mock_pb.celery,
            'celery': self.mock_celery,
            'celery.utils': self.mock_celery.utils,
            'celery.utils.log': self.mock_celery.utils.log
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import phim_backoffice.subscription
        self.subscription = phim_backoffice.subscription
        self.subscription.SUBSCRIPTION = {'DEFAULT_RULES': ['magicrule']}

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_create_subscription(self):
        self.subscription.get_validated_fields.return_value = {
            'rules': ['somerule'], 'path': '/', 'countries': ['Mexico', 'USA']
        }
        self.subscription.create_subscription(
            subscription='sub1',
            countries=['Mexico', 'USA'],
            rules=['somerule']
        )
        self.subscription.upsert_one_by_id.assert_called_once_with(
            'SUBSCRIPTIONS',
            'sub1',
            {'rules': ['somerule'], 'path': '/', 'countries': ['Mexico', 'USA']}
        )
        self.subscription.get_validated_fields.assert_called_once_with(
            self.subscription.SUBSCRIPTION_FIELDS,
            countries=['Mexico', 'USA'], rules=['somerule']
        )

    def test_create_subscription_defaults(self):
        self.subscription.get_validated_fields.return_value = {'countries': ['Mexico', 'USA']}
        self.subscription.create_subscription(
            subscription='sub1',
            countries=['Mexico', 'USA']
        )
        self.subscription.upsert_one_by_id.assert_called_once_with(
            'SUBSCRIPTIONS',
            'sub1',
            {'rules': ['magicrule'], 'path': '/', 'countries': ['Mexico', 'USA']}
        )
        self.subscription.get_validated_fields.assert_called_once_with(
            self.subscription.SUBSCRIPTION_FIELDS,
            countries=['Mexico', 'USA']
        )

    def test_update_subscription(self):
        self.subscription.update_subscription(
            subscription='sub1',
            path='/root/one',
            countries=['Mexico', 'USA'],
            rules=['somerule']
        )
        self.subscription.update_one_by_id.assert_called_once_with(
            'SUBSCRIPTIONS',
            'sub1',
            self.subscription.get_validated_fields.return_value
        )
        self.subscription.get_validated_fields.assert_called_once_with(
            self.subscription.SUBSCRIPTION_FIELDS,
            countries=['Mexico', 'USA'], path='/root/one', rules=['somerule']
        )


if __name__ == '__main__':
    unittest.main()
