import unittest
from mock import MagicMock, patch, DEFAULT


# Parameterized decorator to mock celery.task
# It returns the function undecorated
def fakeorator_arg(*args, **kwargs):
    def fakeorator(func):
        return func
    return fakeorator


class AuditTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_pb = MagicMock(name='phim_backoffice')
        self.mock_pb.celery.app.task = fakeorator_arg
        self.mock_celery = MagicMock(name='celery')
        modules = {
            'phim_backoffice.datastore': self.mock_pb.datastore,
            'phim_backoffice.helpers': self.mock_pb.helpers,
            'phim_backoffice.celery': self.mock_pb.celery,
            'phim_backoffice.persist': self.mock_pb.persist,
            'celery': self.mock_celery,
            'celery.utils': self.mock_celery.utils,
            'celery.utils.log': self.mock_celery.utils.log
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import phim_backoffice.audit
        import phim_backoffice.persist
        self.audit = phim_backoffice.audit
        self.datastore_patch = patch.multiple(
            'phim_backoffice.audit',
            find_one=DEFAULT
        )
        self.mock_datastore = self.datastore_patch.start()
        self.audit.datetime = MagicMock(name='utcnow')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_insert_dated_audit(self):
        self.audit.insert_dated_audit('distribution', {'name': 'anywhere-1.2'})
        self.audit.insert_one.assert_called_once_with(
            'AUDIT',
            {
                'type': 'distribution',
                'name': 'anywhere-1.2',
                'timestamp': self.audit.datetime.utcnow.return_value,
            }
        )

    def test_insert_distribution(self):
        self.audit.insert_dated_audit = MagicMock(name='insert_dated_audit')
        self.audit.get_validated_fields.return_value = {'name': 'anywhere-1.2'}
        self.audit.insert_distribution(
            name='anywhere-1.2',
            rules=['multisitedeployment'],
            status='Submitted',
            createdby='IDM Portal',
            timestamp='2016-03-21T11:14:37.412Z',
            modifiedby='IDM Portal',
            type='distribution',
            siteid='AMC01',
            version='VX'
        )
        self.audit.insert_dated_audit.assert_called_once_with('distribution', {'name': 'anywhere-1.2'})
        self.audit.get_validated_fields.assert_called_once_with(
            self.audit.DISTRIBUTION_FIELDS,
            createdby='IDM Portal',
            modifiedby='IDM Portal',
            name='anywhere-1.2',
            rules=['multisitedeployment'],
            siteid='AMC01',
            status='Submitted',
            timestamp='2016-03-21T11:14:37.412Z',
            type='distribution',
            version='VX'
        )

    def test_insert_deployment(self):
        self.audit.insert_dated_audit = MagicMock(name='insert_dated_audit')
        self.audit.get_validated_fields.return_value = {'packagename': 'anywhere-1.2'}
        self.audit.insert_deployment(
            name='anywhere-1.2',
            rules=['multisitedeployment'],
            status='Submitted',
            createdby='IDM Portal',
            timestamp='2016-03-21T11:14:37.412Z',
            modifiedby='IDM Portal',
            type='distribution',
            siteid=['AMC01', 'BENHC'],
            version='v1'
        )
        self.audit.insert_dated_audit.assert_called_once_with('deployment', {'packagename': 'anywhere-1.2'})
        self.audit.get_validated_fields.assert_called_once_with(
            self.audit.DEPLOYMENT_FIELDS,
            createdby='IDM Portal',
            modifiedby='IDM Portal',
            name='anywhere-1.2',
            rules=['multisitedeployment'],
            siteid=['AMC01', 'BENHC'],
            status='Submitted',
            timestamp='2016-03-21T11:14:37.412Z',
            type='distribution',
            version='v1'
        )

    def test_update_package_status(self):
        self.audit.insert_distribution = MagicMock(name='insert_distribution')
        payload = {'localhost': {'PCM': [{'version': 'X.Y.Z', 'name': 'XXX'}]}}
        siteid = "ID"
        data = {
                'subscription': 'XXX',
                'version': 'X.Y.Z',
                'siteid': siteid,
                'status':'Available'
            }
        self.audit.insert_dated_audit = MagicMock(name='insert_dated_audit')
        self.audit.find_one.return_value = {}
        self.audit.get_validated_fields.return_value = data
        self.audit.update_package_status(siteid, payload)
        data['user'] = 'IDM Portal'
        self.audit.insert_distribution.assert_called_once_with(**data)

    def test_deployment_status(self):
        payload = {'status': 'Success', 'deployment_id': 'ID', 'siteid': 'siteid','output':'X:Y'}
        self.audit.insert_dated_audit = MagicMock(name='insert_dated_audit')
        self.audit.get_validated_fields.return_value = payload
        self.audit.find_one.return_value = {'status': 'Success', 'timestamp': 'ts', '_id': '_id', 'type': 'deployment'}
        self.audit.deployment_status(**payload)
        self.mock_datastore['find_one'].assert_called_once_with('AUDIT', {'type': 'deployment', 'deployment_id': 'ID'})
        self.audit.insert_dated_audit.assert_called_once_with('deployment', payload)
        self.audit.poperator_del.assert_called_once_with('siteid', 'PCM_Manifest', payload)


if __name__ == '__main__':
    unittest.main()
