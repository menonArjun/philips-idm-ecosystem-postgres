import unittest
from mock import MagicMock, patch, DEFAULT, call


# Parameterized decorator to mock celery.task
# It returns the function undecorated
def fakeorator_arg(*args, **kwargs):
    def fakeorator(func):
        return func
    return fakeorator


class FactTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tbconfig = MagicMock(name='tbconfig')
        self.mock_pb = MagicMock(name='phim_backoffice')
        self.mock_pb.celery.app.task = fakeorator_arg
        self.mock_celery = MagicMock(name='celery')
        modules = {
            'tbconfig': self.mock_tbconfig,
            'phim_backoffice.datastore': self.mock_pb.datastore,
            'phim_backoffice.celery': self.mock_pb.celery,
            'phim_backoffice.helpers': self.mock_pb.helpers,
            'phim_backoffice.persist': self.mock_pb.persist,
            'phim_backoffice.state': self.mock_pb.state,
            'celery': self.mock_celery,
            'celery.utils': self.mock_celery.utils,
            'celery.utils.log': self.mock_celery.utils.log,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import phim_backoffice.fact
        self.fact = phim_backoffice.fact
        self.fact.POPERATOR_NAGIOS_DELAY_AFTER_COLLECTION = 2
        self.fact.datetime = MagicMock(name='fact.datetime')
        self.datastore_patch = patch.multiple(
            'phim_backoffice.fact',
            upsert_one_by_id=DEFAULT,
            find=DEFAULT,
            find_one=DEFAULT,
            delete_many=DEFAULT
        )
        self.mock_datastore = self.datastore_patch.start()

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()
        self.datastore_patch.stop()

    def test_get_host_sets_update(self):
        self.assertEqual(
            self.fact.get_host_sets_update({'scanner': 'somescan', 'address': '10.0.1.2'}, ['mod1', 'mod2']),
            {
                '$addToSet': {
                    'endpoints': {'scanner': 'somescan', 'address': '10.0.1.2'},
                    'modules': {'$each': ['mod1', 'mod2']}
                }
            }
        )

    def test_get_host_sets_update_no_endpoint(self):
        self.assertEqual(
            self.fact.get_host_sets_update(None, ['mod1', 'mod2']),
            {
                '$addToSet': {
                    'modules': {'$each': ['mod1', 'mod2']}
                }
            }
        )

    def test_get_host_sets_update_no_modules(self):
        self.assertEqual(
            self.fact.get_host_sets_update({'scanner': 'somescan', 'address': '10.0.1.2'}, None),
            {
                '$addToSet': {
                    'endpoints': {'scanner': 'somescan', 'address': '10.0.1.2'}
                }
            }
        )

    def test_get_host_sets_update_none(self):
        self.assertEqual(self.fact.get_host_sets_update(None, None), {})

    def test_get_host_update(self):
        self.fact.get_host_sets_update = MagicMock(name='get_host_sets_update', return_value={'$addToSet': 'value'})
        self.fact.find_one = MagicMock(name='find_one', return_value={"name": "N1", "Components": []})
        self.fact.guess_component_version = MagicMock(name='guess_component_version', return_value='4,4,4,4')
        self.fact.datetime.utcnow = MagicMock(name='datetime.utcnow', return_value='TIME')
        self.assertEqual(
            self.fact.get_host_update(
                {
                    'address': '10.0.2.3',
                    'modules': ['mod1', 'mod2'],
                    'manufacturer': 'IBM',
                    'Components': [{"name": "N1", "version": "N2"}],
                    'siteid': 'SITEID'

                },
                {'scanner': 'ISP', 'address': '10.2.2.2'},
                'IDENTIFIER'
            ),
            {'$set': {'manufacturer': 'IBM', 'Components': [{'timestamp': 'TIME', 'version': 'N2', 'name': 'N1'}],
                      'address': '10.0.2.3', 'siteid': 'SITEID'}, '$addToSet': 'value'}
        )
        self.fact.get_host_sets_update.assert_called_once_with(
            {'scanner': 'ISP', 'address': '10.2.2.2'}, ['mod1', 'mod2']
        )
        self.fact.guess_component_version.assert_called_once_with('SITEID', 'IntelliSpace PACS')

    def test_get_host_update_with_components(self):
        self.fact.get_host_sets_update = MagicMock(name='get_host_sets_update', return_value={'$addToSet': 'value'})
        self.fact.guess_component_version = MagicMock(name='guess_component_version', return_value='4,4,4,4')
        self.fact.datetime.utcnow = MagicMock(name='datetime.utcnow', return_value='TIME')
        self.fact.find_one = MagicMock(name='find_one',
                                       return_value={"name": "APP1", "version": "V2", "Components": [],
                                                     'siteid': 'SITEID'})
        self.fact.update_site_component_version = MagicMock(name='update_site_component_version')

        components = [{'name': 'APP2', 'version': 'V2'}]
        components_old = [{'name': 'APP1', 'version': 'V1'}]
        self.mock_datastore['find_one'].return_value = {'siteid': 'SITEID', 'Components': components_old}
        self.assertEqual(
            self.fact.get_host_update(
                {
                    'address': '10.0.2.3',
                    'modules': ['mod1', 'mod2'],
                    'manufacturer': 'IBM',
                    'Components': components,
                    'siteid': 'SITEID'
                },
                {'scanner': 'ISP', 'address': '10.2.2.2'},
                'SITEID'
            ),

            {'$set': {
                'Components': [
                    {'name': 'APP2', 'version': 'V2', 'timestamp': 'TIME'}],
                'manufacturer': 'IBM',
                'address': '10.0.2.3',
                'siteid': 'SITEID'
            },
                '$addToSet': 'value'
            }
        )
        self.fact.get_host_sets_update.assert_called_once_with(
            {'scanner': 'ISP', 'address': '10.2.2.2'}, ['mod1', 'mod2']
        )
        self.fact.update_site_component_version.assert_called_once_with('SITEID', {'pacs_version': '4,4,4,4','Components': [
            {'name': 'APP2', 'version': 'V2', 'timestamp': self.fact.datetime.utcnow.return_value}]})
        self.fact.guess_component_version.assert_called_once_with('SITEID', 'IntelliSpace PACS')

    def test_get_host_update_with_components_gd(self):
        self.fact.get_host_sets_update = MagicMock(name='get_host_sets_update', return_value={'$addToSet': 'value'})
        self.fact.guess_component_version = MagicMock(name='guess_component_version', return_value='4,4,4,4')
        self.fact.datetime.utcnow = MagicMock(name='datetime.utcnow', return_value='TIME')
        self.fact.find_one = MagicMock(name='find_one',
                                       return_value={"name": "APP1", "version": "V2", "Components": [],
                                                     'siteid': 'SITEID'})
        self.fact.update_site_component_version = MagicMock(name='update_site_component_version')
        components = [{'name': 'APP2', 'version': 'V2'}, {'version': 'p_version', 'name': 'p_name'}]
        components_old = [{'name': 'APP1', 'version': 'V1'}]
        self.mock_datastore['find_one'].return_value = {'siteid': 'SITEID', 'Components': components_old}
        self.assertEqual(
            self.fact.get_host_update(
                {
                    'address': '10.0.2.3',
                    'modules': ['mod1', 'mod2'],
                    'manufacturer': 'IBM',
                    'Components': components,
                    'product_name': 'p_name',
                    'product_version': 'p_version',
                    'siteid': 'SITEID'
                },
                {'scanner': 'ISP', 'address': '10.2.2.2'},
                'SITEID'
            ),
            {'$set': {
                'Components': [
                    {'name': 'APP2', 'version': 'V2', 'timestamp': 'TIME'},
                    {'name': 'p_name', 'version': 'p_version', 'timestamp': 'TIME'}],
                'manufacturer': 'IBM',
                'address': '10.0.2.3',
                'product_version': 'p_version',
                'product_name': 'p_name',
                'siteid': 'SITEID'
            },
                '$addToSet': 'value'
            }
        )
        self.fact.get_host_sets_update.assert_called_once_with(
            {'scanner': 'ISP', 'address': '10.2.2.2'}, ['mod1', 'mod2']
        )
        self.fact.update_site_component_version.assert_called_once_with('SITEID', {'pacs_version': '4,4,4,4','Components': [
            {'name': 'APP2', 'version': 'V2', 'timestamp': self.fact.datetime.utcnow.return_value},
            {'name': 'p_name', 'version': 'p_version', 'timestamp': self.fact.datetime.utcnow.return_value}
        ]})
        self.fact.guess_component_version.assert_called_once_with('SITEID', 'IntelliSpace PACS')

    def test_update_components_generic_discovery(self):
        host_facts = {'product_name': 'pn', 'product_version': 'v1'}
        self.assertEqual(self.fact.update_components_generic_discovery(host_facts),
                         {'product_version': 'v1', 'product_name': 'pn',
                          'Components': [{'version': 'v1', 'name': 'pn'}]})

        host_facts = {'product_name': 'NA', 'product_version': 'NA'}
        self.assertEqual(self.fact.update_components_generic_discovery(host_facts), host_facts)

        host_facts = {'product_name': 'pn', 'product_version': 'v1', 'extra': True}
        self.assertEqual(self.fact.update_components_generic_discovery(host_facts),
                         {'product_version': 'v1', 'product_name': 'pn',
                          'Components': [{'version': 'v1', 'name': 'pn'}], 'extra': True})

    def test_discovery_collect(self):
        self.fact.get_identified_host_dictionaries = MagicMock('get_identified_host_dictionaries')
        self.fact.get_identified_host_dictionaries.return_value = [
            ('ident1', {'hostname': 'host1'}),
            ('ident2', {'hostname': 'host2'})
        ]
        self.fact.get_host_update = MagicMock(name='get_host_update')
        self.fact.discovery_collect('site1', {'host1': {'key1': 'val1'}, 'host2': {'key2': 'val2'}})
        self.fact.get_identified_host_dictionaries.assert_called_once_with(
            'site1', {'host2': {'key2': 'val2'}, 'host1': {'key1': 'val1'}}
        )
        self.assertEqual(
            self.mock_datastore['upsert_one_by_id'].mock_calls,
            [
                call('FACTS', 'ident1', update=self.fact.get_host_update.return_value),
                call('FACTS', 'ident2', update=self.fact.get_host_update.return_value)
            ]
        )
        self.assertEqual(
            self.fact.get_host_update.mock_calls,
            [call({'hostname': 'host1'}, None, 'ident1'), call({'hostname': 'host2'}, None, 'ident2')]
        )
        self.fact.generate_from_facts.apply_async.assert_called_once_with(('site1', ['host2', 'host1']), countdown=2)

    def test_discovery_collect_with_endpoint(self):
        self.fact.get_identified_host_dictionaries = MagicMock('get_identified_host_dictionaries')
        self.fact.get_identified_host_dictionaries.return_value = [
            ('ident1', {'hostname': 'host1'}),
            ('ident2', {'hostname': 'host2'})
        ]
        self.fact.get_host_update = MagicMock(name='get_host_update')
        self.fact.discovery_purge_missing = MagicMock(name='discovery_purge_missing')
        self.fact.discovery_collect(
            'site1',
            {'host1': {'key1': 'val1'}, 'host2': {'key2': 'val2'}},
            {'scanner': 'one', 'address': '10.4.4.5'}
        )
        self.fact.get_identified_host_dictionaries.assert_called_once_with(
            'site1', {'host2': {'key2': 'val2'}, 'host1': {'key1': 'val1'}}
        )
        self.assertEqual(
            self.mock_datastore['upsert_one_by_id'].mock_calls,
            [
                call('FACTS', 'ident1', update=self.fact.get_host_update.return_value),
                call('FACTS', 'ident2', update=self.fact.get_host_update.return_value)
            ]
        )
        self.assertEqual(
            self.fact.get_host_update.mock_calls,
            [
                call({'hostname': 'host1'}, {'scanner': 'one', 'address': '10.4.4.5'}, 'ident1'),
                call({'hostname': 'host2'}, {'scanner': 'one', 'address': '10.4.4.5'}, 'ident2')
            ]
        )
        self.fact.generate_from_facts.apply_async.assert_called_once_with(('site1', ['host2', 'host1']), countdown=2)
        self.fact.discovery_purge_missing.delay.assert_called_once_with(
            'site1', ['host2', 'host1'], {'scanner': 'one', 'address': '10.4.4.5'}
        )

    def test_discovery_collect_with_monitor_flg_true(self):
        self.fact.get_identified_host_dictionaries = MagicMock('get_identified_host_dictionaries')
        self.fact.get_identified_host_dictionaries.return_value = [
            ('ident1', {'hostname': 'host1'}),
            ('ident2', {'hostname': 'host2'})
        ]
        _endpoint = {'scanner': 'one', 'address': '10.4.4.5', "monitor": True}
        self.fact.get_host_update = MagicMock(name='get_host_update')
        self.fact.discovery_purge_missing = MagicMock(name='discovery_purge_missing')
        self.fact.discovery_collect(
            'site1',
            {'host1': {'key1': 'val1'}, 'host2': {'key2': 'val2'}},
            _endpoint
        )
        self.fact.get_identified_host_dictionaries.assert_called_once_with(
            'site1', {'host2': {'key2': 'val2'}, 'host1': {'key1': 'val1'}}
        )
        self.assertEqual(
            self.mock_datastore['upsert_one_by_id'].mock_calls,
            [
                call('FACTS', 'ident1', update=self.fact.get_host_update.return_value),
                call('FACTS', 'ident2', update=self.fact.get_host_update.return_value)
            ]
        )
        self.assertEqual(
            self.fact.get_host_update.mock_calls,
            [
                call({'hostname': 'host1'}, _endpoint, 'ident1'),
                call({'hostname': 'host2'}, _endpoint, 'ident2')
            ]
        )
        self.fact.generate_from_facts.apply_async.assert_called_once_with(('site1', ['host2', 'host1']), countdown=2)
        self.fact.discovery_purge_missing.delay.assert_called_once_with(
            'site1', ['host2', 'host1'], _endpoint
        )

    def test_discovery_collect_with_monitor_flg_false(self):
        self.fact.get_identified_host_dictionaries = MagicMock('get_identified_host_dictionaries')
        self.fact.get_identified_host_dictionaries.return_value = [
            ('ident1', {'hostname': 'host1'}),
            ('ident2', {'hostname': 'host2'})
        ]
        _endpoint = {'scanner': 'one', 'address': '10.4.4.5', "monitor": False}
        self.fact.get_host_update = MagicMock(name='get_host_update')
        self.fact.discovery_purge_missing = MagicMock(name='discovery_purge_missing')
        self.fact.discovery_collect(
            'site1',
            {'host1': {'key1': 'val1'}, 'host2': {'key2': 'val2'}},
            _endpoint
        )
        self.fact.get_identified_host_dictionaries.assert_called_once_with(
            'site1', {'host2': {'key2': 'val2'}, 'host1': {'key1': 'val1'}}
        )
        self.assertEqual(
            self.mock_datastore['upsert_one_by_id'].mock_calls,
            [
                call('FACTS', 'ident1', update=self.fact.get_host_update.return_value),
                call('FACTS', 'ident2', update=self.fact.get_host_update.return_value)
            ]
        )
        self.assertEqual(
            self.fact.get_host_update.mock_calls,
            [
                call({'hostname': 'host1'}, _endpoint, 'ident1'),
                call({'hostname': 'host2'}, _endpoint, 'ident2')
            ]
        )
        self.assertEqual(self.fact.generate_from_facts.apply_async.call_count, 0)
        self.fact.discovery_purge_missing.delay.assert_called_once_with(
            'site1', ['host2', 'host1'], _endpoint
        )

    def test_discovery_purge_missing(self):
        self.mock_datastore['find'].return_value = [{'hostname': 'host1'}, {'hostname': 'host2'}]
        self.fact.discovery_purge_missing('site1', ['host2', 'host1'], {'scanner': 'one', 'address': '10.4.4.5'})
        self.mock_datastore['find'].assert_called_once_with(
            'FACTS',
            {
                'endpoints': {'scanner': 'one', 'address': '10.4.4.5'},
                'hostname': {'$nin': ['host2', 'host1']},
                'siteid': 'site1'
            }
        )
        self.fact.poperator_gen.delay.assert_called_once_with(
            data=self.mock_datastore['find'].return_value,
            data_type='NagiosNull',
            siteid='site1'
        )
        self.mock_datastore['delete_many'].assert_called_once_with(
            'FACTS',
            {
                'endpoints': {'scanner': 'one', 'address': '10.4.4.5'},
                'hostname': {'$nin': ['host2', 'host1']},
                'siteid': 'site1'
            }
        )
        self.fact.purge_extra_hosts_state.delay.assert_called_once_with(siteid='site1', hostnames=['host2', 'host1'])

    def test_discovery_purge_missing_no_extras(self):
        self.mock_datastore['find'].return_value = []
        self.fact.discovery_purge_missing('site1', ['host2', 'host1'], {'scanner': 'one', 'address': '10.4.4.5'})
        self.mock_datastore['find'].assert_called_once_with(
            'FACTS',
            {
                'endpoints': {'scanner': 'one', 'address': '10.4.4.5'},
                'hostname': {'$nin': ['host2', 'host1']},
                'siteid': 'site1'
            }
        )
        self.assertFalse(self.fact.poperator_gen.delay.called)
        self.assertFalse(self.mock_datastore['delete_many'].called)
        self.assertFalse(self.fact.purge_extra_hosts_state.delay.called)

    def test_get_host_facts(self):
        self.assertEqual(
            self.fact.get_host_facts('ABC01', 'host01', ['mod1', 'address']),
            self.mock_datastore['find_one'].return_value
        )
        self.fact.get_identifier.assert_called_once_with('ABC01', 'host01')
        self.mock_datastore['find_one'].assert_called_once_with(
            'FACTS',
            self.fact.get_identifier.return_value,
            {'mod1': True, '_id': False, 'address': True}
        )

    def test_get_host_facts_not_found(self):
        self.mock_datastore['find_one'].return_value = None
        self.assertEqual(
            self.fact.get_host_facts('ABC01', 'host01', ['mod1', 'address']),
            {}
        )
        self.fact.get_identifier.assert_called_once_with('ABC01', 'host01')
        self.mock_datastore['find_one'].assert_called_once_with(
            'FACTS',
            self.fact.get_identifier.return_value,
            {'mod1': True, '_id': False, 'address': True}
        )

    def test_temp_host(self):
        host_data={
          "powerState": "poweredOff",
          "guest": {
                    "guestFamily": "linuxGuest",
                    "hostName": "sln36sln36.stsln36.isyntax.net"
                   }
           }
        ip = '1.1.1.1'
        hostname = 'testhost'
        return_val = {
                      "testhost": {
                        "product_version": "NA",
                        "powerState": "poweredOff",
                        "product_id": "NA",
                        "address": "1.1.1.1",
                        "model": "NA",
                        "vmHost": {
                          "powerstate": "poweredOff",
                          "endpoint": {
                            "scanner": "vCenter",
                            "address": "1.1.1.1"
                          },
                          "type": "Linux"
                        },
                        "modules": [
                          "Linux"
                        ],
                        "product_name": "NA",
                        "guest": {
                          "hostName": "sln36sln36.stsln36.isyntax.net",
                          "guestFamily": "linuxGuest"
                        },
                        "manufacturer": "NA"
                      }
                    }
        self.assertEqual(
            self.fact.temp_host(ip, hostname, host_data), return_val)

    def test_fact_gen(self):
        ab=[('testhost', '1.1.1.1')]
        bc=[{'powerState': 'poweredOff', 'guest': {'hostName': 'testhost', 'guestFamily': 'linuxGuest'}}]
        return_val = {
                      "testhost": {
                        "product_version": "NA",
                        "powerState": "poweredOff",
                        "product_id": "NA",
                        "address": "1.1.1.1",
                        "model": "NA",
                        "vmHost": {
                          "powerstate": "poweredOff",
                          "endpoint": {
                            "scanner": "vCenter",
                            "address": "1.1.1.1"
                          },
                          "type": "Linux"
                        },
                        "modules": [
                          "Linux"
                        ],
                        "product_name": "NA",
                        "guest": {
                          "hostName": "testhost",
                          "guestFamily": "linuxGuest"
                        },
                        "manufacturer": "NA"
                      }
                    }
        self.assertEqual(
            self.fact.fact_gen(ab, bc), return_val)


if __name__ == '__main__':
    unittest.main()
