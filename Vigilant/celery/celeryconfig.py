# Broker settings.

# BROKER_URL = 'amqp://'
BROKER_URL = 'amqp://sb.phim.isyntax.net'

# List of modules to import when celery starts.
CELERY_IMPORTS = ('phim_backoffice.fact', 'phim_backoffice.routers', 'phim_backoffice.perfdata',
                  'phim_backoffice.datastore', 'phim_backoffice.dashboard', 'phim_backoffice.descriptor',
                  'phim_backoffice.transport', 'phim_backoffice.event', 'phim_backoffice.state', 'phim_backoffice.persist',
                  'phim_backoffice.subscription', 'phim_backoffice.audit', 'phim_backoffice.billing.statistics')

# disable if not needed (best practice)
CELERY_DISABLE_RATE_LIMITS = True

# name accepted content (best practice)
CELERY_ACCEPT_CONTENT = ['pickle', 'json', 'yaml']
