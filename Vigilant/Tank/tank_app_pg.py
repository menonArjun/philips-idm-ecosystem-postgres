#!/usr/bin/env python

from bottle import Bottle, BaseRequest
from tank.tnconfig import PGRES, PGRESlocal
from tank.util import get_logger
from tank.controllers.pg import \
    (facts,
     site,
     dashboardp,
     country,
     events,
     state,
     audit,
     info,
     tags,
     subscription,
     field)
# the mock-0.3.1 dir contains testcase.py, testutils.py & mock.py

log = get_logger()


def get_app():
    app = Bottle()          #Initialize Bottle App instance
    params_pg= "postgres://{user}:{password}@{url}:{port}/{db}".format(**PGRESlocal)  # DB Connection URI specification
    app.mount('/facts/', facts.get_app(params_pg))          # Mount Controllers which in turn initializes Connection Ob
    app.mount('/site/', site.get_app(params_pg))
    app.mount('/dashboard/', dashboardp.get_app(params_pg))
    app.mount('/country/', country.get_app(params_pg))
    app.mount('/event/', events.get_app(params_pg))
    app.mount('/state/', state.get_app(params_pg))
    app.mount('/audit/', audit.get_app(params_pg))
    app.mount('/info/', info.get_app(params_pg))
    app.mount('/tags/', tags.get_app(params_pg))
    app.mount('/subscription/', subscription.get_app(params_pg))
    app.mount('/field/', field.get_app(params_pg))
    return app


def health():
    log.debug('Returning Health')
    return 'OK'


BaseRequest.MEMFILE_MAX = 1024 * 512 * 2        # Only Relevant to logging (Incomplete, validate and complete logging)
app = get_app()     # App call to start Tank

if __name__ == '__main__':
    # log.info('MemFileMax set to %s', BaseRequest.MEMFILE_MAX)
    app.run(host='127.0.0.1', port=8080, debug=True)        # Endpoint Address definition for Tank_app

