import unittest
from mock import patch, MagicMock, call


class FakeQueryBuilder(object):
    def __init__(self, query):
        self.query = query


class TankEventModelTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_tank.util.QueryBuilder = FakeQueryBuilder
        self.mock_bottle = MagicMock(name='bottle')
        modules = {
            'tank.util': self.mock_tank.util,
            'tank.models.site': self.mock_tank.models.site,
            'bottle': self.mock_bottle
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.models import event
        self.event = event
        self.event.COLLECTIONS = {'EVENTS': 'events'}
        self.mongodb = MagicMock(name='mongo_db')
        self.model = self.event.EventModel(self.mongodb)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_matches(self):
        self.model.filter = {'filter': 'please'}
        self.model.get_projection = MagicMock(name='get_projection')
        self.assertEqual(self.model.get_matches(), self.model.collection.aggregate.return_value)
        self.model.collection.aggregate.assert_called_once_with([
            {'$match': self.model.filter},
            {'$project': self.model.get_projection.return_value},
            {'$sort': {'timestamp': 1}}
        ])

    def test_get_historical(self):
        self.model.get_matches = MagicMock(name='get_matches', return_value=iter(['one', 'two']))
        self.event.EventQuery = MagicMock(name='EventQuery')
        self.assertEqual(self.model.get_historical({'key1': 'val1'}), {'events': ['one', 'two']})
        self.assertEqual(self.model.filter, self.event.EventQuery.return_value.get_filter.return_value)
        self.event.EventQuery.assert_called_once_with({'key1': 'val1'})

    def test_get_projection(self):
        self.assertEqual(
            self.model.get_projection(),
            {
                'state': '$state',
                'service': '$service',
                'timestamp': self.event.get_date_to_string_field_projection.return_value,
                'siteid': '$siteid',
                'hostaddress': '$hostaddress',
                'hostname': '$hostname',
                'output': '$output',
                'type': '$type',
                'components': '$components',
                'modules': '$modules',
                '_id': 0
            }
        )
        self.event.get_date_to_string_field_projection.assert_called_once_with('$timestamp')

    def test_event_query_process_query(self):
        query_builder = self.event.EventQuery({'key1': 'val2'})
        query_builder.get_between_dates_filter = MagicMock(name='get_between_dates_filter')
        query_builder.query = {'start_date': '2012-12-12', 'end_date': '2013-04-05'}
        query_builder.process_query()
        self.assertEqual(
            query_builder.query,
            {'timestamp': query_builder.get_between_dates_filter.return_value}
        )
        query_builder.get_between_dates_filter.assert_called_once_with('2012-12-12', '2013-04-05')

    def test_get_affected_siteids(self):
        self.model.collection.distinct.return_value = iter(['one', 'two'])
        self.assertEqual(self.model.get_affected_siteids(), ['one', 'two'])

    def test_site_names(self):
        self.model.get_affected_siteids = MagicMock(name='get_affected_siteids')
        self.assertEqual(
            self.model.site_names,
            self.event.SiteModel.return_value.get_site_names.return_value
        )
        self.event.SiteModel.assert_called_once_with(self.mongodb)
        self.event.SiteModel.return_value.get_site_names.assert_called_once_with(
            self.model.get_affected_siteids.return_value
        )

    def test_get_handler_matches(self):
        self.model.filter = {'filter': 'please'}
        self.assertEqual(
            self.model.get_handler_matches(),
            self.model.collection.aggregate.return_value
        )
        self.model.collection.aggregate.assert_called_once_with([
            {'$match': self.model.filter},
            {
                '$group': {
                    '_id': {'hostname': '$hostname','siteid': '$siteid','timestamp': '$timestamp'},
                    'success': {'$sum': {'$cond': [{'$eq': ['$state', 'OK']}, 1, 0]}},
                    'failure': {'$sum': {'$cond': [{'$eq': ['$state', 'WARNING']}, 1, 0]}},
                    'total': {'$sum': 1}
                }
            },
            {
                '$group': {
                    '_id': '$_id.siteid',
                    'success': {'$sum': '$success'},
                    'failure': {'$sum': '$failure'},
                    'total': {'$sum': '$total'},
                    'hosts': {
                        '$push': {
                            'hostname': '$_id.hostname', 'success': '$success', 'failure': '$failure',
                            'total': '$total',
                            'timestamp': { '$dateToString' : {'format' : "%Y/%m/%d %H:%M:%S", 'date': "$_id.timestamp"}}
                         }
                    }
                }
             }
        ])

    def test_adjust_handler_entry(self):
        self.model._site_names = {'site1': 'the first site'}
        self.event.rename_id.return_value = {'siteid': 'site1'}
        self.assertEqual(
            self.model.adjust_handler_entry({'_id':'site1'}),
            {'siteid':'site1', 'name': 'the first site'}
        )
        self.event.rename_id.assert_called_once_with('siteid', {'_id': 'site1'})

    def test_get_handler_report(self):
        self.model.adjust_handler_entry = MagicMock(name='adjust_handler_entry')
        self.model.get_handler_matches = MagicMock(name='get_handler_matches', return_value=iter(['one', 'two']))
        self.event.EventQuery = MagicMock(name='EventQuery')
        self.event.EventQuery.return_value.get_filter.return_value = {'key2': 'val2'}
        self.assertEqual(
            self.model.get_handler_report('service1', {'key1': 'val1'}),
            {'result': [self.model.adjust_handler_entry.return_value, self.model.adjust_handler_entry.return_value]}
        )
        self.assertEqual(self.model.adjust_handler_entry.mock_calls, [call('one'), call('two')])
        self.assertEqual(self.model.filter, {'key2': 'val2', 'state': {'$in': ['OK', 'WARNING']}, 'type': 'HANDLER'})
        self.event.EventQuery.assert_called_once_with({'key1': 'val1', 'service': 'service1'})


if __name__ == '__main__':
    unittest.main()
