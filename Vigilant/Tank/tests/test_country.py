import unittest

from mock import patch, MagicMock


class TankCountryModelTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        modules = {'tank.util': self.mock_tank.util}
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.models import country
        self.country = country
        self.country.COLLECTIONS = {'COUNTRY': 'country'}
        self.mongodb = MagicMock(name='mongo_db')
        self.model = self.country.CountryModel(self.mongodb)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_data(self):
        self.model.collection.find.return_value = iter(
            [
                {'market': 'AFRICA', 'sap_code': 'KG', 'country': 'COUNTRY1'},
                {'market': 'AFRICA', 'sap_code': 'KG', 'country': 'COUNTRY2'}
            ]
        )
        self.assertEqual(
            self.model.get_countries(), 
            {'result': [
                {'market': 'AFRICA', 'sap_code': 'KG', 'country': 'COUNTRY1'},
                {'market': 'AFRICA', 'sap_code': 'KG', 'country': 'COUNTRY2'}
            ]}
        )
        self.model.collection.find.assert_called_once_with(
            {}, {'country': '$country', '_id': 0, 'sap_code': '$sap_code', 'market': '$market'}
        )


if __name__ == '__main__':
    unittest.main()
