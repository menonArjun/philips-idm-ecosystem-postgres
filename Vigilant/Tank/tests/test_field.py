import unittest

from mock import patch, MagicMock
from datetime import datetime


class FakeQueryBuilder(object):
    def __init__(self, query):
        self.query = query

class TankFieldModelTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_tank.util.QueryBuilder = FakeQueryBuilder
        self.mock_bottle = MagicMock(name='bottle')
        modules = {
            'tank.util': self.mock_tank.util,
            'bottle': self.mock_bottle
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        from tank.models import field
        self.field = field
        self.hosts_with_component = [
            {
                'siteid': 'site9',
                'hosts': [
                    {
                        'hostname': 'host2.site9.isyntax.net',
                        'version': '4,4,2,1'
                    },
                    {
                        'hostname': 'host3.site9.isyntax.net',
                        'version': '4,4,2,1'
                    },
                    {
                        'hostname': 'host2.site9.isyntax.net',
                        'version': '4,4,3,1'
                    }
                ]
            },
            {
                'siteid': 'site2',
                'hosts': [
                    {
                        'hostname': 'host2.site2.isyntax.net',
                        'version': '3,6,22,1'
                    },
                    {
                        'hostname': 'host3.site2.isyntax.net',
                        'version': '4,4,3,1'
                    },

                ]


            }
        ]

        self.mongodb = MagicMock(name='mongo_db')
        self.model = self.field.FieldModel(self.mongodb)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_component_ual(self):
        self.field.is_version_match.side_effect = [True, True, True, False, True]
        result = self.model.get_component_ual(self.hosts_with_component, '4,4')
        self.assertEqual(
            result,
            [
                {
                    'siteid': 'site9',
                    'hosts': [
                        {'hostname': 'host2.site9.isyntax.net', 'version': '4,4,2,1'},
                        {'hostname': 'host3.site9.isyntax.net', 'version': '4,4,2,1'},
                        {'hostname': 'host2.site9.isyntax.net', 'version': '4,4,3,1'}
                    ]
                },
                {
                    'siteid': 'site2',
                    'hosts': [
                        {'hostname': 'host3.site2.isyntax.net', 'version': '4,4,3,1'}
                    ]
                }
            ]
        )

    def test_get_component_ual_no_match(self):
        self.field.is_version_match.return_value = False
        result = self.model.get_component_ual(self.hosts_with_component, '4,4')
        self.assertEqual(result, [])

    def test_get_ual_component_version(self):
        self.model.get_component_ual = MagicMock(name='get_component_ual')
        self.assertEqual(
            self.model.get_ual_component_version('ISP', '4,4'),
            self.model.get_component_ual.return_value
        )
        self.model.collection.aggregate.assert_called_once_with(
            [
                {'$match': {'Components.name': 'ISP'}},
                {'$project': {'_id': 0, 'siteid': 1, 'hostname': 1, 'Components': 1}},
                {'$unwind': '$Components'},
                {'$match': {'Components.name': 'ISP'}},
                {'$group': {
                    '_id': '$siteid',
                    'hosts': {'$push': {'hostname': '$hostname', 'version': '$Components.version'}}
                }},
                {'$project': {'hosts': 1, '_id': 0, 'siteid': '$_id'}}
            ]
        )
        self.model.get_component_ual.assert_called_once_with(self.model.collection.aggregate.return_value, '4,4')

    def test_get_component_details(self):
        self.field.FieldQuery = MagicMock(name='FieldQuery')
        self.field.FieldQuery.return_value.get_filter.return_value = {}
        self.model.collection.aggregate.return_value = [{'siteid':'XX','Components':[{'verion':'V1' ,'name':'N','timestamp':'2011/02/23  00:00:00'}]}]
        self.assertEqual(self.model.get_component_details({'productid':'PID','product':'PID'}), {'result': [
            {'siteid': 'XX', 'Components': [{'verion': 'V1', 'name': 'N', 'timestamp': '2011/02/23  00:00:00'}]}]})
        self.model.collection.aggregate.assert_called_once_with(
            [
                {'$project': {'_id': 0, 'siteid': 1, 'Components': 1, 'Windows.tags': 1}},
                {'$unwind': '$Components'},
                {'$sort': {'Components.timestamp': -1}},
                {'$match': {}},
                {'$group': {'_id': '$siteid', 'Components': {'$push': {'name': '$Components.name', 'version': '$Components.version', 'tag': '$Windows.tags',
                                                                       'timestamp': {'$dateToString': {'format': "%Y/%m/%d %H:%M:%S", 'date': '$Components.timestamp'}}}}}},
                {'$project': {'_id': 0, 'siteid': '$_id', 'Components': 1}}
            ]
        )

    def test_get_component_details_with_filter(self):
        query={'siteid':'XXX','Components.name':'WINDOWS'}
        self.field.FieldQuery = MagicMock(name='FieldQuery')
        self.field.FieldQuery.return_value.get_filter.return_value = query
        self.model.collection.aggregate.return_value = [
            {'siteid': 'XX', 'Components': [{'verion': 'V1', 'name': 'N', 'timestamp': '2011/02/23  00:00:00'}]}]
        self.assertEqual(self.model.get_component_details(query),{'result':[{'siteid':'XX','Components':[{'verion':'V1' ,'name':'N','timestamp': '2011/02/23  00:00:00' }]}]})
        self.model.collection.aggregate.assert_called_once_with(
            [
                {'$project': {'_id': 0, 'siteid': 1, 'Components': 1, 'Windows.tags':1}},
                {'$unwind': '$Components'},
                {'$sort': {'Components.timestamp': -1}},
                {'$match':{'siteid':'XXX','Components.name':'WINDOWS'}},
                {'$group': {'_id': '$siteid', 'Components': {
                    '$push': {'name': '$Components.name', 'version': '$Components.version', 'tag': '$Windows.tags',
                              'timestamp': {'$dateToString': {'format': "%Y/%m/%d %H:%M:%S",
                                                              'date': '$Components.timestamp'}}}}}},
                {'$project': {'_id': 0, 'siteid': '$_id', 'Components': 1}}
            ]
        )

    def test_get_component_details_with_productid__filter(self):
        query = {'siteid': 'XXX', 'productid': 'PID'}
        self.field.FieldQuery = MagicMock(name='FieldQuery')
        self.field.FieldQuery.return_value.get_filter.return_value = query
        self.model.collection.aggregate.return_value = [
            {'siteid': 'XX', 'Components': [{'verion': 'V1', 'name': 'N', 'timestamp': '2011/02/23  00:00:00'}]}]
        self.assertEqual(self.model.get_component_details(query), {'result': [
            {'siteid': 'XX', 'Components': [{'verion': 'V1', 'name': 'N', 'timestamp': '2011/02/23  00:00:00'}]}]})
        self.model.collection.aggregate.assert_called_once_with(
            [
                {'$project': {'_id': 0, 'siteid': 1, 'Components': 1, 'Windows.tags':1}},
                {'$unwind': '$Components'},
                {'$sort': {'Components.timestamp': -1}},
                {'$match': {'$or': [{'Components.name': 'PID'}, {'product_name': 'PID'}],'siteid': 'XXX'}},
                {'$group': {'_id': '$siteid', 'Components': {
                    '$push': {'name': '$Components.name', 'version': '$Components.version', 'tag': '$Windows.tags',
                              'timestamp': {'$dateToString': {'format': "%Y/%m/%d %H:%M:%S",
                                                              'date': '$Components.timestamp'}}}}}},
                {'$project': {'_id': 0, 'siteid': '$_id', 'Components': 1}}
            ]
        )

    def test_get_unique_components(self):
        return_values = ['one','two']
        self.model.collection.distinct.return_value=return_values
        self.assertEqual(self.model.get_unique_components(),{'components':return_values})
        self.model.collection.distinct.assert_called_once_with('Components.name')

    def test_get_unique_models(self):
        self.model.get_unique_models()
        self.model.collection.distinct.assert_called_once_with('product_id')

    def test_get_unique_product_names(self):
        self.model.get_unique_product_names()
        self.model.collection.distinct.assert_called_once_with('product_name')

if __name__ == '__main__':
    import sys
    sys.path.append(r'D:\workspace\office\multiprod_update_philips-idm-ecosystem\Vigilant\Tank')
    unittest.main()