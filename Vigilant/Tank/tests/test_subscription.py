import re
import unittest

from mock import patch, MagicMock, call


class TankSubscriptionsModelTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        modules = {'tank.util': self.mock_tank.util}
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        from tank.models import subscription
        self.subscription = subscription
        self.subscription.COLLECTIONS = {'SUBSCRIPTIONS': 'subscriptions'}
        self.subscription.DEFAULT_SUBSCRIPTION_RULES = ['magicrule']

        self.mongodb = MagicMock(name='mongo_db')
        self.model = subscription.SubscriptionModel(self.mongodb)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_slashed_path_none(self):
        self.model.path = None
        self.assertEqual(self.model.slashed_path, '/')

    def test_slashed_path_empty(self):
        self.assertEqual(self.model.slashed_path, '/')

    def test_get_direct_descendant_groups_empty_subs(self):
        self.assertEqual(self.model.get_direct_descendant_groups('/test/', []), set([]))

    def test_get_direct_descendant_groups_no_subs(self):
        self.assertEqual(self.model.get_direct_descendant_groups('/test/', ['/nope/test', '/path2']), set([]))

    def test_get_direct_descendant_groups_one_subs(self):
        sub_paths = ['/nope/test', '/test/path2']
        self.assertEqual(self.model.get_direct_descendant_groups('/test/', sub_paths), set(['path2']))

    def test_get_direct_descendant_groups_two_and_repeat_subs(self):
        sub_paths = ['/nope/test', '/test/path2', '/test/path3/why', '/test/path2', ]
        self.assertEqual(self.model.get_direct_descendant_groups('/test/', sub_paths), set(['path2', 'path3']))

    def test_get_direct_descendant_groups_two_deep_subs(self):
        sub_paths = ['/test/path2/deep', '/test/path3/why']
        self.assertEqual(self.model.get_direct_descendant_groups('/test/', sub_paths), set(['path2', 'path3']))

    def test_get_direct_descendant_groups_deep_path(self):
        sub_paths = ['/test/path2/deep', '/test/path3/why']
        self.assertEqual(self.model.get_direct_descendant_groups('/test/path3/', sub_paths), set(['why']))

    def test_get_starts_with_regex(self):
        regex = self.model.get_starts_with_regex('/test')
        self.assertTrue(re.match(regex, '/test/path3/'))

    def test_get_starts_with_regex_deep(self):
        regex = self.model.get_starts_with_regex('/test/now')
        self.assertTrue(re.match(regex, '/test/now/path3/'))

    def test_get_starts_with_regex_false(self):
        regex = self.model.get_starts_with_regex('/tesx')
        self.assertFalse(re.match(regex, '/test/path3/'))

    def test_get_starts_with_regex_does_not_start(self):
        regex = self.model.get_starts_with_regex('/test')
        self.assertFalse(re.match(regex, '/root/test/path3/'))

    def test_get_path_equal_or_starts_with_regex(self):
        regex = self.model.get_path_equal_or_starts_with_regex('test')
        self.assertTrue(re.match(regex, '/test/path3/'))

    def test_get_path_equal_or_starts_with_regex_match_with_slash(self):
        regex = self.model.get_path_equal_or_starts_with_regex('test')
        self.assertTrue(re.match(regex, '/test/'))

    def test_get_path_equal_or_starts_with_regex_match_without_slash(self):
        regex = self.model.get_path_equal_or_starts_with_regex('test')
        self.assertTrue(re.match(regex, '/test'))

    def test_get_path_equal_or_starts_with_regex_not_start(self):
        regex = self.model.get_path_equal_or_starts_with_regex('test')
        self.assertFalse(re.match(regex, '/root/test'))

    def test_get_groups(self):
        self.model.path = '/path3'
        self.model.get_direct_descendant_groups = MagicMock(
            name='get_direct_descendant_groups', return_value=['sub1', 'sub2']
        )
        self.assertEqual(self.model.get_groups(), {'groups': ['sub1', 'sub2']})
        self.model.collection.distinct.assert_called_once_with('path', {'path': {'$regex': '^/path3/'}})
        self.model.get_direct_descendant_groups.assert_called_once_with(
            '/path3/',
             self.model.collection.distinct.return_value
        )

    def test_get_names(self):
        self.model.path = '/path4'
        self.model.collection.distinct.return_value = ['sub3', 'sub4']
        self.assertEqual(self.model.get_names(), {'subscriptions': ['sub3', 'sub4']})
        self.model.collection.distinct.assert_called_once_with('_id', {'path': {'$regex': '^/path4(/|$)'}})

    def test_get_subscriptions(self):
        self.model.filter = {'some': 'value'}
        self.model.collection.find.return_value = ['one', 'two']
        self.model.reformat_raw = MagicMock(name='reformat_raw')
        self.assertEqual(
            self.model.get_subscriptions(),
            {'subscriptions': [self.subscription.rename_id.return_value, self.subscription.rename_id.return_value]}
        )
        self.model.collection.find.assert_called_once_with({'some': 'value'})
        self.assertEqual(
            self.subscription.rename_id.mock_calls,
            [call('subscription', 'one'), call('subscription', 'two')]
        )

    def test_get_subscription(self):
        self.assertEqual(self.model.get_subscription('sub1'), self.model.collection.find_one.return_value)
        self.model.collection.find_one.assert_called_once_with('sub1')


if __name__ == '__main__':
    unittest.main()
