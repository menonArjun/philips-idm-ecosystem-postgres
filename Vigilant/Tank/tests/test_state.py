import unittest

from mock import patch, MagicMock


class TankStateModelTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        modules = {'tank.util': self.mock_tank.util}
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.models import state
        self.state = state
        self.state.COLLECTIONS = {'STATES': 'states'}
        self.mongodb = MagicMock(name='mongo_db')
        self.model = self.state.StateModel(self.mongodb, 'YY123')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_state(self):
        self.model.collection.find_one.return_value = {'key1': 'val1'}
        self.assertEqual(self.model.get_state('host1', 'key1'), {'result': 'val1'})
        self.model.collection.find_one.assert_called_once_with(
            {'siteid': 'YY123', 'hostname': 'host1'},
            self.state.get_projection.return_value
        )
        self.state.get_projection.assert_called_once_with(['key1'])

    def test_get_siteids(self):
        self.model.collection.distinct.return_value = iter(['ABC00', 'YXR44'])
        self.assertEqual(self.model.get_siteids(), {'siteids': ['ABC00', 'YXR44']})
        self.model.collection.distinct.assert_called_once_with('siteid')

    def test_get_mappings(self):
        self.model.get_siteids = MagicMock(name='get_siteids')
        self.model.get_siteids.return_value = {'siteids': ['SITE1', 'SITE2']}
        self.model.get_mappings('HOSTNAME')
        self.model.collection.aggregate.assert_called_once_with(
            [
                {'$match': {'hostname': 'HOSTNAME', 'siteid': {'$in': ['SITE1','SITE2']}}},
                {'$group': {'_id': {'siteid': '$siteid', 'ip': '$Administrative__Philips__Host__Information__IPAddress'}}},
                {'$project': {'_id': 0, 'siteid': '$_id.siteid', 'address': '$_id.ip'}}
            ]
        )


if __name__ == '__main__':
    unittest.main()
