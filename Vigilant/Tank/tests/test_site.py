import re
import unittest
from mock import patch, MagicMock, call


class TankSiteModelTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_tank.util.date = MagicMock(name='datetime')
        modules = {'tank.util': self.mock_tank.util}
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.models import site
        self.site = site
        self.site.COLLECTIONS = {'SITES': 'sites'}
        self.mongodb = MagicMock(name='mongo_db')
        self.model = self.site.SiteModel(self.mongodb)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_siteids(self):
        self.model.collection.distinct.return_value = iter(['ABC00', 'YXR44'])
        self.assertEqual(self.model.get_siteids(), {'siteids': ['ABC00', 'YXR44']})
        self.model.collection.distinct.assert_called_once_with('_id')

    def test_set_siteid_filter(self):
        self.model.set_siteid_filter(['site1', 'site2'])
        self.assertEqual(self.model.filter, {'_id': {'$in': ['site1', 'site2']}})

    def test_set_siteid_filter_none(self):
        self.model.set_siteid_filter(None)
        self.assertEqual(self.model.filter, {})

    def test_set_country_filter(self):
        self.model.set_country_filter(['usa', 'mx'])
        self.assertEqual(self.model.filter, {'country.country': {'$in': ['usa', 'mx']}})

    def test_set_country_filter_none(self):
        self.model.set_country_filter(None)
        self.assertEqual(self.model.filter, {})

    def test_get_site_names(self):
        self.model.set_siteid_filter = MagicMock(name='get_siteid_filter')
        self.model.collection.find.return_value = [
            {'_id': 'site1', 'name': 'the first site'},
            {'_id': 'site2', 'name': 'the second site'},
        ]
        self.assertEqual(
            self.model.get_site_names(['ABC01', 'XYZ00']),
            {'site2': 'the second site', 'site1': 'the first site'}
        )
        self.model.set_siteid_filter.assert_called_once_with(['ABC01', 'XYZ00'])
        self.model.collection.find.assert_called_once_with(self.model.filter, {'name': 1})

    def test_get_sites(self):
        self.model.set_siteid_filter = MagicMock(name='get_siteid_filter')
        self.model.set_country_filter = MagicMock(name='set_country_filter')
        self.model.collection.find.return_value = [{'_id': 'one', 'siteid': 'one'}, {'_id': 'two', 'siteid': 'two'}]
        self.model.rename_id = MagicMock(name='rename_id')
        self.assertEqual(
            self.model.get_sites(['ABC01', 'XYZ00'], ['usa']),
            {'sites': [self.site.rename_id.return_value, self.site.rename_id.return_value]}
        )
        self.model.set_siteid_filter.assert_called_once_with(['ABC01', 'XYZ00'])
        self.model.set_country_filter.assert_called_once_with(['usa'])
        self.model.collection.find.assert_called_once_with(self.model.filter, {'Components': 0})
        self.assertEqual(self.site.rename_id.mock_calls, [call('siteid', {'_id': 'one', 'siteid': 'one'}),
                                                          call('siteid', {'_id': 'two', 'siteid': 'two'})])

    def test_get_subscription_candidates(self):
        self.site.SubscriptionModel = MagicMock(name='SubscriptionModel')
        self.site.SubscriptionModel.return_value.get_subscription.return_value = {
            'countries': ['usa', 'mx'], 'name': 'av-1.4'
        }
        self.model.get_sites = MagicMock(name='get_sites')
        self.assertEqual(self.model.get_subscription_candidates('av-1.4'), self.model.get_sites.return_value)
        self.model.get_sites.assert_called_once_with(countries=['usa', 'mx'])

    def test_update_site(self):
        self.model.update_site(siteid='SS', name='XX', country={'country': 'CC', 'market': 'MM'})
        self.model.collection.update_one.assert_called_once_with(
            {'_id': 'SS'},
            {'$set': {'country': {'country': 'CC', 'market': 'MM'}, 'name': 'XX'}},
            upsert=False
        )

    def test_delete_site(self):
        self.model.delete_site('SS')
        self.model.collection.delete_one.assert_called_once_with({'_id': 'SS'})

    def test_get_facts_hosts_details(self):
        self.model.get_sites = MagicMock(name='get_sites')
        self.model.get_sites.return_value = {'sites': [
            {'name': 'site1', 'pacs_version': '1',
             'country': {'country': 'country1', 'market': 'market1'}, 'siteid': 'S1'},
            {'name': 'site2', 'pacs_version': '1',
             'country': {'country': 'country2', 'market': 'market2'}, 'siteid': 'S2'}
        ]}
        self.site.DashboardModel = MagicMock(name='DashboardModel')
        self.site.DashboardModel.return_value.get_matches.return_value = {'result': [{'siteid': 'S2',
                                                                                      'name': 'site2',
                                                                                      'pacs_version': '1'}]}
        self.site.SiteFactsModel = MagicMock(name='SiteFactsModel')
        self.site.SiteFactsModel.return_value.get_facts_hosts.return_value = [
            {'_id': 'S1', 'result': [{'hostname': 'host1'}, {
                "hostname": "host2"
            }, {
                                         "hostname": "host3",
                                         "vCenter": {"vms": [
                                             {
                                                 "powerState": "poweredOff",
                                                 "guest": {
                                                     "hostName": "host4",
                                                     "ipAddress": "ip1",
                                                 }
                                             }
                                         ]}}]}]
        self.assertEqual(self.model.get_facts_hosts_details(), {'result': [{'host_count': 4,
                                                                            'country': {'country': 'country1',
                                                                                        'market': 'market1'},
                                                                            'siteid': 'S1',
                                                                            'name': 'site1',
                                                                            'pacs_version': '1',
                                                                            'status': 'Up'},
                                                                           {'host_count': 0,
                                                                            'country': {'country': 'country2',
                                                                                        'market': 'market2'},
                                                                            'siteid': 'S2',
                                                                            'name': 'site2',
                                                                            'pacs_version': '1',
                                                                            'status': 'Down'}
                                                                           ]})

    def test_get_host_count(self):
        vCenters = [{"vms": [
            {
                "guest": {
                    "hostName": 'host1',
                }
            },
            {
                "guest": {
                    "hostName": 'Host2',
                }
            },
            {
                "guest": {
                    "hostName": "localhost",
                }
            }
        ]}]
        localhost_regex = re.compile('localhost')
        hostnames = ['host2']
        self.assertEqual(self.model.get_host_count(vCenters, localhost_regex, hostnames), 1)

    def test_get_site_count(self):
        self.model.get_site_count()
        self.model.collection.distinct.assert_called_once_with('_id',{'production':{'$ne': False}})

if __name__ == '__main__':
    unittest.main()
