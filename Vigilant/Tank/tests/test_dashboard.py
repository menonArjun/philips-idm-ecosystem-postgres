import re
import unittest

from mock import patch, MagicMock


class TankDashboardModelTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_bottle = MagicMock(name='bottle')
        self.mock_tank.util.date = MagicMock(name='datetime')
        modules = {
            'tank.util': self.mock_tank.util,
            'bottle': self.mock_bottle
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.models import dashboard
        self.dashboard = dashboard
        self.dashboard.COLLECTIONS = {'DASHBOARD': 'dashboard'}
        self.mongodb = MagicMock(name='mongo_db')
        self.dashboard.datetime = MagicMock(name='dashboard.datetime')
        self.model= self.dashboard.DashboardModel(self.mongodb)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_matches(self):
        self.model.collection.aggregate.return_value = iter(['valA', 'valB'])
        self.model.get_projection = MagicMock(name='get_projection')
        self.model.filter = {'filter': 'please'}
        self.assertEqual(self.model.get_matches(), {'result': ['valA', 'valB']})
        self.model.collection.aggregate.assert_called_once_with(
            [{'$match': {'filter': 'please'}}, {'$project': self.model.get_projection.return_value}]
        )

    def test_get_dashboard(self):
        self.dashboard.DashboardQuery = MagicMock(name='DashboardQuery')
        self.dashboard.DashboardQuery.return_value.get_filter.return_value = {'siteid': 'AMC01'}
        self.model.get_matches = MagicMock(name='get_matches')
        query = {'siteid': 'AMC01', 'state': 'CRITICAL', 'service': 'Admin', 'category': 'namespace.category'}
        self.assertEqual(
            self.model.get_dashboard(query),
            self.model.get_matches.return_value
        )
        self.assertEqual(
            self.model.filter,
            {
                'unreachable': {'$not': {'$eq': True}},
                'siteid': 'AMC01'
            }
        )
        self.dashboard.DashboardQuery.assert_called_once_with(query)
        self.dashboard.DashboardQuery.return_value.get_filter.assert_called_once_with()

    def test_get_dashboard_with_timestamp(self):
        self.dashboard.DashboardQuery = MagicMock(name='DashboardQuery')
        self.dashboard.DashboardQuery.return_value.get_filter.return_value = {'siteid': 'AMC01', 'timestamp': {
            '$gte': 'YYYY-mm-dd HH:ii:ss'}}
        self.model.get_matches = MagicMock(name='get_matches')
        query = {'siteid': 'AMC01', 'state': 'CRITICAL', 'service': 'Admin', 'category': 'namespace.category',
                 'timestamp': 'YYYY-mm-dd HH:ii:ss'}
        self.assertEqual(
            self.model.get_dashboard(query),
            self.model.get_matches.return_value
        )
        self.assertEqual(
            self.model.filter,
            {
                'unreachable': {'$not': {'$eq': True}},
                'siteid': 'AMC01',
                'timestamp': {'$gte': 'YYYY-mm-dd HH:ii:ss'}
            }
        )
        self.dashboard.DashboardQuery.assert_called_once_with(query)
        self.dashboard.DashboardQuery.return_value.get_filter.assert_called_once_with()

    def test_get_dashboard_none_case(self):
        self.dashboard.DashboardQuery = MagicMock(name='DashboardQuery')
        self.dashboard.DashboardQuery.return_value.get_filter.return_value = {}
        self.model.get_matches = MagicMock(name='get_matches')
        query = {'siteid': 'AMC01', 'state': 'CRITICAL', 'service': 'Admin'}
        self.assertEqual(
            self.model.get_dashboard(query),
            self.model.get_matches.return_value
        )
        self.assertEqual(self.model.filter, {'unreachable': {'$not': {'$eq': True}}})
        self.dashboard.DashboardQuery.assert_called_once_with(query)
        self.dashboard.DashboardQuery.return_value.get_filter.assert_called_once_with()

    def test_get_unreachable_services(self):
        self.model.get_matches = MagicMock(name='get_matches')
        self.assertEqual(
            self.model.get_unreachable_services(),
            self.model.get_matches.return_value
        )
        self.assertEqual(self.model.filter, {'unreachable': True})

    def test_get_unreachable_services_count(self):
        fake_value = [{'count': 'count', 'service': 'S1'}]
        self.model.UNREACHABLE_SERVICES = ['Product__IDM__Nebuchadnezzar__Alive__Status', 'Administrative__Philips__Host__Reachability__Status']
        self.model.collection.aggregate.return_value = fake_value
        self.assertEqual(self.model.get_unreachable_services_count(), {'result': fake_value})
        self.model.collection.aggregate.assert_called_once_with(
            [
                {'$match': {'service': {'$in': self.model.UNREACHABLE_SERVICES},'unreachable': {'$not': {'$eq': True}}}},
                {
                    '$group': {
                        '_id': '$service',
                        'count': {'$sum': 1},
                        'neb_down': {'$sum': {'$cond': [{'$eq': ['$service', self.model.UNREACHABLE_SERVICES[0]]}, 1, 0]}},
                        'host_down': {'$sum': {'$cond': [{'$eq': ['$service', self.model.UNREACHABLE_SERVICES[1]]}, 1, 0]}}
                    }
                },
                {
                    '$project': {'_id': 0, 'count': 1, 'service': '$_id'}
                }
            ]
        )

    def test_get_projection(self):
        self.assertEqual(
            self.model.get_projection(),
            {
                'site_name': '$site.name',
                'site': {'name': '$site.name', 'id': '$site._id', 'pacs_version': '$site.pacs_version',
                     'country': '$site.country'},
                'perfdata': '$perfdata',
                'last_received': self.dashboard.get_date_to_string_field_projection.return_value,
                'hostaddress': '$hostaddress',
                'case_number': {'$ifNull': ['$case_number', '?']},
                'duration': {'$subtract': [self.dashboard.datetime.utcnow.return_value, '$initial_timestamp']},
                'elementid': '$_id',
                'service': '$service',
                'hostname': '$hostname',
                'namespace': '$namespace',
                'state': '$state',
                'version': '$site.version',
                'siteid': '$siteid',
                'output': '$output',
                'components': '$components',
                'modules': '$modules',
                'tag': '$tag',
                'product_id': '$product_id'
            }
        )
        self.dashboard.get_date_to_string_field_projection.assert_called_once_with('$timestamp')

    def test_get_namespace_container(self):
        self.dashboard.DashboardQuery = MagicMock(name='DashboardQuery')
        self.dashboard.DashboardQuery.return_value.get_filter.return_value = {'namespace.category': 'product'}
        self.model.collection.distinct.return_value = ['category2', 'category1']
        query = {'container': 'vendor', 'category': 'product'}
        self.assertEqual(
            self.model.get_namespace_container('vendor', query),
            {'result': ['category1', 'category2']}
        )
        self.model.collection.distinct.assert_called_once_with(
            'namespace.vendor',
            self.dashboard.DashboardQuery.return_value.get_filter.return_value
        )

    def test_update_case_number(self):
        identifier = 'site1-host1-service2'
        case_number = '443556'
        self.assertEqual(self.model.update_case_number(identifier, case_number), {'result': True})
        self.model.collection.update_one.assert_called_once_with(
            {'_id': identifier}, {'$set': {'case_number': case_number}}, upsert=False
        )

    def test_get_unique_models(self):
        self.model.collection.distinct.return_value = iter(['MOD2', 'MOD1'])
        self.assertEqual(self.model.get_modules(), {'result': ['MOD1', 'MOD2']})
        self.model.collection.distinct.assert_called_once_with('modules')

    def test_get_stats(self):
        fak_value = {'result': [{'count': 1, 'status': 'status'}], 'total_count': 1}
        self.model.collection.find.return_value.count.return_value = 1
        self.model.collection.aggregate.return_value = iter([{'count': 1, 'status': 'status'}])
        self.model.filter = {'F1': 'FITTER1', 'F2': 'FILTER2'}
        self.assertEqual(self.model.get_stats(), fak_value)
        self.model.collection.find.assert_called_once_with(self.model.filter)

    def test_get_prod_stats(self):
        fake_value = {'result': [{'status1': 'status1', 'status2': 'status2', 'product': ['P1', 'p2']}]}
        self.model.collection.aggregate.return_value = iter(
            [{'status1': 'status1', 'status2': 'status2', 'product': ['P1', 'p2']}])
        self.assertEqual(self.model.get_prod_stats(), fake_value)
        self.model.collection.aggregate.assert_called_once_with([
            {'$match': self.model.filter},
            {
                '$group': {
                    '_id': {'modules': '$modules'},
                    'critical': {'$sum': {'$cond': [{'$eq': ['$state', 'CRITICAL']}, 1, 0]}},
                    'warning': {'$sum': {'$cond': [{'$eq': ['$state', 'WARNING']}, 1, 0]}},
                    'unknown': {'$sum': {'$cond': [{'$eq': ['$state', 'UNNKNOWN']}, 1, 0]}},
                    'total': {'$sum': 1},
                }
            },
            {
                '$project': {'_id': 0, 'critical': 1, 'warning': 1, 'unknown': 1, 'total': 1,
                             'product': '$_id.modules'}
            }
        ])

    def test_acknowledge_events(self):
        self.assertEqual(self.model.acknowledge_events(['ID1'], 'TAG'), {'result': True})
        self.model.collection.update_one.assert_called_once_with({'_id': 'ID1'},
                                                                 {'$set': {'case_number': 'ACK', 'tag': 'TAG'}},
                                                                 upsert=False)

    def test_get_nodes_down(self):
        self.model.collection.aggregate.return_value = [
            {'hostname': 'host1'},
            {'hostname': 'host2'}]
        self.assertEqual(self.model.get_nodes_down('site1'), ['host1', 'host2'])

        self.model.collection.aggregate.assert_called_once_with(
            [{'$match': {'$and': [{'siteid': 'site1'}, {'hostname': {'$not': re.compile('localhost')}},
                                  {'service': 'Administrative__Philips__Host__Reachability__Status'}]}},
             {'$project': {
                 '_id': 0,
                 'hostname': 1,
             }}
             ])

if __name__ == '__main__':
    unittest.main()
