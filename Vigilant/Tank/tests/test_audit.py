import unittest

from mock import patch, MagicMock


class FakeQueryBuilder(object):
    def __init__(self, query):
        self.query = query


class TankAuditModelTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_tank.util.QueryBuilder = FakeQueryBuilder
        self.mock_bottle = MagicMock(name='bottle')
        modules = {
            'tank.util': self.mock_tank.util,
            'bottle': self.mock_bottle
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.models import audit
        self.audit = audit
        self.audit.COLLECTIONS = {'AUDIT': 'audit'}
        self.mongodb = MagicMock(name='mongo_db')
        self.model = self.audit.AuditModel(self.mongodb)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_matches(self):
        self.model.filter = {'filter': 'please'}
        self.model.get_projection = MagicMock(name='get_projection')
        self.model.collection.aggregate.return_value = iter(['test1', 'test2'])
        self.assertEqual(self.model.get_matches(), {'result': ['test1', 'test2']})
        self.model.collection.aggregate.assert_called_once_with([
            {'$match': self.model.filter},
            {'$project': self.model.get_projection.return_value},
            {'$sort': {'timestamp': -1}}
        ])

    def test_get_data(self):
        self.model.get_matches = MagicMock(name='get_matches')
        self.model.query_builder = MagicMock(name='QueryBuilder')
        self.assertEqual(self.model.get_data({'key1': 'val1'}), self.model.get_matches.return_value)
        self.assertEqual(self.model.filter, self.model.query_builder.return_value.get_filter.return_value)
        self.model.query_builder.assert_called_once_with({'key1': 'val1'})

    def test_audit_distribution_model_get_projection(self):
        model = self.model = self.audit.AuditDistributionModel(self.mongodb)
        self.assertEqual(
            model.get_projection(),
            {
                'status': '$status',
                'subscription': '$subscription',
                'siteid': '$siteid',
                'user': '$user',
                'timestamp': self.audit.get_date_to_string_field_projection.return_value,
                '_id': 0,
                'version': '$version'
            }
        )
        self.audit.get_date_to_string_field_projection.assert_called_once_with('$timestamp')

    def test_audit_deployment_model_get_projection(self):
        model = self.model = self.audit.AuditDeploymentModel(self.mongodb)
        self.assertEqual(
            model.get_projection(),
            {
                'status': '$status',
                'hosts': '$hosts',
                'user': '$user',
                'timestamp': self.audit.get_date_to_string_field_projection.return_value,
                'package': '$package',
                '_id': 0,
                'siteid': '$siteid',
                'version': '$version'
            }
        )
        self.audit.get_date_to_string_field_projection.assert_called_once_with('$timestamp')

    def test_audit_query_process_query(self):
        query_builder = self.audit.AuditQuery({'key1': 'val2'})
        query_builder.audit_type = 'type1'
        query_builder.get_between_dates_filter = MagicMock(name='get_between_dates_filter')
        query_builder.query = {'start_date': '2012-12-12', 'end_date': '2013-04-05', 'siteid': 'x,y'}
        query_builder.process_query()
        self.assertEqual(
            query_builder.query,
            {'type': 'type1', 'timestamp': query_builder.get_between_dates_filter.return_value, 'siteid':'x,y'}
        )
        query_builder.get_between_dates_filter.assert_called_once_with('2012-12-12', '2013-04-05')


if __name__ == '__main__':
    unittest.main()