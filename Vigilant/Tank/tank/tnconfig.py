from collections import OrderedDict

CELERY_PATH = '/usr/lib/celery'
CELERY_CONFIGURATION_OBJECT = 'celeryconfig'
IN_TASK_NAME = 'phim_backoffice.routers.inbound'

## REDIS
REDIS = {
    'HOST': 'redis.phim.isyntax.net',
    'PORT': '6379'
}

## MONGODB / Collections
MONGODB = {
    # 'URL': 'mongodb://127.0.0.1:27017/',
    'URL': 'mongodb://192.168.180.58:27017/',
    'RETRIES': 2,
    'RETRY_DELAY': 10,
    'DB_NAME': 'somedb'
}

PGRESlocal=OrderedDict([('user', 'postgres'),
                   ('password', 'root'),
                   ('url', 'localhost'),
                   ('port', 5432),
                   ('db', 'local')])

PGRES=OrderedDict([('user', 'postgres'),
                   ('password', 'root'),
                   ('url', '192.168.180.103'),
                   ('port', 5432),
                   ('db', 'local')])

COLLECTIONS = {
    'ALIVES': 'alives',
    'AUDIT': 'audit',
    'COUNTRY': 'country',
    'DASHBOARD': 'dashboard',
    'EVENTS': 'events',
    'EXCEPTIONS': 'exceptions',
    'FACTS': 'facts_produ',
    'NAMESPACES': 'namespaces',
    'SITES': 'sites',
    'STATES': 'states',
    'SUBSCRIPTIONS': 'subscriptions',
    'TAGS': 'tags',
    'USERKEYS': 'userkeys'
}

PUBLIC_MODULES = {
    'ISP': 'Intellispace PACS',
    'LN': 'Legacy Nagios',
    'SWD': 'Software Distribution',
    'Windows': 'Window Related Facts',
    'Components': 'List of Software Components',
    'PCM': 'Package Configuration Management'
}

PUBLIC_HOST_KEYS = ['address', 'domain']
NEB_IP_NAMESPACE = 'Administrative__Philips__Host__Information__IPAddress'
NEB_HOSTNAME = 'localhost'
MONITOR_URL_TEMPLATE = 'https://{address}/thruk'

HANDLER_SERVICES = ['Product__IntelliSpace__PACS__iSyntaxServer__Aggregate']

UNREACHABLE_SERVICES = ['Product__IDM__Nebuchadnezzar__Alive__Status',
                        'Administrative__Philips__Host__Reachability__Status'];
