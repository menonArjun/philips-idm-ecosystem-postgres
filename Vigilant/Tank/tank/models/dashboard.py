import re
from datetime import datetime
from tank.tnconfig import COLLECTIONS, UNREACHABLE_SERVICES
from tank.util import QueryBuilder, get_date_to_string_field_projection


class DashboardModel(object):
    def __init__(self, mongodb):
        self.mongodb = mongodb
        self.collection = mongodb[COLLECTIONS['DASHBOARD']]
        self.filter = {}

    def get_matches(self):
        matches = self.collection.aggregate([
            {'$match': self.filter},
            {'$project': self.get_projection()}
        ])
        return {'result': list(matches)}

    def get_dashboard(self, query):
        """ Get Service Events dashboard data """
        self.filter = DashboardQuery(query).get_filter()
        self.filter['unreachable'] = {'$not': {'$eq': True}}
        return self.get_matches()

    def get_unreachable_services(self):
        """ Return unreachable services """
        self.filter = {'unreachable': True}
        return self.get_matches()

    def get_unreachable_services_count(self):
        matches = self.collection.aggregate(
            [
                {'$match': {'service': {'$in': UNREACHABLE_SERVICES}, 'unreachable': {'$not': {'$eq': True}}}},
                {
                    '$group': {
                        '_id': '$service',
                        'count': {'$sum': 1},
                        'neb_down': {'$sum': {'$cond': [{'$eq': ['$service', UNREACHABLE_SERVICES[0]]}, 1, 0]}},
                        'host_down': {'$sum': {'$cond': [{'$eq': ['$service', UNREACHABLE_SERVICES[1]]}, 1, 0]}}
                    }
                },
                {
                    '$project': {'_id': 0, 'count': 1, 'service': '$_id'}
                }
            ])
        return {'result': list(matches)}

    def get_projection(self):
        """Projection for Dashboard"""
        # {'$dateToString': {'format': '%Y/%m/%d  %H:%M:%S', 'date': '$timestamp'}},
        return {
            'elementid': '$_id',
            'state': '$state',
            'service': '$service',
            'last_received': get_date_to_string_field_projection('$timestamp'),
            'hostname': '$hostname',
            'perfdata': '$perfdata',
            'siteid': '$siteid',
            'site_name': '$site.name',
            'hostaddress': '$hostaddress',
            'output': '$output',
            'site': {'name': '$site.name', 'id': '$site._id', 'pacs_version': '$site.pacs_version',
                     'country': '$site.country'},
            'namespace': '$namespace',
            'case_number': {'$ifNull': ['$case_number', '?']},
            'version': '$site.version',
            'duration': {'$subtract': [datetime.utcnow(), '$initial_timestamp']},
            'components': '$components',
            'modules': '$modules',
            'tag': '$tag',
            'product_id': '$product_id'
        }

    def get_namespace_container(self, container, query):
        """Return distinct field values"""
        self.filter = DashboardQuery(query).get_filter()
        key = 'namespace.{container}'.format(container=container)
        return {'result': sorted(self.collection.distinct(key, self.filter))}

    def update_case_number(self, identifier, case_number):
        self.collection.update_one({'_id': identifier}, {'$set': {'case_number': case_number}}, upsert=False)
        return {'result': True}

    def get_modules(self):
        return {'result': sorted(self.collection.distinct('modules'))}

    def get_stats(self):
        self.filter = {'service': {'$nin': UNREACHABLE_SERVICES}, 'unreachable': {'$not': {'$eq': True}}}
        total_count = self.collection.find(self.filter).count()
        """SELECT value->'state' AS status, array_agg(DISTINCT value->>'siteid'), count(*) FROM dashboard WHERE NOT value->>'service' = ANY (ARRAY['Product__IDM__Nebuchadnezzar__Alive__Status',
                        'Administrative__Philips__Host__Reachability__Status']) AND NOT value->>'unreachable'='true' 
        OR (value->'unreachable') IS null
        GROUP BY status ;"""

        return {'result': list(self.collection.aggregate(
            [
                {'$match': self.filter},
                {
                    '$group': {
                        '_id': '$state',
                        'count': {'$sum': 1},
                        'sites': {'$addToSet': '$siteid'},

                    }},
                {
                    '$project': {'_id': 0, 'status': '$_id', 'sites': 1, 'count': 1}
                }
            ]
        )), 'total_count': total_count}

    def get_prod_stats(self):
        return {'result': list(self.collection.aggregate(
            [
                {'$match': self.filter},
                {
                    '$group': {
                        '_id': {'product_id': '$product_id'},
                        'critical': {'$sum': {'$cond': [{'$eq': ['$state', 'CRITICAL']}, 1, 0]}},
                        'warning': {'$sum': {'$cond': [{'$eq': ['$state', 'WARNING']}, 1, 0]}},
                        'unknown': {'$sum': {'$cond': [{'$eq': ['$state', 'UNNKNOWN']}, 1, 0]}},
                        'total': {'$sum': 1},
                    }
                },
                {
                    '$project': {'_id': 0, 'critical': 1, 'warning': 1, 'unknown': 1, 'total': 1,
                                 'product': '$_id.product_id'}
                }
            ]
        ))}

    def acknowledge_events(self, ids, tag):
        map(lambda id: self.collection.update_one({'_id': id}, {'$set': {'case_number': 'ACK', 'tag': tag}},
                                                  upsert=False), ids)
        return {'result': True}

    def get_app_services(self, query, applists):
        self.filter = {'product_id': {'$in': applists.split(',')}} if applists else {}
        key = 'namespace.object'
        return {'result': sorted(self.collection.distinct(key, self.filter))}

    def get_nodes_down(self, siteid):
        match = self.collection.aggregate(
            [{'$match': {'$and': [{'siteid': siteid}, {'hostname': {'$not': re.compile('localhost')}},
                                  {'service': 'Administrative__Philips__Host__Reachability__Status'}]}},
             {'$project': {
                 '_id': 0,
                 'hostname': 1,
             }}
             ])
        hosts_down = list(match)
        hosts_down_names = [host['hostname'] for host in hosts_down]
        return hosts_down_names


class DashboardQuery(QueryBuilder):
    or_query = {}
    parameters = {
        'siteid': 'siteid',
        'state': 'state',
        'service': 'service',
        'category': 'namespace.category',
        'vendor': 'namespace.vendor',
        'object': 'namespace.object',
        'attribute': 'namespace.attribute',
        'dimension': 'namespace.dimension',
        'component': 'components.name',
        'product': 'product_id'
    }

    def __init__(self, query):
        self.query = query

    def process_query(self):
        self.or_query = {}
        if self.query.get('product_id', None):
            prodct_ids = filter(lambda x: str(x), self.query.pop('product_id', '').split(','))
            self.or_query = {'product_id': {'$in': prodct_ids}}
        if self.query.get('timestamp', None):
            ts = self.query.get('timestamp')
            dt = datetime.strptime(ts, '%Y-%m-%d %H:%M:%S')
            self.or_query = {'timestamp': {'$gte': dt}}

        if self.query.get('timestamp', None):
            ts = self.query.get('timestamp')
            dt = datetime.strptime(ts, '%Y-%m-%d %H:%M:%S')
            self.or_query = {'timestamp': {'$gte': dt}}

    def get_filter(self):
        result = super(DashboardQuery, self).get_filter() or {}
        in_params = frozenset(['state', 'modules', 'namespace.category', 'siteid', 'type', 'product_id', 'namespace.vendor'])
        parameters = dict((field, self.query.get(query_key)) for query_key, field in self.parameters.iteritems())
        for query_key in in_params:
            if parameters.get(query_key):
                result.pop(query_key, None)
                result[query_key] = {'$in': parameters.get(query_key).split(',')}
        result.update(self.or_query)
        return result

