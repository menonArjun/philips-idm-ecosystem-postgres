from tank.tnconfig import COLLECTIONS


class TagsModel(object):
    def __init__(self, mongodb):
        self.mongodb = mongodb
        self.collection = self.mongodb[COLLECTIONS['TAGS']]

    def get_tags(self):
        matches = self.collection.aggregate([
            {'$match': {}},
            {'$project': {'tag': '$_id', '_id': 0, 'description': 1}}
        ])
        return {'result': list(matches)}

    def save_tag(self, tag, desc):
        self.collection.update_one({'_id': tag}, {'$set': {'description': desc}}, upsert=True)
        return {'result': True}