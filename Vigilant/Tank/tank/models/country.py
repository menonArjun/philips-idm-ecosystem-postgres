from tank.tnconfig import COLLECTIONS


class CountryModel(object):
    def __init__(self, mongodb):
        self.mongodb = mongodb
        self.collection = self.mongodb[COLLECTIONS['COUNTRY']]

    def get_countries(self):
        matches = self.collection.find(
            {},
            {
                'market': '$market',
                'sap_code': '$sap_code',
                'country': '$country',
                '_id': 0
            }
        )
        return {'result': list(matches)}
