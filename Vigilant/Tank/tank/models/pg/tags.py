from tank.tnconfig import COLLECTIONS


class TagsModel(object):
    def __init__(self, db):
        self.db = db

    def get_tags(self):
        self.db.execute("SELECT id AS tag, value->>'description' as description FROM tags;")
        return {'result': list(self.db.fetchall())}

    def save_tag(self, tag, desc):
        self.db.execute("""PREPARE updatesite AS UPDATE tags SET value = (jsonb_set(value, '{description}',
                        to_jsonb(($2)::text))) WHERE id=$1;""")
        self.db.execute("""EXECUTE updatesite(%s,%s)""", (tag,desc,))
        return {'result': True}
