import re
from datetime import datetime
from tank.tnconfig import COLLECTIONS, UNREACHABLE_SERVICES
from tank.util import QueryBuilder, get_date_to_string_field_projection,rep
from pymongo import MongoClient

class DashboardModel(object):
    def __init__(self, db):
        # client = MongoClient('192.168.180.58', 27017)
        # self.mongodb = client.local
        # self.collection = self.mongodb[COLLECTIONS['DASHBOARD']]
        self.filter = {}
        self.whr=""
        self.db=db
        self.prepinp=[]
        self.exeparam=[]

    # def get_matches(self):
    #     matches = self.collection.aggregate([
    #         {'$match': self.filter},
    #         {'$project': self.get_projection()}
    #     ])
    #     return {'result': list(matches)}

    def get_matches(self):
        if(self.whr!=""):
            query = "PREPARE get_matches AS SELECT {0} FROM dashboard WHERE {1}".format(self.get_projection_pg(), self.whr)
            self.db.execute(query)
            if(self.exeparam==[]):
                self.db.execute("EXECUTE get_matches")
            else:
                num=len(self.exeparam)
                self.db.execute("EXECUTE get_matches({0})".format(rep("%s",num)),tuple(self.exeparam))
        else:
            query = "PREPARE get_matches AS SELECT {0} FROM dashboard;".format(self.get_projection_pg())
            self.db.execute(query)
            self.db.execute("EXECUTE get_matches")
        matches=self.db.fetchall()
        return {'result': list(matches)}

    # def get_dashboard(self, query):
    #     """ Get Service Events dashboard data """
    #     self.filter = DashboardQuery(query).get_filter()
    #     self.filter['unreachable'] = {'$not': {'$eq': True}}
    #     return self.get_matches()

    def get_dashboard(self, query):
        """ Get Service Events dashboard data """
        self.prepinp = DashboardQuery(query).get_filter()
        # print(self.prepinp)
        self.whr=self.prepinp[0]
        self.exeparam=self.prepinp[1]
        if(self.whr!=""):
            self.whr=self.whr+" AND (value->'unreachable') IS null OR value->>'unreachable'='false';"
        else:
            self.whr = self.whr + " (value->'unreachable') IS null OR value->>'unreachable'='false';"
        # self.filter['unreachable'] = {'$not': {'$eq': True}}
        return self.get_matches()


    def get_unreachable_services(self):
        """ Return unreachable services """
        self.whr = self.whr + " value->>'unreachable' IN ('true')"
        return self.get_matches()

    def get_unreachable_services_count(self):
        query="""PREPARE getunreachcount as SELECT value->'service' as service, count(value->'service') as count 
        FROM dashboard WHERE value->>'service'= $1 OR value->>'service'= $2 and (value->>'unreachable' IN ('false') OR 
        (value->'unreachable') is null)  GROUP BY value->'service';"""
        self.db.execute(query)
        # self.db.execute("EXECUTE getunreachcount(%s,%s))",(UNREACHABLE_SERVICES[0],UNREACHABLE_SERVICES[1],))
        self.db.execute("EXECUTE getunreachcount(%s,%s)",(UNREACHABLE_SERVICES[0],UNREACHABLE_SERVICES[1],))
        matches = self.db.fetchall()
        return {'result': list(matches)}

    def get_projection_pg(self):
        sel_str = "{cls}"
        l=[]
        self.columns={
            'elementid': "value->'_id'",
            'state': "value->'state'",
            'service': "value->'service'",
            'last_received':"value->'timestamp'",
                # """to_char(value->'timestamp','YYYY-MM-DD"T"HH24:MI:SS')""",
                # """to_char(to_date(value->>'timestamp','YYYY-MM-DD"T"HH24:MI:SS'),'YYYY-MM-DD"T"HH24:MI:SS')""",
            # get_date_to_string_field_projection
            'hostname': "value->'hostname'",
            'perfdata': "value->'perfdata'",
            'siteid': "value->'siteid'",
            'site_name': "value->'site'->'name'",
            'hostaddress': "value->'hostaddress'",
            'output': "value->'output'",
            'site': "value->'site'",
            'namespace': "value->'namespace'",
            'case_number': "value->'case_number'",
            'version': "value->'site'->'version'",
            # 'duration': """to_char(LOCALTIMESTAMP - to_date(value->>'initial_timestamp','YYYY-MM-DD"T"HH24:MI:SS'),'YYYY-MM-DD"T"HH24:MI:SS')""",
            'duration': """EXTRACT(epoch FROM(current_timestamp-to_date(value->>'initial_timestamp','YYYY-MM-DD"T"HH24:MI:SS')))*1000""",
            'components': "value->'components'",
            'modules': "value->'modules'",
            'tag': "value->'tag'",
            'product_id': "value->'product_id'"
        }
        for key,value in self.columns.items():
            l.append(str(value)+" AS "+str(key))
        sel_str = sel_str.format(cls=','.join(l))
        return sel_str

    def get_namespace_container(self, container, query):
        """Return distinct field values"""
        self.whr = DashboardQuery(query).get_filter()[0]
        if self.whr!="":
            query="""PREPARE namecontain AS SELECT DISTINCT value->'namespace'->>$1 AS {1} FROM dashboard WHERE {0};""".format(self.whr,container)
        else:
            query = """PREPARE namecontain AS SELECT DISTINCT value->'namespace'->>$1 AS {0} FROM dashboard;""".format(container)
        self.db.execute(query)
        self.db.execute("EXECUTE namecontain(%s)", (container,))
        matches = [row[container] for row in self.db.fetchall()]
        return {'result': list(matches)}

    def update_case_number(self, identifier, case_number):
        # self.collection.update_one({'_id': identifier}, {'$set': {'case_number': case_number}}, upsert=False)
        self.db.execute("""PREPARE updatesite AS UPDATE sites SET value = (jsonb_set(value, '{case_number}',
                to_jsonb(($1)::text))) WHERE id=$2;""")
        self.db.execute("""EXECUTE updatesite(%s,%s)""", (case_number, identifier,))
        return {'result': True}

    def get_modules(self):
        self.db.execute("""SELECT DISTINCT value->'modules' AS Modules FROM dashboard;""")
        matches = self.db.fetchall()
        matches=[module['modules'] for module in matches]
        out=set()
        for lis in matches:
            out = out.union(set(lis))
        return {'result': sorted(list(out))}

    def get_stats(self):
        self.whr="""NOT value->>'service' = ANY (ARRAY{0}) AND NOT value->>'unreachable'='true' 
        OR (value->'unreachable') IS null""".format(UNREACHABLE_SERVICES)
        self.db.execute('SELECT count(*) FROM dashboard WHERE {0};'.format(self.whr))
        total_count=self.db.fetchall()[0]['count']
        self.db.execute("""SELECT value->'state' AS status, array_agg(DISTINCT value->>'siteid'), count(*) FROM dashboard 
        WHERE {0} GROUP BY status;""".format(self.whr))
        return {'result': list(self.db.fetchall()),'total_count': total_count}

    def get_prod_stats(self):
        if(self.whr==""):
            self.db.execute("""SELECT value->'modules' AS product , count(value->>'state')  filter (WHERE value->>'state'='CRITICAL' )
            AS "critical", count(value->>'state')  filter (WHERE value->>'state'='WARNING' ) AS "warning", 
            count(value->>'state')  filter (WHERE value->>'state'='UNKNOWN' ) AS "unknown", count(*) AS total 
            from dashboard GROUP BY value->'modules';""")
        else:
            self.db.execute("""SELECT value->'modules' AS product , count(value->>'state')  filter (WHERE value->>'state'='CRITICAL' )
                        AS "CRITICAL", count(value->>'state')  filter (WHERE value->>'state'='WARNING' ) AS "WARNING", 
                        count(value->>'state')  filter (WHERE value->>'state'='UNKNOWN' ) AS "UNKNOWN", count(*) AS total 
                        from dashboard WHERE {0} GROUP BY value->'modules';""".format(self.whr))
        return {'result': list(self.db.fetchall())}


    def acknowledge_events(self, ids, tag):
        # map(lambda id: self.collection.update_one({'_id': id}, {'$set': {'case_number': 'ACK', 'tag': tag}},
        #                                           upsert=False), ids)
        self.db.execute("""PREPARE ackevents AS UPDATE dashboard SET value = (jsonb_set((jsonb_set(value, '{case_number}',
                      to_jsonb(('ACK')::text))), '{tag}',
                      to_jsonb(($2)::text))) WHERE id=$1;""")
        map(lambda id: self.db.execute("""EXECUTE ackevents(%s,%s)""", (id,tag)), ids)
        # self.db.execute("""EXECUTE updatesite(%s,%s)""", (id,tag))
        return {'result': True}


    def get_nodes_down(self, siteid):
        self.db.execute("""PREPARE getnodesdown AS SELECT value->'hostname' AS hostname FROM dashboard WHERE value->>'siteid'=$1 AND value->>'hostname'!='localhost' 
        AND value->>'service'='Administrative__Philips__Host__Reachability__Status';""")
        self.db.execute("""EXECUTE getnodesdown(%s)""",(siteid,))
        hosts_down = list(self.db.fetchall())
        hosts_down_names = [host['hostname'] for host in hosts_down]
        return hosts_down_names

    def get_app_services(self, query, applists):
        # self.filter = {'product_id': {'$in': applists.split(',')}} if applists else {}
        self.whr="WHERE value->>'product_id'=ANY(($1));" if applists!="" else ";"
        applists = applists.split(',')
        self.db.execute("""PREPARE appservice AS SELECT DISTINCT value->'namespace'->'object' AS "App Service" FROM dashboard {0}""".format(self.whr))
        self.db.execute("""EXECUTE appservice(%s)""", (applists,))
        return {'result': sorted(row['App Service'] for row in self.db.fetchall())}


class DashboardQuery(QueryBuilder):
    or_query = {}

    parameters={
        'siteid': "value->>'siteid'",
        'state': "value->>'state'",
        'service': "value->>'service'",
        'category': "value->'namespace'->>'category'",
        'vendor': "value->'namespace'->>'vendor'",
        'object': "value->'namespace'->>'object'",
        'attribute': "value->'namespace'->>'attribute'",
        'dimension': "value->'namespace'->>'dimension'",
        'component': "value->'components'->>'name'",
        'product': "value->>'product_id'"
    }

    def __init__(self, query):
        self.query = query

    def process_query(self):
        self.or_query = {}
        #Not parameterized query implementation
        if self.query.get('product_id', None):
            prodct_ids = filter(lambda x: str(x), self.query.pop('product_id', '').split(','))
            self.or_query = "WHERE value->'product_id'=ANY {0}".format('$1')
            return prodct_ids

        if self.query.get('timestamp', None):
            ts = self.query.get('timestamp')
            dt = datetime.strptime(ts, '%Y-%m-%d %H:%M:%S')
            self.or_query =  "WHERE value->>'timestamp'>=ANY {0}".format('$1')
            return dt


    def get_filter(self):
        result = super(DashboardQuery, self).get_filter() or {}
        in_params = frozenset(["value->'state'", "value->'modules'", "value->'namespace'->'category'", "value->'siteid'",
                               "value->'type'", "value->'product_id'", "value->'namespace'->'vendor'"])
        parameters = dict((field, self.query.get(query_key)) for query_key, field in self.parameters.iteritems())
        count = 0
        l = []
        values = []
        pack=[]
        for query_key in in_params:
            if parameters.get(query_key):
                result.pop(query_key, None)
                result[query_key] = parameters.get(query_key).split(',')
        for key,value in result.items():
            if isinstance(value,list):
                l.append(str(key)+" = ANY ("+"${0})".format(count+1))
            else:
                l.append(str(key)+" = "+"${0}".format(count+1))
            values.append(value)
            count+=1
        if(len(l)>1):
            whr = "".format(' AND '.join(l))
        if (len(l)==1):
            whr=l[0]
        else:
            whr=""
        # result.update(self.or_query)
        pack.append(whr)
        pack.append(values)
        return pack



