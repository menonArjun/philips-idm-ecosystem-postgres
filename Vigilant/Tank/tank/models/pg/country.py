from tank.tnconfig import COLLECTIONS


class CountryModel(object):
    def __init__(self, db):
        self.db = db
        # self.collection = self.mongodb[COLLECTIONS['COUNTRY']]

    def get_countries(self):
        # matches = self.collection.find(
        #     {},
        #     {
        #         'market': '$market',
        #         'sap_code': '$sap_code',
        #         'country': '$country',
        #         '_id': 0
        #     }
        # )
        self.db.execute("""SELECT value->'market' as market, value->'sap_code' as sap_code, value->'country' as country from country;""")
        return {'result': list(self.db.fetchall())}
