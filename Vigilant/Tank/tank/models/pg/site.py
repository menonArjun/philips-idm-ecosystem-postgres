import re

import psycopg2
from datetime import datetime
from tank.models.pg.dashboard import DashboardModel
from tank.models.pg.subscription import SubscriptionModel
from tank.util import get_logger,rename_id_pg
from tank.models.pg.fact import SiteFactsModel

log = get_logger()

conn = psycopg2.connect("host='localhost' dbname='local' user='postgres' password='root' port=5432 connect_timeout=1 ")
cursor1=conn.cursor()

class SiteModel(object):
    def __init__(self, db):
        self.db = db
        self.filter = {}

    def get_siteids(self):
        self.db.execute("""PREPARE getsiteids AS SELECT DISTINCT value->'_id' AS siteid FROM sites;""")
        self.db.execute("""EXECUTE getsiteids""")
        result = self.db.fetchall()
        print(result)
        return {'siteids': list(result)}

    def set_siteid_filter(self, siteids=None):    #for get_site_names, implemented directly in PG query
        if siteids:
            self.filter['_id'] = {'$in': siteids}

    def set_country_filter(self, countries=None):
        if countries:
            self.filter['country.country'] = {'$in': countries}

    def get_site_names(self, siteids=None):
        """Returns a map of siteid to site name"""
        self.db.execute("""PREPARE getsitenames AS SELECT value->'_id' as id,value->'name' AS sitename FROM sites 
        WHERE value->>'_id' = ANY ($1);""")
        self.db.execute("""EXECUTE getsitenames(%s)""",(siteids,))
        sites_info = self.db.fetchall()
        return dict((site['id'], site.get('sitename')) for site in sites_info)

    def get_sites(self, siteids=None, countries=None):
        def getKey(site):
            return site.get('id')
        """ Get Site details """
        if(siteids!=None or countries!=None):
            self.db.execute("""PREPARE getsites AS SELECT value->'_id' as id, value->'name' as name,
        value->'pacs_version' as pacs_version, value->'country' as country, value->'production' as production  FROM sites 
                WHERE value->>'_id' = ANY ($1) OR value->'country'->>'country' = ANY ($2);""")
        else:
            self.db.execute("""PREPARE getsites AS SELECT value->'_id' as id, value->'name' as name,
                    value->'pacs_version' as pacs_version, value->'country' as country, value->'production' as production 
                     FROM sites;""")
        self.db.execute("""EXECUTE getsites(%s,%s)""", (siteids,countries,))
        sites_info=self.db.fetchall()
        return {'sites': [rename_id_pg('siteid', site) for site in sorted(sites_info, key=getKey)]}

    def get_subscription_candidates(self, subscription_name):
        subscription = SubscriptionModel().get_subscription(subscription_name)
        countries = subscription.get('countries')
        if not countries:
            return {'sites': []}
        return self.get_sites(countries=countries)
    #
    # def get_facts_hosts_details(self):
    #     sites = self.get_sites()['sites']
    #     print("Printing sites:",sites)
    #     hosts_details = SiteFactsModel(self.db,siteid=None).get_facts_hosts()
    #     db = DashboardModel(self.db)
    #     db.filter = {'service': 'Product__IDM__Nebuchadnezzar__Alive__Status', 'unreachable': {'$not': {'$eq': True}}}
    #     neb_status = db.get_matches()
    #     print(neb_status)
    #     neb_siteids = []
    #     if len(neb_status) > 0:
    #         neb_siteids = [site['siteid'] for site in neb_status['result']]
    #     print(neb_siteids)
    #     localhost_regex = re.compile('localhost')
    #     hostnames = []
    #     print(sites)
    #     for site in sites:
    #         site['host_count'] = 0
    #         site['status'] = 'Up'
    #         for hd in hosts_details:
    #             vcenters = []
    #             if site['siteid'] == hd['_id']:
    #                 hosts = [host
    #                          for host in hd['result']
    #                          if not localhost_regex.match(host['hostname'])]
    #                 hostnames = [host['hostname'] for host in hosts]
    #                 site['host_count'] = len(hosts)
    #                 vcenters = [host['vCenter']
    #                             for host in hd['result']
    #                             if 'vCenter' in host]
    #             if len(vcenters) > 0:
    #                 site['host_count'] += self.get_host_count(vcenters, localhost_regex, hostnames)
    #         if site['siteid'] in neb_siteids:
    #             site['status'] = 'Down'
    #     return {'result': sites}


    def get_facts_hosts_details(self):
        sites = self.get_sites()['sites']
        db = DashboardModel(self.db)
        db.filter = {'service': 'Product__IDM__Nebuchadnezzar__Alive__Status', 'unreachable': {'$not': {'$eq': True}}}
        neb_status = db.get_matches()
        neb_siteids = []
        if len(neb_status) > 0:
            neb_siteids = [site['siteid'] for site in neb_status['result']]
        localhost_regex = re.compile('localhost')
        hostnames = []
        for site in sites:
            if site['siteid'] in neb_siteids:
                site['status'] = 'Down'
            else:
                site['status'] = 'Up'
        return {'result': sites}

    def get_host_count(self, vcenters, localhost_regex, hostnames):
        count = 0
        for vcenter in vcenters:
            if 'vms' in vcenter:
                for vm in vcenter['vms']:
                    if ('hostName' in vm['guest']) and \
                            (not localhost_regex.match(vm['guest']['hostName'])) and \
                            ((vm['guest']['hostName']).lower() not in hostnames):
                        count += 1
        return count

    def update_site(self, siteid, name, country):
        # name="""\{"name":{0}\}""".format(name)
        print(name)
        self.db.execute("""PREPARE updatesite AS UPDATE sites SET value = (jsonb_set((jsonb_set(value, '{country,country}',
        to_jsonb(($2)::text))), '{name}',
        to_jsonb(($3)::text))) WHERE id=$1;""")
        self.db.execute("""EXECUTE updatesite(%s,%s,%s)""", (siteid,country,name))

    def delete_site(self, siteid):
        # self.collection.delete_one({'_id': siteid})
        self.db.execute("PREPARE delsite AS DELETE FROM sites WHERE id=$1;")
        self.db.execute("""EXECUTE delsite(%s)""", (siteid,))

    def get_site_count(self):
        # print('Sites'+str(type(self.mongodb)))
        self.db.execute("PREPARE sitecount AS SELECT count(*) FROM public.sites WHERE value->>'production'='true';")
        self.db.execute("""EXECUTE sitecount""")
        result = self.db.fetchall()
        return {'count': result}
