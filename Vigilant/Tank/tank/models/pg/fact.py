import re

import pypgwrap

from tank.tnconfig import PUBLIC_MODULES, PUBLIC_HOST_KEYS, PGRES
from tank.util import rep,get_keys_in_site_pg
from tank.models.pg.dashboard import DashboardModel


class SiteFactsModel(object):

    def __init__(self,db,siteid):
        self.siteid = siteid
        self.db= db

    def get_host_count(self):
        hname = 'localhost'
        self.db.execute("""PREPARE hcount AS SELECT COUNT(DISTINCT id) as COUNT
                           FROM facts_produ WHERE value->>'hostname' NOT IN($1);""")
        self.db.execute("EXECUTE hcount (%s)", (hname,))
        res=self.db.fetchall()
        return {'count': res[0]['count']}

    def get_facts_hosts(self):
        # log.debug('Get nodes for %s', siteid)
        self.db.execute("""PREPARE hostFacts AS SELECT DISTINCT ON(s.value->'siteid')s.value->'siteid' AS id , (SELECT json_agg(Result) 
        FROM(SELECT P.value->'hostname' AS hostname,P.value->'vCenter' AS vCenter 
        FROM facts_produ AS P WHERE P.value->'siteid' IN(s.value->'siteid'))AS Result)  AS result FROM facts_produ AS s;""")
        self.db.execute("EXECUTE hostFacts")
        res = self.db.fetchall()
        return list(res)


    def get_one_component(self, component):
        self.db.execute("""PREPARE get_comp AS """ """SELECT temp2.hostname AS hostname ,temp2.Components AS Components
                              FROM(SELECT temp.hostname AS hostname ,jsonb_array_elements(temp.Components) 
                              AS Components FROM(SELECT value->'hostname' AS hostname,
                              value->'Components' AS Components
                              FROM facts_produ AS f WHERE value->>'siteid'=$1)AS temp)AS temp2 
                              WHERE temp2.Components->>'name'=$2;""")
        self.db.execute("EXECUTE get_comp(%s,%s)",(self.siteid,component,))
        res = self.db.fetchall()
        return {'results':list(res)}

    def get_host_components(self, hostname):
        fullId = self.siteid + '-' + hostname
        self.db.execute("""PREPARE get_hcomp AS """ """SELECT TEMP2.Components->'name' AS name, TEMP2.Components->'version' 
                                AS version,TEMP2.Components->'timestamp' AS timestamp
                              FROM(SELECT TEMP1.ID,jsonb_array_elements(TEMP1.Components) AS Components FROM
                              (SELECT DISTINCT ON(value->>'_id')value->>'_id' AS ID,value->'Components' AS Components
                              FROM facts_produ WHERE value->>'Components' NOT IN('null','[]'))as TEMP1
                              WHERE TEMP1.ID=$1) AS TEMP2 
                              ORDER BY TEMP2.Components->>'version' DESC, 
                              TEMP2.Components->>'timestamp' DESC LIMIT 1;""")
        self.db.execute("EXECUTE get_hcomp(%s)",(fullId,))
        res = self.db.fetchall()
        return {'results':list(res)}

    def get_components(self):
        self.db.execute("""PREPARE compo AS SELECT value->'_id' AS id,value->'hostname' as hostname, value->'Components' as Components 
        FROM facts_produ WHERE value->'Components' NOT IN('null') AND value->>'siteid'=$1;""")
        self.db.execute("EXECUTE compo(%s)",(self.siteid,))
        res = self.db.fetchall()
        return {'results': res}

    def get_item_details(self, item):
        self.db.execute("""PREPARE itm_det AS SELECT value->'_id' AS id,value->'hostname' as hostname, value->'"""+item+"""' as """+item+""" 
        FROM facts_produ 
        WHERE value->'"""+item+"""' NOT IN('null') AND value->>'siteid'=$1;""")
        self.db.execute("EXECUTE itm_det(%s)",(self.siteid,))
        res = self.db.fetchall()
        return {'result': list(res)}

    def get_modules(self):
        ar = list(PUBLIC_MODULES.keys())
        self.db.execute("""PREPARE mod AS SELECT value->$2 AS "ISP", value->$3 as "LN", value->$4 AS "SWD", value->$5 as "Windows",
        value->$6 AS "Components", value->$7 AS "PCM" FROM facts_produ WHERE value->>'siteid'=$1;""")
        st=rep("%s,",7)
        self.db.execute("EXECUTE mod(%s,%s,%s,%s,%s,%s,%s)",(self.siteid,'ISP', 'LN', 'SWD', 'Windows', 'Components', 'PCM',))
        res = self.db.fetchall()
        return {'modules': get_keys_in_site_pg(res)}
        # return {'result': list(res)}

    def get_host_detail(self, hostname):
        hostname = hostname
        siteid = self.siteid
        hoDet = ['hostname'] + PUBLIC_HOST_KEYS + list(PUBLIC_MODULES.keys())
        HoDet=self.db.execute("""PREPARE ho_det AS SELECT value->$3 AS hostname, value->$4 AS address,
        value->$5 AS domain,value->$6 AS "ISP",
        value->$7 as "LN", value->$8 AS "SWD", value->$9 as "Windows",
        value->$10 AS "Components",value->$11 AS "PCM" FROM facts_produ
        WHERE value->>'hostname'=$1 AND value->>'siteid'=$2;""")
        st = rep("%s", 11)
        self.db.execute("EXECUTE ho_det("+st+")",(hostname,self.siteid,'hostname','address','domain','ISP', 'LN', 'SWD', 'Windows', 'Components', 'PCM',))
        res =self.db.fetchone()
        # print(res['ISP'])
        # print('Hmmmmmm' + str(type(res)))
        return self.get_filtered_host_facts(res, PUBLIC_HOST_KEYS, list(PUBLIC_MODULES.keys()))

    def get_filtered_host_facts(self, host_facts, keys, modules):
        result = {}
        if host_facts:
            result['hostname'] = host_facts['hostname']
            result.update([(key, host_facts[key]) for key in keys if key in host_facts])
            result['modules'] = [key for key in host_facts.keys() if key in modules  and host_facts[key]!=None]
        return result

    def get_hosts(self):
        self.db.execute("""PREPARE ghosts AS SELECT DISTINCT value->'hostname' AS hostname from facts_produ 
                WHERE value->>'siteid'=$1;""")
        self.db.execute("EXECUTE ghosts(%s)",(self.siteid,))
        hosts = [host['hostname'] for host in self.db.fetchall()]
        return {'hosts': hosts}

    def get_node_details(self):
        self.db.execute("""PREPARE node AS SELECT value->'hostname' as hostname, value->'address' as address, value->'vCenter'->'vms' as vCenter, 
        value->'ISP'->'module_type' as module_type, value->'modules' as modules, value->'servicetag' as servicetag,
        value->'Windows' as Windows, value->'HPSwitch' as HPSwitch, value->'Components' as Components
        FROM factspgs where value->>'siteid'=$1 AND value->>'hostname' NOT IN ($2);""")
        self.db.execute("EXECUTE node(%s,%s)",(self.siteid,'localhost',))
        match=self.db.fetchall()
        nodes = list(match)
        nodesu=[]
        hosts_down = DashboardModel(self.db).get_nodes_down(self.siteid)
        vms = []
        vm_status = {'poweredOn': 'Up', 'poweredOff': 'Down', 'suspended': 'Down'}
        hostnames = []
        localhost_regex = re.compile('localhost')
        vcenters = []
        for node in nodes:
            node=dict(node)
            node.update({'discovery_type': 'Discovery'})
            if node['hostname'] in hosts_down:
                node['status'] = 'Down'
            else:
                node['status'] = 'Up'
                hostnames.append(node['hostname'])
            components = node.get('components', [])
            if type(components) is not None:
                for component in components:
                    component['timestamp'] = (component.get('timestamp', None)#.strftime('%Y/%m/%d  %H:%M:%S')
                        if component.get('timestamp', None) else None)
            if node['vcenter']!=None:
                vcenter_nodes = node['vcenter']
                node.pop('vcenter')
                vcenters.append(vcenter_nodes)
            nodesu.append(node)
        if len(vcenters) > 0:
            for vcenter in vcenters:
                for vm in vcenter:
                    vm_data = {}
                    if ('hostname' in vm['guest']) and (not localhost_regex.match(vm['guest']['hostname'])) and (
                                (vm['guest']['hostname']).lower() not in hostnames):
                        vm_data.update({'status': vm_status[vm['powerstate']]})
                        vm_data.update({'hostname': vm['guest'].get('hostname', None)})
                        vm_data.update({'address': vm['guest'].get('ipaddress', None)})
                        vm_data.update({'discovery_type':'vcenter'})
                        vms.append(vm_data)
            nodesu.extend(vms)
        return {'result': nodesu}

