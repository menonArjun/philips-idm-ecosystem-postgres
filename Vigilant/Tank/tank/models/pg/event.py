from tank.models.pg.site import SiteModel
from tank.tnconfig import COLLECTIONS
from tank.util import QueryBuilder
from tank.util import get_date_to_string_field_projection, rename_id_pg,rep


class EventModel(object):
    def __init__(self, db):
        self.db = db
        # self.collection = self.mongodb[COLLECTIONS['EVENTS']]
        self.whr = ""
        self.values=[]
        self._site_names = None

    def get_matches(self):
        if (self.whr!=""):
            self.db.execute("""PREPARE getmatches AS SELECT {0} FROM events WHERE {1} ORDER BY value->'timestamp' ASC LIMIT 1000;""".format(self.get_projection(),self.whr))
            if(self.values==[]):
                self.db.execute("EXECUTE getmatches")
            else:
                num=len(self.values)
                self.db.execute("EXECUTE getmatches({0})".format(rep("%s",num)),*self.values)
        else:
            self.db.execute("SELECT {0} FROM events ORDER BY value->'timestamp' ASC LIMIT 1000;".format(self.get_projection()))
        # print(query)
        matches=self.db.fetchall()
        return matches

    def get_historical(self, query):
        bundle = EventQuery(query).get_filter()
        self.whr=bundle[0]
        self.values=bundle[1]
        return {'events': list(self.get_matches())}

    def get_projection(self):
        sel_str = "{cls}"
        l = []
        columns = {
            'state': "value->'state'",
            'service': "value->'service'",
            'timestamp': "value->>'initial_timestamp'",
            'siteid': "value->'siteid'",
            'hostaddress': "value->'hostaddress'",
            'hostname': "value->'hostname'",
            'output': "value->'output'",
            'type': "value->'type'",
            'components': "value->'components'",
            'modules': "value->'modules'"
        }
        for key, value in columns.items():
            l.append(str(value) + " AS " + str(key))
        sel_str = sel_str.format(cls=','.join(l))
        return sel_str


    def get_affected_siteids(self):
        if(self.whr!=""):
            self.db.execute("PREPARE affect AS SELECT DISTINCT value->'siteid' as site FROM events WHERE {0};".format(self.whr))
            self.db.execute("EXECUTE affect({0})".format(rep("%s", len(self.values))),(tuple(self.values)))
        else:
            self.db.execute("SELECT DISTINCT value->'siteid' as site FROM events;")
        matches=self.db.fetchall()
        matches = [module['site'] for module in matches]
        return sorted(matches)


    @property
    def site_names(self):
        if self._site_names is None:
            self._site_names = SiteModel(self.db).get_site_names(self.get_affected_siteids())
        return self._site_names




    def get_handler_matches(self):
        if self.whr=="":
            self.db.execute("""PREPARE handles AS SELECT siteid as id, sum(success) as success, sum(failure) as failure, sum(total) as total, 
                array_agg('[' || hostname || ',' || success || ',' || failure ||',' || total || ',' || timestampa || ']') 
                as hosts from (SELECT value->>'hostname' as hostname,value->>'siteid' as siteid, value->>'timestamp' 
                as timestampa, count(*) filter (WHERE value->>'state'='OK') AS success, count(*) filter 
                (WHERE value->>'state'='WARNING' ) as failure, count(*) AS total from events 
                GROUP BY value->>'hostname',value->>'siteid', value->>'timestamp') AS temp GROUP BY siteid;""")

        else:
            query="""PREPARE handles AS SELECT siteid as id, sum(success) as success, sum(failure) as failure, sum(total) as total, 
                array_agg('[' || hostname || ',' || success || ',' || failure ||',' || total || ',' || timestampa || ']') 
                as hosts from (SELECT value->>'hostname' as hostname,value->>'siteid' as siteid, value->>'timestamp' 
                as timestampa, count(*) filter (WHERE value->>'state'='OK') AS success, count(*) filter 
                (WHERE value->>'state'='WARNING' ) as failure, count(*) AS total from events WHERE {0} GROUP BY value->>'hostname'
                ,value->>'siteid', value->>'timestamp',id) AS temp GROUP BY siteid;""".format(self.whr)
            self.db.execute(query)
            print(query)
        self.db.execute("EXECUTE handles({0})".format(rep("%s", len(self.values))), (tuple(self.values)))
        matches=self.db.fetchall()
        # matches=list(matches)
        print(matches)
        # matches = [module for module in matches]
        return {'result': list(matches)}

    def adjust_handler_entry(self, site):
        """Destructively adjust the key names for a site object"""
        site = rename_id_pg('siteid', site)
        site['name'] = self.site_names.get(site['siteid'])
        return site

    def get_handler_report(self, service, query):
        query['service'] = service
        pack = EventQuery(query).get_filter()
        self.whr=pack[0]
        self.values=pack[1]
        if (self.whr!=""):
            self.whr=self.whr+""" AND value->>'type'='HANDLER' AND value->>'state'= ANY (ARRAY['OK','WARNING'])"""
        else:
            self.whr = self.whr + """ value->>'type'='HANDLER' AND value->>'state'= ANY (ARRAY['OK','WARNING'])"""
        # for site in self.get_handler_matches():
        #     print(site['siteid'])
        return {'result': [self.adjust_handler_entry(site) for site in self.get_handler_matches()['result']]}


class EventQuery(QueryBuilder):
    parameters = {
        'siteid': "value->>'siteid'",
        'state': "value->>'state'",
        'service': "value->>'service'",
        'timestamp': "value->>'timestamp'",
        'component': "value->>'components.name'",
        'module': "value->>'modules'",
    }

    def get_filter(self):
        filter = super(EventQuery, self).get_filter() or {}
        count = 0
        l = []
        values = []
        pack = []
        for key, value in filter.items():
            if isinstance(value, list):
                l.append(str(key) + " = ANY (" + "${0})".format(count+1))
            else:
                l.append(str(key) + " = " + "${0}".format(count+1))
            values.append(str(value))
            count += 1
        if(len(l)>1):
            whr = "".format(' AND '.join(l))
        if (len(l)==1):
            whr=l[0]
        else:
            whr=""
        # result.update(self.or_query)
        pack.append(whr)
        pack.append(values)
        return pack

    def process_query(self):
        start_date = self.query.pop('start_date', None)
        end_date = self.query.pop('end_date', None)
        self.query['timestamp'] = self.get_between_dates_filter(start_date, end_date)
