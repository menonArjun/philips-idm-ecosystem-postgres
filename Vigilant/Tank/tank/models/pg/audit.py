from tank.tnconfig import COLLECTIONS
from tank.util import QueryBuilder
from tank.util import rep


class AuditQuery(QueryBuilder):
    audit_type = None

    def process_query(self):
        site_id = self.query.pop('site_id', None)
        if site_id:
            self.query['site_id'] = {'$in': site_id.split(',')}
        start_date = self.query.pop('start_date', None)
        end_date = self.query.pop('end_date', None)
        self.query['timestamp'] = self.get_between_dates_filter(start_date, end_date)
        self.query['type'] = self.audit_type


class AuditDeploymentQuery(AuditQuery):
    audit_type = 'deployment'

    parameters = {
        'site_id': "value->>'siteid'",
        'package': "value->>'package'",
        'timestamp': "value->>'timestamp'",
        'type': "value->>'type'"
    }


class AuditDistributionQuery(AuditQuery):
    audit_type = 'distribution'

    parameters = {
        'site_id': "value->>'siteid'",
        'subscription': "value->>'subscription'",
        'timestamp': "value->>'timestamp'",
        'type': "value->>'type'"
    }


class AuditModel(object):
    query_builder = None

    def __init__(self, db):
        self.db = db
        # self.collection = self.mongodb[COLLECTIONS['AUDIT']]
        self.whr = ""
        self.exeparam = []


    def get_matches(self,params):
        self.whr=params[0]
        self.exeparam =params[1]
        if self.whr!="":
            query="PREPARE mats AS SELECT {0} FROM audit WHERE {1} ORDER BY value->>'timestamp' DESC;".format( self.get_projection(), self.whr)
            self.db.execute(query)
            self.db.execute("EXECUTE mats({0})".format(rep("%s",len(self.exeparam))),tuple(self.exeparam))
        else:
            self.db.execute("SELECT {0} FROM audit ORDER BY value->>'timestamp' DESC;".format(self.get_projection()))
        return {'result': list(self.db.fetchall())}

    def get_projection(self):
        pass

    def get_data(self, query):
        l=[]
        values=[]
        count=0
        pack=[]
        self.filter = self.query_builder(query).get_filter()
        for key,value in self.filter.items():
            if isinstance(value,list):
                l.append(str(key)+" = ANY ("+"${0})".format(count+1))
            else:
                l.append(str(key)+" = "+"${0}".format(count+1))
            values.append(value)
            count+=1
        if (len(l)>1):
            whr = "".format(' AND '.join(l))
        else:
            whr = l[0]
        pack.append(whr)
        pack.append(values)
        return self.get_matches(pack)


class AuditDistributionModel(AuditModel):
    query_builder = AuditDistributionQuery

    def get_projection(self):
        sel_str = "{cls}"
        l = []
        self.columns= {
            'subscription': "value->>'subscription'",
            'status': "value->>'status'",
            'user': "value->>'user'",
            'siteid': "value->>'siteid'",
            'timestamp': "value->>'timestamp'",
            'version': "value->>'version'"
        }
        for key,value in self.columns.items():
            l.append(str(value)+" AS "+str(key))
        sel_str = sel_str.format(cls=','.join(l))
        return sel_str


class AuditDeploymentModel(AuditModel):
    query_builder = AuditDeploymentQuery

    def get_projection(self):
        sel_str = "{cls}"
        l = []
        self.columns= {
            'subscription': "value->>'subscription'",
            'status': "value->>'status'",
            'hosts': "value->'hosts'",
            'user': "value->>'user'",
            'siteid': "value->>'siteid'",
            'timestamp': "value->>'timestamp'",
            'version': "value->>'version'"
        }
        for key,value in self.columns.items():
            l.append(str(value)+" AS "+str(key))
        sel_str = sel_str.format(cls=','.join(l))
        return sel_str