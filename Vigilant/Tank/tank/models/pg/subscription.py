from tank.tnconfig import COLLECTIONS
from tank.util import get_logger, rename_id_pg
from pymongo import MongoClient

log = get_logger()


class SubscriptionModel():
    def __init__(self, db, path=''):
        # client = MongoClient('192.168.180.58', 27017)
        # self.mongodb = client.local
        # self.collection = self.mongodb[COLLECTIONS['SUBSCRIPTIONS']]
        self.path = path
        self.db=db
        self._slashed_path = None
        self.filter = {}
        self.whr=""

    @property
    def slashed_path(self):
        if self._slashed_path is None:
            self._slashed_path = '/{0}/'.format(self.path.strip('/')) if self.path else '/'
        return self._slashed_path

    def get_groups(self):
        # all_subpaths = self.collection.distinct(
        #     'path',
        #     {'path': {'$regex': self.get_starts_with_regex(self.slashed_path)}}
        # )
        self.db.execute("""SELECT substring(value->>'path' from '(.+)') FROM subscriptions WHERE value->>'path' ~ '{0}'""".format(self.get_starts_with_regex(self.slashed_path)))
        all_subpaths=[pa['substring'] for pa in self.db.fetchall()]
        result = self.get_direct_descendant_groups(self.slashed_path, all_subpaths)
        return {'groups': list(result)}

    def get_direct_descendant_groups(self, parent_path, sub_paths):
        result = set()
        parent_path_len = len(parent_path)
        for full_path in sub_paths:
            if not full_path.startswith(parent_path):
                continue
            sub_path = full_path[parent_path_len:]
            first_group = sub_path.split('/')[0]
            if first_group:
                result.add(str(first_group))
        return result

    def get_names(self):
        # query = {}
        # if self.path:
        #     query['path'] = {'$regex': self.get_path_equal_or_starts_with_regex(self.path)}
        # result = self.collection.distinct('_id', query)
        self.db.execute("""SELECT id FROM subscriptions WHERE value->>'path' ~ '{0}';"""
            .format(self.get_path_equal_or_starts_with_regex(self.path)))
        result = [pa['id'] for pa in self.db.fetchall()]
        return {'subscriptions': sorted(result)}

    def get_starts_with_regex(self, text):
        return r'^{0}'.format(text)

    def get_path_equal_or_starts_with_regex(self, text):
        return r'^{0}(/|$)'.format(text.strip('/'))

    def get_subscriptions(self):
        # subscriptions = self.collection.find(self.filter)
        l=[]
        if(self.filter!={}):
            for key,val in self.filter.items():
                l.append("value->'{0}' = {1}").format(key,val)
            if len(l)>1:
                self.whr=" AND ".join(l)
            else:
                self.whr=l[0]
        if(self.whr!=""):
            self.db.execute('SELECT * FROM subscriptions WHERE {0};'.format(self.whr))
        else:
            self.db.execute('SELECT * FROM subscriptions;')
        subscriptions=self.db.fetchall()
        return {'subscriptions': [rename_id_pg('subscription', sub) for sub in subscriptions]}

    def get_subscription(self, name):
        # self.db.execute("""PREPARE getsubscription AS SELECT value->'_id',value->'name' AS sitename FROM sites
        #         WHERE value->'_id' IN ($1);""")
        # self.db.execute("""EXECUTE getsiteids(%s)""", (siteids,))
        return self.collection.find_one(name)
