from tank.models.pg.site import SiteModel
from tank.util import get_logger, is_version_match, QueryBuilder
from tank.util import rep

log = get_logger()


class FieldModel(object):
    def __init__(self, db):
        self.db = db
        self.filter = {}
        self.mod_types_ignored = [3]
        self.whr=""

    def get_ual_component_version(self, component, version):
        self.db.execute("""PREPARE get_all_comp_ver AS SELECT temp1.siteid as siteid,  jsonb_agg(json_build_object
        ('version',temp1.Components->'version','hostname',temp1.hostname  )) as hosts FROM (SELECT id as id,jsonb_array_elements(value->'Components') 
         AS Components, value->'hostname' as hostname, value->>'siteid' AS siteid FROM facts_produ)AS temp1 
         WHERE temp1.Components->>'name'=$1 GROUP BY temp1.siteid;""")
        self.db.execute("EXECUTE get_all_comp_ver(%s)",(component,))
        return self.get_component_ual(self.db.fetchall(), version)

    def get_component_details(self, query=None):
        l=[]
        values=[]
        count=0
        orp=[]
        self.filter = FieldQuery(query).get_filter()
        print(self.filter)
        if self.filter.get('region') and not self.filter.get('siteid'):
            sites = SiteModel(self.db).get_sites(countries=[self.filter.get('region')])
            self.filter['siteid'] = [site.get('siteid') for site in sites.get('sites')]
            self.filter.pop('region')

        for key, value in self.filter.items():
            if isinstance(value, list):
                l.append(str(key) + " = ANY (" + "${0})".format(count+3))
            else:
                l.append(str(key) + " = " + "${0}".format(count+3))
            values.append(value)
            count += 1
        if (len(l)>1):
            self.whr = "".format(' AND '.join(l))
        if (len(l)==1):
            self.whr=l[0]

        if self.filter.get('productid', None):
            orp.append(" value->'Components'->>'name'=$1 OR value->>'product_name'=$2")
            orp.append(self.filter.pop('productid'))

            if self.whr!="":
                self.whr += "OR {0}".format(orp[0])
            else:
                self.whr+=orp[0]
            count+=2
        # print("WHERE IS",self.whr)
        if self.whr=="":
            # print("HERE")
            self.db.execute("""SELECT temp1.siteid as siteid,  json_agg(json_build_object('name',temp1.Components->'name','version',temp1.Components->'version',
                'timestamp',temp1.Components->>'timestamp', 'tag',temp1.tags )) as  "Components"
                FROM(SELECT value->>'siteid' as siteid ,jsonb_array_elements(value->'Components') as Components, 
                value->'Windows'->>'tags' as tags FROM facts ORDER BY value->'Components'->>'timestamp' DESC)as temp1 GROUP BY temp1.siteid;""")
        else:
            # print("Here")
            # print("In ELSE")
            self.db.execute("""PREPARE getcompdet AS SELECT temp1.siteid as siteid,  json_agg(json_build_object('name',temp1.Components->'name','version',temp1.Components->'version',
                'timestamp',temp1.Components->>'timestamp', 'tag',temp1.tags )) as  "Components"
                FROM(SELECT value->>'siteid' as siteid ,jsonb_array_elements(value->'Components') as Components, 
                value->'Windows'->>'tags' as tags FROM facts WHERE {0} ORDER BY value->'Components'->>'timestamp' DESC)as temp1 GROUP BY temp1.siteid;""".format(self.whr))
            params=orp[1]+orp[1]+values
            self.db.execute("EXECUTE getcompdet({0})".format(rep('%s',count)),params)
        match=self.db.fetchall()
        results = list(match)
        return {'result': results}

    def get_component_ual(self, site_hosts_with_component, version):
        result = []
        for site in site_hosts_with_component:
            match_hosts = filter(lambda x: is_version_match(version, x['version']), site['hosts'])
            if match_hosts:
                result.append(dict(siteid=site['siteid'], hosts=match_hosts))
        return result

    def get_unique_components(self):
        # matches = self.collection.distinct('Components.name')
        self.db.execute("""SELECT DISTINCT temp1.Component->>'name' as component 
        FROM(SELECT jsonb_array_elements(value->'Components') as Component FROM facts_produ)as temp1;""")
        matches=[temp['component'] for temp in self.db.fetchall() ]
        return {"components": list(matches)}

    def get_unique_models(self):
        # matches = self.collection.distinct('product_id')
        self.db.execute("""SELECT DISTINCT value->'product_id' as pro FROM facts_produ;""")
        matches = [temp['pro'] for temp in self.db.fetchall()]
        return {"result": list(matches)}

    def get_unique_product_names(self):
        # matches = self.collection.distinct('product_name')
        self.db.execute("""SELECT DISTINCT value->'product_name' as pro FROM facts_produ;""")
        matches = [temp['pro'] for temp in self.db.fetchall()]
        return {"result": list(matches)}

class FieldQuery(QueryBuilder):
    parameters = {
        'siteid': "value->>'siteid'",
        'productid': "value->>'productid'",
        'region': "value->>'region'"
    }