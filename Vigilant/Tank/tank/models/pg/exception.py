from tank.tnconfig import COLLECTIONS
from tank.util import get_logger


log = get_logger()


class ExceptionsModel(object):
    def __init__(self, db):
        self.db = db

    def get_exceptions(self, exception_type):
        return {'exceptions': "ENDPOINT NOT IN USE OR FUNCTION NOT DEFINED"}
