from tank.tnconfig import COLLECTIONS
from tank.util import get_projection_pg


class StateModel(object):
    def __init__(self, db, siteid):
        self.db = db
        # self.collection = mongodb[COLLECTIONS['STATES']]
        self.siteid = siteid

    def get_state(self, hostname, key):
        # match = self.collection.find_one(
        #     {'hostname': hostname, 'siteid': self.siteid},
        #     get_projection([key])
        # )
        self.db.execute("""PREPARE getstate AS SELECT {0} FROM states WHERE value->>'hostname'=$1 AND value->>'siteid'=$2""".format(get_projection_pg([key])))
        self.db.execute("EXECUTE getstate(%s,%s)",(hostname,self.siteid))
        return {'result':self.db.fetchall()}

    def get_siteids(self):
        self.db.execute("SELECT DISTINCT value->'siteid' AS siteid FROM states;")
        result = [site['siteid'] for site in self.db.fetchall()]
        return {'siteids': sorted(result)}

    def get_mappings(self, hostname):
        site_list = self.get_siteids().get('siteids')
        # result = self.collection.aggregate([
        #     {'$match': {'hostname': hostname, 'siteid': {'$in': site_list}}},
        #     {'$group': {'_id': {'siteid': '$siteid', 'ip': '$Administrative__Philips__Host__Information__IPAddress'}}},
        #     {'$project': {'_id': 0, 'siteid': '$_id.siteid', 'address': '$_id.ip'}}
        # ])
        self.db.execute("""PREPARE mappings AS SELECT value->'siteid' AS siteid, 
        value->'Administrative__Philips__Host__Information__IPAddress' as address FROM states WHERE value->>'hostname'
        =$1 and value->>'siteid'=ANY($2) GROUP BY value->'siteid',
        value->'Administrative__Philips__Host__Information__IPAddress'""")
        self.db.execute("EXECUTE mappings(%s,%s)",(hostname,site_list))
        result = [site for site in self.db.fetchall()]
        return {'result': result}
