from tnconfig import PGRES
import pypgwrap


class PgConnect(object):

    def __init__(self):
        self.uri =  "postgres://{0}:{1}@{2}:{3}/{4}".format(*PGRES)
        self.conn = pypgwrap.config_pool(max_pool=10, url=self.uri)
        self.db = pypgwrap.connection()
        # global db
