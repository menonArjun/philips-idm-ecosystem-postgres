import linecache
import psycopg2
from phimutils.resource import NagiosResourcer

DISCOVERY = {
    'NAGIOS_RESOURCE_FILE': '/etc/philips/shinken/resource.d/resource.cfg',
    'SECRET_FILE': '/etc/philips/secret'
}
secret = linecache.getline(DISCOVERY['SECRET_FILE'], 1).strip()
nr = NagiosResourcer(DISCOVERY['NAGIOS_RESOURCE_FILE'], secret)


class _CursorMgr(object):
    def __init__(self, user='', password='', db='DataOrchestration'):
        self._server = self.get_db_host_name()
        self._user = user
        self._password = password
        self._db = db
        self._cursor = None

    def get_db_host_name(self):
        return nr.get_resource("$USER137$")

    def connect(self):
        connection = pymssql.connect(self._server, self._user, self._password, self._db)
        self._cursor = connection.cursor()
        return self._cursor

    def __del__(self):
        if self._cursor:
            self._cursor.close()

user = nr.get_resource("$USER135$")
password = nr.get_resource("$USER136$")
_cm = _CursorMgr(user=user, password=password)

def CursorMgr():
    return _cm


class QueryMgr(object):
    columns = ['CONFIGVALUE']
    table_name = 'DORCONFIGURATION'
    where_cdn = ''

    def __init__(self):
        self.cursor = CursorMgr().connect()

    def execute(self):
        query_str = "select {cls} from {tbl} where {cdn}"
        query = query_str.format(cls=','.join(self.columns), tbl=self.table_name, cdn=self.where_cdn)
        self.cursor.execute(query)
        return self.cursor.fetchone()

