from tank.tnconfig import COLLECTIONS


class UserKeyModel(object):
    def __init__(self, mongodb):
        self.mongodb = mongodb
        self.collection = self.mongodb[COLLECTIONS['USERKEYS']]

    def get_userkey(self, siteid):
        matches = self.collection.aggregate([
            {'$match': {'siteid': siteid}},
            {'$project': {'hostname':1, 'siteid': 1, 'key': 1, '_id': 0}}
        ])
        return {'result': list(matches)}

    def save_userkey(self, hostname, siteid, key):
	self.collection.insert_one({'hostname': hostname, 'siteid': siteid, 'key': key})
        return {'result': True}
