from tank.models.site import SiteModel
from tank.tnconfig import COLLECTIONS
from tank.util import QueryBuilder
from tank.util import get_date_to_string_field_projection, rename_id


class EventModel(object):
    def __init__(self, mongodb):
        self.mongodb = mongodb
        self.collection = self.mongodb[COLLECTIONS['EVENTS']]
        self.filter = {}
        self._site_names = None

    def get_matches(self):
        matches = self.collection.aggregate([
            {'$match': self.filter},
            {'$project': self.get_projection()},
            # {'$sort': {'timestamp': 1}}
        ])
        return matches

    def get_historical(self, query):
        self.filter = EventQuery(query).get_filter()
        return {'events': list(self.get_matches())}

    def get_projection(self):
        return {
            'state': '$state',
            'service': '$service',
            'timestamp': get_date_to_string_field_projection('$timestamp'),
            'siteid': '$siteid',
            'hostaddress': '$hostaddress',
            'hostname': '$hostname',
            'output': '$output',
            'type': '$type',
            'components': '$components',
            'modules': '$modules',
            '_id': 0
        }

    def get_affected_siteids(self):
        return list(self.collection.distinct('siteid', self.filter))

    @property
    def site_names(self):
        if self._site_names is None:
            self._site_names = SiteModel(self.mongodb).get_site_names(self.get_affected_siteids())
        return self._site_names

    def get_handler_matches(self):
        matches = self.collection.aggregate([
            {'$match': self.filter},
            {
                '$group': {
                    '_id': {'hostname': '$hostname','siteid': '$siteid','timestamp': '$timestamp'},
                    'success': {'$sum': {'$cond': [{'$eq': ['$state', 'OK']}, 1, 0]}},
                    'failure': {'$sum': {'$cond': [{'$eq': ['$state', 'WARNING']}, 1, 0]}},
                    'total': {'$sum': 1}
                }
            },
            {
                '$group': {
                    '_id': '$_id.siteid',
                    'success': {'$sum': '$success'},
                    'failure': {'$sum': '$failure'},
                    'total': {'$sum': '$total'},
                    'hosts': {
                        '$push': {
                            'hostname': '$_id.hostname', 'success': '$success', 'failure': '$failure',
                            'total': '$total',
                            'timestamp': {'$dateToString': {'format': "%Y/%m/%d %H:%M:%S", 'date': "$_id.timestamp"}}
                        }
                    }
                }
            }
        ])
        print(matches)
        return matches

    def adjust_handler_entry(self, site):
        """Destructively adjust the key names for a site object"""
        site = rename_id('siteid', site)
        site['name'] = self.site_names.get(site['siteid'])
        return site

    def get_handler_report(self, service, query):
        query['service'] = service
        self.filter = EventQuery(query).get_filter()
        self.filter['type'] = 'HANDLER'
        self.filter['state'] = {'$in': ['OK', 'WARNING']}
        return {'result': [self.adjust_handler_entry(site) for site in self.get_handler_matches()]}


class EventQuery(QueryBuilder):
    parameters = {
        'siteid': 'siteid',
        'state': 'state',
        'service': 'service',
        'timestamp': 'timestamp',
        'component': 'components.name',
        'module': 'modules',
    }

    def process_query(self):
        start_date = self.query.pop('start_date', None)
        end_date = self.query.pop('end_date', None)
        self.query['timestamp'] = self.get_between_dates_filter(start_date, end_date)
