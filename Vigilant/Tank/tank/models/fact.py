import re

from tank.tnconfig import COLLECTIONS, PUBLIC_MODULES, PUBLIC_HOST_KEYS
from tank.util import get_logger, get_projection, get_keys_in_site
from tank.models.dashboard import DashboardModel
from datetime import datetime

log = get_logger()


class SiteFactsModel(object):
    def __init__(self, mongodb, siteid):
        self.mongodb = mongodb
        self.siteid = siteid
        self.collection = mongodb[COLLECTIONS['FACTS']]

    def get_hosts(self):
        hosts = self.collection.distinct('hostname', filter={'siteid': self.siteid})
        return {'hosts': hosts}

    def get_host_detail(self, hostname):
        public_modules_keys = list(PUBLIC_MODULES.keys())
        match = self.collection.find_one(
            {'hostname': hostname, 'siteid': self.siteid},
            get_projection(['hostname'] + PUBLIC_HOST_KEYS + public_modules_keys)
        )
        return self.get_filtered_host_facts(match, PUBLIC_HOST_KEYS, public_modules_keys)

    def get_modules(self):
        site_facts = self.collection.find({'siteid': self.siteid}, get_projection(PUBLIC_MODULES.keys()))
        return {'modules': get_keys_in_site(site_facts)}

    def get_item_details(self, item):
        match = self.collection.find(
            {'siteid': self.siteid, item: {'$exists': True}},
            get_projection(['hostname', item])
        )
        return {'result': list(match)}

    def get_components(self):
        match = self.collection.find(
            {'siteid': self.siteid, 'Components': {'$exists': True}},
            get_projection(['hostname', 'Components'])
        )
        return {'results': list(match)}

    def get_host_components(self, hostname):
        match = self.collection.aggregate([
            {'$unwind': '$Components'},
            {'$match': {'_id': self.siteid + '-' + hostname, 'Components': {'$exists': True}}},
            {'$group': {'_id': {'name': '$Components.name', 'version': '$Components.version',
                                'timestamp': {'$dateToString': {'format': "%Y/%m/%d %H:%M:%S",
                                                              'date': '$Components.timestamp'}}}}},
            {'$group': {'_id': '$_id.name', 'version': {'$first':'$_id.version'}, 'timestamp':{'$first':'$_id.timestamp'}}},
            {"$sort": {"timestamp": -1}},
            {'$project': {'_id': 0, 'name': '$_id', 'version': '$version', 'timestamp': '$timestamp'}}
        ])
        return {'results': list(match)}

    def get_one_component(self, component):
        match = self.collection.aggregate(
            [
                {'$match': {'siteid': self.siteid, 'Components.name': component}},
                {'$project': {'_id': 0, 'hostname': 1, 'Components': 1}},
                {'$unwind': '$Components'},
                {'$match': {'Components.name': component}}
            ]
        )
        return {'results': list(match)}

    def get_filtered_host_facts(self, host_facts, keys, modules):
        result = {}
        if host_facts:
            result['hostname'] = host_facts['hostname']
            result.update([(key, host_facts[key]) for key in keys if key in host_facts])
            result['modules'] = [key for key in host_facts.keys() if key in modules]
        return result

    def get_node_details(self):
        match = self.collection.aggregate(
            [{'$match': {'$and': [{'siteid': self.siteid}, {'hostname': {'$not': re.compile('localhost')}}]}},
             {
                 '$project': {
                     '_id': 0,
                     'hostname': '$hostname',
                     'address': '$address',
                     'vCenter': '$vCenter.vms',
                     'module_type': '$ISP.module_type',
                     'modules': '$modules',
                     'servicetag': '$servicetag',
                     'Windows': '$Windows',
                     'HPSwitch': '$HPSwitch',
                     'Components': '$Components'
                 }
             }
             ])
        nodes = list(match)
        hosts_down = DashboardModel(self.mongodb).get_nodes_down(self.siteid)
        vms = []
        vm_status = {'poweredOn': 'Up', 'poweredOff': 'Down', 'suspended': 'Down'}
        hostnames = []
        localhost_regex = re.compile('localhost')
        vcenters = []
        for node in nodes:
            node.update({'discovery_type':'Discovery'})
            if node['hostname'] in hosts_down:
                node['status'] = 'Down'
            else:
                node['status'] = 'Up'
                hostnames.append(node['hostname'])
            components = node.get('Components', [])
            for component in components:
                component['timestamp'] = (component.get('timestamp', None).strftime('%Y/%m/%d  %H:%M:%S') 
                    if component.get('timestamp', None) else None)
            if 'vCenter' in node:
                vcenter_nodes = node['vCenter']
                node.pop('vCenter')
                vcenters.append(vcenter_nodes)
        if len(vcenters) > 0:
            for vcenter in vcenters:
                for vm in vcenter:
                    vm_data = {}
                    if ('hostName' in vm['guest']) and (not localhost_regex.match(vm['guest']['hostName'])) and (
                                (vm['guest']['hostName']).lower() not in hostnames):
                        vm_data.update({'status': vm_status[vm['powerState']]})
                        vm_data.update({'hostname': vm['guest'].get('hostName',None)})
                        vm_data.update({'address': vm['guest'].get('ipAddress',None)})
                        vm_data.update({'discovery_type':'vCenter'})
                        vms.append(vm_data)
            nodes.extend(vms)
        return {'result': nodes}

    def get_facts_hosts(self):
        match = self.collection.aggregate([
            {'$group': {
                '_id': '$siteid',
                'result': {'$push': {
                    'hostname': '$hostname',
                    "vCenter": "$vCenter"
                }}
            }},
            {
                '$project': {
                    '_id': 1,
                    'result': 1
                }
            }
        ])
        return list(match)

    def get_host_count(self):
        return {'count': len(self.collection.distinct('_id',{'hostname':{'$nin':['localhost']}}))}

