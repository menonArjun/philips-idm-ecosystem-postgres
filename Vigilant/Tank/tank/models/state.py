from tank.tnconfig import COLLECTIONS
from tank.util import get_projection


class StateModel(object):
    def __init__(self, mongodb, siteid):
        self.mongodb = mongodb
        self.collection = mongodb[COLLECTIONS['STATES']]
        self.siteid = siteid
        
    def get_state(self, hostname, key):
        match = self.collection.find_one(
            {'hostname': hostname, 'siteid': self.siteid},
            get_projection([key])
        )
        return {'result': match.get(key)}

    def get_siteids(self):
        result = self.collection.distinct('siteid')
        return {'siteids': sorted(list(result))}

    def get_mappings(self, hostname):
        site_list = self.get_siteids().get('siteids')
        result = self.collection.aggregate([
            {'$match': { 'hostname': hostname,'siteid': {'$in': site_list}}},
            {'$group': { '_id' : { 'siteid' : '$siteid', 'ip' : '$Administrative__Philips__Host__Information__IPAddress'}}},
            {'$project' : { '_id':0 , 'siteid':'$_id.siteid', 'address':'$_id.ip'}}
        ])
        return {'result': list(result)}
