import re

from tank.models.fact import SiteFactsModel
from tank.models.dashboard import DashboardModel
from subscription import SubscriptionModel
from tank.tnconfig import COLLECTIONS
from tank.util import rename_id


class SiteModel(object):
    def __init__(self, mongodb):
        self.mongodb = mongodb
        self.collection = mongodb[COLLECTIONS['SITES']]
        self.filter = {}

    def get_siteids(self):
        result = self.collection.distinct('_id')
        return {'siteids': list(result)}

    def set_siteid_filter(self, siteids=None):
        if siteids:
            self.filter['_id'] = {'$in': siteids}

    def set_country_filter(self, countries=None):
        if countries:
            self.filter['country.country'] = {'$in': countries}

    def get_site_names(self, siteids=None):
        """Returns a map of siteid to site name"""
        self.set_siteid_filter(siteids)
        sites_info = self.collection.find(self.filter, {'name': 1})
        return dict((site['_id'], site.get('name')) for site in sites_info)

    def get_sites(self, siteids=None, countries=None):
        def getKey(site):
            return site.get('_id')
        """ Get Site details """
        self.set_siteid_filter(siteids)
        self.set_country_filter(countries)
        print(self.filter)
        sites_info = self.collection.find(self.filter,{'Components':0})
        return {'sites': [rename_id('siteid', site) for site in sorted(sites_info, key=getKey)]}

    def get_subscription_candidates(self, subscription_name):
        subscription = SubscriptionModel(self.mongodb).get_subscription(subscription_name)
        countries = subscription.get('countries')
        if not countries:
            return {'sites': []}
        return self.get_sites(countries=countries)


    def get_facts_hosts_details(self):
        sites = self.get_sites()['sites']
        db = DashboardModel(self.mongodb)
        db.filter = {'service': 'Product__IDM__Nebuchadnezzar__Alive__Status', 'unreachable': {'$not': {'$eq': True}}}
        neb_status = db.get_matches()
        neb_siteids = []
        if len(neb_status) > 0:
            neb_siteids = [site['siteid'] for site in neb_status['result']]
        localhost_regex = re.compile('localhost')
        hostnames = []
        for site in sites:
            if site['siteid'] in neb_siteids:
                site['status'] = 'Down'
            else:
                site['status'] = 'Up'
        return {'result': sites}

    def get_host_count(self, vcenters, localhost_regex, hostnames):
        count = 0
        for vcenter in vcenters:
            if 'vms' in vcenter:
                for vm in vcenter['vms']:
                    if ('hostName' in vm['guest']) and \
                            (not localhost_regex.match(vm['guest']['hostName'])) and \
                            ((vm['guest']['hostName']).lower() not in hostnames):
                        count += 1
        return count

    def update_site(self, siteid, name, country):
        self.collection.update_one({'_id': siteid}, {'$set': {'name': name, 'country': country}}, upsert=False)

    def delete_site(self, siteid):
        self.collection.delete_one({'_id': siteid})

    def get_site_count(self):
        return {'count': len(self.collection.distinct('_id',{'production':{'$ne': False}}))}