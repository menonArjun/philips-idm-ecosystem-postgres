from tank.models.site import SiteModel
from tank.tnconfig import COLLECTIONS
from tank.util import get_logger, is_version_match, QueryBuilder
from tank.util import get_date_to_string_field_projection

log = get_logger()


class FieldModel(object):
    def __init__(self, mongodb):
        self.mongodb = mongodb
        self.collection = mongodb[COLLECTIONS['FACTS']]
        self.filter = {}
        self.mod_types_ignored = [3]

    def get_ual_component_version(self, component, version):
        site_hosts_with_component = self.collection.aggregate(
            [
                {'$match': {'Components.name': component}},
                {'$project': {'_id': 0, 'siteid': 1, 'hostname': 1, 'Components': 1}},
                {'$unwind': '$Components'},
                {'$match': {'Components.name': component}},
                {'$group': {
                    '_id': '$siteid',
                    'hosts': {'$push': {'hostname': '$hostname', 'version': '$Components.version'}}
                }},
                {'$project': {'_id': 0, 'siteid': '$_id', 'hosts': 1}}
            ]
        )
        return {'res' :self.get_component_ual(site_hosts_with_component, version)}

    def get_component_details(self, query=None):
        self.filter = FieldQuery(query).get_filter()
        if self.filter.get('region') and not self.filter.get('siteid'):
            sites = SiteModel(self.mongodb).get_sites(countries=[self.filter.get('region')])
            self.filter['siteid'] = {'$in': [site.get('siteid') for site in sites.get('sites')]}
            self.filter.pop('region')

        if self.filter.get('productid', None):
             self.filter['$or'] = [{'Components.name': self.filter.get('productid')},
                                  {'product_name': self.filter.pop('productid')}]
        match = self.collection.aggregate(
            [
                {'$project': {'_id': 0, 'siteid': 1, 'Components': 1, 'Windows.tags': 1}},
                {'$unwind': '$Components'},
                {'$sort': {'Components.timestamp': -1}},
                {'$match': self.filter},
                {'$group': {'_id': '$siteid', 'Components': {
                    '$push': {'name': '$Components.name', 'version': '$Components.version',
                              'timestamp': {'$dateToString': {'format': "%Y/%m/%d %H:%M:%S",
                                                              'date': '$Components.timestamp'}},
                              'tag': '$Windows.tags'
                              }}}},
                {'$project': {'_id': 0, 'siteid': '$_id', 'Components': 1}}
            ]
        )
        print(self.filter)
        results = list(match)
        return {'result': results}

    def get_component_ual(self, site_hosts_with_component, version):
        result = []
        for site in site_hosts_with_component:
            match_hosts = filter(lambda x: is_version_match(version, x['version']), site['hosts'])
            if match_hosts:
                # result[site['siteid']]=match_hosts
                result.append(dict(siteid=site['siteid'], hosts=match_hosts))
        return result

    def get_unique_components(self):
        matches = self.collection.distinct('Components.name')
        return {"components": matches}

    def get_unique_models(self):
        matches = self.collection.distinct('product_id')
        return {"result": sorted(matches)}

    def get_unique_product_names(self):
        matches = self.collection.distinct('product_name')
        return {"result": sorted(matches)}

class FieldQuery(QueryBuilder):
    parameters = {
        'siteid': 'siteid',
        'productid': 'productid',
        'region': 'region'
    }