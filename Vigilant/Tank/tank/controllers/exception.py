from tank.models.exception import ExceptionsModel
from tank.util import get_bottle_app_with_mongo


def get_app(uri, db_name):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name)
    app.get('/<exception_type>', callback=get_exceptions)
    return app


def get_exceptions(mongodb, exception_type):
    return ExceptionsModel(mongodb).get_exceptions(exception_type)
