from bottle import request
from tank.models.pg.dashboard import DashboardModel
from tank.util import get_bottle_app_with_Pgres


def get_app(uri):
    app = get_bottle_app_with_Pgres(uri) #Postgres connection object is intialiazed with this call that is defined in Util
    app.get('/', callback=get_dashboard_data)
    app.get('/unreachableservices/', callback=get_unreachable_services)
    app.get('/unreachableservices/count', callback=get_unreachable_services_count)
    app.get('/namespace/containers/<container>', callback=get_namespace_container)
    app.get('/modules', callback=get_modules)
    app.get('/stats', callback=get_stats)
    app.get('/stats/products', callback=get_products_stats)
    app.get('/appservices/<applists>', callback=get_app_services)
    app.get('/appservices/', callback=get_app_services)
    app.get('/stats/devices', callback=get_device_stats)
    app.post('/case_number/', callback=update_case_number)
    app.post('/acknowledge', callback=acknowledge_events)
    return app

#db parameter is the default value defined by pg-sql to identify connection object requests

def get_dashboard_data(db):
    return DashboardModel(db).get_dashboard(request.query)


def get_unreachable_services(db):
    return DashboardModel(db).get_unreachable_services()

def get_unreachable_services_count(db):
    return DashboardModel(db).get_unreachable_services_count()

def update_case_number(db):
    identifier = '{siteid}-{hostname}-{service}'.format(**request.json)
    return DashboardModel(db).update_case_number(identifier, request.json['case_number'])


def get_namespace_container(db, container):
    return DashboardModel(db).get_namespace_container(container, request.query)

def get_modules(db):
    return DashboardModel(db).get_modules()

def get_stats(db):
    return DashboardModel(db).get_stats()

def get_products_stats(db):
    return DashboardModel(db).get_prod_stats()

#Same call as get_stats()...
def get_device_stats(db):
    return DashboardModel(db).get_stats()

def acknowledge_events(db):
    return DashboardModel(db).acknowledge_events(request.json['ids'],request.json['tag'])

def get_app_services(db, applists=""):
    return DashboardModel(db).get_app_services(request.query, applists)