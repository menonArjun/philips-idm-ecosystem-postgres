from tank.models.pg.state import StateModel
from tank.util import get_bottle_app_with_Pgres
from tank.tnconfig import NEB_HOSTNAME


def get_app(uri):
    app = get_bottle_app_with_Pgres(uri=uri)    #Postgres connection object is intialiazed with this call that is defined in Util
    app.get('/siteid/<siteid>/hostname/<hostname>/key/<key>', callback=get_key_state)
    app.get('/siteids', callback=get_all_siteids)
    app.get('/sites/thruk/mapping', callback=get_sites_thruk_mapping)
    return app

#db parameter is the default value defined by pg-sql to identify connection object requests

def get_key_state(db, siteid, hostname, key):
    return StateModel(db, siteid).get_state(hostname, key)


def get_all_siteids(db):
    return StateModel(db, None).get_siteids()

def get_sites_thruk_mapping(db):
    return StateModel(db, None).get_mappings(NEB_HOSTNAME)


