from bottle import request
from tank.models.pg.tags import TagsModel
from tank.util import get_bottle_app_with_Pgres


def get_app(uri):
    app = get_bottle_app_with_Pgres(uri=uri)    #Postgres connection object is intialiazed with this call that is defined in Util
    app.get('/', callback=get_tags_data)
    app.post('/addtag', callback=save_tag)
    return app

#db parameter is the default value defined by pg-sql to identify connection object requests

def get_tags_data(db):
    return TagsModel(db).get_tags()


def acknoledge_event(db):
    identifier = '{siteid}-{hostname}-{service}--{status}'.format(**request.json)
    return TagsModel(db).acknoledge_event(identifier, 'ACK')


def save_tag(db):
    return TagsModel(db).save_tag(request.json['tag'], request.json['description'])

