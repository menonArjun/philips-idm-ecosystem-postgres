from tank.models.pg.exception import ExceptionsModel
from tank.util import get_bottle_app_with_Pgres


def get_app(uri):
    app = get_bottle_app_with_Pgres(uri=uri)    #Postgres connection object is intialiazed with this call that is defined in Util
    app.get('/<exception_type>', callback=get_exceptions)
    return app

#db parameter is the default value defined by pg-sql to identify connection object requests

def get_exceptions(db, exception_type):
    return ExceptionsModel(db).get_exceptions(exception_type)
