from bottle import request

from tank.models.pg.field import FieldModel
from tank.util import get_bottle_app_with_Pgres, get_logger


log = get_logger()


def get_app(uri):
    app = get_bottle_app_with_Pgres(uri=uri)    #Postgres connection object is intialiazed with this call that is defined in Util
    app.get('/ual/<component>/version/<version>', callback=get_ual_component_version)
    app.get('/components', callback=get_field_component_details)
    app.get('/components/search', callback=get_field_component_details)
    app.get('/components/unique', callback=get_unique_components)
    app.get('/tags/unique', callback=get_unique_tags)
    app.get('/modules', callback=get_unique_models)
    app.get('/product_names', callback=get_unique_product_names)
    return app

#db parameter is the default value defined by pg-sql to identify connection object requests

def get_ual_component_version(db, component, version):
    log.debug('Get UAL for component %s matching a search of %s', component, version)
    return FieldModel(db).get_ual_component_version(component, version)


def get_field_component_details(db):
    log.debug('Getting all components installed in the field by site')
    return FieldModel(db).get_component_details(request.query)


def get_unique_components(db):
    return FieldModel(db).get_unique_components()

def get_unique_models(db):
    return FieldModel(db).get_unique_models()

def get_unique_product_names(db):
    return FieldModel(db).get_unique_product_names()

def get_unique_tags(db):
    return FieldModel(db).get_unique_tags()