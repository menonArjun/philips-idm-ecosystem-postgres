from collections import defaultdict
from tank.util import get_logger
from tank.models.pg.state import StateModel
from tank.tnconfig import NEB_HOSTNAME, COLLECTIONS, NEB_IP_NAMESPACE, MONITOR_URL_TEMPLATE
from tank.util import get_bottle_app_with_Pgres


log = get_logger()


def get_app(uri):
    app = get_bottle_app_with_Pgres(uri=uri)    # Postgres connection object is initialized --> Util
    app.get('/status', callback=get_info_status)
    app.get('/status/siteid/<siteid>', callback=get_info_status)
    app.get('/status/state/<state>', callback=get_info_status)
    app.get('/monitor/url/siteid/<siteid>', callback=get_monitor_url)
    app.get('/monitor/address/siteid/<siteid>', callback=get_monitor_address)
    return app

# db parameter is the default value defined by pg-sql to identify connection object requests


def get_info_status(db, siteid=None, state=None):
    union = True
    if siteid:
        log.debug('Getting Site Status Report')
        if state:
            union = False
            db.execute("""PREPARE alive AS SELECT id, value->>'alive_expiration' AS alive_expiration,
                          value->>'last_state' AS last_state FROM alives WHERE id=$1 and value->>'last_state'=$2;""")
            db.execute("EXECUTE alive(%s,%s)", (siteid, state,))
        else:
            db.execute("""PREPARE alive AS SELECT id, value->>'alive_expiration' AS alive_expiration,
                                       value->>'last_state' AS last_state FROM alives WHERE id=$1;""")
            db.execute("EXECUTE alive(%s)", (siteid,))
        alives = db.fetchall()

        db.execute("""PREPARE states AS SELECT value->>'siteid' AS siteid,value->>'{0}' FROM states WHERE 
                      value->>'siteid'=$1 AND value->>'hostname'=$2;""".format(NEB_IP_NAMESPACE))
        db.execute("EXECUTE states(%s,%s)", (siteid, NEB_HOSTNAME,))
        states = db.fetchall()

        db.execute("""PREPARE hosts AS SELECT value->>'siteid' AS id, array_length(array_agg(value->>'hostname'),1)
                      AS hosts FROM states WHERE id=$1 GROUP BY value->>'siteid';""")
        db.execute("EXECUTE hosts(%s)", (siteid,))
        hosts = db.fetchall()

        db.execute("""PREPARE site AS SELECT value->>'siteid' AS siteid, value->>'sitename' as sitename FROM sites
                      WHERE value->>'siteid'=$1;""")
        db.execute("EXECUTE site(%s)", (siteid,))
        names = db.fetchall()

    else:
        db.execute("""SELECT id, value->>'alive_expiration' AS alive_expiration,
                                           value->>'last_state' AS last_state FROM alives;""")
        alives = db.fetchall()

        db.execute("""SELECT value->>'siteid' AS siteid,value->>'{0}' FROM states;""".format(NEB_IP_NAMESPACE))
        states = db.fetchall()

        db.execute("""SELECT value->>'siteid' AS id, array_length(array_agg(value->>'hostname'),1) AS hosts FROM states
                    GROUP BY value->>'siteid';""")
        hosts = db.fetchall()

        db.execute("""SELECT value->>'_id' AS siteid, value->>'name' as sitename FROM sites;""")
        names = db.fetchall()
    return get_site_status(hosts, alives, states, names, union)


def get_site_status(hosts, alives, states, names, union=True):
    joined_result = defaultdict(dict)

    for alive in alives:
        joined_result[alive['id']]['siteid'] = alive['id']
        if 'last_state' in alive:
            joined_result[alive['id']]['last_state'] = alive['last_state']
        if 'alive_expiration' in alive:
            joined_result[alive['id']]['alive_expiration'] = str(alive['alive_expiration'])

    for host in hosts:
        if union or host['id'] in joined_result:
            if 'hosts' in host:
                joined_result[host['id']]['hosts'] = host['hosts']

    for state in states:
        if union or state['siteid'] in joined_result:
            if NEB_IP_NAMESPACE in state:
                joined_result[state['siteid']]['nebnode'] = state[NEB_IP_NAMESPACE]

    for name in names:
        if name['siteid'] in joined_result:
            joined_result[name['siteid']]['sitename'] = name['sitename']

    return {'info' : joined_result.values()}


def get_monitor_address(db, siteid):
    address = StateModel(db, siteid).get_state(NEB_HOSTNAME, NEB_IP_NAMESPACE).get('result')
    return dict(address=address)


def get_monitor_url(db, siteid):
    address = get_monitor_address(db, siteid).get('address')
    return dict(url=MONITOR_URL_TEMPLATE.format(address=address) if address else None)
