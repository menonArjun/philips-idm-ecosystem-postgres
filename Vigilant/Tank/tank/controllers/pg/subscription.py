from tank.models.pg.subscription import SubscriptionModel
from tank.util import get_bottle_app_with_Pgres, get_logger


log = get_logger()


def get_app(uri):
    app = get_bottle_app_with_Pgres(uri=uri)    #Postgres connection object is intialiazed with this call that is defined in Util
    app.get('/groups', callback=get_groups)
    app.get('/groups/<path:path>', callback=get_groups)
    app.get('/names', callback=get_names)
    app.get('/names/<path:path>', callback=get_names)
    app.get('/details', callback=get_subscriptions)
    return app


def get_groups(db, path=''):
    model = SubscriptionModel(db, path)
    log.debug('Getting subscription groups in path %s', model.slashed_path)
    return model.get_groups()


def get_names(db, path=''):
    log.debug('Getting subscriptions in path %s', path)
    return SubscriptionModel(db, path).get_names()


def get_subscriptions(db):
    log.debug('Getting detailed subscriptions list')
    return SubscriptionModel(db).get_subscriptions()
