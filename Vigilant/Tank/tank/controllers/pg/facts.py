#from tank.modelsPgress.PgWrapFact import SiteFactsModel

from tank.util import get_bottle_app_with_Pgres, get_logger
from tank.models.pg.fact import SiteFactsModel
log = get_logger()


def get_app(uri):
    app = get_bottle_app_with_Pgres(uri)    #Postgres connection object is intialiazed with this call that is defined in Util
    app.get('/<siteid>/hosts', callback=get_site_hosts)
    app.get('/<siteid>/hosts/<hostname>', callback=get_host_detail)
    app.get('/<siteid>/modules', callback=get_site_modules)
    app.get('/<siteid>/modules/<module>', callback=get_site_item_details)
    app.get('/<siteid>/modules/<module>/keys/<key>', callback=get_site_item_details)
    app.get('/<siteid>/components', callback=get_site_components)
    app.get('/<siteid>/<hostname>/components', callback=get_site_host_components)
    app.get('/<siteid>/components/<component>', callback=get_site_one_component)
    app.get('/<siteid>/nodes', callback=get_site_nodes)
    app.get('/count', callback=get_host_count)
    return app

#db parameter is the default value defined by pg-sql to identify connection object requests

def get_site_hosts(db, siteid):
    # log.debug('Getting hosts for site %s', siteid)
    return SiteFactsModel(db, siteid).get_hosts()


def get_host_detail(db, siteid, hostname):
    # log.debug('Getting info on %s in site %s', hostname, siteid)
    return SiteFactsModel(db, siteid).get_host_detail(hostname)


def get_site_modules(db, siteid):
    # log.debug('Getting modules for site %s', siteid)
    return SiteFactsModel(db, siteid).get_modules()


def get_site_item_details(db, siteid, module, key=None):
    key_path = [module]
    if key:
        key_path.append(key)
    item = '.'.join(key_path)
    # log.debug('Getting hosts with %s for site %s', item, siteid)
    return SiteFactsModel(db, siteid).get_item_details(item)


def get_site_components(db, siteid):
    # log.debug('Get components for %s', siteid)
    return SiteFactsModel(db, siteid).get_components()


def get_site_host_components(db, siteid, hostname):
    # log.debug('Get components for %s and %s', hostname, siteid)
    return SiteFactsModel(db, siteid).get_host_components(hostname)


def get_site_one_component(db, siteid, component):
    # log.debug('Get components for %s', siteid)
    return SiteFactsModel(db, siteid).get_one_component(component)


def get_site_nodes(db, siteid):
    # log.debug('Get nodes for %s', siteid)
    return SiteFactsModel(db, siteid).get_node_details()


def get_host_count(db):
    # log.debug('Get total host count')
    return SiteFactsModel(db, '').get_host_count()
