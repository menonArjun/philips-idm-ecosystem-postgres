from bottle import request

from tank.models.pg.event import EventModel
from tank.tnconfig import HANDLER_SERVICES
from tank.util import get_bottle_app_with_Pgres


def get_app(uri):
    app = get_bottle_app_with_Pgres(uri=uri)    #Postgres connection object is intialiazed with this call that is defined in Util
    app.get('/history', callback=get_historical_data)
    app.get('/handlerservices', callback=get_handler_services)
    app.get('/handlerreport/services/<service>', callback=get_handler_report)
    return app

#db parameter is the default value defined by pg-sql to identify connection object requests

def get_historical_data(db):
    return EventModel(db).get_historical(request.query)


def get_handler_services():
    return {'services': HANDLER_SERVICES}


def get_handler_report(db, service):
    return EventModel(db).get_handler_report(service, request.query)
