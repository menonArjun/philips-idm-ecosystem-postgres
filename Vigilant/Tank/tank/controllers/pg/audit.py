from bottle import request
from tank.models.pg.audit import AuditDistributionModel, AuditDeploymentModel
from tank.util import get_bottle_app_with_Pgres


def get_app(uri):
    app = get_bottle_app_with_Pgres(uri=uri) #Postgres connection object is intialiazed with this call that is defined in Util
    app.get('/distribution', callback=get_distribution_data)
    app.get('/deployment', callback=get_deployment_data)
    return app

#db parameter is the default value defined by pg-sql to identify connection object requests

def get_distribution_data(db):
    return AuditDistributionModel(db).get_data(request.query)


def get_deployment_data(db):
    return AuditDeploymentModel(db).get_data(request.query)
