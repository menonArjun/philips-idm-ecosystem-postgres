from tank.models.pg.country import CountryModel
from tank.util import get_bottle_app_with_Pgres


def get_app(uri):
    app = get_bottle_app_with_Pgres(uri=uri)
    app.get('/', callback=get_countries)
    return app


def get_countries(db):
    return CountryModel(db).get_countries()
