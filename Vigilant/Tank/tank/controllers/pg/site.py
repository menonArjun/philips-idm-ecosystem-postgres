from tank.models.pg.site import SiteModel
from tank.util import get_bottle_app_with_Pgres, get_list_from_param
from bottle import request

def get_app(uri):
    app = get_bottle_app_with_Pgres(uri)    #Postgres connection object is intialiazed with this call that is defined in Util
    app.get('/details/siteids/<siteids>', callback=get_sites_details)
    app.get('/details/countries/<countries>', callback=get_sites_details)
    app.get('/details/facts', callback=get_sites_facts_details)
    app.get('/details', callback=get_sites_details)
    app.get('/siteids', callback=get_all_siteids)
    app.get('/details/candidates/subscription/<subscription_name>', callback=get_subscription_candidates)
    app.post('/<siteid>', callback=update_site)
    app.delete('/<siteid>', callback=delete_site)
    app.get('/count', callback=get_site_count)
    return app

#db parameter is the default value defined by pg-sql to identify connection object requests

def get_sites_details(db, siteids=None, countries=None):
    # siteids = get_list_from_param(siteids)
    # countries = get_list_from_param(countries)
    return SiteModel(db).get_sites(siteids, countries)


def get_all_siteids(db):
    return SiteModel(db).get_siteids()


def get_subscription_candidates(db, subscription_name):
    return SiteModel(db).get_subscription_candidates(subscription_name)


def get_sites_facts_details(db):
    return SiteModel(db).get_facts_hosts_details()


def update_site(db, siteid):
    name = request.json['name']
    country = request.json['country']
    return SiteModel(db).update_site(siteid=siteid, name=name, country=country)


def delete_site(db, siteid):
    return SiteModel(db).delete_site(siteid)


def get_site_count(db):
    return SiteModel(db).get_site_count()
