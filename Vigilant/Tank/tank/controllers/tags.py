from bottle import request
from tank.models.tags import TagsModel
from tank.util import get_bottle_app_with_mongo


def get_app(uri, db_name):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name)
    app.get('/', callback=get_tags_data)
    app.post('/addtag', callback=save_tag)
    return app


def get_tags_data(mongodb):
    return TagsModel(mongodb).get_tags()


def acknoledge_event(mongodb):
    identifier = '{siteid}-{hostname}-{service}--{status}'.format(**request.json)
    return TagsModel(mongodb).acknoledge_event(identifier, 'ACK')


def save_tag(mongodb):
    return TagsModel(mongodb).save_tag(request.json['tag'], request.json['description'])
