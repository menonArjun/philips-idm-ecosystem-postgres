from bottle import request
from tank.models.userkey import UserKeyModel
from tank.util import get_bottle_app_with_mongo


def get_app(uri, db_name):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name)
    app.get('/<siteid>', callback=get_userkey_data)
    app.post('/', callback=save_userkey)
    return app


def get_userkey_data(mongodb, siteid):
    return UserKeyModel(mongodb).get_userkey(siteid)


def acknoledge_event(mongodb):
    identifier = '{siteid}-{hostname}-{service}--{status}'.format(**request.json)
    return UserKeyModel(mongodb).acknoledge_event(identifier, 'ACK')


def save_userkey(mongodb):
    return UserKeyModel(mongodb).save_userkey(request.json.get('hostname'),
                                              request.json.get('siteid'),
                                              request.json.get('key'))
