from tank.models.country import CountryModel
from tank.util import get_bottle_app_with_mongo


def get_app(uri, db_name):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name)
    app.get('/', callback=get_countries)
    return app


def get_countries(mongodb):
    return CountryModel(mongodb).get_countries()