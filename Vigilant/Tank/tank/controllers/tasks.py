import sys

from bottle import Bottle, request
from celery import Celery

from tank.tnconfig import CELERY_CONFIGURATION_OBJECT, CELERY_PATH, IN_TASK_NAME
from tank.util import get_logger


celery = Celery()
sys.path.append(CELERY_PATH)
celery.config_from_object(CELERY_CONFIGURATION_OBJECT)

log = get_logger()


def get_app():
    app = Bottle()
    app.post('/in/<resource>/<action>', callback=sp_task)
    return app


def sp_task(resource, action):
    attributes = {'resource': resource, 'action': action, 'path': request.path}
    log.debug('Path %(path)s invoking: Resource "%(resource)s" - Action "%(action)s"', attributes)
    if not request.json:
        log.error('no json')
        log.debug(request.body.read())
        return
    attributes['payload'] = request.json
    del attributes['path']
    log.debug(request.json)
    celery.send_task(IN_TASK_NAME, [], kwargs=attributes)
    log.debug('Path %s sent to task: %s', request.path, IN_TASK_NAME)
