from collections import defaultdict
from tank.util import get_logger
from tank.models.state import StateModel
from tank.tnconfig import NEB_HOSTNAME, COLLECTIONS, NEB_IP_NAMESPACE, MONITOR_URL_TEMPLATE
from tank.util import get_bottle_app_with_mongo


log = get_logger()


def get_app(uri, db_name):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name)
    app.get('/status', callback=get_info_status)
    app.get('/status/siteid/<siteid>', callback=get_info_status)
    app.get('/status/state/<state>', callback=get_info_status)
    app.get('/monitor/url/siteid/<siteid>', callback=get_monitor_url)
    app.get('/monitor/address/siteid/<siteid>', callback=get_monitor_address)
    return app


def get_info_status(mongodb, siteid=None, state=None):
    alives_filter = {}
    states_filter = {'hostname': NEB_HOSTNAME}
    hosts_filter = {}
    names_filter = {}

    union = True

    if siteid:
        hosts_filter['_id'] = siteid
        alives_filter['_id'] = siteid
        states_filter['siteid'] = siteid
        names_filter['siteid'] = siteid

    if state:
        union = False
        alives_filter['last_state'] = state

    log.debug('Getting Site Status Report')
    alives_collection = mongodb[COLLECTIONS['ALIVES']]
    alives = alives_collection.find(alives_filter, {'alive_expiration': 1, 'last_state': 1})

    states_collection = mongodb[COLLECTIONS['STATES']]
    states = states_collection.find(states_filter, {'siteid': 1, NEB_IP_NAMESPACE: 1, '_id': 0})

    hosts = states_collection.aggregate([
        {'$group': {'_id': '$siteid', 'hosts': {'$addToSet': '$hostname'}}},
        {'$match': hosts_filter},
        {'$project': {'hosts': {'$size': '$hosts'}}}
    ])

    names_collection = mongodb[COLLECTIONS['SITES']]
    names = names_collection.find(names_filter, {'siteid': '$_id', 'sitename':'$name' })

    return get_site_status(hosts, alives, states, names, union)


def get_site_status(hosts, alives, states, names, union=True):
    joined_result = defaultdict(dict)

    for alive in alives:
        joined_result[alive['_id']]['siteid'] = alive['_id']
        if 'last_state' in alive:
            joined_result[alive['_id']]['last_state'] = alive['last_state']
        if 'alive_expiration' in alive:
            joined_result[alive['_id']]['alive_expiration'] = str(alive['alive_expiration'])

    for host in hosts:
        if union or host['_id'] in joined_result:
            if 'hosts' in host:
                joined_result[host['_id']]['hosts'] = host['hosts']

    for state in states:
        if union or state['siteid'] in joined_result:
            if NEB_IP_NAMESPACE in state:
                joined_result[state['siteid']]['nebnode'] = state[NEB_IP_NAMESPACE]

    for name in names:
        if name['siteid'] in joined_result:
            joined_result[name['siteid']]['sitename'] = name['sitename']

    return {'info' : joined_result.values()}


def get_monitor_address(mongodb, siteid):
    address = StateModel(mongodb, siteid).get_state(NEB_HOSTNAME, NEB_IP_NAMESPACE).get('result')
    return dict(address=address)


def get_monitor_url(mongodb, siteid):
    address = get_monitor_address(mongodb, siteid).get('address')
    return dict(url=MONITOR_URL_TEMPLATE.format(address=address) if address else None)