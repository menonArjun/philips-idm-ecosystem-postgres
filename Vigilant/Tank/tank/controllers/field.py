from bottle import request

from tank.models.field import FieldModel
from tank.util import get_bottle_app_with_mongo, get_logger


log = get_logger()


def get_app(uri, db_name):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name)
    app.get('/ual/<component>/version/<version>', callback=get_ual_component_version)
    app.get('/components', callback=get_field_component_details)
    app.get('/components/search', callback=get_field_component_details)
    app.get('/components/unique', callback=get_unique_components)
    app.get('/tags/unique', callback=get_unique_tags)
    app.get('/modules', callback=get_unique_models)
    app.get('/product_names', callback=get_unique_product_names)
    return app


def get_ual_component_version(mongodb, component, version):
    log.debug('Get UAL for component %s matching a search of %s', component, version)
    return FieldModel(mongodb).get_ual_component_version(component, version)


def get_field_component_details(mongodb):
    log.debug('Getting all components installed in the field by site')
    return FieldModel(mongodb).get_component_details(request.query)


def get_unique_components(mongodb):
    return FieldModel(mongodb).get_unique_components()

def get_unique_models(mongodb):
    return FieldModel(mongodb).get_unique_models()

def get_unique_product_names(mongodb):
    return FieldModel(mongodb).get_unique_product_names()

def get_unique_tags(mongodb):
    return FieldModel(mongodb).get_unique_tags()