CELERY_PATH = '/usr/lib/celery'
CELERY_CONFIGURATION_OBJECT = 'celeryconfig'
PATH_TASK_MAP = {
     '/billingapp/upload': 'phim_backoffice.routers.isite_billing',
}

# MONGODB / Collections
MONGODB = {
    'URL': 'mongodb://mongo.phim.isyntax.net:27017/',
    'RETRIES': 2,
    'RETRY_DELAY': 10,
    'DB_NAME': 'somedb'
}

COLLECTIONS = {
    'BILLING': 'billing'
}
