import sys
import json

from celery import Celery

from utils import get_logger, mongo_connection, custom_serializer

from app_config import (
    CELERY_CONFIGURATION_OBJECT,
    CELERY_PATH,
    PATH_TASK_MAP,
    MONGODB,
    COLLECTIONS
)


celery = Celery()
sys.path.append(CELERY_PATH)
celery.config_from_object(CELERY_CONFIGURATION_OBJECT)

log = get_logger()



def upload_to_s3(request_path, json_data):
    celery.send_task(PATH_TASK_MAP[request_path], [json_data], kwargs={})
    log.debug('Sent to task: %s', PATH_TASK_MAP[request_path])


def get_recent_status(site_id):
    """
         Function returns recent billing status for a site.
         if status is available in the status_history
         field it will be retrieved.
         If recent billing status is not available in the status_history feild
         then the, recent_upload value will be used.
    """
    
    mongo_conn = mongo_connection(MONGODB['URL'], MONGODB['DB_NAME'])
    billing_collection = mongo_conn[COLLECTIONS['BILLING']]
    results = billing_collection.find_one({'site_id': site_id}, projection={
        '_id': False, 'status_history': True, 'recent_upload': True})
    # iterating the status_history in reverse order, as the
    # field in the database maintained in acending order.
    recent_success = {}
    if results:
        for status in reversed(results.get('status_history', [])):
            if status['upload_status']:
                recent_success = status
                break
        else:
            recent_success = results.get('recent_upload', {})
    return json.dumps(recent_success, default=custom_serializer)
