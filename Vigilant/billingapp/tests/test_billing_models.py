import unittest
from mock import MagicMock, patch


class BillingModels(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_json = MagicMock(name='json')
        self.mongo_con_mock = MagicMock(name='mongo_connection')
        self.mock_utils = MagicMock(name='utils')
        self.mock_utils.mongo_connection.return_value = self.mongo_con_mock
        self.mock_celery = MagicMock(name='celery')
        self.mock_appconfig = MagicMock(name='appconfig')
        self.celeryobj_mock = MagicMock(name='celery_object')
        self.mock_celery.Celery.return_value = self.celeryobj_mock
        self.mock_appconfig.CELERY_CONFIGURATION_OBJECT = 'configobj'
        self.mock_appconfig.CELERY_PATH = 'celery_path'
        self.mock_appconfig.PATH_TASK_MAP = {'billing': 'billing_task'}
        self.mock_appconfig.MONGODB = {'URL': 'url', 'DB_NAME': 'db'}
        self.mock_appconfig.COLLECTIONS = {'BILLING': 'billing'}
        modules = {
            'json': self.mock_json,
            'celery': self.mock_celery,
            'utils': self.mock_utils,
            'app_config': self.mock_appconfig
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import billing_models
        self.module = billing_models

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_module_initial_load(self):
        self.mock_celery.Celery.assert_called_once_with()
        self.celeryobj_mock.config_from_object.assert_called_once_with(
            'configobj')
        # self.mock_utils.mongo_connection.assert_called_once_with('url', 'db')
        self.mock_utils.get_logger.assert_called_once_with()

    def test_upload_to_s3(self):
        self.module.upload_to_s3('billing', 'json_data')
        self.celeryobj_mock.send_task.assert_called_once_with(
            'billing_task', ['json_data'], kwargs={})

    def test_get_recent_status(self):
        status_history = [{'upload_status': True}]
        reversed_mock = MagicMock(name='reverse')
        collection_mock = MagicMock(name='collection')
        collection_mock.find_one.return_value = {
            'status_history': status_history}
        self.mongo_con_mock.__getitem__.return_value = collection_mock
        reversed_mock.return_value = status_history
        self.module.reversed = reversed_mock
        self.mock_json.dumps.return_value = "json"
        recent_status = self.module.get_recent_status('site_id')
        self.assertEqual(recent_status, "json")
        reversed_mock.assert_called_once_with(status_history)
        self.mock_utils.mongo_connection.assert_called_once_with('url', 'db')
        self.mongo_con_mock.__getitem__.assert_called_once_with('billing')
        collection_mock.find_one.assert_called_once_with({'site_id': 'site_id'}, projection={
            '_id': False, 'status_history': True, 'recent_upload': True})
        self.mock_json.dumps.assert_called_once_with(
            {'upload_status': True}, default=self.mock_utils.custom_serializer)

    def test_get_recent_status_upload_status_false(self):
        status_history = [{'upload_status': False}]
        reversed_mock = MagicMock(name='reverse')
        collection_mock = MagicMock(name='collection')
        collection_mock.find_one.return_value = {
            'status_history': status_history}
        self.mongo_con_mock.__getitem__.return_value = collection_mock
        reversed_mock.return_value = status_history
        self.module.reversed = reversed_mock
        self.mock_json.dumps.return_value = "json"
        recent_status = self.module.get_recent_status('site_id')
        self.assertEqual(recent_status, "json")
        reversed_mock.assert_called_once_with(status_history)
        self.mongo_con_mock.__getitem__.assert_called_once_with('billing')
        collection_mock.find_one.assert_called_once_with({'site_id': 'site_id'}, projection={
            '_id': False, 'status_history': True, 'recent_upload': True})
        self.mock_json.dumps.assert_called_once_with({}, default=self.mock_utils.custom_serializer)

    def test_get_recent_status_mongo_empty_response(self):
        reversed_mock = MagicMock(name='reverse')
        collection_mock = MagicMock(name='collection')
        collection_mock.find_one.return_value = None
        self.mongo_con_mock.__getitem__.return_value = collection_mock
        self.module.reversed = reversed_mock
        self.mock_json.dumps.return_value = "json"
        recent_status = self.module.get_recent_status('site_id')
        self.assertEqual(recent_status, "json")
        self.assertEqual(reversed_mock.call_count, 0)
        self.mongo_con_mock.__getitem__.assert_called_once_with('billing')
        collection_mock.find_one.assert_called_once_with({'site_id': 'site_id'}, projection={
            '_id': False, 'status_history': True, 'recent_upload': True})
        self.mock_json.dumps.assert_called_once_with({}, default=self.mock_utils.custom_serializer)

    def test_get_recent_status_recent_upload(self):
        status_history = [{'upload_status': False}]
        reversed_mock = MagicMock(name='reverse')
        collection_mock = MagicMock(name='collection')
        collection_mock.find_one.return_value = {
            'status_history': status_history, 'recent_upload': {'recent': 'yes'}}
        self.mongo_con_mock.__getitem__.return_value = collection_mock
        reversed_mock.return_value = status_history
        self.module.reversed = reversed_mock
        self.mock_json.dumps.return_value = "json"
        recent_status = self.module.get_recent_status('site_id')
        self.assertEqual(recent_status, "json")
        reversed_mock.assert_called_once_with(status_history)
        self.mongo_con_mock.__getitem__.assert_called_once_with('billing')
        collection_mock.find_one.assert_called_once_with({'site_id': 'site_id'}, projection={
            '_id': False, 'status_history': True, 'recent_upload': True})
        self.mock_json.dumps.assert_called_once_with({'recent': 'yes'}, default=self.mock_utils.custom_serializer)
