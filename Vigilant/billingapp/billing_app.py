#!/usr/bin/env python
from bottle import Bottle, request, BaseRequest


from billing_models import (upload_to_s3, get_recent_status)


BaseRequest.MEMFILE_MAX = 1024 * 1024 * 100

app = Bottle()

from utils import get_logger

log = get_logger()


@app.post('/billingapp/upload')
def upload():
    if not request.json:
        log.error('no json')
        log.debug('%s', request.body.read())
        return
    log.debug('%s', request.json)
    upload_to_s3(request.path, request.json)


@app.get('/billingapp/health')
def health():
    log.info('Billingapp health..! %s', request.path)
    return "<p>OK</p>"


@app.get('/billingapp/status/<site_id>')
def recent_status(site_id):
    """
        Fetches Site's recent successfull upload status
        to s3, from database
    """
    log.debug('Billing Status request for site - %s', site_id)
    return get_recent_status(site_id)


if __name__ == '__main__':
    #    app.run(host='0.0.0.0', port=8080, debug=True)
    #    log.info('MemFileMax set to %s', BaseRequest.MEMFILE_MAX)
    app.run(debug=True, reload=True)
