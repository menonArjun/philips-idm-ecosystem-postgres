---

- hostname:
    - attribute: host_name
      value: hostname
    - attribute: alias
      value: hostname
    - attribute: address
      value: address

- Linux:
    - attribute: use
      value: '"linux-server"'

- Windows:
    - attribute: use
      value: '"windows-server"'

    - attribute: _atdomain
      value: "['@', domain]|join"
      when: '"domain_needed" in Windows.flags'

    - attribute: hostgroups
      value: '"+win2k-servers"'
      when: Windows.name == 'Microsoft Windows 2000 Server'

- LN:
    - attribute: use
      source: LN.use
      map:
        esx_server: esxi-server
        windows_server: vcenter-server
        aws_server: aws-server
        wfl_server: wfl-server
        equallogic_san: equallogic-san

- ISP:
    - attribute: _isp_module_type
      value: ISP.module_type

    - attribute: _isp_input_drive
      value: ISP.input_folder.split(':')|first
      when: 'ISP.noncore_node != "DB"'

    - attribute: _isp_input_folder
      value: ISP.input_folder.split(':')|last|replace('\\', '/')
      when: 'ISP.noncore_node != "DB"'

    - attribute: use
      source: ISP.identifier
      map:
        3x: isp-server
        4x: ispff-server
        '4_1': ispff-server

    - attribute: hostgroups
      source: ISP.module_type
      default: ''  # Not really needed. For future use.
      map:
        '1': ''  # ISuite module
        '2': +isyntaxserver-servers,consolidation-servers,stack-servers  # Processing module
        '3': ''  # ISuite module + Processing module
        '4': +isyntaxserver-servers,stack-servers,msmq-servers  # Cache module
        '8': +isyntaxserver-servers,stack-servers,msmq-servers  # Persistent Storage module
        '16': +isyntaxserver-servers,stack-servers,msmq-servers  # Filter Cache module
        '32': +isyntaxserver-servers,stack-servers,msmq-servers  # Persistent Mirror module
      when: ISP.identifier == '3x'

    - attribute: hostgroups
      value: '"windows-servers,ispapps-servers"' # AP nodes in 3.x
      when: hostname.split('.')|first|upper is re_search('AP\d{1,2}$') and ISP.identifier == '3x'

    - attribute: hostgroups
      source: ISP.module_type
      default: ''  # Not really needed. For future use.
      map:
        '1': ''  # ISuite module, this type should never happen on 4.x
        '2': +consolidation-servers,stack-servers  # Processing module
        '4': +stack-servers,msmq-servers  # Cache module
        '8': +stack-servers,msmq-servers  # Persistent Storage module
        '16': +stack-servers,msmq-servers  # Filter Cache module
        '32': +stack-servers,msmq-servers  # Persistent Mirror module
        '64': ''  # Integration module
        '66': +consolidation-servers,msmq-servers,isp-in-servers  # Integration module + Processing module
        '128': +isp-infrastructure  # Infrastructure module
        '256': +consolidation-servers,stack-servers,msmq-servers  # Single Server iVault
        '270': +udm-rabbitmq-servers # Single Server iVault for UDM
        '512': +udm-strg-server # Single Server iVault for UDM
        '1024': +udm-app-server # Single Server iVault for UDM
        '2048': +udm-prep-server # Single Server iVault for UDM
      when: ISP.identifier == '4x'

    - attribute: hostgroups
      source: ISP.module_type
      default: ''  # Not really needed. For future use.
      map:
        '1': ''  # ISuite module, this type should never happen on 4.x
        '2': +consolidation-servers,stack-servers  # Processing module
        '4': +stack-servers,msmq-servers  # Cache module
        '8': +stack-servers,msmq-servers  # Persistent Storage module
        '16': +stack-servers,msmq-servers  # Filter Cache module
        '32': +stack-servers,msmq-servers  # Persistent Mirror module
        '64': ''  # Integration module
        '66': +isp-in-servers  # Integration module + Processing module
        '128': +isp-infrastructure  # Infrastructure module
        '256': +consolidation-servers,stack-servers,msmq-servers  # Single Server iVault
        '270': +udm-rabbitmq-servers # Single Server iVault for UDM
        '512': +udm-strg-server # Single Server iVault for UDM
        '1024': +udm-app-server # Single Server iVault for UDM
        '2048': +udm-prep-server # Single Server iVault for UDM
      when: ISP.identifier == '4_1'

    - attribute: hostgroups
      value: '"+isp-rabbitmq-servers"'
      when: 'ISP.module_type == "Rabbitmqnode"'

    - attribute: hostgroups
      value: '"+isppr-rabbitmq-servers,consolidation-servers,stack-servers"'
      when: 'ISP.ex_module_type == "Rabbitmqnode" and ISP.module_type == 2'

    - attribute: hostgroups
      value: '"+anywhere-servers"'
      when: '"anywhere" in ISP.flags'

    - attribute: hostgroups
      value: '"+f5-pool-servers"'
      when: 'ISP.tags == "f5" and ISP.module_type == 2'

    - attribute: hostgroups
      value: '"+visiblelight-servers"'
      when: '"vlcapture" in ISP.flags'

    - attribute: hostgroups
      value: '"+cca-servers"'
      when: '"cca" in ISP.flags'

    - attribute: use
      value: '"ispff-hisec-server"'
      when: 'ISP.identifier == "4x" and "hisec" in ISP.flags'

    - attribute: use
      value: '"isp-db-servers"'
      when: '"Database" == ISP.module_type'

    - attribute: use
      value: '"ispff-hisec-server"'
      when: '"Mgnode" == ISP.module_type'

    - attribute: _mountpoint
      value: ISP.input_folder|replace('\\', '\\\\')
      when: 'ISP.noncore_node == "DB"'

    - attribute: hostgroups
      value: '"+vmware-guest-servers"'
      when: manufacturer == 'VMware, Inc.'

    - attribute: use
      value: '"i4-prep-server"'
      when: '"I4Processing" == ISP.module_type'

    - attribute: use
      value: '"i4-viewer-server"'
      when: '"Rviewer" == ISP.module_type'

    - attribute: use
      value: '"i4-ev-server"'
      when: '"Eviewer" == ISP.module_type'

    - attribute: use
      value: '"isp-hl7-servers"'
      when: 'ISP.module_type == "Hl7"'

#    - attribute: hostgroups
#      value: '"+aws-archive-servers"'
#      when: '"awsservices" in ISP.tags and ISP.module_type == 8'

- AdvancedWorkflowServices:
    - attribute: use
      value: '"aws-server"'

- AdvancedWorkflowArchiveServices:
    - attribute: use
      value: '"awsff-server,ispff-server"'

- AdvancedWorkflowArchiveHDServices:
    - attribute: use
      value: '"awsffhd-server,ispff-server"'

- AnalyticsPublisher:
    - attribute: use
      value: '"analytics-publisher-node"'

- HPMSASAN:
    - attribute: use
      value: '"hp-msa-san"'

- DellEqualLogicSAN:
    - attribute: use
      value: '"dell-equallogic-san"'

- IBMStorwizeSAN:
    - attribute: use
      value: '"ibm-storwize-san"'

- CiscoSwitch:
    - attribute: use
      value: '"cisco-switch"'

- HPSwitch:
    - attribute: use
      value: '"hp-switch"'

    - attribute: hostgroups
      value: '"hp-5500-switches"'
      when: 'HPSwitch.switch_model == 5500'

    - attribute: hostgroups
      value: '"hp-5700-switches"'
      when: 'HPSwitch.switch_model == 5700'

- F5LoadBalancer:
    - attribute: use
      value: '"f5-loadbalancer"'

- vCenter:
    - attribute: use
      value: '"vcenter-server"'

- vmHost:
    - attribute: use
      value: '"delta-linux-servers"'
      when: vmHost.type == 'Linux' and vmHost.powerstate == 'poweredOn'

    - attribute: use
      value: '"delta-win-servers"'
      when: vmHost.type == 'Windows' and vmHost.powerstate == 'poweredOn'

- ESXi:
    - attribute: use
      value: '"esxi-server"'
      when: manufacturer == 'IBM'

    - attribute: use
      value: '"hp-esxi-server"'
      when: manufacturer == 'HP'

    - attribute: use
      value: '"dell-esxi-server"'
      when: manufacturer == 'DELL'

- I4:
    - attribute: use
      value: '"i4-server"'

- I4Prep:
    - attribute: use
      value: '"i4-prep-server"'

- I4Viewer:
    - attribute: use
      value: '"i4-viewer-server"'

- I4EV:
    - attribute: use
      value: '"i4-ev-server"'

- ISPortal:
    - attribute: use
      value: '"isportal-server"'

- Concerto:
    - attribute: use
      value: '"concerto-server"'

- ISCV:
    - attribute: use
      value: '"iscv-server"'

- IBEGD:
    - attribute: use
      value: '"ibegd-server"'  

- IBE:
    - attribute: use
      value: '"ibe-server"'  

- DWP:
    - attribute: use
      value: '"dwp-server"'

- XPERCONNECT:
    - attribute: use
      value: '"xperconnect-server"'

- XPERDATACENTER:
    - attribute: use
      value: '"xperdatacenter-server"'

- iECG:
    - attribute: use
      value: '"iecg-server"'

- modules:
    - attribute: _discovery_modules
      value: modules|json

- Components:
    - attribute: _discovery_components
      value: Components|json

- MSSQL:
    - attribute: use
      value: '"mssql-adv-server"'

- UDMRedis:
    - attribute: use
      value: '"udm-redis-server"'

- Helion:
    - attribute: use
      value: '"helion-server"'

- ISPACS Billing:
    - attribute: use
      value: '"isitebilling-server"'

- iECGGD:
    - attribute: use
      value: '"iecggd-server"'

- ISCVGD:
    - attribute: use
      value: '"windows-server"'

    - attribute: hostgroups
      value: '"+iscvgd-web-servers"'
      when: '"web" in role'

    - attribute: hostgroups
      value : '"+iscvgd-app-servers"'
      when: '"application" in role'

    - attribute: hostgroups
      value: '"+iscvgd-analytics-servers"'
      when: '"analytics" in role'

- AnalyticsPublisherGD:
    - attribute: use
      value: '"analytics-publisher-node"'

- AdvancedWorkflowHDServices:
    - attribute: use
      value: '"awshd-server"'

- ISEEUtilityServer:
    - attribute: use
      value: '"utility-servers"'

#- hostname:
#    - attribute: use
#      value: '"empty-server"'
#      when: address is not defined
#    - attribute: hostgroups
#      value: '"empty-servers"'
#      when: address is not defined
