import logging
import json

from lxml.etree import tostring
from lxml.builder import E


spit_bucket = logging.getLogger(__name__)


class ConfiggenPCM(object):
    def __init__(self, data):
        self.data = data
        #Set encrypted as True
        self.encrypt_keys = ['DomainAdminPassword']

    def generate(self):
        deployment_id = self.data.pop('deployment_id')
        return { "siteinfo/pcm_manifest_{deployment_id}.json".format(deployment_id=deployment_id) : json.dumps(self.data, sort_keys=True, indent=4),
                 "siteinfo/SiteInfo_{deployment_id}.xml".format(deployment_id=deployment_id): self.create_xml()}

    def create_xml(self):

        site_keys = E.Keys()
        if "site_vault" in self.data :
            for site_vault_key, site_vault_value in self.data["site_vault"].items() :
                xml_key = E.Key(KeyName=site_vault_key, Encrypted="true")
                xml_key.text = site_vault_value
                site_keys.append(xml_key)

        if "site_data" in self.data:
            for site_data_key, site_data_value in self.data["site_data"].items() :
                encrypted = "false"
                if site_data_key in self.encrypt_keys:
                    encrypted = "true"
                xml_key = E.Key(KeyName=site_data_key, Encrypted=encrypted)
                xml_key.text = site_data_value
                site_keys.append(xml_key)

        if 'siteid' in self.data:
            xml_key = E.Key(KeyName='SiteID', Encrypted="false")
            xml_key.text = self.data['siteid']
            site_keys.append(xml_key)

        roles = E.Roles()
        for host_key, host_value in self.data["hosts"].items() :

            ipv4 = E.IPv4()
            ipv4.text = host_value["ip"]
            fqdn = E.FQDN()
            fqdn.text = host_key

            packages = E.Packages()
            for package in host_value["packages"] :
                pcm_package_components = package.rsplit('-', 1)
                if len(pcm_package_components) > 1 :
                    packages.append(E.PackageInfo(Name=pcm_package_components[0], Version=pcm_package_components[1]))
                else :
                    packages.append(E.PackageInfo(Name=pcm_package_components[0], Version=""))

            host_keys = E.RoleBag()
            if "host_vault" in host_value :
                for host_vault_key, host_vault_value in host_value["host_vault"].items() :
                    xml_key = E.Key(KeyName=host_vault_key, Encrypted="true")
                    xml_key.text = host_vault_value

                    host_keys.append(xml_key)

            if "host_data" in host_value :
                for data_key, data_value in host_value["host_data"].items() :
                    keys = E.Keys()
                    xml_key = E.Key(KeyName=data_key, Encrypted="false")
                    xml_key.text = data_value
                    keys.append(xml_key)
                    host_keys.append(keys)
            roles.append(E.Role(ipv4, fqdn, packages, host_keys,Sequence=host_value['Sequence']))

        return tostring(E.SiteMap((E.SiteBag(site_keys)), roles), pretty_print=True, xml_declaration=True, encoding='UTF-8')
