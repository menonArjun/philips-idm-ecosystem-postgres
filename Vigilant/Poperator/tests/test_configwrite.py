import unittest
from mock import MagicMock, patch, call, PropertyMock


class SVNConfigWriterTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        self.mock_logging = MagicMock(name='logging')
        self.mock_phimutils = MagicMock(name='phimutils')
        self.mock_py = MagicMock(name='py')
        modules = {
            'cached_property': self.mock_cached_property,
            'logging': self.mock_logging,
            'py': self.mock_py,
            'phimutils': self.mock_phimutils,
            'phimutils.timestamp': self.mock_phimutils.timestamp,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from poperator import configwrite
        self.configwrite = configwrite

        self.patch_wc = patch('poperator.configwrite.SVNConfigWriter.wc', new_callable=PropertyMock)
        self.wc_mock = self.patch_wc.start()
        self.svncw = self.configwrite.SVNConfigWriter(None, None, None, None, 'abc01')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.patch_wc.stop()
        self.module_patcher.stop()

    def test_process_config_file_none(self):
        self.svncw.process_config_file('tmp/host1.cfg', None)
        self.wc_mock.return_value.join.assert_called_once_with('tmp/host1.cfg')
        self.wc_mock.return_value.join.return_value.assert_has_calls([call.check(), call.remove()], any_order=True)

    def test_process_config_file_content(self):
        self.svncw.process_config_file('tmp/host1.cfg', 'CONTENT')
        self.wc_mock.return_value.ensure.assert_called_once_with('tmp/host1.cfg')
        self.wc_mock.return_value.ensure.return_value.assert_has_calls([call.write('CONTENT')])


if __name__ == '__main__':
    unittest.main()
