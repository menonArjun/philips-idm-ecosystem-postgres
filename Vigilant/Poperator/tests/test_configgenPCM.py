import json
import unittest
from mock import MagicMock, patch


class ConfiggenTestPCM(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_logging = MagicMock(name='logging')
        modules = {
            'logging': self.mock_logging,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from poperator import configgenPCM
        self.configgenPCM = configgenPCM

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_generate(self):
        config = self.configgenPCM.ConfiggenPCM(self.manifest_one)

        generation = config.generate()

        expected = {'siteinfo/SiteInfo_id.xml' : self.site_xml_one,
                    'siteinfo/pcm_manifest_id.json' : json.dumps(self.manifest_one, sort_keys=True, indent=4)}
        self.maxDiff = None
        self.assertEqual(generation,expected)

    manifest_one =  {
            "siteid": "DEV03",
            "site_vault": {
                "ISiteServiceAccountPassword": "fQRAfhjsfa1fOiVR0JugjZqJOF3uuHeqLOzTeUj1Qqhip34uxpMkqTmGKclokOyJqlgJsBptRgX+LuZ8FjlPECoU+OoNwvutQAiAxSFM5ZrtrmUjn7H1oElB7SY3zEzuXJu53GXAhAGkdguR7ryWRiFfLubswE9sq7CE+WDdBD6nLxxPYDMKyaW0SVNjtEv4Qt7w+DFQrAbHwoTxYuSQc7+bb8NzjJ2543Pg0D+t86+p0hloW/6ziks5VBn5QzSlfQMqGyXoVditwuE8N866I6sy4xlIq2trOuzhn7V8Tp7fy3bW02eKxYB0d2LOQXz1AnFQ1fIvoh8ZV03dyViIJg=="
            },
            "site_data": {
                "Federated": "False",
                "LocalizationCode": "ENG",
                "ISiteServiceAccountUser": "DEVO3\\iSiteService",
                "SolutionRootFolder": "S:\\Philips\\Apps\\iSite",
                "DeploymentScripts": "C:\\provision\\isite\\deploymentscripts"
            },
            'deployment_id':'id',
            "hosts": {
                "DEV03IF1.DEV03.iSyntax.net": {
                    "RoleType": "Infrastructure",
                    "Sequence": "1",
                    "ip": "167.81.184.117",
                    "host_vault": {},
                    "host_data": {
                        "RoleType": "Infrastructure"
                    },
                    "packages": [
                        "AWV-1.3",
                        "PCM-2.3.1.1.234.0"
                    ]
                },
                "DEV03AM1.DEV03.iSyntax.net": {
                    "RoleType": "ActiveMirror",
                    "Sequence": "5",
                    "ip": "167.81.184.117",
                    "host_vault": { "ISiteServiceAccountPassword": "fQRAfhjsfa1fOiVR0JugjZqJOF3uuHeqLOzTeUj1Qqhip34uxpMkqTmGKclokOyJqlgJsBptRgX+LuZ8FjlPECoU+OoNwvutQAiAxSFM5ZrtrmUjn7H1oElB7SY3zEzuXJu53GXAhAGkdguR7ryWRiFfLubswE9sq7CE+WDdBD6nLxxPYDMKyaW0SVNjtEv4Qt7w+DFQrAbHwoTxYuSQc7+bb8NzjJ2543Pg0D+t86+p0hloW/6ziks5VBn5QzSlfQMqGyXoVditwuE8N866I6sy4xlIq2trOuzhn7V8Tp7fy3bW02eKxYB0d2LOQXz1AnFQ1fIvoh8ZV03dyViIJg=="},
                    "host_data": {
                        "RoleType": "ActiveMirror"
                    },
                    "packages": [
                        "AWV",
                        "PCM-2.3.1.1.234.0"
                    ]
                }
            }
        }


    site_xml_one = """<?xml version='1.0' encoding='UTF-8'?>
<SiteMap>
  <SiteBag>
    <Keys>
      <Key Encrypted="true" KeyName="ISiteServiceAccountPassword">fQRAfhjsfa1fOiVR0JugjZqJOF3uuHeqLOzTeUj1Qqhip34uxpMkqTmGKclokOyJqlgJsBptRgX+LuZ8FjlPECoU+OoNwvutQAiAxSFM5ZrtrmUjn7H1oElB7SY3zEzuXJu53GXAhAGkdguR7ryWRiFfLubswE9sq7CE+WDdBD6nLxxPYDMKyaW0SVNjtEv4Qt7w+DFQrAbHwoTxYuSQc7+bb8NzjJ2543Pg0D+t86+p0hloW/6ziks5VBn5QzSlfQMqGyXoVditwuE8N866I6sy4xlIq2trOuzhn7V8Tp7fy3bW02eKxYB0d2LOQXz1AnFQ1fIvoh8ZV03dyViIJg==</Key>
      <Key Encrypted="false" KeyName="LocalizationCode">ENG</Key>
      <Key Encrypted="false" KeyName="DeploymentScripts">C:\provision\isite\deploymentscripts</Key>
      <Key Encrypted="false" KeyName="ISiteServiceAccountUser">DEVO3\iSiteService</Key>
      <Key Encrypted="false" KeyName="Federated">False</Key>
      <Key Encrypted="false" KeyName="SolutionRootFolder">S:\Philips\Apps\iSite</Key>
      <Key Encrypted="false" KeyName="SiteID">DEV03</Key>
    </Keys>
  </SiteBag>
  <Roles>
    <Role Sequence="5">
      <IPv4>167.81.184.117</IPv4>
      <FQDN>DEV03AM1.DEV03.iSyntax.net</FQDN>
      <Packages>
        <PackageInfo Version="" Name="AWV"/>
        <PackageInfo Version="2.3.1.1.234.0" Name="PCM"/>
      </Packages>
      <RoleBag>
        <Key Encrypted="true" KeyName="ISiteServiceAccountPassword">fQRAfhjsfa1fOiVR0JugjZqJOF3uuHeqLOzTeUj1Qqhip34uxpMkqTmGKclokOyJqlgJsBptRgX+LuZ8FjlPECoU+OoNwvutQAiAxSFM5ZrtrmUjn7H1oElB7SY3zEzuXJu53GXAhAGkdguR7ryWRiFfLubswE9sq7CE+WDdBD6nLxxPYDMKyaW0SVNjtEv4Qt7w+DFQrAbHwoTxYuSQc7+bb8NzjJ2543Pg0D+t86+p0hloW/6ziks5VBn5QzSlfQMqGyXoVditwuE8N866I6sy4xlIq2trOuzhn7V8Tp7fy3bW02eKxYB0d2LOQXz1AnFQ1fIvoh8ZV03dyViIJg==</Key>
        <Keys>
          <Key Encrypted="false" KeyName="RoleType">ActiveMirror</Key>
        </Keys>
      </RoleBag>
    </Role>
    <Role Sequence="1">
      <IPv4>167.81.184.117</IPv4>
      <FQDN>DEV03IF1.DEV03.iSyntax.net</FQDN>
      <Packages>
        <PackageInfo Version="1.3" Name="AWV"/>
        <PackageInfo Version="2.3.1.1.234.0" Name="PCM"/>
      </Packages>
      <RoleBag>
        <Keys>
          <Key Encrypted="false" KeyName="RoleType">Infrastructure</Key>
        </Keys>
      </RoleBag>
    </Role>
  </Roles>
</SiteMap>\n"""


if __name__ == '__main__':
    unittest.main()
