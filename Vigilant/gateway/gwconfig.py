CELERY_PATH = '/usr/lib/celery'
CELERY_CONFIGURATION_OBJECT = 'celeryconfig'
PATH_TASK_MAP = {
    '/notification': 'phim_backoffice.routers.do_notification',
    '/state': 'phim_backoffice.routers.do_state',
    '/perfdata': 'phim_backoffice.routers.do_perfdata',
    '/metricsdata': 'phim_backoffice.routers.do_metricsdata',
    '/discovery': 'phim_backoffice.routers.do_discovery',
    '/billing': 'phim_backoffice.routers.isite_billing',
    '/deployment': 'phim_backoffice.routers.deployment_status',
    '/heartbeat': 'phim_backoffice.transport.hbmessage2hbdashboard',
    '/queuetransport': 'phim_backoffice.transport.queuemapper',
}
