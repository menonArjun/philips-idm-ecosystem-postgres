import unittest
from mock import patch


class PhimutilsDeciderTermTreeNodeTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        modules = {}
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        from phimutils import decider
        self.module = decider

        self.ttn = self.module.TermTreeNode()

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_root(self):
        self.ttn.add_data(['one', 'two', '!three'])
        self.assertEqual(self.ttn.addends, set(['two', 'one']))
        self.assertEqual(self.ttn.subtrahends, set(['three']))

    def test_deep_add(self):
        self.ttn.add_data(['one', 'two', '!three', 'four', '!five'],
                          'Category1__Vendor1__Object1__Attribute1__Dimension1')
        self.assertEqual(self.ttn.addends, set())
        self.assertEqual(self.ttn.subtrahends, set())
        self.assertEqual(self.ttn['Category1'].addends, set())
        self.assertEqual(self.ttn['Category1'].subtrahends, set())
        self.assertEqual(self.ttn['Category1']['Vendor1'].addends, set())
        self.assertEqual(self.ttn['Category1']['Vendor1'].subtrahends, set())
        self.assertEqual(self.ttn['Category1']['Vendor1']['Object1'].addends, set())
        self.assertEqual(self.ttn['Category1']['Vendor1']['Object1'].subtrahends, set())
        self.assertEqual(self.ttn['Category1']['Vendor1']['Object1']['Attribute1'].addends, set())
        self.assertEqual(self.ttn['Category1']['Vendor1']['Object1']['Attribute1'].subtrahends, set())
        self.assertEqual(self.ttn['Category1']['Vendor1']['Object1']['Attribute1']['Dimension1'].addends,
                         set(['four', 'two', 'one']))
        self.assertEqual(self.ttn['Category1']['Vendor1']['Object1']['Attribute1']['Dimension1'].subtrahends,
                         set(['five', 'three']))

    def test_deep_add_multiple(self):
        self.ttn.add_data(['one', 'two', '!three', 'four', '!five'],
                          'Category1__Vendor1__Object1__Attribute1__Dimension1')
        self.ttn.add_data(['!one', 'six'],
                          'Category1__Vendor1__Object1__Attribute1__Dimension2')
        self.ttn.add_data(['!six', 'one'],
                          'Category1__Vendor1__Object1')
        self.assertEqual(self.ttn.addends, set())
        self.assertEqual(self.ttn.subtrahends, set())
        self.assertEqual(self.ttn['Category1'].addends, set())
        self.assertEqual(self.ttn['Category1'].subtrahends, set())
        self.assertEqual(self.ttn['Category1']['Vendor1'].addends, set())
        self.assertEqual(self.ttn['Category1']['Vendor1'].subtrahends, set())
        self.assertEqual(self.ttn['Category1']['Vendor1']['Object1'].addends, set(['one']))
        self.assertEqual(self.ttn['Category1']['Vendor1']['Object1'].subtrahends, set(['six']))
        self.assertEqual(self.ttn['Category1']['Vendor1']['Object1']['Attribute1'].addends, set())
        self.assertEqual(self.ttn['Category1']['Vendor1']['Object1']['Attribute1'].subtrahends, set())
        self.assertEqual(self.ttn['Category1']['Vendor1']['Object1']['Attribute1']['Dimension1'].addends,
                         set(['four', 'two', 'one']))
        self.assertEqual(self.ttn['Category1']['Vendor1']['Object1']['Attribute1']['Dimension1'].subtrahends,
                         set(['five', 'three']))
        self.assertEqual(self.ttn['Category1']['Vendor1']['Object1']['Attribute1']['Dimension2'].addends,
                         set(['six']))
        self.assertEqual(self.ttn['Category1']['Vendor1']['Object1']['Attribute1']['Dimension2'].subtrahends,
                         set(['one']))


class PhimutilsDeciderDeciderTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        modules = {}
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        from phimutils import decider
        self.module = decider

        self.decider = self.module.Decider()
        sample_rules = [
            {
                'sites': ['XYZ01', 'ABC02'],
                'namespaces': [
                    'Category1__Vendor1',
                    'Category1__Vendor2',
                    'Category2__Vendor1',
                ],
                'result': ['contact1', 'contact2', '!contact3']
            },
            {
                'sites': ['XYZ01'],
                'namespaces': [],
                'result': ['contact5']
            },
            {
                'sites': [],
                'namespaces': [],
                'result': ['contact4']
            },
            {
                'sites': [],
                'namespaces': ['Category3__Vendor2'],
                'result': ['contact3']
            },
            {
                'sites': [],
                'namespaces': ['Category1'],
                'result': ['contact3']
            },

        ]
        self.decider.load(sample_rules)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_decide_in_root_in_unbound_in_site_in_more_specific(self):
        self.assertEqual(
            self.decider.decide('XYZ01', 'Category1__Vendor1__Object1__Attribute1__Dimension2'),
            set(['contact4', 'contact5', 'contact1', 'contact2'])
        )

    def test_decide_in_root_in_site_in_more_specific(self):
        self.assertEqual(
            self.decider.decide('XYZ01', 'Category2__Vendor1__Object1__Attribute1__Dimension2'),
            set(['contact4', 'contact5', 'contact1', 'contact2'])
        )

    def test_decide_in_root_in_unbound_in_site(self):
        self.assertEqual(
            self.decider.decide('XYZ01', 'Category1__Vendor9__Object1__Attribute1__Dimension2'),
            set(['contact4', 'contact5', 'contact3'])
        )

    def test_decide_in_root_in_site(self):
        self.assertEqual(
            self.decider.decide('XYZ01', 'Category6__Vendor9__Object1__Attribute1__Dimension2'),
            set(['contact4', 'contact5'])
        )

    def test_decide_in_root_in_unbound(self):
        self.assertEqual(
            self.decider.decide('XYZ99', 'Category1__Vendor9__Object1__Attribute1__Dimension2'),
            set(['contact4', 'contact3'])
        )

    def test_decide_in_root(self):
        self.assertEqual(
            self.decider.decide('XYZ99', 'Category6__Vendor9__Object1__Attribute1__Dimension2'),
            set(['contact4'])
        )


if __name__ == '__main__':
    unittest.main()