import unittest
from mock import MagicMock, patch, call


class PhimutilsPassiveTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_pynag = MagicMock(name='pynag')
        self.mock_cryptography = MagicMock(name='cryptography')
        modules = {
            'pynag': self.mock_pynag,
            'pynag.Parsers': self.mock_pynag.Parsers,
            'cryptography': self.mock_cryptography,
            'cryptography.fernet': self.mock_cryptography.fernet,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        from phimutils import resource
        self.module = resource

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_retrieve_resource_no_secret(self):
        self.resource = self.module.NagiosResourcer('ResourceFile')
        self.assertEqual(self.resource.get_resource('$USER1$'), self.resource._config.get_resource.return_value)
        self.resource._config.get_resource.assert_called_once_with('$USER1$')

    def test_retrieve_resource_not_found(self):
        self.resource = self.module.NagiosResourcer('ResourceFile')
        self.resource._config.get_resource.return_value = None
        self.assertEqual(self.resource.get_resource('$USER1$'), None)
        self.resource._config.get_resource.assert_called_once_with('$USER1$')

    def test_retrieve_resource_secret(self):
        self.module.ResourceDecrypter = MagicMock(spec_set=self.module.ResourceDecrypter)
        self.resource = self.module.NagiosResourcer('ResourceFile', 'super_sectret')
        self.assertEqual(
            self.resource.get_resource('$USER1$'),
            self.module.ResourceDecrypter.return_value.decrypt.return_value
        )
        self.resource._config.get_resource.assert_called_once_with('$USER1$')
        self.module.ResourceDecrypter.assert_called_once_with('super_sectret')
        self.module.ResourceDecrypter.return_value.decrypt.assert_called_once_with(
            self.resource._config.get_resource.return_value
        )

    def test_retrieve_resources(self):
        self.resource = self.module.NagiosResourcer('ResourceFile')
        self.assertEqual(
            self.resource.get_resources('$USER1$', '$USER2$'),
            [
                self.resource._config.get_resource.return_value,
                self.resource._config.get_resource.return_value
            ]
        )
        self.assertEqual(self.resource._config.get_resource.call_args_list, [call('$USER1$'), call('$USER2$')])

    def test_decrypt_resources(self):
        self.decrypter = self.module.ResourceDecrypter('some_secret')
        self.decrypter._cipher_suite = MagicMock(name='cipher_suite')
        self.decrypter._cipher_suite.decrypt.return_value = 'not_secret'
        self.assertEqual(
            self.decrypter.decrypt('/bin/echo "{+some_ecrypt1+} also {+some_ecrypt2+}" more -param'),
            '/bin/echo "not_secret also not_secret" more -param'
        )
        self.assertEqual(self.decrypter._cipher_suite.decrypt.mock_calls, [call('some_ecrypt1'), call('some_ecrypt2')])

    def test_decrypt_one_resource(self):
        self.decrypter = self.module.ResourceDecrypter('some_secret')
        self.decrypter._cipher_suite = MagicMock(name='cipher_suite')
        self.decrypter._cipher_suite.decrypt.return_value = 'not_secret'
        self.assertEqual(self.decrypter.decrypt('{+some_ecrypt1+}'), self.decrypter._cipher_suite.decrypt.return_value)
        self.decrypter._cipher_suite.decrypt.assert_called_once_with('some_ecrypt1')

if __name__ == '__main__':
    unittest.main()
