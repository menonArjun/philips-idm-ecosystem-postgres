import unittest
from mock import MagicMock, patch, call


class PhimutilsStateRedisTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        modules = {}
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        from phimutils import stateredis
        self.module = stateredis

        self.rdb = MagicMock(name='RedisClient')
        self.state_redis = self.module.StateRedis(self.rdb, 'PREFIX_', 'name_of_the_set')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_prefix_hostname(self):
        self.assertEqual(self.state_redis.prefix_hostname('localhost'), 'PREFIX_localhost')

    def test_remove_prefix(self):
        self.assertEqual(self.state_redis.remove_prefix('PREFIX_localhost'), 'localhost')

    @patch('phimutils.stateredis.metric_value')
    def test_insert_metrics(self, mock_metricval):
        mock_metricval.return_value = 'aaa', 'bbb'
        self.state_redis._insert_metrics([{'data': 'something'}], 'key1', 'service1')
        mock_metricval.assert_called_once_with({'data': 'something'}, 'service1')
        self.rdb.hset.assert_called_once_with('key1', 'aaa', 'bbb')

    @patch('phimutils.stateredis.information_id')
    def test_insert_information(self, mock_infoval):
        mock_infoval.return_value = 'aaa'
        self.state_redis._insert_information({'data': 'something'}, 'key1', 'service1')
        mock_infoval.assert_called_once_with('data', 'service1')
        self.rdb.hset.assert_called_once_with('key1', 'aaa', 'something')

    @patch('phimutils.stateredis.ns_attach')
    def test_get_state_key_name(self, mock_nsa):
        mock_nsa.return_value = 'namespace_State'
        self.assertEqual(self.state_redis.get_state_key_name('namespace'), mock_nsa.return_value)
        mock_nsa.assert_called_once_with('namespace', 'State')

    def test_insert(self):
        self.state_redis._insert_metrics = MagicMock(name='insert_metrics')
        self.state_redis._insert_information = MagicMock(name='insert_info')
        self.state_redis.get_state_key_name = MagicMock(name='get_state_key_name')
        self.state_redis.insert('host1', 'service1', 'OK', {'data': 'something'}, [])
        self.state_redis.get_state_key_name.assert_called_once_with('service1')
        self.rdb.sadd.assert_called_once_with('name_of_the_set', 'PREFIX_host1')
        self.rdb.hset.assert_called_once_with('PREFIX_host1', self.state_redis.get_state_key_name.return_value, 'OK')
        self.state_redis._insert_metrics.assert_called_once_with([], 'PREFIX_host1', 'service1')
        self.state_redis._insert_information.assert_called_once_with({'data': 'something'}, 'PREFIX_host1', 'service1')

    def test_find_keys_no_keys(self):
        test_kv1 = {'key1': 'value1'}
        self.rdb.hgetall.return_value = test_kv1
        result = self.state_redis.find_keys('host1', None)
        self.rdb.hgetall.assert_called_once_with('host1')
        self.assertEqual(result, test_kv1)

    def test_find_keys_keys_1(self):
        test_vals1 = ['val1', 'val2']
        test_keys1 = ['key1', 'key2']
        self.rdb.hmget.return_value = test_vals1
        result = self.state_redis.find_keys('host1', test_keys1)
        self.rdb.hmget.assert_called_once_with('host1', test_keys1)
        self.assertEqual(result, dict(zip(test_keys1, test_vals1)))

    def test_map_keys(self):
        result = self.state_redis.map_keys({'key1': 'val1', 'key2': 'val2'}, {'key1': 'fancy1'})
        self.assertEqual(result, {'fancy1': 'val1', 'key2': 'val2'})

    def test_map_keys_keys_mapped_not_there(self):
        original = {'key1': 'val1', 'key2': 'val2'}
        result = self.state_redis.map_keys(original, {'key3': 'fancy3'})
        self.assertEqual(result, original)

    def test_get_mapped_keys(self):
        result = self.state_redis.get_mapped_keys(['key1', 'key2'], {'key1': 'fancy1'})
        self.assertEqual(result, ['fancy1', 'key2'])

    def test_get_mapped_keys_longer(self):
        result = self.state_redis.get_mapped_keys(['key1', 'key2', 'key3', 'key4', 'key5', 'key6'],
                                                  {'key1': 'fancy1', 'key5': 'fancy5'})
        self.assertEqual(result, ['fancy1', 'key2', 'key3', 'key4', 'fancy5', 'key6'])

    def test_get_mapped_keys_keys_mapped_not_there(self):
        result = self.state_redis.get_mapped_keys(['key1', 'key2'], {'key3': 'fancy3'})
        self.assertEqual(result, ['key1', 'key2'])

    def test_flush(self):
        self.rdb.smembers.return_value = set(['me1', 'me2'])
        self.state_redis.flush()
        self.rdb.smembers.assert_called_once_with('name_of_the_set')
        self.rdb.delete.assert_called_once_with('name_of_the_set', *self.rdb.smembers.return_value)


if __name__ == '__main__':
    unittest.main()
