import os
import pysvn
from shutil import rmtree


class PhiSubversion(object):
    def __init__(self, url, work_dir, username=None, password=None):
        self._hosts = None
        self.url = url
        self.work_dir = work_dir
        self.username = username
        self.password = password
        self._client = None
        self.ignore_externals = True

    def _item_full_path(self, item):
        return os.path.join(self.work_dir, item.lstrip(os.sep))

    def remove(self, item):
        item_fp = self._item_full_path(item)
        if os.path.exists(item_fp):
            self.client.remove(item_fp)

    def add(self, item):
        item_fp = self._item_full_path(item)
        if self.client.status(item_fp)[0].text_status == pysvn.wc_status_kind.unversioned:
            self.client.add(item_fp)

    def login(self, realm, username, may_save):
        #return retcode, username, psswrd, save
        return True, self.username, self.password, False

    @property
    def client(self):
        if not self._client:
            self._client = pysvn.Client()
            self._client.callback_get_login = self.login
        return self._client

    def setup_wc(self):
        """Checks out from subversion to wc.
        Will try to delete anything in the destination directory before checking out"""
        rmtree(self.work_dir, ignore_errors=True)
        os.makedirs(self.work_dir)
        self.client.checkout(self.url, self.work_dir, ignore_externals=self.ignore_externals)

    def status(self, sub=''):
        """Generates a dictionary with the key being a status type:
            added
            removed
            changed
            conflicts
            unversioned
        The value for each key is a list of the items in that status.

        :param sub: a sub location inside the working copy
        :return: a dictionary with the key being a status type
        """

        status_groups = {
            pysvn.wc_status_kind.added: ['added', []],
            pysvn.wc_status_kind.deleted: ['removed', []],
            pysvn.wc_status_kind.modified: ['changed', []],
            pysvn.wc_status_kind.conflicted: ['conflicts', []],
            pysvn.wc_status_kind.unversioned: ['unversioned', []]
        }
        sub = sub.lstrip(os.sep)
        changes = self.client.status(os.path.join(self.work_dir, sub))
        for f in changes:
            status_group = status_groups.get(f.text_status)
            if status_group:
                status_group[1].append(f.path)

        return dict(status_groups.values())

    def commit(self, message='', sub=''):
        sub = sub.lstrip(os.sep)
        return self.client.checkin(os.path.join(self.work_dir, sub), message)


if __name__ == "__main__":
    pass
