from kombu import Exchange, Queue


class QueueMan(object):
    def __init__(self, exchange_queue_map):
        """
        Initializes a QueueMan object
        :param exchange_queue_map: a dictionary with exchange names as keys and
            a list of the queue names for that exchange as values
        """
        self.exchange_queue_map = exchange_queue_map
        self._exchanges = {}
        self._queues = {}

    def _create_exchanges_and_queues(self):
        for exchange, e_queues in self.exchange_queue_map.iteritems():
            self._exchanges[exchange] = Exchange(exchange, type="fanout")
            for e_queue in e_queues:
                self._queues[e_queue] = Queue(e_queue, self._exchanges[exchange])

    @property
    def exchanges(self):
        if not self._exchanges:
            self._create_exchanges_and_queues()
        return self._exchanges

    @property
    def queues(self):
        if not self._queues:
            self._create_exchanges_and_queues()
        return self._queues

    def get_queue(self, queue_name):
        return self.queues[queue_name]

    def get_exchange(self, exchange_name):
        return self.exchanges[exchange_name]