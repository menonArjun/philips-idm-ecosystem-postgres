from email.mime.text import MIMEText
import smtplib


def send_message(message, smtpfrom, smtpto, smtpserver, subject=None, user=None, password=None, port=25):
    msg = MIMEText(message)

    if subject:
        msg['Subject'] = subject
    msg['From'] = smtpfrom
    msg['To'] = smtpto

    # Send the message via SMTP server, but don't include the envelope header.
    s = smtplib.SMTP(smtpserver, port)
    if user:
        s.login(user, password)
    s.sendmail(smtpfrom, smtpto, msg.as_string())
    s.quit()


def create_msg(siteid, hostname, hostaddress, timestamp, errtype, errcode, output):
    messagestring = """\
?<?xml version="1.0" encoding="utf-8"?>
<MESSAGE_ROOT>
  <VERSION>1.0</VERSION>
  <CATEGORY>LM_NT_EVENT</CATEGORY>
  <COMPONENT>RemoteEventLogMonitor</COMPONENT>
  <SITE_NAME>{siteid}</SITE_NAME>
  <LOCAL_HOST>{hostname}</LOCAL_HOST>
  <CLUSTER>NO</CLUSTER>
  <FILTER_CONFIG_VERSION>1.0.1.0</FILTER_CONFIG_VERSION>
  <LOCATION>Main Location</LOCATION>
  <BODY>
    <REPORT>
      <SITE_NAME>{siteid}</SITE_NAME>
      <LOCAL_HOST>{hostname}</LOCAL_HOST>
      <CLUSTER>NO</CLUSTER>
      <LOGMESSAGE>
        <ORIGINATING_HOST>{hostname}</ORIGINATING_HOST>
        <ORIGINATING_HOST_IPS> {hostaddress} </ORIGINATING_HOST_IPS>
        <TIMESTAMP>{timestamp}</TIMESTAMP>
        <PRIORITY>LM_NT_EVENT</PRIORITY>
        <PRIORITY_CODE>12345</PRIORITY_CODE>
        <THREAD_ID>1</THREAD_ID>
        <MESSAGE_BODY>Application log:
{errtype} [{hostname}] generated error Event {errcode}: {output}

</MESSAGE_BODY>
      </LOGMESSAGE>
      <SYSTEM_INFO>Module Info: {hostname} {hostaddress}</SYSTEM_INFO>
    </REPORT>
  </BODY>
</MESSAGE_ROOT>"""
    return messagestring.format(**locals())