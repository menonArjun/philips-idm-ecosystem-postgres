import re
from cryptography.fernet import Fernet, InvalidToken
from pynag.Parsers import config


class NagiosResourcer(object):
    def __init__(self, resource_file, secret=None):
        self.resource_file = resource_file
        self._config = config()
        self._config.maincfg_values.append(('resource_file', resource_file))
        self.decrypter = ResourceDecrypter(secret) if secret else None

    def get_resource(self, resource):
        result = self._config.get_resource(resource)
        if result and self.decrypter is not None:
            result = self.decrypter.decrypt(result)
        return result

    def get_resources(self, *args):
        return map(self.get_resource, args)


class ResourceDecrypter(object):
    pattern = re.compile(r'\{\+(.*?)\+\}')

    def __init__(self, secret):
        self.secret = bytes(secret)
        self._cipher_suite = None

    @property
    def cipher_suite(self):
        if self._cipher_suite is None:
            self._cipher_suite = Fernet(self.secret)
        return self._cipher_suite

    def decrypt_first_group(self, cipher_text_match):
        first_group = cipher_text_match.group(1)
        return self.cipher_suite.decrypt(bytes(first_group))

    def decrypt(self, text):
        return self.pattern.sub(self.decrypt_first_group, text)
