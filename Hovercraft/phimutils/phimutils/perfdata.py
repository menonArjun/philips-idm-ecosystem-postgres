import re
from numbers import Number


PERFDATA_ATTRIBUTES = ('label', 'value', 'uom', 'warn', 'crit', 'min', 'max')

# Regex to grab data in the nagios perfdata format (square brackets denote optional):
# 'label'=value[UOM];[warn];[crit];[min];[max]
# value, warn, crit, min and max must be valid numbers
RAW_PERFDATA_TOKENIZER = r"([^\s]+|'[^']+')=([-.\d]+)([^\W\d]+|%)?(?:;([-.\d]*))?(?:;([-.\d]*))?(?:;([-.\d]*))?(?:;([-.\d]*))?"
INFO_MARKER = '++I#'
INFO_TOKENIZER = r"([^=]+)=([^;=]+)(?:;|$)"


def get_perfdata_tokens(data):
    return re.findall(RAW_PERFDATA_TOKENIZER, data)


def perfdata2dict(rawperfdata):
    if not rawperfdata:
        return []
    return list(get_perfdata(rawperfdata))


def get_perfdata(data):
    for token in get_perfdata_tokens(data):
        try:
            metric = PerfData(*token)
            yield metric.to_dict()
        except ValueError:
            continue


def raw_metric(label, value, uom='', warn='', crit='', min='', max=''):
    """Returns in format 'label'=value[UOM];[warn];[crit];[min];[max]"""
    return "'{label}'={value}{uom};{warn};{crit};{min};{max}".format(**locals()).rstrip(';')


def info2dict(output):
    infos = {}
    if output:
        # Find data in the ++I# format (square brackets denote optional):
        # <ignore> ++I# 'label' = value; 'label' = value

        # the third part (called "after") is what is interesting
        after = output.partition(INFO_MARKER)[2].strip()
        tokens = re.findall(INFO_TOKENIZER, after)

        for token in tokens:
            infos[token[0].replace('"', '').replace("'", '').strip()] = token[1].strip('" \'')
    return infos


def get_numeric_value(value):
    if isinstance(value, Number):
        return value
    try:
        return float(value)
    except ValueError:
        pass


# Descriptors
class NonEmptyValue(object):
    def __init__(self):
        # label to be set by metaclass
        self.label = None

    def __get__(self, instance, cls):
        if instance is None:
            return self
        return instance.__dict__.get(self.label)

    def __set__(self, instance, value):
        if value:
            instance.__dict__[self.label] = value


class LabelValue(NonEmptyValue):
    def __set__(self, instance, value):
        value = value.strip("' ")
        if not value:
            raise ValueError('{label} cannot be empty'.format(label=self.label))
        instance.__dict__[self.label] = value


class NumericValue(NonEmptyValue):
    def __set__(self, instance, value):
        value = get_numeric_value(value)
        if value is not None:
            instance.__dict__[self.label] = value


class RequiredNumericValue(NumericValue):
    def __set__(self, instance, value):
        numeric_value = get_numeric_value(value)
        if numeric_value is None:
            raise ValueError('"{value}" is not a valid value for "{label}"'.format(value=value, label=self.label))
        instance.__dict__[self.label] = numeric_value


# Metaclass
class NonEmptyValueOwner(type):
    def __new__(cls, name, bases, class_dict):
        # find all NonEmptyValue descriptors, auto-set their labels
        for n, v in class_dict.items():
            if isinstance(v, NonEmptyValue):
                v.label = n
        return super(NonEmptyValueOwner, cls).__new__(cls, name, bases, class_dict)


class PerfData(object):
    __metaclass__ = NonEmptyValueOwner

    label = LabelValue()
    value = RequiredNumericValue()
    uom = NonEmptyValue()
    warn = NumericValue()
    crit = NumericValue()
    min = NumericValue()
    max = NumericValue()

    def __init__(self, label, value, uom, warn, crit, min, max):
        self.label = label
        self.value = value
        self.uom = uom
        self.warn = warn
        self.crit = crit
        self.min = min
        self.max = max

    def to_dict(self):
        return self.__dict__.copy()
