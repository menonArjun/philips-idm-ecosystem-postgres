import re

from lxml import objectify, etree

class PCMPackage(object):
    def __init__(self, filepath):
        self.doc_tree = objectify.parse(filepath).getroot()

    @property
    def Name(self):
        return self.doc_tree["Name"].text

    @property
    def Version(self):
        return self.doc_tree["Version"].text

    @property
    def Package_Name(self):
        return "{name}-{version}".format(name=self.Name, version=self.Version)

    @property
    def Description(self):
        return self.doc_tree["Description"].text

    def get_required_databags(self):
        return list(set(re.findall(r'(?:\[databag@)(.*?)(?:\])', etree.tostring(self.doc_tree), re.IGNORECASE | re.DOTALL)))

    def get_dependencies(self):
        dependencies = {}

        for dep in self.doc_tree["Dependencies"].iterchildren() :
            try :
                dependencies[str(dep["Name"])] = str(dep["Version"]) #str needed else 1.2 comes through as float
            except AttributeError :
                pass
        return dependencies

