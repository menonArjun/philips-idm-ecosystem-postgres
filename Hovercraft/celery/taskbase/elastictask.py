from celery import Task
from celery.utils import cached_property
from elasticsearch import Elasticsearch, helpers


class ElasticTask(Task):
    abstract = True
    _urls = []

    @cached_property
    def es(self):
        return Elasticsearch(self._urls, sniff_timeout=10)

    def bulk(self, actions):
        helpers.bulk(self.es, actions, stats_only=True)


def create_elastic_task(urls):
    class EST(ElasticTask):
        abstract = True
        _urls = urls

    return EST
