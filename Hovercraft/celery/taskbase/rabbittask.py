from celery import Task
from celery.utils import cached_property
from kombu import Connection


class RabbitTask(Task):
    abstract = True
    _url = None

    @cached_property
    def pool(self):
        return self.conn.Pool()

    @pool.deleter
    def pool(self, value):
        if value:
            value.release()

    @cached_property
    def conn(self):
        return Connection(self._url)

    @conn.deleter
    def conn(self, value):
        if value:
            value.release()
        
    def __del__(self):
        del(self.pool)
        del(self.conn)


def create_rabbit_task(url):
    class RBT(RabbitTask):
        abstract = True
        _url = url

    return RBT
