import unittest
from mock import MagicMock, patch


class TCHelpTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_yaml = MagicMock(name='yaml')
        modules = {
            'yaml': self.mock_yaml,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import tchelp
        self.tchelp = tchelp

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_are_same_base_bool(self):
        self.assertTrue(self.tchelp.are_same_base(True, False))

    def test_are_same_base_number(self):
        self.assertTrue(self.tchelp.are_same_base(1, 4.6))

    def test_are_same_base_str(self):
        self.assertTrue(self.tchelp.are_same_base('someone', u'noone'))

    def test_are_same_base_bool_and_int(self):
        self.assertFalse(self.tchelp.are_same_base(True, 1))

    # This does not work as in python 2 booleans are subclass of int, and therefore numbers.
    # def test_are_same_base_int_and_bool(self):
    #     self.assertFalse(self.tchelp.are_same_base(0, False))

    def test_are_same_base_str_and_dict(self):
        self.assertFalse(self.tchelp.are_same_base('stringo', {'x': 'y'}))

    def test_get_value_override_none(self):
        self.assertEqual(self.tchelp.get_value('someval', None), 'someval')

    def test_get_value_both_dicts(self):
        self.assertEqual(self.tchelp.get_value({'a': 'x', 'b': 'y'}, {'a': 'z'}), {'a': 'z', 'b': 'y'})

    def test_get_value_same_base(self):
        self.tchelp.are_same_base = MagicMock(name='are_same_base', return_value=True)
        self.assertEqual(self.tchelp.get_value('some', 'some2'), 'some2')

    def test_get_value_not_same_base(self):
        self.tchelp.are_same_base = MagicMock(name='are_same_base', return_value=False)
        self.assertEqual(self.tchelp.get_value('some', 'some2'), 'some')


if __name__ == '__main__':
    unittest.main()
