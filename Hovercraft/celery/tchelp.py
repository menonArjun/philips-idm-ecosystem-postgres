import yaml
from numbers import Number
from copy import copy


def get_yaml(yaml_file):
    """Retrieves YAML data, a dictionary is expected.
    returns empty dictionary if not able to retrieve data from YAML file"""
    result = {}
    try:
        with open(yaml_file) as f:
            result = yaml.load(f)
    except IOError:
        pass
    return result


def are_same_base(default, override):
    # default is considered to have the desired type
    # order is important as bool is int
    comparable_types = [bool, Number, basestring]
    for base_type in comparable_types:
        if isinstance(default, base_type):
            return isinstance(override, base_type)
    return False


def get_value(default, override):
    result = copy(default)
    if override is not None:
        try:
            result.update(override)
        except (AttributeError, TypeError, ValueError):
            if are_same_base(default, override):
                result = override
    return result


def load_config(defaults, override_file):
    overrides = get_yaml(override_file)
    return dict((name, get_value(default, overrides.get(name))) for name, default in defaults.iteritems())
