from __future__ import absolute_import

from types import MethodType
from phimutils.queueman import QueueMan
from celery import bootsteps
from kombu import Consumer
from kombu.utils.encoding import safe_repr
from celery.utils.log import get_logger

logger = get_logger(__name__)


MESSAGE_DECODE_ERROR = """\
Received and deleted unknown message. [type:%r encoding:%r headers:%s]
Can't decode message body: %r
"""


def make_taskmapper_step(app, exchange_queue_map, queue_task_map):
    class TaskMapperConsumerStep(bootsteps.ConsumerStep):
        def __init__(self, *args, **kwargs):
            super(TaskMapperConsumerStep, self).__init__(*args, **kwargs)
            self.qm = QueueMan(exchange_queue_map)
            self.functions = {}

        def queue2func(self, queue):
            def dec(self, body, message):
                logger.info('Mapped queue: "%s" to task: "%s"', queue, queue_task_map[queue])
                logger.debug(body)
                app.send_task(queue_task_map[queue], [body], kwargs={})
                message.ack()
            self.functions[queue] = MethodType(dec, self, TaskMapperConsumerStep)
            return self.functions[queue]

        def consumer_factory(self, queue, channel):
            return Consumer(
                channel,
                queues=[self.qm.get_queue(queue)],
                callbacks=[self.queue2func(queue)],
                accept=['json'],
                on_decode_error=self.on_decode_error
            )

        def get_consumers(self, channel):
            return [self.consumer_factory(co, channel) for co in queue_task_map.keys()]

        def on_decode_error(self, message, exc):
            """Callback called if an error occurs while decoding
            a message received.

            Simply logs the error and acknowledges the message so it
            doesn't enter a loop.

            :param message: The message with errors.
            :param exc: The original exception instance.

            """
            logger.warning(
                MESSAGE_DECODE_ERROR,
                message.content_type, message.content_encoding, safe_repr(message.headers), exc
            )
            logger.debug('body: %s', message.body)
            message.ack()

    return TaskMapperConsumerStep
