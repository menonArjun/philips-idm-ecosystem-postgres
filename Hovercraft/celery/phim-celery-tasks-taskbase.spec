# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Basic Information
Name:      phim-celery-tasks-taskbase
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   Task classes used for defining actual tasks

Group:     none
License:   GPL
URL:       http://www.philips.com/healthcare
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

# Source Information
Source0:   %{name}-%{version}.tar.gz
#Patch0:

Requires:  celery-daemon
Provides:  phim-celery-tasks-taskbase

%description
Task classes used for defining actual tasks

%prep
%setup -q

%build
echo OK

# Using Multilib Exempt Location %{_prefix}/lib instead of %{_libdir}

%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{_prefix}/lib/celery
%{__cp} -r %{_builddir}/%{name}-%{version}/taskbase %{buildroot}%{_prefix}/lib/celery/
%{__rm} -f %{buildroot}%{_prefix}/lib/celery/taskbase/*.pyc
%{__cp} %{_builddir}/%{name}-%{version}/taskmapper.py %{buildroot}%{_prefix}/lib/celery/
%{__cp} %{_builddir}/%{name}-%{version}/tchelp.py %{buildroot}%{_prefix}/lib/celery/


%clean
%{__rm} -rf %{buildroot}


%files
%defattr(-,root,root,-)
%{_prefix}/lib/celery/taskbase
%{_prefix}/lib/celery/taskmapper.py
%{_prefix}/lib/celery/tchelp.py


%changelog
* Wed Aug 17 2016 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Added tchelp module.

* Thu Jun 26 2014 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Added taskmapper module.

* Fri Oct 25 2013 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File
