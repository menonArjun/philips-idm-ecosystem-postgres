# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Basic Information
Name:      celery-daemon
Version:   %{_phiversion}
Release:   3%{?dist}
Summary:   celery-daemon provides scripts to daemonize celery

Group:     none
License:   GPL
URL:       http://www.celeryproject.org/
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

# Source Information
Source0:   %{name}-%{version}.tar.gz
#Patch0:

Requires:  shadow-utils
Requires:  initscripts
Requires(post):   chkconfig
Requires(preun):  chkconfig
# This is for /sbin/service
Requires(preun):  initscripts
Requires(postun): initscripts
Provides:  celery-daemon

%description
celery-daemon provides scripts to daemonize celery
Services for tasks 'celeryd' and one for scheduled tasks 'celerybeat'

%prep
%setup -q

%build
echo OK

# Using Multilib Exempt Location %{_prefix}/lib instead of %{_libdir}

%install
%{__rm} -rf %{buildroot}
%{__install} -d -m 0755 %{buildroot}%{_initrddir}
%{__install} -Dp -m 0755 %{_builddir}/%{name}-%{version}/celeryd %{buildroot}%{_initrddir}/
%{__install} -Dp -m 0755 %{_builddir}/%{name}-%{version}/celerybeat %{buildroot}%{_initrddir}/
%{__install} -d -m 0755 %{buildroot}%{_sysconfdir}/logrotate.d
%{__install} -Dp -m 0755 %{_builddir}/%{name}-%{version}/celeryd.logrotate %{buildroot}%{_sysconfdir}/logrotate.d/celeryd
%{__install} -d -m 0755 %{buildroot}%{_localstatedir}/log/celery
%{__install} -d -m 0755 %{buildroot}%{_localstatedir}/run/celery
%{__install} -d -m 0755 %{buildroot}%{_prefix}/lib/celery


%clean
%{__rm} -rf %{buildroot}

%pre
groupadd -f celery
if ! getent passwd celery >/dev/null ; then
    useradd -g celery celery
fi

%post
if [ $1 -eq 1 ] ; then
    # Initial installation
    # %{_sbindir} != /sbin
    /sbin/chkconfig --add celeryd
    /sbin/chkconfig --add celerybeat
fi

%preun
if [ $1 -eq 0 ] ; then
    # Package removal, not upgrade
    # %{_sbindir} != /sbin
    /sbin/service celeryd stop >/dev/null 2>&1
    /sbin/service celerybeat stop >/dev/null 2>&1
    /sbin/chkconfig --del celeryd
    /sbin/chkconfig --del celerybeat
fi

#%postun
if [ "$1" -ge "1" ] ; then
    # package upgrade
    # does this need to be restarted?
    # %{_sbindir} != /sbin
    /sbin/service celeryd restart >/dev/null 2>&1 || :
    /sbin/service celerybeat restart >/dev/null 2>&1 || :
fi

%files
%defattr(-,root,root,-)
%attr(0755,root,root) %{_initrddir}/celeryd
%attr(0755,root,root) %{_initrddir}/celerybeat
%ghost %{_localstatedir}/log/celery
%ghost %{_localstatedir}/run/celery
%dir %{_prefix}/lib/celery
%attr(0755,root,root) %{_sysconfdir}/logrotate.d/celeryd

%changelog
* Mon Jun 30 2014 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Removed celeryd default config

* Fri Sep 13 2013 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File
